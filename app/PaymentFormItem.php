<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentFormItem extends Model
{
    public function form()
    {
        return $this->hasOne('App\Models\Form');
    }
}
