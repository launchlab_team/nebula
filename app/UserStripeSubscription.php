<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStripeSubscription extends Model
{
    public function user()
    {
        return $this->hasOne('App\User');
    }
}
