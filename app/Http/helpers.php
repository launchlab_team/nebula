<?php

/**
 * @return  pixel amount from top for corresponding time
 */
function getReservationStartValue($start_time) {
  $seconds = timeToSeconds($start_time);
  $offset_by = 300; // 300px - 5AM the number of pixels (or minutes) the top is offset by 5hrs * 60
  $start_pixel = ((($seconds / 3600) * 60)) - $offset_by;
  return $start_pixel;
}

function getUnavailableBlockHeight($available_end_time) {
  $seconds = timeToSeconds($available_end_time);
  $pixels = (1440 - (($seconds / 3600)* 60) + 300);
  return $pixels;
}

function timeToSeconds($time)
{
  $timeExploded = explode(':', $time);
  if (isset($timeExploded[2])) {
     return $timeExploded[0] * 3600 + $timeExploded[1] * 60 + $timeExploded[2];
  }
  return $timeExploded[0] * 3600 + $timeExploded[1] * 60;
}

function getReservationDurationValue($start_time, $end_time) {
   $start_time = strtotime(date("h:i A",strtotime($start_time)));
   $end_time = strtotime(date("h:i A",strtotime($end_time)));
   return (($end_time - $start_time) / 3600) * 60;
}

function force_ssl() {
  $protocol = 'https://';
  if (substr(@$_SERVER['HTTP_HOST'], 0, 4) == 'www.') {
     header('Location: '.$protocol.str_replace('www.', '', $_SERVER['HTTP_HOST']).$_SERVER['REQUEST_URI']);
     exit;
  }
  if (@$_SERVER['HTTPS'] != 'on') {
     header('Location: '.$protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
     exit;
  }
}

// array_sort_by_column($array, 'order');
function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = $row[$col];
    }
    array_multisort($sort_col, $dir, $arr);
}

// http://stackoverflow.com/questions/15188033/human-readable-file-size
function humanFileSize($size,$unit="") {
  if( (!$unit && $size >= 1<<30) || $unit == "GB")
    return number_format($size/(1<<30),2)."GB";
  if( (!$unit && $size >= 1<<20) || $unit == "MB")
    return number_format($size/(1<<20),2)."MB";
  if( (!$unit && $size >= 1<<10) || $unit == "KB")
    return number_format($size/(1<<10),2)."KB";
  return number_format($size)." bytes";
}

function getIconFromType($type){
   if ($type == 'image') return 'icon-circle-image';
   if ($type == 'csv') return 'icon-circle-sheet';
   if ($type == 'xls') return 'icon-circle-sheet';
   if ($type == 'xlsx') return 'icon-circle-sheet';
   if ($type == 'pdf') return 'icon-circle-pdf';
   if ($type == 'doc') return 'icon-circle-doc';
   if ($type == 'docx') return 'icon-circle-doc';
   // otherwise
   return 'icon-circle-file';
}

function create_slug($string){
   $slug = strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
   return $slug;
}

function get_file_extension($file_name) {
  return substr(strrchr($file_name,'.'),1);
}

function obfuscate_email($email) {
  $em   = explode("@",$email);
  $name = implode(array_slice($em, 0, count($em)-1), '@');
  $len  = floor(strlen($name)/2);
  return substr($name,0, $len) . str_repeat('*', $len) . "@" . end($em);
}

function time_ago($date, $granularity = 2) { // converts time into readable time ago format
    $retval = '';
    $date = strtotime ( $date );
    $difference = time () - $date;
    $periods = array (
            'decade' => 315360000,
            'year' => 31536000,
            'month' => 2628000,
            'week' => 604800,
            'day' => 86400,
            'hour' => 3600,
            'minute' => 60,
            'second' => 1
    );
    foreach ( $periods as $key => $value ) {
        if ($difference >= $value) {
            $time = floor ( $difference / $value );
            $difference %= $value;
            $retval .= ($retval ? ' ' : '') . $time . ' ';
            $retval .= (($time > 1) ? $key . 's' : $key);
            $granularity --;
        }
        if ($granularity == '0') {
            break;
        }
    }
  if (empty($retval)) return 'Now';
    return $retval . ' ago';
}

function capitalize($string) {
    $word_splitters = array(' ', '-', "O'", "L'", "D'", 'St.', 'Mc');
    $lowercase_exceptions = array('the', 'van', 'den', 'von', 'und', 'der', 'de', 'da', 'of', 'and', "l'", "d'");
    $uppercase_exceptions = array('III', 'IV', 'VI', 'VII', 'VIII', 'IX');
 
    $string = strtolower($string);
    foreach ($word_splitters as $delimiter)
    { 
        $words = explode($delimiter, $string); 
        $newwords = array(); 
        foreach ($words as $word)
        { 
            if (in_array(strtoupper($word), $uppercase_exceptions))
                $word = strtoupper($word);
            else
            if (!in_array($word, $lowercase_exceptions))
                $word = ucfirst($word); 
 
            $newwords[] = $word;
        }
 
        if (in_array(strtolower($delimiter), $lowercase_exceptions))
            $delimiter = strtolower($delimiter);
 
        $string = join($delimiter, $newwords); 
    } 
    return $string; 
}

function phone_format($phone) { // adds spaces, ()'s, and -'s to format phone number
    $phone = preg_replace('/[^0-9]/','',$phone);
    if(strlen($phone) > 10) {
        $countryCode = substr($phone, 0, strlen($phone)-10);
        $areaCode = substr($phone, -10, 3);
        $nextThree = substr($phone, -7, 3);
        $lastFour = substr($phone, -4, 4);

        $phone = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
    }
    else if(strlen($phone) == 10) {
        $areaCode = substr($phone, 0, 3);
        $nextThree = substr($phone, 3, 3);
        $lastFour = substr($phone, 6, 4);
        $phone = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
    }
    else if(strlen($phone) == 7) {
        $nextThree = substr($phone, 0, 3);
        $lastFour = substr($phone, 3, 4);

        $phone = $nextThree.'-'.$lastFour;
    }
    return $phone;
}