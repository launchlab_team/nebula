<?php

namespace App\Http\Controllers;

// Core
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

// Additions
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Validator;
use View;
use File;
use Image;
use PDF;
use Mail;
use DB;
use Carbon\Carbon;
use Debugbar;
use Redirect;
use Session;
use Excel;
use Storage;
use Exception;
use \Stripe\Stripe;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


// Models
use App\User;
use App\Upload;
use App\Organization;
use App\Page;
use App\PageElement;
use App\PageMenuItem;
use App\Manager;
use App\Member;
use App\Subscription;
use App\Module;
use App\RecordLabel;
use App\Record;
use App\RecordSet;
use App\RecordSetShare;
use App\Setting;
use App\FeedItem;
use App\FileFolder;
use App\FileFolderShare;
use App\FileShare;
use App\Link;
use App\PageTemplate;
use App\Form;
use App\FormAlert;
use App\FormItem;
use App\FormResponse;
use App\Payment;
use App\PaymentForm;
use App\PaymentFormItem;
use App\Invoice;
use App\InvoiceSetting;
use App\StripeCustomer;
use App\UserStripeSubscription;
use App\StripePayment;
use App\Reservable;
use App\Reservation;
use App\ReservationSchedule;
use App\Crm;
use App\CrmRecord;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;


    public function __construct() 
    {
        // Share the user object with every view
        // if (Auth::user()) {
        //     $this->user = Auth::user();
        //     // Set first and last name if user exists
        //     $this->user->firstname = strtok($this->user['name'], " ");
        //     $this->user->lastname =  strrpos($this->user['name'], ' ') + 1;
        //     View::share('user', $this->user);
        // }
        $this->menu_items = PageMenuItem::all();
        View::share('menu_items', $this->menu_items);

        // Share $settings with all views
        $this->settings = Setting::first();
        View::share('settings', $this->settings);

        // Share $storage_used with all views
        $this->storage_used = humanFileSize(Upload::sum('size'));
        View::share('storage_used', $this->storage_used);

        // Share $users with all views
        $this->users = Setting::first();
        View::share('users', $this->users);
    }

    public function contactNebula(Request $request)
    {
        $content = 'var_dump($request)';
        $mail = Mail::send('emails.default', [
            'subject'=>'Nebula Website Lead',
            'content' => $request->company . ' ' . $request->email,
        ], function ($mail) {
            if (env('APP_ENV') == 'local') {
                $mail->from('notices@nebulasuite.com', $this->settings->organization_name);
                $mail->to('team@launchlab.us');
                $mail->subject('Nebula Website Lead');
            } else {
                $mail->from('notices@'.env('APP_DOMAIN'), $this->settings->organization_name);
                $mail->to('team@launchlab.us');
                $mail->subject('Nebula Website Lead');
            }
        });
        if (!$mail) return response()->json(['error' => 'Failed to send mail']);
    }

    /**
     * Shows 500 error
     */
    public function showError(Request $request) {
        dd($request->session());
        return view('errors.500');
    }

    public function search(Request $request) {
        // dd($request->input('query'));
        if($request->input('query') !== '' && $request->input('query') !== ' ' && strlen($request->input('query')) > 1) {
            // Seperate $request->input('query') into csv and check each
            // $query_terms = preg_replace('#\s+#',',',trim($request->input('query')));
            $keywords = explode(" ",$request->input('query'));
            $query = $request->input('query');
            // Search for full term and then by each keyword
            $records = Record::where('data','LIKE','%'.$query.'%');
            if(count($keywords) > 1) {
                foreach ($keywords as $keyword)
                {
                    if ($keyword != '') $records->orWhere('data','LIKE','%'.$keyword.'%');
                }
            }
            $records = $records->get();
            $files = Upload::where('name','LIKE','%'.$query.'%')->get();
            $users = User::where('name','LIKE','%'.$query.'%')->orWhere('email','LIKE','%'.$query.'%')->get();
            $pages = Page::where('title','LIKE','%'.$query.'%')->orWhere('slug','LIKE','%'.$query.'%')->get();
            $forms = Form::where('name','LIKE','%'.$query.'%')->orWhere('slug','LIKE','%'.$query.'%')->get();
            $links = Link::where('title','LIKE','%'.$query.'%')->orWhere('url','LIKE','%'.$query.'%')->get();
            $crm_records = CrmRecord::where('data','LIKE','%'.$query.'%')->get();
            $reservations = Reservation::where('name','LIKE','%'.$query.'%')->get();
            $reservation_schedules = ReservationSchedule::where('name','LIKE','%'.$query.'%')->get();
            // $payments = Payment::where('description','LIKE','%'.$query.'%')->orWhere('email','LIKE','%'.$query.'%')->get();
            $payment_forms = PaymentForm::where('title','LIKE','%'.$query.'%')->get();
            $stripe_customers = StripeCustomer::where('name','LIKE','%'.$query.'%')->orWhere('email','LIKE','%'.$query.'%')->get();
            return view('dashboard.search-results',[
                'records' => $records,
                'files' => $files,
                'users' => $users,
                'pages' => $pages,
                'forms' => $forms,
                'links' => $links,
                'crm_records' => $crm_records,
                // 'payments' => $payments,
                'payment_forms' => $payment_forms,
                'stripe_customers' => $stripe_customers,
                'reservations' => $reservations,
                'reservation_schedules' => $reservation_schedules,
            ]);

        }
    }

    /*
    |--------------------------------------------------------------------------
    | Links Module Methods
    |--------------------------------------------------------------------------
    */

    public function showLinks() {
        $links = Link::orderBy('title')->paginate(25);
        return view('links.index',[
            'links' => $links,
        ]);
    }

    public function createLink() {
        return view('links.create');
    }

    public function storeLink(Request $request) {
        // Require fields
        $validator = Validator::make($request->all(), [
            'title' => 'bail|required',
            'url' => 'bail|required',
        ]);

        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        // Create example record
        try {
            $link = new Link;
            $link->url = $request->url;
            $link->title = $request->title;
            $link->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $link->title.' link was added to Links';
        $feed_item->modal = 'link/'.$link->id.'/edit';
        $feed_item->icon = 'icon-circle-link';
        $feed_item->save();

        // Return json response
        return response()->json([
            'success' => 'Successfully added',
            'refreshDynamicView' => true,
            'closeModal' => true,
        ]);
    }

    public function editLink($id) {
        $link = Link::findOrFail($id);
        return view('links.edit',['link'=>$link]);
    }

    public function deleteLink($id)
    {
        $link = Link::findOrFail($id);
        $link->delete();

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $link->title.' was removed from Links';
        $feed_item->href = 'dashboard#links';
        $feed_item->icon = 'icon-circle-link';
        $feed_item->save();

        return response()->json([
            'success' => 'Link Removed',
            'refreshDynamicView' => true,
            'closeModal' => true,
        ]);
    }


    public function updateLink(Request $request, $id) {
        // Require fields
        $validator = Validator::make($request->all(), [
            'title' => 'bail|required',
            'url' => 'bail|required',
        ]);

        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        // Create example record
        try {
            $link = Link::findOrFail($id);
            $link->url = $request->url;
            $link->title = $request->title;
            $link->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $link->title.' was updated.';
        $feed_item->details = 'link/'.$link->id.'/edit';
        $feed_item->icon = 'icon-circle-link';
        $feed_item->save();

        // Return json response
        return response()->json([
            'success' => 'Link updated',
            'refreshDynamicView' => true,
            'closeModal' => true,
        ]);
    }
    /*
    |--------------------------------------------------------------------------
    | Page Methods
    |--------------------------------------------------------------------------
    */

    public function showLanding() {
        $page = Page::where('slug','')->first(); // Get the homepage
        $module = Module::where('type','pages')->first(); // get pages module
        // If the pages module is enabled and a homepage exists then return the page view
        if ($module !== null && $module->enabled && $page !== null) {
            $elements = $page->elements()->orderBy('order')->get();
            return view('page.view',[
                'page' => $page,
                'elements' => $elements,
            ]);
        } else {
            if (Auth::user() == null) {
                return view('auth.login');
            } else {
                // Show the account page
                $user = Auth::user();
                return view('user.account',[
                    'user' => $user,
                ]);
            }
        }
    }

    /**
     * Add organization settings for the first time
     *
     * @param request $request
     * @return redirect
     *
     */
    public function storeSettings(Request $request) {
        // Require fields
        $validator = Validator::make($request->all(), [
            'organization_name' => 'bail|required',
        ]);

        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        try {
            $settings = new Setting;
            $settings->organization_name = $request->organization_name;
            $settings->logo = $request->logo;
            $settings->save();

            // Add feed item
            $feed_item = new FeedItem;
            $feed_item->text = $settings->organization_name . '\'s dashboard was created.';
            $feed_item->href = 'dashboard';
            $feed_item->icon = 'icon-circle-layout';
            $feed_item->save();

            // Give them manager and admin status
            $user = Auth::user();
            $user->is_admin = 1;
            $user->is_manager = 1;
            $user->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Return json response
        return response()->json([
            'success' => 'Success',
            'redirect' => 'dashboard',
        ]);
    }

    public function updateSettings(Request $request) {
        // Require fields
        $validator = Validator::make($request->all(), [
            'organization_name' => 'bail|required',
        ]);

        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        try {
            $settings = Setting::first();
            $settings->organization_name = $request->organization_name;
            $settings->logo = $request->logo;
            $settings->sidebar = $request->sidebar;
            $settings->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $settings->organization_name . '\'s settings were updated.';
        $feed_item->href = 'dashboard#feed';
        $feed_item->icon = 'icon-circle-settings';
        $feed_item->save();

        // Return json response
        return response()->json([
            'success' => 'Settings updated',
            'refresh' => true,
        ]);
    }

    /**
     * Shows an organization's dashboard
     *
     * @param  string  $subdomain
     * @return view
     */
    public function showDashboard() {
        $is_configured = Setting::count();
        if (!$is_configured) return redirect()->to(url('account'));
        $modules = Module::orderBy('order')->get();
        if (Auth::user()->is_manager) {
            return view('dashboard.index',[
                'modules' => $modules,
            ]);
        } else {
            // User is not a manager of the organization
            return redirect()->to(url(''))->withErrors('Insufficient privileges – an organization manager must first add your account to the managers list.');
        }
    }


    public function showUsers(Request $request) {
        $users = User::where('is_manager',0)->get();
        $admins = User::where('is_manager',1)->get();
        return view('user.index',['users'=>$users,'admins'=>$admins]);
    }

    public function addModule(Request $request, $type = NULL) {
        return view('module.add',['type'=>$type]);
    }

    public function editModule(Request $request, $id) 
    {
        $module = Module::findOrFail($id);
        return view('module.edit',[
            'module' => $module,
        ]);
    }

    public function deleteModule(Request $request, $id) 
    {
        $module = Module::findOrFail($id);

        if ($module->type == 'records') {
            foreach($module->record_sets as $record_set) {
                // Delete associated records
                foreach ($record_set->records as $record) {
                    $record = Record::findOrFail($record->id);
                    $record->delete();
                }
                // Delete associated record set
                $record_set = RecordSet::findOrFail($record_set->id);
                $record_set->delete();
            }
        }

        if ($module->type == 'pages') {
            foreach(Page::all() as $page) {
                // Delete associated page elements
                foreach ($page->elements as $page_element) {
                    $page_element = PageElement::findOrFail($page_element->id);
                    $page_element->delete();
                }
                // Delete associated page
                $page = Page::findOrFail($page->id);
                $page->delete();
            }
            foreach(PageMenuItem::all() as $menu_item) {
                $menu_item->delete();
            }
        }

        if ($module->type == 'crm') {
            Crm::firstOrfail()->delete();
            $records = CrmRecord::all();
            foreach($records as $record) {
                $record->findOrfail($record->id)->delete();
            }
        }

        // Delete associated files
        $module->delete();

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $module->label . ' was deleted.';
        $feed_item->href = 'dashboard';
        $feed_item->icon = $module->icon;
        $feed_item->save();

        return response()->json([
            'refresh' => true,
            'redirect' => 'dashboard',
        ]);
    }

    public function storeModule(Request $request) 
    {
        // Validate fields
        $validator = Validator::make($request->all(), [
            'type' => 'bail|required',
            'label' => 'bail|required',
            'icon' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        try {
            $module_exists = Module::where('slug',$request->slug)->count();
            if ($module_exists == 1) {
                return response()->json([
                    'error' => 'You already have a tab with that name. First rename or delete the other tab.',
                ]);
            }

            if ($request->type == 'pages') {
                $pages_module_exists = Module::where('type','pages')->count();
                if ($pages_module_exists > 0) {
                    return response()->json([
                        'error' => 'You already have a pages tab. However, you can create unlimited pages on the existing tab.',
                    ]);
                }

                // Store module
                $module = new Module;
                $module->type = $request->input('type');
                $module->icon = $request->input('icon');
                $module->label = $request->input('label');
                $module->slug = create_slug($request->input('label'));
                $module->save();

                // Insert homepage
                $page = new Page;
                $page->title = 'Homepage';
                $page->slug = '';
                $page->save();

                // Add feed item
                $feed_item = new FeedItem;
                $feed_item->text = $module->label .' was added.';
                $feed_item->href = 'dashboard#'.$module->slug;
                $feed_item->icon = $module->icon;
                $feed_item->save();
            }

            if ($request->type == 'forms') {
                $forms_module_exists = Module::where('type','forms')->count();
                if ($forms_module_exists > 0) {
                    return response()->json([
                        'error' => 'You already have a forms tab. However, you can create unlimited forms on the existing tab.',
                    ]);
                }
                // Store module
                $module = new Module;
                $module->type = $request->input('type');
                $module->icon = $request->input('icon');
                $module->label = $request->input('label');
                $module->slug = create_slug($request->input('label'));
                $module->save();
                // Add feed item
                $feed_item = new FeedItem;
                $feed_item->text = $module->label .' was added.';
                $feed_item->href = 'dashboard#forms';
                $feed_item->icon = $module->icon;
                $feed_item->save();
            }

            if ($request->type == 'links') {
                $links_module_exists = Module::where('type','links')->count();
                if ($links_module_exists > 0) {
                    return response()->json([
                        'error' => 'You already have a links tab. However, you can create unlimited links on the existing tab.',
                    ]);
                }
                // Store module
                $module = new Module;
                $module->type = $request->input('type');
                $module->icon = $request->input('icon');
                $module->label = $request->input('label');
                $module->slug = create_slug($request->input('label'));
                $module->save();
                // Add feed item
                $feed_item = new FeedItem;
                $feed_item->text = $request->label .' was added.';
                $feed_item->href = 'dashboard#links';
                $feed_item->icon = $request->icon;
                $feed_item->save();
            }

            if ($request->type == 'payments') {
                $payments_module_exists = Module::where('type','payments')->count();
                if ($payments_module_exists > 0) {
                    return response()->json([
                        'error' => 'You already have a payments tab. Try using the existing tab.',
                    ]);
                }
                // Store module
                $module = new Module;
                $module->type = $request->input('type');
                $module->icon = $request->input('icon');
                $module->label = $request->input('label');
                $module->slug = 'payments';
                $module->save();
                // if (InvoiceSetting::first() !== null)
                // {
                //     // Add invoice settings if not exists
                //     $invoice_setting = new InvoiceSetting;
                //     $invoice_setting->save();
                // }
                // Add feed item
                $feed_item = new FeedItem;
                $feed_item->text = $request->label .' was added.';
                $feed_item->href = 'dashboard#payments';
                $feed_item->icon = $request->icon;
                $feed_item->save();
            }

            if ($request->type == 'records') {
                // Store module
                $module = new Module;
                $module->type = $request->input('type');
                $module->icon = $request->input('icon');
                $module->label = $request->input('label');
                $module->slug = create_slug($request->input('label'));
                $module->save();
                // Add feed item
                $feed_item = new FeedItem;
                $feed_item->text = $request->label .' was added.';
                $feed_item->href = 'dashboard#records/'.$module->slug;
                $feed_item->icon = $request->icon;
                $feed_item->save();
            }

            if ($request->type == 'crm') {
                $module_exists = Module::where('type','crm')->count();
                if ($module_exists > 0) {
                    return response()->json([
                        'error' => 'You already have a CRM tab.',
                    ]);
                }
                // Store module
                $module = new Module;
                $module->type = $request->input('type');
                $module->icon = $request->input('icon');
                $module->label = $request->input('label');
                $module->slug = 'crm';
                $module->save();
                // Store crm
                $crm = new Crm;
                $crm->pipeline = '["Leads", "Opportunities", "Customers"]';
                $crm->label_key = '0';
                $crm->sublabel_key = NULL;
                $crm->save();
                // Add feed item
                $feed_item = new FeedItem;
                $feed_item->text = $request->label .' was added.';
                $feed_item->href = 'dashboard#crm';
                $feed_item->icon = $request->icon;
                $feed_item->save();
            }

            if ($request->type == 'files') {
                $files_module_exists = Module::where('type','files')->count();
                if ($files_module_exists > 0) {
                    return response()->json([
                        'error' => 'You already have a files tab. However, you can create unlimited files on the existing tab.',
                    ]);
                }
                // Store module
                $module = new Module;
                $module->type = $request->input('type');
                $module->icon = $request->input('icon');
                $module->label = $request->input('label');
                $module->slug = create_slug($request->input('label'));
                $module->save();
                // Add feed item
                $feed_item = new FeedItem;
                $feed_item->text = $module->label .' was added.';
                $feed_item->href = 'dashboard#files';
                $feed_item->icon = $module->icon;
                $feed_item->save();
            }
            if ($request->type == 'reservations') {
                $reservations_module_exists = Module::where('type','reservations')->count();
                if ($reservations_module_exists > 0) return response()->json([
                    'error' => 'You already have a reservations tab. However, you can create unlimited schedules on the existing tab.',
                ]);
                // Store module
                $module = new Module;
                $module->type = $request->input('type');
                $module->icon = $request->input('icon');
                $module->label = $request->input('label');
                $module->slug = create_slug($request->input('label'));
                $module->save();
                // Add feed item
                $feed_item = new FeedItem;
                $feed_item->text = $module->label .' was added.';
                $feed_item->href = 'dashboard#reservations';
                $feed_item->icon = $module->icon;
                $feed_item->save();
            }
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);         
        }
        return response()->json([
            'refresh' => true,
            'success' => 'Settings Saved',
        ]);
        return view('module.store');
    }

    public function editField() {
        return view('form.field.edit');
    }

    public function updateModule(Request $request, $id) {
        // Require fields
        $validator = Validator::make($request->all(), [
            'label' => 'bail|required',
            'icon' => 'bail|required',
        ]);

        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        // Update module record
        try {
            $module = Module::findOrFail($id);
            $module->label = $request->label;
            $module->slug = create_slug($request->label);
            $module->icon = $request->icon;
            $module->enabled = isset($_POST['enabled']) ? 1 : 0;
            $module->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $module->label . ' was updated.';
        $feed_item->href = 'dashboard#'.$module->slug;
        $feed_item->icon = $module->icon;
        $feed_item->save();

        // Return json response
        return response()->json([
            'success' => 'Successfully updated',
            'redirect' => url('dashboard'),
        ]); 
    }

    public function updateModules(Request $request) {
        foreach ($request->id as $key => $id) {
            $module = Module::findOrFail($id);
            $module->order = $key;
            $module->save();
        }
    }

    /**
     * Shows Nebula user interface reference documentation
     *
     * @return view
     */
    public function showReferenceUI() {
        return view('reference.ui');
    }

    public function showFeed() {
        return view('dashboard.feed',[
            'feed_items'=> FeedItem::orderBy('created_at','DESC')->paginate(25)
        ]);
    }

    /*
    |--------------------------------------------------------------------------
    | User Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Show form to edit example record
     *
     * @param integer $id
     * @return view
     *
     */
    public function editUser($id) 
    {
        $user = User::findOrFail($id);
        return view('user.edit',[
            'user' => $user,
        ]);
    }

    public function deleteUser($id)
    {
        $user = User::findOrFail($id);
        // Loop through the users files and delete each of them
        $files = Upload::where('user_id',$id)->get();
        if (count($files) > 0) {
            foreach ($files as $file) { 
                // Delete the file
                Storage::delete($file->source);
            }
        }
        $user->delete();

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $user->name . ' was deleted.';
        $feed_item->href = 'dashboard#users';
        $feed_item->icon = 'icon-circle-user';
        $feed_item->save();

        return response()->json([
            'success' => 'User Removed',
            'refresh' => true,
        ]);
    }

    /**
     * Update user record
     *
     * @param integer $id
     * @return view
     *
     */
    public function updateUser(Request $request, $id)
    {
        // Require fields
        $validator = Validator::make($request->all(), [
            'email' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        // Update user record
        $user = User::findOrFail($id);
        $user->email = $request->email;
        $user->name = $request->name;
        $user->photo = $request->photo;
        if ($request->has('password')) $user->password = bcrypt($request->password);
        $user->save();

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $user->name . ' was updated.';
        $feed_item->details = 'user/'.$user->id.'/edit';
        $feed_item->icon = 'icon-circle-user';
        $feed_item->save();

        // Return json response
        return response()->json([
            'success' => 'User updated',
            'refreshDynamicView' => true,
        ]);
    }

    public function createUser()
    {
        return view('user.create');
    }

    public function storeUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'bail|required',
            'name' => 'bail|required',
            'password' => 'bail|required',
        ]);

        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        // Create user record
        try {
            $user = new User;
            $user->email = $request->email;
            $user->name = $request->name;
            $user->photo = $request->photo;
            $user->password = bcrypt($request->password);
            $user->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $user->name . ' was added to users.';
        $feed_item->details = 'user/'.$user->id.'/edit';
        $feed_item->icon = 'icon-circle-user';
        $feed_item->save();

        // Return json response
        return response()->json([
            'success' => 'Successfully added',
            'refreshDynamicView' => true,
        ]);
    }


    /*
    |--------------------------------------------------------------------------
    | Payment Methods
    |--------------------------------------------------------------------------
    */


    public function createPayment(Request $request)
    {
        return view('payment.create');
    }

    // Called via scheduler in Kernal.php - DEPRECATED
    public function chargeScheduledPayment()
    {
        $scheduled_payments = Payment::where('recurring',1)->get();
        if (count($scheduled_payments) > 0) {
            foreach ($scheduled_payments as $scheduled_payment) {
                // If payment next_charge matches todays date
                if($scheduled_payment->next_charge == date('Y-m-d')) {
                    // Update the number of days until the next charge
                    $scheduled_payment->next_charge =  date('Y-m-d',$scheduled_payment->next_charge + strtotime("+$scheduled_payment->interval days")); // Add days interval to date to calculate next charge date
                    $scheduled_payment->last_charge =  date('Y-m-d'); // last charge is today
                    $scheduled_payment->save();
                    // Set Stripe key
                    $stripe_secret = config('services')['stripe']['secret']; // fetch using config since this method is run from the shell
                    \Stripe\Stripe::setApiKey( $stripe_secret );
                    // Create Stripe charge for cutomer id on file
                    if (isset($this->settings->stripe_id) && $this->settings->stripe_id !== '') {
                        $stripe_charge = \Stripe\Charge::create(array(
                          "currency" => "usd",
                          "customer" => $scheduled_payment->stripe_customer_id,
                          "amount" => $scheduled_payment->amount * 100, // amount in cents
                          'description' => $scheduled_payment->description . ' ('. $scheduled_payment->email.')',
                          //'destination' => Setting::firstOrFail()->stripe_id, // to connected account
                        ), array('stripe_account' => $this->settings->stripe_id));
                        // Store a one time payment record for the charge
                        $payment = new Payment;
                        $payment->stripe_customer_id = $scheduled_payment->stripe_customer_id;
                        $payment->amount = $scheduled_payment->amount;
                        $payment->email = $scheduled_payment->email;
                        $payment->description = $scheduled_payment->description;
                        $payment->first_charge =  date('Y-m-d', $stripe_charge->created);
                        $payment->last_charge = date('Y-m-d'); // today
                        $payment->next_charge = NULL; // NULL as this is recording the one time payment for the day
                        $payment->save();
                    }
                    // TODO: send email reciept?
                }
            }
        }
    }

    public function viewStripeCustomer($customer_id) 
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        // Retrieve the customer from Stripe
        $customer = \Stripe\Customer::retrieve($customer_id,['stripe_account'=>$this->settings->stripe_id]);
        return view('payment.customer.view',['customer'=>$customer]);
    }

    public function viewStripeCharge($stripe_charge_id) 
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        // Retrieve the charge from Stripe
        $payment = \Stripe\Charge::retrieve($stripe_charge_id,['stripe_account'=>$this->settings->stripe_id]);
        $payment_record = StripePayment::where('payment_id',$stripe_charge_id)->first();
        if ($payment_record !== NULL) {
            $payment->questions = $payment_record->questions;
            $payment->answers = $payment_record->answers;
        }
        $customer = NULL;
        if ($payment->customer !== NULL) {
            $customer = \Stripe\Customer::retrieve($payment->customer,['stripe_account'=>$this->settings->stripe_id]);
        }
        return view('payment.charge.view',['payment'=>$payment,'customer'=>$customer]);
    }

    /**
     * Create either a Stripe charge or subscription from a payment form
     */
    public function createStripeChargeFromPaymentForm(Request $request) 
    {
        // Validate fields
        $validator = Validator::make($request->all(), [
            'cardholder' => 'bail|required',
            'email' => 'bail|required|email',
            'amount' => 'bail|required',
        ]);
        if ($validator->fails()) return Redirect::back()->withInput()->withErrors($validator);
        if (!(float)$request->amount > 0) return Redirect::back()->withInput()->withErrors('Amount must be greater than zero.');
        if (isset($this->settings->stripe_id) && $this->settings->stripe_id !== '') {        
            try {
                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                // Create a customer with the charge token
                $stripe_customer = \Stripe\Customer::create(array(
                  "source" => $request->stripeToken,
                  'email' => $request->email,
                  'description' => $request->cardholder,
                ), array('stripe_account' => $this->settings->stripe_id));
                // Store the customer e.g. to lookup later when adding subscriptions
                $customer = new StripeCustomer;
                $customer->customer_id = $stripe_customer->id;
                $customer->email = $request->email;
                $customer->name = $request->cardholder;
                $customer->save();
                if ($request->recurring == 1) {
                    // Lookup the plan
                    $plan = \Stripe\Plan::retrieve($request->plan,['stripe_account'=>$this->settings->stripe_id]);
                    // Subscribe the customer
                    $subscription = \Stripe\Subscription::create(array(
                      "customer" => $stripe_customer->id,
                      "plan" => $plan->id,
                    ),['stripe_account'=>$this->settings->stripe_id]);
                    if (Auth::user() !== NULL) {
                        // Add a record of the subscription so they can manage it from their account in the future
                        $user_stripe_subscription = new UserStripeSubscription;
                        $user_stripe_subscription->subscription_id = $subscription->id;
                        $user_stripe_subscription->user_id = Auth::id();
                        $user_stripe_subscription->description = money_format("%.2n", $plan->amount/100) . '/'.  $plan->interval;
                        if(isset($_POST['answer'])) $user_stripe_subscription->answers = json_encode($_POST['answer']);
                        if(isset($_POST['question'])) $user_stripe_subscription->questions = json_encode($_POST['question']);
                        $user_stripe_subscription->save();
                    } else {
                        // Otherwise save the subscription info in the session to be stored after they log in
                        session([
                            'answers' => (isset($_POST['answer'])) ? json_encode($_POST['answer']) : NULL,
                            'questions' => (isset($_POST['question'])) ? json_encode($_POST['question']) : NULL,
                            'subscription_id' => $subscription->id,
                            'subscription_description' => money_format("%.2n", $plan->amount/100) . '/'.  $plan->interval,
                        ]);
                    }
                } else {                
                    // Charge the card
                    $stripe_charge = \Stripe\Charge::create(array(
                      "currency" => "usd",
                      "customer" => $stripe_customer->id,
                      "amount" => $request->amount * 100, // amount in cents
                      // 'description' => $request->cardholder . ' ('. $request->email.')',
                      'description' => $request->cardholder,
                    ), array('stripe_account' => $this->settings->stripe_id));
                    // Add a record of the payment so they can view questions/answers from the form
                    $stripe_payment = new StripePayment;
                    $stripe_payment->payment_id = $stripe_charge->id;
                    // TODO: Add email and cardholder so the payment form responses can be viewed and exported
                    if(isset($_POST['answer'])) $stripe_payment->answers = json_encode($_POST['answer']);
                    if(isset($_POST['question'])) $stripe_payment->questions = json_encode($_POST['question']);
                    $stripe_payment->save();
                }
            } catch(Exception $e){
                return redirect()->back()->withInput()->withErrors($e->getMessage());
            }
            if ($request->recurring == 1) {
                // Add feed item
                $feed_item = new FeedItem;
                $feed_item->text = 'A new subscription was created.';
                $feed_item->details = 'stripe/subscription/'.$subscription->id;
                $feed_item->icon = 'icon-circle-calendar';
                $feed_item->save();
                return redirect()->to('payment/subscription/'.$subscription->id.'/confirm')->withSuccess('Subscription successfully initiated!');
            } else {
                // Add feed item
                $feed_item = new FeedItem;
                $feed_item->text = 'A new charge for $'.money_format("%.2n", $request->amount).' was processed.';
                $feed_item->details = 'charge/'.$stripe_charge->id.'/view';
                $feed_item->icon = 'icon-circle-payment';
                $feed_item->save();
                return redirect()->back()->withSuccess('Payment successfully processed!');
            }
        } else {
            // Error
            return redirect()->back()->withInput()->withErrors('No Stripe account connected.');
        }
    }

    /**
     * Shows a confirmation page for newly created user subscription
     */
    public function viewSubscriptionConfirmation($subscription_id) 
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $subscription = \Stripe\Subscription::retrieve($subscription_id,['stripe_account'=>$this->settings->stripe_id]);
        $customer = \Stripe\Customer::retrieve($subscription->customer,['stripe_account'=>$this->settings->stripe_id]);
        return view('payment.subscription.confirmation',['subscription'=>$subscription,'customer'=>$customer]);
    }

    /**
     * Shows the form to manage subscription from account page
     */
    public function showManageStripeSubscription($subscription_id) 
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $stripe_subscription = \Stripe\Subscription::retrieve($subscription_id,['stripe_account'=>$this->settings->stripe_id]);
        $subscription = UserStripeSubscription::where('subscription_id',$subscription_id)->firstOrfail();
        return view('payment.subscription.manage',['subscription'=>$subscription,'stripe_subscription'=>$stripe_subscription]);
    }


    /**
     * Shows the form to create a customer from the dashboard
     */
    public function showCreateStripeCustomer() 
    {
        return view('payment.customer.create');
    }

    /**
     * Shows the form to create a plan from the dashboard
     */
    public function showCreateStripePlan() 
    {
        return view('payment.plan.create');
    }

    /**
     * Creates a Stripe plan
     */
    public function createStripePlan(Request $request) 
    {
        // Validate fields
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required',
            'id' => 'bail|required',
            'amount' => 'bail|required',
            'interval' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);
        try {
            // Set API key
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $stripe_plan = \Stripe\Plan::create(array(
              "amount" =>($request->amount * 100),
              "interval" => $request->interval,
              "name" => $request->name,
              "currency" => "usd",
              "id" => $request->id)
            ,['stripe_account'=>$this->settings->stripe_id]);
        } catch(Exception $e){
            return response()->json(['error' => 'Error: ' . $e->getMessage()]);
        }
        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = 'A new payment plan "'.$request->name.'" was created.';
        $feed_item->details = 'stripe/plan/'.$stripe_plan->id;
        $feed_item->icon = 'icon-circle-calendar';
        $feed_item->save();
        return response()->json([
            'success' => 'Plan successfully created!',
            'refreshDetailsView' => true,
            'closeModal' => true
        ]);
    }

    /**
     * Lookup customer from Stripe
     */
    public function findStripeCustomer(Request $request) 
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $customers = StripeCustomer::orWhere('name','LIKE','%'.$request->search.'%')->orWhere('customer_id','=',$request->search)->orWhere('email','LIKE','%'.$request->search.'%')->get();
        return view('payment.customer.list',['customers'=>$customers]);
        // foreach($customers as $customer) {
        //     echo '<button class="rounded">'.$customer->id;.''
        //     echo count($customers);
        // }
    }

    /**
     * Creates a Stripe customer
     */
    public function createStripeCustomer(Request $request) 
    {
        // Validate fields
        $validator = Validator::make($request->all(), [
            'cardholder' => 'bail|required',
            'email' => 'bail|required|email',
            // 'amount' => 'bail|required',
        ]);
        if ($validator->fails()) return Redirect::back()->withInput()->withErrors($validator);
        // if (!(float)$request->amount > 0) return Redirect::back()->withInput()->withErrors('Amount must be greater than zero.');
        if (isset($this->settings->stripe_id) && $this->settings->stripe_id !== '') {        
            try {
                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                // Create a customer with the charge token
                $stripe_customer = \Stripe\Customer::create(array(
                  "source" => $request->stripeToken,
                  'email' => $request->email,
                  'description' => $request->cardholder,
                ), array('stripe_account' => $this->settings->stripe_id));
                // Store the customer e.g. to lookup later when adding subscriptions
                $customer = new StripeCustomer;
                $customer->customer_id = $stripe_customer->id;
                $customer->email = $request->email;
                $customer->name = $request->cardholder;
                $customer->save();
                // Add feed item
                $feed_item = new FeedItem;
                $feed_item->text = 'A new customer was created.';
                $feed_item->details = 'stripe/customer/'.$stripe_customer->id;
                $feed_item->icon = 'icon-circle-user';
                $feed_item->save();
            } catch(Exception $e){
                return redirect()->back()->withInput()->withErrors($e->getMessage());
            }
            return redirect()->back()->withSuccess('Customer successfully added!');
        } else {
            // Error
            return redirect()->back()->withInput()->withErrors('No Stripe account connected.');
        }
    }

    public function createStripeChargeFromDashboard(Request $request)
    {
        // Validate fields
        $validator = Validator::make($request->all(), [
            'cardholder' => 'bail|required',
            'email' => 'bail|required|email',
            'amount' => 'bail|required',
        ]);
        if ($validator->fails()) return Redirect::back()->withInput()->withErrors($validator);
        if (!(float)$request->amount > 0) return Redirect::back()->withInput()->withErrors('Amount must be greater than zero.');
        if (isset($this->settings->stripe_id) && $this->settings->stripe_id !== '') {        
            try {
                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                // Create a customer with the charge token
                $stripe_customer = \Stripe\Customer::create(array(
                  "source" => $request->stripeToken,
                  'email' => $request->email,
                  'description' => $request->cardholder,
                ), array('stripe_account' => $this->settings->stripe_id));
                // Store the customer e.g. to lookup later when adding subscriptions
                $customer = new StripeCustomer;
                $customer->customer_id = $stripe_customer->id;
                $customer->email = $request->email;
                $customer->name = $request->cardholder;
                $customer->save();
                // Charge the card
                $stripe_charge = \Stripe\Charge::create(array(
                  "currency" => "usd",
                  "customer" => $stripe_customer->id,
                  "amount" => $request->amount * 100, // amount in cents
                  'description' => $request->cardholder,
                ), array('stripe_account' => $this->settings->stripe_id));
            } catch(Exception $e){
                return redirect()->back()->withInput()->withErrors($e->getMessage());
            }
            // Add feed item
            $feed_item = new FeedItem;
            $feed_item->text = 'A new charge for $'.money_format("%.2n", $request->amount).' was processed.';
            $feed_item->details = 'charge/'.$stripe_charge->id.'/view';
            $feed_item->icon = 'icon-circle-payment';
            $feed_item->save();
            return redirect()->back()->withSuccess('Payment successfully processed!');
        } else {
            // Error
            return redirect()->back()->withInput()->withErrors('No Stripe account connected.');
        }
    }

    public function showStripeCharge($charge_id)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $charge = \Stripe\Charge::retrieve($charge_id,['stripe_account'=>$this->settings->stripe_id]);
        return view('payment.confirmation');
    }

    public function viewStripeSubscription($subscription_id)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $stripe_subscription = \Stripe\Subscription::retrieve($subscription_id,['stripe_account'=>$this->settings->stripe_id]);
        $customer = \Stripe\Customer::retrieve($stripe_subscription->customer,['stripe_account'=>$this->settings->stripe_id]);
        $subscription = UserStripeSubscription::where('subscription_id',$subscription_id)->first();
        return view('payment.subscription.view',['subscription'=>$subscription,'stripe_subscription'=>$stripe_subscription,'customer'=>$customer]);
    }

    public function viewStripePlan($plan_id)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $plan = \Stripe\Plan::retrieve($plan_id,['stripe_account'=>$this->settings->stripe_id]);
        return view('payment.plan.view',['plan'=>$plan]);
    }

    public function createStripeSubscription(Request $request)
    {
        // Validate fields
        $validator = Validator::make($request->all(), [
            'customer' => 'bail|required',
            'plan' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);
        try {
            $quantity = (count($request->quantity) > 1) ? $request->quantity : 1; // Default to 1            
            $trial_end = (strtotime($request->trial_end) !== NULL && $request->trial_end !== '') ? strtotime($request->trial_end) : 'now'; // default to now
            $tax_rate = ($request->tax_rate > 0.1) ? $request->tax_rate : 0; // default to NULL
            // Set API key
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $subscription = \Stripe\Subscription::create(array(
              "customer" => $request->customer,
              "plan" => $request->plan,
              "quantity" => $quantity,
              "tax_percent" => $tax_rate,
              "trial_end" => $trial_end
            ),['stripe_account'=>$this->settings->stripe_id]);
        } catch(Exception $e){
            return response()->json(['error' => 'Error: ' . $e->getMessage() ]);
        }
        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = 'A customer was subscribed to '.$subscription->plan->name.'.';
        $feed_item->details = 'stripe/subscription/'.$subscription->id;
        $feed_item->icon = 'icon-circle-calendar';
        $feed_item->save();
        return response()->json([
            'success' => 'Customer successfully subscribed to '.$subscription->plan->name,
            'refreshDetailsView' => true,
            'closeModal' => true
        ]);
    }
    // public function createStripeCharge(Request $request)
    // {
    //     // Validate fields
    //     $validator = Validator::make($request->all(), [
    //         'cardholder' => 'bail|required',
    //         'email' => 'bail|required|email',
    //         'amount' => 'bail|required',
    //     ]);
    //     if ($validator->fails()) return Redirect::back()->withInput()->withErrors($validator);
    //     if (!(float)$request->amount > 0) return Redirect::back()->withInput()->withErrors('Amount must be greater than zero.');
    //     if (isset($this->settings->stripe_id) && $this->settings->stripe_id !== '') {        
    //         \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
    //         // Create a customer with the charge token
    //         $stripe_customer = \Stripe\Customer::create(array(
    //           "source" => $request->stripeToken,
    //           'email' => $request->email,
    //           'description' => $request->cardholder,
    //         ), array('stripe_account' => $this->settings->stripe_id));
    //         // Charge the card
    //         $stripe_charge = \Stripe\Charge::create(array(
    //           "currency" => "usd",
    //           "customer" => $stripe_customer->id,
    //           "amount" => $request->amount * 100, // amount in cents
    //           'description' => $request->cardholder . ' ('. $request->email.')',
    //         ), array('stripe_account' => $this->settings->stripe_id));
    //         try {
    //             $payment = new Payment;
    //             $payment->stripe_customer_id = $stripe_customer->id;
    //             $payment->amount = $request->amount;
    //             if (isset($request->payment_form_id)) $payment->payment_form_id = $request->payment_form_id;
    //             $payment->email = $request->email;
    //             if (isset($_POST['question'])) $payment->questions = json_encode($_POST['question']);
    //             if (isset($_POST['answer'])) $payment->answers = json_encode($_POST['answer']);
    //             $payment->description = $request->cardholder;
    //             $payment->first_charge =  date('Y-m-d', $stripe_charge->created);
    //             $payment->last_charge = date('Y-m-d', $stripe_charge->created); // last day they were charged
    //             if (!$request->recurring) {
    //                 $payment->next_charge = NULL; // if final payment
    //             } else { // recurring payment plan          
    //                 $payment->recurring = 1;
    //                 if ((int)$request->interval > 0) {
    //                     $payment->interval = $request->interval; // interval in days between charges
    //                 } else {
    //                     return Redirect::back()->withInput()->withErrors('Interval must be greater than zero.');
    //                 }
    //                 $payment->next_charge = date('Y-m-d', strtotime("+$request->interval days")); // Add days interval to date to calculate next charge date
    //             }
    //             $payment->save();
    //             if ($request->recurring) { // recurring payment plan
    //                 // Make a record for the first charge since it's charged one time automatically when creating a recurring payment
    //                 $one_time_payment = new Payment;
    //                 $one_time_payment->stripe_customer_id = $stripe_customer->id;
    //                 $one_time_payment->amount = $request->amount;
    //                 if (isset($request->payment_form_id)) $one_time_payment->payment_form_id = $request->payment_form_id;
    //                 if (isset($_POST['question'])) $one_time_payment->questions = json_encode($_POST['question']);
    //                 if (isset($_POST['answer'])) $one_time_payment->answers = json_encode($_POST['answer']);
    //                 $one_time_payment->email = $request->email;
    //                 $one_time_payment->description = $request->cardholder;
    //                 $one_time_payment->first_charge =  date('Y-m-d', $stripe_charge->created);
    //                 $one_time_payment->last_charge = date('Y-m-d', $stripe_charge->created); // last day they were charged
    //                 $one_time_payment->save();
    //             }
    //         } catch(Exception $e){
    //             return redirect()->back()->withInput()->withErrors($e->getMessage());
    //         }
    //         return redirect()->back()->withSuccess('Payment successfully processed!');
    //     } else {
    //         // Error
    //         return redirect()->back()->withInput()->withErrors('No Stripe account connected.');
    //     }
    // }

    public function cancelPayment($id)
    {
        $payment = Payment::findOrFail($id);
        $payment->delete();
        return response()->json([
            'success' => 'Payment Cancelled',
            'refreshDynamicView' => true,
        ]);
    }

    public function createPaymentForm()
    {
        return view('payment.form.create');
    }

    public function showPaymentFormPage(Request $request, $slug) 
    {
        $form = PaymentForm::where('slug',$slug)->where('active',1)->firstOrFail();
        return view('payment.form.view',['form' => $form]);
    }

    public function showPayment($id)
    {
        $payment = Payment::findOrFail($id);
        return view('payment.view',['payment'=>$payment]);
    }

    public function createPaymentField($id)
    {
        $form = PaymentForm::findOrFail($id);
        return view('form.field.create',['form'=>$form]);
    }

    public function deletePaymentForm($id)
    {
        // Delete the payment form
        $payment_form = PaymentForm::findOrFail($id);
        $payment_form->delete();
        // Return json response
        return response()->json([
            'refreshDynamicView' => true,
            'closeModal' => true,
        ]);
    }

    public function updatePaymentForm(Request $request, $id) 
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');

        try {
            $form = PaymentForm::findOrFail($id);
            // Check if name is already taken
            $form_exists = PaymentForm::where('title',$request->title)->first();
            if ($form_exists !== null && $form_exists->id !== $form->id) return response()->json([
                'error' => 'A payment form with that name already exists. Please choose another name.',
            ]);

            if (isset($request->title) && $request->title !== '') {            
                $form->title = $request->title;
                $form->amount = $request->amount;
                if ($request->active == 'on') {
                    $form->active = 1;
                } else {
                    $form->active = 0;
                }
                // If recurring box checked
                if ($request->recurring == 'on') {
                    $form->recurring = 1;
                    $form->plan_id = $request->plan;
                    if ($form->plan_id == '') {
                        return response()->json([
                            'error' => 'Please select a payment plan.',
                        ]);
                    }
                    // Get plan from Stripe
                    \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                    $plan = \Stripe\Plan::retrieve($form->plan_id,['stripe_account'=>$this->settings->stripe_id]);
                    $form->plan_name = $plan->name;
                } else {
                    $form->recurring = 0;
                }
                // Interval has been deprecated
                // if ($request->interval == '') {
                //     $form->interval = NULL;
                // } else {
                //     $form->interval = $request->interval;
                // }
                $form->slug = create_slug($request->title);
                $form->save();
            }

            // First delete the old form items
            $form_items = PaymentFormItem::where('payment_form_id',$form->id)->delete();

            // Added new form items
            if (isset($_POST['field_type'])) {
                foreach ($_POST['field_type'] as $key => $type) {
                    $field = new PaymentFormItem;
                    $field->order = $key;
                    $field->payment_form_id = $id;
                    if (isset($_POST['field_label'][$key]))         $field->label = $_POST['field_label'][$key];
                    if (isset($_POST['field_hint'][$key]))          $field->hint = $_POST['field_hint'][$key];
                    if (isset($_POST['field_placeholder'][$key]))   $field->placeholder = $_POST['field_placeholder'][$key];
                    if (isset($_POST['field_type'][$key]))          $field->type = $_POST['field_type'][$key];
                    if (isset($_POST['field_options'][$key]))       $field->options = $_POST['field_options'][$key];
                    $field->save();
                }
            }

        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Return json response
        return response()->json([
            'success' => 'Successfully updated',
            'refreshDetailsView' => true,
        ]); 
    }

    public function editPaymentForm($id)
    {
        $form = PaymentForm::findOrFail($id);
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        if (isset($this->settings->stripe_id) && $this->settings->stripe_id !== '') {
            $plans = \Stripe\Plan::all('',array('stripe_account' => $this->settings->stripe_id));
        }
        return view('payment.form.edit',['form'=>$form,'plans'=>$plans]);
    }

    /**
     * Stores payment form in the database
     */
    public function storePaymentForm(Request $request)
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');

        // Require fields
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required',
        ]);

        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        try {
            // Check if name is already takem
            $form_exists = PaymentForm::where('title',$request->name)->first();
            if ($form_exists !== null) return response()->json([
                'error' => 'A payment form with that name already exists. Please choose another name.',
            ]);
            $form = new PaymentForm;
            $form->active = 1;
            $form->title = $request->name;
            $form->slug = create_slug($request->name);
            $form->save();
            // Add feed item
            $feed_item = new FeedItem;
            $feed_item->text = 'A new payment form was created.';
            $feed_item->details = 'payment/form/'.$form->id.'/edit';
            $feed_item->icon = 'icon-circle-form';
            $feed_item->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        // Return json response
        return response()->json([
            'success' => 'Successfully added',
            'loadDetailsView' => 'payment/form/'.$form->id.'/edit',
            'closeModal' => true,
        ]); 
    }

    public function showPaymentForms()
    {
        $payment_forms = PaymentForm::where('active',1)->orderBy('created_at','DESC')->paginate(25);
        return view('payment.form.index',[
            'payment_forms' => $payment_forms
        ]);
    }

    public function showScheduledPayments()
    {
        $payments = Payment::where('next_charge','>',Carbon::now())->orderBy('created_at','DESC')->paginate(25);
        return view('payment.scheduled',[
            'payments' => $payments
        ]);
    }

    public function setupPayments($stripe_id) {
        $settings = Setting::firstOrfail();
        // check if they have a Stripe ID
        if (!isset($settings->stripe_id) || $settings->stripe_id == null || $settings->stripe_id !== '') {
            // No Stripe ID is currently set, update platform settings
            $settings->stripe_id = $stripe_id;
            $settings->save();
        }
        return Redirect::to('dashboard#payments')->withSuccess('You\'re all set up for collecting payments!');
    }

    /**
     * Accept a reponse from Stripe and redirect the user to the Nebula url they belong to
     */
    public function redirectToSetupPayments() {
        // If a token for authenticating with Stripe is available
        if (isset($_GET['code'])) { // add Stripe ID & redirect w/ code
            $code = $_GET['code'];
            // check if they have a Stripe ID
            if (!isset($settings->stripe_id) || $settings->stripe_id == null || $settings->stripe_id !== '') {
                $client_secret = env('STRIPE_SECRET');
                $token_request_body = array(
                    'client_secret' => $client_secret,
                    'grant_type' => 'authorization_code',
                    'client_id' => env('STRIPE_CLIENT_ID'),
                    'code' => $code,
                    );
                $req = curl_init('https://connect.stripe.com/oauth/token');
                curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($req, CURLOPT_POST, true );
                curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
                $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
                $resp = json_decode(curl_exec($req), true);
                curl_close($req);
                // Invalid response, return error
                if (!isset($resp['stripe_user_id'])) {
                    // return Redirect::to('payments')->withErrors($resp['error_description']);
                    dd($resp['error_description']);
                } else { // update the stripe customer id for the current property
                    // This part of the method will only be run on https://nebulasuite.com/payments/setup therefore do not update the settings
                    // Here's where you redirect to another URL off of nebulasuite.com (e.g. $_GET['state']/payments/setup/$resp['stripe_user_id']) the application
                    return Redirect::to($_GET['state'].'/payments/setup/'.$resp['stripe_user_id']);
                    // $settings->stripe_id = $resp['stripe_user_id'];
                    // $settings->stripe_publishable_key = $resp['stripe_publishable_key'];
                    // $settings->save();
                    // Session::set('success', 'testng');
                    // return Redirect::to('payments')->withSuccess('You\'re all set up for collecting payments!');
                } 
            } else { // already connected
                // return Redirect::to('payments')->withErrors('You\'re already connected with Stripe.');
            }
        } else if (isset($_GET['error'])) { // Error
            dd($_GET['error_description']);
            // return Redirect::to('dashboard')->withErrors($_GET['error_description']);
        }
    }

    public function showPayments()
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        // $settings = Setting::firstOrfail();
        // Generate Stripe url to link account
        $authorize_request_body = array(
          'response_type' => 'code',
          //'redirect_uri' => env('APP_URL').'/dashboard#payments',
          'scope' => 'read_write',
          'client_id' => env('STRIPE_CLIENT_ID'),
          'state' => env('APP_URL'), // This is a Nebula client/account's url e.g. accountant.nebulasuite.com
        );
        $url = 'https://connect.stripe.com/oauth/authorize' . '?' . http_build_query($authorize_request_body);

        // // If a token for authenticating with Stripe is available
        // if (isset($_GET['code'])) { // add Stripe ID & redirect w/ code
        //     $code = $_GET['code'];
        //     // check if they have a Stripe ID
        //     // if (!isset($settings->stripe_id) || $settings->stripe_id == null || $settings->stripe_id !== '') {
        //         $client_secret = env('STRIPE_SECRET');
        //         $token_request_body = array(
        //             'client_secret' => $client_secret,
        //             'grant_type' => 'authorization_code',
        //             'client_id' => env('STRIPE_CLIENT_ID'),
        //             'code' => $code,
        //             );
        //         $req = curl_init('https://connect.stripe.com/oauth/token');
        //         curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        //         curl_setopt($req, CURLOPT_POST, true );
        //         curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
        //         $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
        //         $resp = json_decode(curl_exec($req), true);
        //         curl_close($req);
        //         // Invalid response, return error
        //         if (!isset($resp['stripe_user_id'])) {
        //             // return Redirect::to('payments')->withErrors($resp['error_description']);
        //             dd($resp['error_description']);
        //         } else { // update the stripe customer id for the current property
        //             // TODO: this method will only be run by https://nebulasuite.com; therefore do not update the settings
        //             // $settings->stripe_id = $resp['stripe_user_id'];
        //             return Redirect::to($_GET['state'].'/payments/setup/'.$resp['stripe_user_id']);
        //             // TODO: Here's where you redirect to another URL off of nebulasuite.com (e.g. $_GET['state']/payments/setup/$resp['stripe_user_id']) the application
        //             // $settings->stripe_publishable_key = $resp['stripe_publishable_key'];
        //             // $settings->save();
        //             // Session::set('success', 'testng');
        //             return Redirect::to('payments')->withSuccess('You\'re all set up for collecting payments!');
        //         } 
        //     // } else { // already connected
        //         // return Redirect::to('payments')->withErrors('You\'re already connected with Stripe.');
        //     // }
        // } else if (isset($_GET['error'])) { // Error
        //     // return Redirect::to('dashboard')->withErrors($_GET['error_description']);
        // }
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        // $payments = Payment::where('recurring',0)->orderBy('created_at','DESC')->get();
        // KEEP CUSTOM PAGINATOR CODE
        // Create custom paginator
        if (isset($this->settings->stripe_id) && $this->settings->stripe_id !== '') {
            $payments = \Stripe\Charge::all('',array('stripe_account' => $this->settings->stripe_id));
            $paginator = new LengthAwarePaginator($payments->data, count($payments->data), 25);
            $paginator->setPath('payments');
            // Slice the results to match the current page
            $payments = array_slice($payments->data,($paginator->perPage() * $paginator->currentPage()) - 25,$paginator->perPage());
        } else {
            $payments = NULL;
        }
        return view('payment.index',['url'=>$url,'payments'=>$payments]);
    }

    public function cancelStripeSubscription(Request $request, $id)
    {
        try {
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $subscription = \Stripe\Subscription::retrieve($id,['stripe_account'=>$this->settings->stripe_id]);
            $subscription->cancel();
            // Remove the matching record from user_stripe_subscriptions
            $user_stripe_subscription = UserStripeSubscription::where('subscription_id',$id)->first();
            if ($user_stripe_subscription !== NULL) {
                $user_stripe_subscription->delete();
            }
            // TODO: complete this feed item
            // // Add feed item
            // $feed_item = new FeedItem;
            // $feed_item->text = ' subscription has been cancelled.';
            // $feed_item->details = 'invoice/'.$invoice->id.'/edit';
            // $feed_item->icon = 'icon-circle-form';
            // $feed_item->save();
            return response()->json([
                'success' => 'Your subscription has been cancelled.',
                'refresh' => true,
            ]);
        } catch(Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
    }

    public function showCreateStripeSubscription(Request $request)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        if (isset($this->settings->stripe_id) && $this->settings->stripe_id !== '') {
            $customers = \Stripe\Customer::all('',array('stripe_account' => $this->settings->stripe_id));
            $plans = \Stripe\Plan::all('',array('stripe_account' => $this->settings->stripe_id));
        }
        // dd($plans);
        return view('payment.subscription.create',['plans'=>$plans,'customers'=>$customers]);
    }

    public function showPaymentPlans()
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        if (isset($this->settings->stripe_id) && $this->settings->stripe_id !== '') {
            $plans = \Stripe\Plan::all('',array('stripe_account' => $this->settings->stripe_id));
        }
        // Create custom paginator
        $paginator = new LengthAwarePaginator($plans->data, count($plans->data), 25);
        $paginator->setPath('payments/plans'); // route of listings
        // Slice the results to match the current page
        $plans = array_slice($plans->data,($paginator->perPage() * $paginator->currentPage()) - 25,$paginator->perPage());
        return view('payment.plan.index',['plans'=>$plans]);
    }

    public function showSubscriptions()
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        if (isset($this->settings->stripe_id) && $this->settings->stripe_id !== '') {
            $subscriptions = \Stripe\Subscription::all('',array('stripe_account' => $this->settings->stripe_id));
        }
        // Create custom paginator
        $paginator = new LengthAwarePaginator($subscriptions->data, count($subscriptions->data), 25);
        $paginator->setPath('payments/subscriptions');
        // Slice the results to match the current page
        $subscriptions = array_slice($subscriptions->data,($paginator->perPage() * $paginator->currentPage()) - 25,$paginator->perPage());
        $invoices = Invoice::orderBy('created_at','DESC')->paginate(25);
        return view('payment.subscription.index',['subscriptions'=>$subscriptions]);
    }

    public function showInvoices()
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        $invoices = Invoice::orderBy('created_at','DESC')->paginate(25);
        return view('payment.invoice.index',['invoices'=>$invoices]);
    }


    public function addInvoiceItem()
    {
        return view('payment.invoice.item');
    }

    public function createInvoice()
    {
        $invoice_settings = InvoiceSetting::first();
        return view('payment.invoice.create',['invoice_settings'=>$invoice_settings]);
    }

    public function shareInvoice($id, Request $request)
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');

        $invoice = Invoice::findOrfail($id);
        $this->invoice = $invoice;

        // Validate fields
        $validator = Validator::make($request->all(), [
            'emails' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        // Seperate commma seperated emails
        $emails = explode(',', $request->emails);
        foreach ($emails as $email) {
            $email = preg_replace('/\s+/', '', $email); // Remove white spaces
            $this->email = $email;
            if (!filter_var( $email ,FILTER_VALIDATE_EMAIL )) {
                return response()->json([
                    'error' => 'One or more of the emails you entered were not valid. Check under "Shared with:" to see who the invoice has been shared with.',
                ]);
            }

            // Lookup whether or not the invoice was already shared, if so skip
            $shared = Invoice::where('id',$invoice->id)->where('shared_with','LIKE','%'.$email.'%')->get();
            if (count($shared) > 0) continue;

            $invoice_settings = InvoiceSetting::first();
            $image = $invoice_settings->logo;
            $path = storage_path('app/'.$invoice_settings->logo);
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            $pdf = PDF::loadView('payment.invoice.pdf',['invoice'=>$this->invoice,'logo'=>$base64])->output();

            // Send an email with the invoice attached
            $content = 'A PDF invoice from '.$this->settings->organization_name.' is attached. Thank you for your business!';
            if ($request->message !== '') $content .= '<br/><br/><b>Message:</b> '.nl2br($request->message);
            $mail = Mail::send('emails.default', [
                'subject'=>'Your invoice from '.$this->settings->organization_name.' is attached.',
                'content' => $content,
            ], function ($mail) use ($pdf) {
                if (env('APP_ENV') == 'local') {
                    $mail->from('notices@nebulasuite.com', $this->settings->organization_name);
                    $mail->to($this->email);
                    $mail->attachData($pdf,$this->invoice->name.'.pdf');
                    $mail->subject('Your invoice from '.$this->settings->organization_name.' is attached.');
                } else {
                    $mail->from('notices@'.env('APP_DOMAIN'), $this->settings->organization_name);
                    $mail->to($this->email);
                    $mail->attachData($pdf,$this->invoice->name.'.pdf');
                    $mail->subject('Your invoice from '.$this->settings->organization_name.' is attached.');
                }
            });
            if (!$mail) return response()->json(['error' => 'Failed to send mail to '.$email]);

            // Update shared with array to include the email 
            if ($this->invoice->shared_with == NULL) {
                $shared_with_array = array();
            } else {
                $shared_with_array = json_decode($this->invoice->shared_with);
            }
            array_push($shared_with_array,$email); // Add email to the existing array
            $this->invoice->shared_with = json_encode($shared_with_array);
            $this->invoice->save();
        }

        // Add feed item
        $feed_item = new FeedItem;
        if (count($emails) > 1) {
            $feed_item->text = $invoice->name . ' was sent to '.count($emails).' people.';
        } else { // shared only with 1 person
            $feed_item->text = $invoice->name . ' was sent to '.$email.'.';
        }
        $feed_item->details = 'invoice/'.$invoice->id.'/edit';
        $feed_item->icon = 'icon-circle-payment';
        $feed_item->save();

        return response()->json([
            'success' => 'Invoice successfully shared',
            'closeModal' => true,
            'refreshDetailsView' => true,
        ]);
    }

    public function editInvoice($id)
    {
        $invoice = Invoice::findOrfail($id);
        return view('payment.invoice.edit',['invoice'=>$invoice]);
    }

    public function updateInvoice(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required',
            'from' => 'bail|required',
            'bill_to' => 'bail|required',
            'due_date' => 'bail|required',
            'total_due' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);
        // Create invoice
        try {
            $invoice = Invoice::findOrfail($id);
            $invoice->name = $request->name;
            $invoice->number = $request->number;
            $invoice->from = $request->from;
            $invoice->bill_to = $request->bill_to;
            $invoice->terms = $request->terms;
            $invoice->tax = $request->tax;
            $invoice->due_date = $request->due_date;
            $invoice->amount_due = $request->total_due;
            $invoice->notes = $request->notes;
            // $invoice_items_array = array(
            //     'items'=>$_POST['item'],
            //     'rates'=>$_POST['rate'],
            //     'quantities'=>$_POST['quantity'],
            // );
            $invoice_items_array = array();
            // Create an array out of the posted items from the invoice form
            foreach ($_POST['item'] as $key => $value) {
                array_push($invoice_items_array, array($_POST['item'][$key],$_POST['rate'][$key],$_POST['quantity'][$key]));
            }
            $invoice->items = json_encode($invoice_items_array);
            $invoice->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        return response()->json([
            'success' => 'Invoice updated',
            'loadDetailsView' => 'invoice/'.$invoice->id.'/edit',
            // 'closeModal' => true,
        ]);
    }

    /**
     * Download an invoice as a PDF
     */
    public function downloadInvoice($id)
    {
        $invoice_settings = InvoiceSetting::first();
        $invoice = Invoice::findOrFail($id);
        $image = $invoice_settings->logo;
        $path = storage_path('app/'.$invoice_settings->logo);
        $type = pathinfo($path, PATHINFO_EXTENSION);
        // If the logo is found
        if (file_exists($path) == true) {
            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            $pdf = PDF::loadView('payment.invoice.pdf',['invoice'=>$invoice,'logo'=>$base64]);
            return $pdf->download($invoice->name .'.pdf');
        }
        $pdf = PDF::loadView('payment.invoice.pdf',['invoice'=>$invoice,'logo'=>NULL]);
        return $pdf->download($invoice->name .'.pdf');
    }

    /**
     * Create's an invoice record
     */
    public function storeInvoice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required',
            'from' => 'bail|required',
            'bill_to' => 'bail|required',
            'due_date' => 'bail|required',
            'total_due' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);
        // Create invoice
        try {
            $invoice = new Invoice;
            $invoice->name = $request->name;
            $invoice->number = $request->number;
            $invoice->from = $request->from;
            $invoice->bill_to = $request->bill_to;
            $invoice->due_date = $request->due_date;
            $invoice->terms = $request->terms;
            $invoice->tax = $request->tax;
            $invoice->amount_due = $request->total_due;
            $invoice->notes = $request->notes;
            $invoice_items_array = array();
            foreach ($_POST['item'] as $key => $value) {
                array_push($invoice_items_array, array($_POST['item'][$key],$_POST['rate'][$key],$_POST['quantity'][$key]));
            }
            // return response()->json([
            //     'error' => $invoice_items_array[1][2],
            // ]);
            // dd($invoice_items_array);
            // $invoice_items_array = array(
            //     'items'=>$_POST['item'],
            //     'rates'=>$_POST['rate'],
            //     'quantities'=>$_POST['quantity'],
            // );
            $invoice->items = json_encode($invoice_items_array);
            $invoice->save();
            // Add feed item
            $feed_item = new FeedItem;
            $feed_item->text = 'A new invoice was created.';
            $feed_item->details = 'invoice/'.$invoice->id.'/edit';
            $feed_item->icon = 'icon-circle-form';
            $feed_item->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        return response()->json([
            'success' => 'Invoice saved',
            'loadDetailsView' => 'invoice/'.$invoice->id.'/edit',
            'closeModal' => true,
        ]);
    }

    public function updateInvoiceSettings(Request $request) 
    {
        // $validator = Validator::make($request->all(), [
        //     'logo' => 'bail|mimes:jpeg,jpg,png,gif',
        // ]);
        // // Return validation error
        // if ($validator->fails()) return response()->json([
        //     'error' => $validator->errors()->first(),
        // ]);
        $invoice_settings = InvoiceSetting::first();
        if (count($invoice_settings) == 0) $invoice_settings = new InvoiceSetting; // First time adding invoice settings
        $invoice_settings->logo = $request->logo;
        $invoice_settings->from = $request->from;
        $invoice_settings->terms = $request->terms;
        $invoice_settings->save();
        // Return json response
        return response()->json([
            'success' => 'Settings updated',
            'refreshDetailsView' => true,
            'closeModal' => true,
        ]);
    }

    public function showShareInvoice($id) 
    {
        $invoice = Invoice::findOrfail($id);
        return view('payment.invoice.share',['invoice'=>$invoice]);
    } 

    public function showInvoiceSettings() 
    {
        $invoice_settings = InvoiceSetting::first();
        return view('payment.invoice.settings',['invoice_settings'=>$invoice_settings]);
    }

    public function showCustomers()
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $customers = \Stripe\Customer::all('',['stripe_account'=>$this->settings->stripe_id]);

        // Create custom paginator
        $paginator = new LengthAwarePaginator($customers->data, count($customers->data), 25);
        $paginator->setPath('payments/customers');

        // Slice the results to match the current page
        $customers = array_slice($customers->data,($paginator->perPage() * $paginator->currentPage()) - 25,$paginator->perPage());
        return view('payment.customer.index',['paginator'=>$paginator,'customers'=>$customers]);
    }


    /*
    |--------------------------------------------------------------------------
    | Organization Methods
    |--------------------------------------------------------------------------
    */

    // // Display record
    // public function showOrganization() {
    //     return view('view');
    // }

    // // Show create view/form
    // public function createOrganization() {
    //     return view('create');
    // }

    // Show edit view
    // public function editOrganization() {
    //     $organization = Organization::where('subdomain',$subdomain)->firstOrfail();
    //     return view('edit',['organization'=>$organization]);
    // }

    // Store record
    // public function storeOrganization(Request $request) {
    //     // Validate fields
    //     $validator = Validator::make($request->all(), [
    //         'name' => 'bail|required',
    //         'subdomain' => 'bail|required',
    //     ]);

    //     // Return validation error
    //     if ($validator->fails()) return Redirect::back()->withInput()->withErrors($validator);

    //     try 
    //     {
    //         // Store organization
    //         $organization = new Organization;
    //         $organization->name = $request->input('name');
    //         $organization->subdomain = create_slug($request->input('subdomain'));
    //         if ($request->has('logo')) $organization->logo = $request->input('logo');
    //         $organization->user_id = Auth::id(); // creator id
    //         $organization->save();

    //         // Create a homepage record for the organization
    //         $page = new Page;
    //         $page->title = $organization->name;
    //         $page->organization_id = $organization->id;
    //         $page->user_id = Auth::id();
    //         $page->save();

    //         // Create a manager record for under the organization for the current user
    //         $manager = new Manager;
    //         $manager->organization_id = $organization->id;
    //         $manager->user_id = Auth::id();
    //         $manager->save();

    //         // Create a member record for the organization under the organization for the current user
    //         $member = new Member;
    //         $member->organization_id = $organization->id;
    //         $member->user_id = Auth::id();
    //         $member->save();
    //     } catch(Exception $e){
    //         return Redirect::back()->withInput()->withErrors($e->getMessage());
    //     }
    //     return Redirect::to('account')->withSuccess('Organization created');
    // }

    // // Update record
    // public function updateOrganization(Request $request, $id) {
    //     // Validate fields
    //     $validator = Validator::make($request->all(), [
    //         'name' => 'bail|required',
    //         'subdomain' => 'bail|required',
    //     ]);
    //     // Return validation error
    //     if ($validator->fails()) return Redirect::back()->withInput()->withErrors($validator);
    //     // Store organization
    //     $organization = Organization::findOrFail($id);
    //     if ($request->has('name')) $organization->name = $request->input('name');
    //     if ($request->has('subdomain')) $organization->subdomain = create_slug($request->input('subdomain'));
    //     if ($request->has('logo')) $organization->logo = $request->input('logo');
    //     $organization->save();
    //     return Redirect::to('//'.$organization->subdomain.'.'.$_ENV['APP_DOMAIN'].'/settings')->withSuccess('Organization updated')->with(['organization'=>$organization]);
    // }

    // // Delete organization
    // // public function deleteOrganization() {
        
    // // }

    public function showSettings() {
        $managers = User::where('is_manager',1);
        return view('dashboard.settings',['managers'=>$managers]);
    }

    /*
    |--------------------------------------------------------------------------
    | Organization Manager Methods
    |--------------------------------------------------------------------------
    */

    public function searchOrganizationManagers(Request $request) 
    {
        return view('manager.list',[
            'organization' => $organization,
            'managers' => $organization->managers,
        ]);
    }


    public function searchToAddOrganizationManagers(Request $request) 
    {
        $users = User::where('email', 'LIKE', '%'. $request->input('query') .'%')
                        ->where('name', 'LIKE', '%'. $request->input('query') .'%')
                        ->get();
        return view('manager.add',[
            'users' => $users,
        ]);
    }

    public function toggleManagerStatus(Request $request, $id) 
    {
        if (Auth::user()->is_admin || Auth::user()->is_manager) {        
            $user = User::findOrFail($id);
            // Toggle manager status
            if ($user->is_manager) {
                $user->is_manager = 0;
            } else {
                $user->is_manager = 1;
            }
            $user->save();
        } else {
            // Return json response
            return response()->json([
                'error' => 'Unauthorized',
            ]);
        }
        if ($user->is_manager) {
            // Add feed item
            $feed_item = new FeedItem;
            $feed_item->text = $user->name . ' was granted manager access.';
            $feed_item->details = 'user/'.$user->id.'/edit';
            $feed_item->icon = 'icon-circle-user';
            $feed_item->save();
            // Return json response
            return response()->json([
                'success' => 'Revoke Manager Access',
            ]);
        } else {
            // Add feed item
            $feed_item = new FeedItem;
            $feed_item->text = $user->name . "'s ". 'manager access was revoked.';
            $feed_item->details = 'user/'.$user->id.'/edit';
            $feed_item->icon = 'icon-circle-user';
            $feed_item->save();

            // Return json response
            return response()->json([
                'success' => 'Grant Manager Access',
            ]);
        }
    }

    public function createManager(Request $request) 
    {
        return view('manager.create',[
            'organization' => $organization,
        ]);
    }


    public function addManager(Request $request) 
    {
        dd('add manager'.$request->user_id);
    }


    /*
    |--------------------------------------------------------------------------
    | Image Uploader Methods
    |--------------------------------------------------------------------------
    */

    // Show choose image modal
    public function chooseImage(Request $request)
    {
        $images = Upload::where('user_id', Auth::id())->where('type', 'image')->orderBy('created_at','DESC')->get();
        return view('upload.images',['images' => $images]);
    }

    public function storeImage(Request $request)
    {
        // Validate fields
        $validator = Validator::make($request->all(), [
            'file' => 'bail|required|mimes:jpeg,jpg,png,svg,gif',
        ]);
        // Return validation error
        if ($validator->fails()) return Redirect::back()->withInput()->withErrors($validator);
        // Create directory with user id
        $directory = 'uploads/user/'.Auth::id();
        $filename = time().'_'.$request->file->getClientOriginalName();
        Storage::MakeDirectory($directory);
        // Store upload in directory
        Storage::put($directory.'/'.$filename,file_get_contents($request->file->getRealPath()));
        // Store upload record in db
        $upload = new Upload;
        $upload->user_id = Auth::id();
        $upload->name = substr($filename,11); // remove timestamp
        $upload->source =  $directory.'/'.$filename;
        $upload->size = filesize($request->file);
        $upload->type = 'image';
        $upload->save();
        return Redirect::back();
    }

    public function chooseFile(Request $request)
    {
        $uploads = Upload::where('user_id', Auth::id())->where('type', 'file')->orderBy('created_at','DESC')->get();
        return view('upload.files',['uploads' => $uploads]);
    }

    public function editFilesFolder($id)
    {
        $folder = FileFolder::findOrFail($id);        
        return view('file.folder.edit',[
            'folder' => $folder,
        ]);
    }

    public function deleteFilesFolder($id)
    {
        try {
            $folder = FileFolder::findOrFail($id);
            $files = Upload::where('folder_id',$folder->id)->get();
            foreach ($files as $file) {
                $file = Upload::findOrFail($file->id);
                // Delete the file
                Storage::delete($file->source);
                $file->delete();
                // Add feed item
                $feed_item = new FeedItem;
                $feed_item->text = $file->name . ' was deleted';
                $feed_item->details = 'dashboard#files';
                $feed_item->icon = getIconFromType($file->type);
                $feed_item->save(); 
            }
            $folder->delete();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        return response()->json([
            'success' => 'Folder deleted.',
            'redirect' => url('files'),
        ]);
    }

    public function updateFilesFolder(Request $request, $id)
    {
        // Require fields
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required',
        ]);

        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        // Update example record
        try {
            $folder = FileFolder::findOrFail($id);
            $folder->name = $request->name;
            $folder->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $folder->name . ' was updated.';
        $feed_item->details = 'files/folder/'.$folder->id.'/edit';
        $feed_item->icon = 'icon-circle-folder';
        $feed_item->save();

        // Return json response
        return response()->json([
            'success' => 'Successfully updated',
            'refresh' => true,
        ]); 
    }

    public function showFile($user_id,$filename)
    {
        $upload = Upload::where('source','uploads/user/'.$user_id.'/'.$filename)->first();
        $shared = FileShare::where('file_id',$upload->id)->where('user_id',Auth::id())->first();
        if ((Auth::user() !== null && Auth::user()->is_manager) || isset($shared)) {
            return response()->file(storage_path('app'.'/uploads/user/'.$user_id.'/'.$filename));
        } else {
            // Make sure it's the settings photo / logo photo if we are going to display it publicly
            if (count($upload) == 1) return response()->file(storage_path('app'.'/uploads/user/'.$user_id.'/'.$filename));
        }
    }


    public function showShareFile($id)
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        $file = Upload::findOrFail($id);
        return view('file.share',['file' => $file]);
    }

    public function shareFilesFolder(Request $request, $id)
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        // Validate fields
        $validator = Validator::make($request->all(), [
            'emails' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);
        $file_folder = FileFolder::findOrFail($id);
        $this->file_folder = $file_folder;
        $emails = explode(',', $request->emails);
        foreach ($emails as $email) {
            $email = preg_replace('/\s+/', '', $email); // Remove white spaces
            if (!filter_var( $email ,FILTER_VALIDATE_EMAIL )) {
                return response()->json([
                    'error' => 'Folder not shared. Enter only valid email addresses.',
                ]);
            }
            $settings = Setting::first();
            $this->org_name = $settings->organization_name;

            // Lookup whether or not the user exist
            $user = User::where('email',$email)->first();
            if ($user == null) {
                return response()->json([
                    'error' => $email . ' must first create an account before you can share a file with them.',
                ]);
            }
            // $this is used to keep in scope of mail
            $this->user = $user;
            // Add a record that the file was shared if folder not already shared else continue
            if (FileFolderShare::where('user_id',$user->id)->where('folder_id',$file_folder->id)->count() > 0) {
                // continue; // Folder already shared
            } else {                
                $share = new FileFolderShare;
                $share->folder_id = $file_folder->id;
                $share->user_id = $user->id;
                $share->save();
            }
            // Send an email that a file has been shared
            $mail = Mail::send('emails.default', [
                'subject'=>'A folder was shared with you at '.env('APP_DOMAIN'),
                'content' => strtok($this->user->name , " "). ', <br/><br/>'.Auth::user()->name.' has shared a file folder: "' . $this->file_folder->name . '" with you. This folder is available under the My Files section of <a href="'.url('account').'">'.'your account.</a>',
            ], function ($mail) {
                if (env('APP_ENV') == 'local') {
                    $mail->from('support@nebulasuite.com', $this->org_name);
                    $mail->to($this->user->email, $this->user->name);
                    $mail->subject('A file was shared with you at '.env('APP_DOMAIN'));
                } else {
                    $mail->from('support@'.env('APP_DOMAIN'), $this->org_name);
                    $mail->to($this->user->email, $this->user->name);
                    $mail->subject('A file was shared with you at '.env('APP_DOMAIN'));
                }
            });
            if (!$mail) return response()->json(['error' => 'Failed to send mail to '.$email]);
        }
        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $file_folder->name . ' was shared with '.$this->user->name.'.';
        $feed_item->modal = 'files/folder/'.$file_folder->id.'/edit';
        $feed_item->icon = 'icon-circle-folder';
        $feed_item->save();
        return response()->json([
            'success' => 'Folder successfully shared',
            'closeModal' => true,
        ]);
    }

    public function shareFile(Request $request, $id)
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        // Validate fields
        $validator = Validator::make($request->all(), [
            'emails' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);
        $file = Upload::findOrFail($id);
        $this->file = $file;
        $emails = explode(',', $request->emails);
        foreach ($emails as $email) {
            $email = preg_replace('/\s+/', '', $email); // Remove white spaces
            if (!filter_var( $email ,FILTER_VALIDATE_EMAIL )) {
                return response()->json([
                    'error' => 'File not shared. Enter only valid email addresses.',
                ]);
            }
            $settings = Setting::first();
            $this->org_name = $settings->organization_name;

            // Lookup whether or not the user exist
            $user = User::where('email',$email)->first();
            if ($user == null) {
                return response()->json([
                    'error' => $email . ' must first create an account before you can share a file with them.',
                ]);
            }

            // Lookup whether or not the file was already shared, if so skip
            $shared = FileShare::where('file_id',$file->id)->where('user_id',$user->id)->first();
            if (count($shared) == 1) continue;

            // $this is used to keep in scope of mail
            $this->user = $user;
            // Add a record that the file was shared
            $share = new FileShare;
            $share->file_id = $file->id;
            $share->user_id = $user->id;
            $share->save();

            // Send an email that a file has been shared
            $mail = Mail::send('emails.default', [
                'subject'=>'A file was shared with you at '.env('APP_DOMAIN'),
                'content' => strtok($this->user->name , " "). ', <br/><br/>'.Auth::user()->name.' has shared a file: <a href="'.url($this->file->source).'">"' . $this->file->name . '"</a> with you. This file is available under the My Files section of <a href="'.url('account').'">'.'your account.</a>',
            ], function ($mail) {
                if (env('APP_ENV') == 'local') {
                    $mail->from('support@nebulasuite.com', $this->org_name);
                    $mail->to($this->user->email, $this->user->name);
                    $mail->subject('A file was shared with you at '.env('APP_DOMAIN'));
                } else {
                    $mail->from('support@'.env('APP_DOMAIN'), $this->org_name);
                    $mail->to($this->user->email, $this->user->name);
                    $mail->subject('A file was shared with you at '.env('APP_DOMAIN'));
                }
            });
            if (!$mail) return response()->json(['error' => 'Failed to send mail to '.$email]);
        }

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $file->name . ' was shared with '.$user->name.'.';
        $feed_item->details = 'file/'.$file->id.'/edit';
        $feed_item->icon = getIconFromType($file->type);
        $feed_item->save();

        return response()->json([
            'success' => 'File successfully shared',
            'closeModal' => true,
        ]);
    }

    public function storeUpload(Request $request)
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        // Validate fields
        $validator = Validator::make($request->all(), [
            'file' => 'bail|required|mimes:doc,docx,pdf,csv,xls,txt,zip',
        ]);
        // Return validation error
        if ($validator->fails()) return Redirect::back()->withInput()->withErrors($validator);

        // Modified to storage directory
        // Create directory with user id
        $directory = Auth::id();
        $filename = time().'_'.$file->getClientOriginalName();
        File::MakeDirectory($upload_directory);
        // Store upload in directory
        Storage::put($directory.'/'.$filename,file_get_contents($request->file->getRealPath()));
        // Store upload record in db
        $upload = new Upload;
        $upload->user_id = Auth::id();
        $upload->name = $filename;
        $upload->source = 'uploads/user/'.$directory.'/'.$filename;
        $upload->size = filesize($request->file);
        $upload->type = 'file';
        $upload->save();

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = Auth::user()->name . ' uploaded "' . $filename.'"';
        $feed_item->details = 'file/'.$file->id.'/edit';
        $feed_item->icon = getIconFromType($file->type);
        $feed_item->save();

        // // Upload image
        // $image = $request->file('file');
        // $filename = time().'.'.$image->getClientOriginalName();
        // $filename = $image->getClientOriginalName();
        // $directory = 'uploads/user/'.Auth::id();
        // $upload_directory = public_path($directory);

        // $path = $upload_directory . '/' . $filename;
        // // Create directory at uploads/user/id if it does not exist
        // // if (!file_exists($upload_directory)) File::MakeDirectory($upload_directory);
        // // Store image
        // Image::make($image->getRealPath())->save($path);
        // // Store upload
        // $upload = new Upload;
        // $upload->user_id = Auth::id();
        // $upload->name = $filename;
        // $upload->source = $directory.'/'.$filename;
        // $upload->size = filesize($image);
        // $upload->type = 'file';
        // $upload->save();
        return Redirect::back();
    }


    /*
    |--------------------------------------------------------------------------
    | User and Account Methods
    |--------------------------------------------------------------------------
    */

    // Search users
    public function searchUsers(Request $request) 
    {
        $users = User::where('name', 'LIKE', '%'. $request->input('query') .'%')->where('email', 'LIKE', '%'. $request->input('query') .'%')->get();
        return view('user.list',[
            'users' => $users
        ]);
    }

    // Shows user account
    public function showAccount() {
        $user = Auth::user();
        // Check if there is a subscription in the session that needs to be saved to their account
        if (Session::get('subscription_id') != NULL) {
            // Add a record of the subscription so they can manage it from their account in the future
            $user_stripe_subscription = new UserStripeSubscription;
            $user_stripe_subscription->subscription_id = Session::get('subscription_id');
            $user_stripe_subscription->user_id = Auth::id();
            $user_stripe_subscription->description = Session::get('subscription_description');
            $user_stripe_subscription->answers = Session::get('answers');
            $user_stripe_subscription->questions = Session::get('questions');
            $user_stripe_subscription->save();
            // reset session
            session([
                'answers' => NULL,
                'questions' => NULL,
                'subscription_id' => NULL,
                'subscription_description' => NULL,
            ]);
        }
        return view('user.account',[
            'user' => $user,
        ]);
    }

    // Updates user account
    public function updateAuthorizedUser(Request $request) {
        // Validate fields
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required|max:255',
            'email' => 'bail|required|max:255',
        ]);
        // Return validation error
        if ($validator->fails()) return Redirect::back()->withInput()->withErrors($validator);
        // Update the database
        $account = Auth::user();
        if ($request->has('name')) $account->name = $request->input('name');
        if ($request->has('email')) $account->email = $request->input('email');
        if ($request->has('photo')) $account->photo = $request->input('photo');
        if ($request->has('password')) $account->password = bcrypt($request->input('password'));
        $account->save();
        // Send back with message
        return Redirect::back()->withInput()->withSuccess('Account successfully updated');
    }

    /*
    |--------------------------------------------------------------------------
    | Files module methods
    |--------------------------------------------------------------------------
    */


    public function showDashboardUncategorizedFiles()
    {
        $files = Upload::where('folder_id',NULL)->where('user_id',Auth::id())->orderBy('created_at','DESC')->paginate(25); // Uploads not inside a folder
        if (!Auth::user()->is_manager) $files = Upload::where('folder_id',NULL)->where('user_id',Auth::id())->orderBy('created_at','DESC')->paginate(25); // Uploads not inside a folder
        return view('file.dashboard.uncategorized',[
            'files' => $files,
        ]);
    }

    /**
     * Index of files page on the dashboard. Displays list items to go to the authorized users folders
     */
    public function showFiles()
    {
        $uncategorized_files = Upload::where('folder_id',NULL)->where('user_id',Auth::id())->orderBy('created_at','DESC')->paginate(25); // Uploads not inside a folder
        $folders = FileFolder::where('user_id',Auth::id())->orderBy('created_at','DESC')->get();
        return view('file.index',[
            'uncategorized_files' => $uncategorized_files,
            'folders' => $folders,
        ]);
    }

    /**
     * Shows uncategorized files that were shared with the authorized user. From the dashboard.
     */
    public function showDashboardUncategorizedSharedFiles()
    {
        // $files = Upload::where('folder_id',NULL)->where('user_id',Auth::id())->orderBy('created_at','DESC')->paginate(25); // Uploads not inside a folder
        // Files that have no folder_id and that have been shared with the user
        $user = User::findOrfail(Auth::id());
        // foreach ($user->file_shares as $file_shares) {
        //    echo $file_shares->file;
        // }
        $files = array();
        foreach($user->file_shares as $file_share) {
             $file = Upload::find($file_share->file_id);
             if ($file !== NULL) {
                 array_push($files,$file);
             }
        }
        return view('file.shared.uncategorized',[
            'files' => $files,
        ]);
    }

    /**
     * Index of shared files page on the dashboard. Displays list items to go to folders shared with them.
     */
    public function showDashboardSharedFiles()
    {
        $user = User::findOrfail(Auth::id());
        return view('file.dashboard.shared',[
            'user' => $user,
        ]);
    }

    public function showAccountSharedFiles()
    {
        $user = User::findOrfail(Auth::id());
        $files = array();
        foreach($user->file_shares as $file_share) {
             $file = Upload::find($file_share->file_id);
             if ($file !== NULL) {
                 array_push($files,$file);
             }
        }
        return view('file.account.shared',[
            'files' => $files,
        ]);
    }

    public function showAccountFiles()
    {
        $files = Upload::where('user_id',Auth::id())->orderBy('created_at','DESC')->paginate(25);
        return view('file.account.files',[
            'files' => $files,
        ]);
    }

    public function editFile($id)
    {
        $folders = FileFolder::all();
        $shared = FileShare::where('file_id',$id)->where('user_id',Auth::id())->count();
        $file = Upload::where('id',$id)->first();
        if ((Auth::user() !== null && Auth::user()->is_manager) || $shared) {
            return view('file.edit',[
                'folders' => $folders,
                'file' => $file,
            ]);
        } else {
            return 'Unauthorized';
        }
    }

    public function downloadFile($id)
    {
        $file = Upload::findOrFail($id);
        $shared = FileShare::where('file_id',$file->id)->where('user_id',Auth::id())->count();
        $folder_shared = FileFolderShare::where('folder_id',$file->folder_id)->where('user_id',Auth::id())->count();
        if ((Auth::user() !== null && Auth::user()->is_manager) || $shared || $folder_shared) {
            return response()->file(storage_path('app').'/'.$file->source);
            // return response()->download(storage_path('app').'/'.$file->source);
        } else {
            echo 'Unauthorized';
        }
    }

    public function deleteFile($id)
    {
        $file = Upload::findOrFail($id);

        // Delete the file
        Storage::delete($file->source);
        $file->delete();

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $file->name . ' was deleted';
        $feed_item->details = 'dashboard#files';
        $feed_item->icon = getIconFromType($file->type);
        $feed_item->save();

        // Return json response
        return response()->json([
            'success' => 'File removed.',
            'refresh' => true,
        ]);
    }

    public function updateFile(Request $request,$id)
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        $file = Upload::findOrFail($id);
        // Remove first 7 characters
        $file->name = $request->name;
        $file->folder_id = $request->folder_id;
        $file->save();
        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $file->name . ' was updated.';
        $feed_item->details = 'file/'.$file->id.'/edit';
        $feed_item->icon = getIconFromType($file->type);
        $feed_item->save();
        // Return json response
        return response()->json([
            'success' => 'Changes saved',
            'refreshDynamicView' => true,
        ]);
    }

    public function showFilesImport($folder_id = NULL)
    {
        // An optional folder id can be passed along
        if ($folder_id !== NULL) {
            return view('file.import',['folder_id'=>$folder_id]);
        } else {
            return view('file.import');
        }
    }

    public function importFiles(Request $request)
    {
        $files = $request->file;
        $directory = 'uploads/user/'.Auth::id();
        // Create storage directory in storage/app/user_id
        Storage::makeDirectory($directory);
        try {        
            foreach ($files as $key => $file) {
                $filename = time().'_'.$file->getClientOriginalName();
                Storage::put($directory.'/'.$filename,file_get_contents($file->getRealPath()));
                $upload = new Upload;
                $upload->user_id = Auth::id();
                if (isset($request->folder_id) && $request->folder_id !== '') {
                    $upload->folder_id = $request->folder_id; // Add folder id if set
                }
                $upload->name = substr($filename,11);
                $upload->source = $directory.'/'.$filename;
                $upload->size = filesize($file);
                $is_image = getimagesize($file);
                $extension = pathinfo($directory.'/'.$filename, PATHINFO_EXTENSION);
                if ($is_image || $extension == 'svg') {
                    $upload->type = 'image';
                } else {
                    $upload->type = $extension;
                }
                $upload->save();
            }
        } catch(Exception $e){
            return Redirect::back()->withInput()->withErrors($e->getMessage());
        }

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = Auth::user()->name .' uploaded '.count($files).' new files.';
        if(count($files) == 1) $feed_item->text = Auth::user()->name .' uploaded '.count($files).' new file.';
        $feed_item->href = 'dashboard#files';
        $feed_item->icon = 'icon-circle-upload';
        $feed_item->save();

        return Redirect::back()->withSuccess('Success! Refresh the page to see new uploads reflected.');
    }

    public function createFilesFolder()
    {
        return view('file.folder.create');
    }

    public function showShareFilesFolder($id)
    {
        $folder = FileFolder::findOrFail($id);
        return view('file.folder.share',[
            'folder' => $folder,
        ]);
    }


    public function showUsersFiles($id)
    {
        $user = User::findOrfail($id);
        if (!Auth::user()->is_manager) return redirect()->to('/')->withErrors('You must be logged in as a manager to do that.');
        return view('file.dashboard.user.files',[
            'user' => $user,
        ]);
    }


    public function showUsersFileFolders()
    {
        $users = User::all();
        if (Auth::user()->is_manager !== 1) return redirect()->to('/')->withErrors('You must be logged in as a manager to do that.');
        return view('file.dashboard.users',[
            'users' => $users,
        ]);
    }


    public function showFilesFolder($id)
    {
        $folder = FileFolder::findOrfail($id);
        // Make sure the folder is shared with the user or owned by user
        if ($folder->user_id == Auth::id() || $folder->user_id == NULL) {        
            return view('file.folder.index',[
                'folder' => $folder,
            ]);
        } else {
            // Make sure the folder was shared with the user
            $shared = FileFolderShare::where('folder_id',$folder->id)->where('user_id',Auth::id())->get();
            if (count($shared) > 0) {
                return view('file.folder.index',[
                    'folder' => $folder,
                ]);
            } else {
                return redirect()->back()->withErrors('You do not have permission to view that folder.');
            }
        }
    }

    public function showFilesFolderFiles($id)
    {
        $folder = FileFolder::findOrFail($id);
        // Make sure the folder is shared with the user or owned by user
        if ($folder->user_id == Auth::id() || $folder->user_id == NULL) {        
            return view('file.folder.files',[
                'folder' => $folder,
            ]);
        } else {
            // Make sure the folder was shared with the user
            $shared = FileFolderShare::where('folder_id',$folder->id)->where('user_id',Auth::id())->get();
            if (count($shared) > 0) {
                return view('file.folder.files',[
                    'folder' => $folder,
                ]);
            } else {
                return redirect()->to('404')->withErrors('You do not have permission to view that folder.');
            }
        }
    }


    public function storeFilesFolder(Request $request)
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        // Require fields
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required',
        ]);

        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        // Create folder
        try {
            $folder = new FileFolder;
            $folder->name = $request->name;
            if (Auth::id() !== NULL) {
                $folder->user_id = Auth::id();
            }
            $folder->name = $request->name;
            $folder->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = Auth::user()->name .' added a file folder called '.$folder->name.'.';
        $feed_item->href = 'dashboard#files';
        $feed_item->icon = 'icon-circle-upload';
        $feed_item->save();

        // Return json response
        return response()->json([
            'success' => 'Successfully added',
            'refresh' => true,
            'closeModal' => true,
        ]);
    }
    /*
    |--------------------------------------------------------------------------
    | Billing methods
    |--------------------------------------------------------------------------
    */

    public function editSubscription()
    {
        return view('subscription.edit');
    }

    // Update the default subscription for the user's Stripe ID
    public function updateSubscription(Request $request) {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        // $user_subscriptions = Subscription::where('user_id',Auth::id())->get();
        // If user has subscriptions
        // if (count($user_subscriptions) > 0) {
        //     // Cancel
        //     foreach ($user_subscriptions as $subscription) {
        //         $subscription->cancel();
        //     }
        //     // $stripe_id = Auth::user()->subscriptions()->first()->stripe_id;
        //     // $subscription = \Stripe\Subscription::retrieve($stripe_id);
        //     // if (count($subscription) !== 0) $subscription->cancel();
        // }
        // Validate fields
        $validator = Validator::make($request->all(), [
            'stripeToken' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return Redirect::back()->withInput()->withErrors($validator);
        try {
            $user = Auth::user();

            $total_managers = User::where('is_manager','1')->count();
            $total_admins = User::where('is_admin','1')->count();
            $total_bytes = Upload::sum('size');

            // Calculate total managers for each organization
            // $quantity = 0;
            // foreach ($user->organizations as $organization)
            // {
            //     $quantity += count($organization->managers);
            // }

            // If they are not subscribed, create a subscription
            // if (!$user->subscribed('main')) 
            // {
            //     $user->newSubscription('main', 'primary')->create($request->stripeToken, [
            //         'email' => $user->email,
            //         'description' => $user->name,
            //         'quantity' => $quantity,
            //     ]);
            // } 
            // else {
                // // Otherwise update their card
                // $user->updateCard($request->stripeToken);
            // }
            if ($user->hasCardOnFile()) {
                // Otherwise update their card
                $user->updateCard($request->stripeToken);
            } else {                
                $user->createAsStripeCustomer($request->stripeToken,[
                    'description' => $user->name,
                    'email' => $user->email,
                ]);
            }

            // If they are not subscribed, create a subscription
            // if (!$user->subscribed('main')) $user->newSubscription('main', 'primary')->create($request->stripeToken, [
            //     'email' => $user->email,
            //     'description' => $user->name,
            //     'quantity' => $quantity,
            // ]);
        } catch(Exception $e){
            return Redirect::back()->withInput()->withErrors($e->getMessage());
        }
        return Redirect::back()->withInput()->withSuccess('Subscription updated');

        // Is user already a Stripe customer?
        // if (!isset($user->stripe_id)) {
        //     // Create customer
        //     $user->createAsStripeCustomer($request->stripeToken,[
        //         'email' => Auth::user()->email,
        //         'description' => Auth::user()->name,
        //     ]);
        // }
        // // Update card
        // $user->updateCard($request->stripeToken,[
        //     'name' => $user->name, 
        // ]);
        // $user->subscription('primary','monthly')->quantity(5)->create($stripeToken);

// NOW retrieve subscriptions from stripe


        // Create a new subscription on the primary plan monthly
        // $user->newSubscription('primary', 'monthly');
        // // Subscribe customer to primary payment plan
        // Auth::user()->newSubscription('primary','monthly')->create(array("plan" => "primary", "quantity" => "120"));
        return view('card.edit');
    }


    // public function createSubscription(Request $request) {
    //     // Validate fields
    //     $validator = Validator::make($request->all(), [
    //         'stripeToken' => 'bail|required',
    //         'cardholder' => 'bail|required|max:255',
    //     ]);
    //     // // Return validation error
    //     // if ($validator->fails()) return Redirect::back()->withInput()->withErrors($validator);
    // }

    // public function createSubscription(Request $request) {
    // }

    /*
    |--------------------------------------------------------------------------
    | Reservations Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Show reservations index which shows a view listing reservation schedules
     */
    public function showReservationsIndex()
    { 
        $schedules = ReservationSchedule::orderBy('created_at','DESC')->get();
        return view('reservations.index',['schedules'=>$schedules]);
    }

    /**
     * Show reservations create form view
     */
    public function createReservationSchedule()
    { 
        return view('reservations.schedule.create');
    }

    /**
     * Store reservation schedule in the database
     */
    public function storeReservationSchedule(Request $request)
    {
        // Require fields
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);
        // Make sure schedule name isn't already taken
        $schedule_name_check = ReservationSchedule::where('slug',create_slug($request->name))->count();
        if ($schedule_name_check) return response()->json([
            'error' => 'The name "'.$request->name.'" is already taken. Please choose another.',
        ]);
        try {
            $schedule = new ReservationSchedule;
            $schedule->name = $request->name;
            $schedule->slug = create_slug($request->name);
            // If both start time and and time are set
            if (strlen($request->available_start_time) > 0 && strlen($request->available_end_time) > 0) {
                if (date('H:i:s',strtotime($request->available_end_time)) > date('H:i:s',strtotime($request->available_start_time))) {
                    // A valid time range was set
                    $schedule->available_start_time = date('H:i:s',strtotime($request->available_start_time));
                    $schedule->available_end_time = date('H:i:s',strtotime($request->available_end_time));
                } else {
                    return response()->json([
                        'error' => 'Please select a valid available time range.',
                    ]);
                }
            }
            $schedule->user_id = Auth::id(); // Store who the schedule was created by just in case we need to query this later
            $schedule->public = ($request->public == 'on') ? 1 : 0;
            $schedule->mondays = ($request->mondays == 'on') ? 1 : 0;
            $schedule->tuesdays = ($request->tuesdays == 'on') ? 1 : 0;
            $schedule->wednesdays = ($request->wednesdays == 'on') ? 1 : 0;
            $schedule->thursdays = ($request->thursdays == 'on') ? 1 : 0;
            $schedule->fridays = ($request->fridays == 'on') ? 1 : 0;
            $schedule->saturdays = ($request->saturdays == 'on') ? 1 : 0;
            $schedule->sundays = ($request->sundays == 'on') ? 1 : 0;
            $schedule->require_login = ($request->require_login == 'on') ? 1 : 0;
            $schedule->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        return response()->json([
            'success' => 'Reservation schedule successfully created!',
            'refresh' => true,
        ]);
    }

    /**
     * Show reservation schedule
     */
    public function showReservationSchedule($id)
    {
        $schedule = ReservationSchedule::findOrfail($id);
        if (!isset($_GET['date'])) {
            $date = date('m/d/y'); // Default to today
        } else {
            $date = $_GET['date'];
        }
        if (isset($_GET['reservable_id']) && $_GET['reservable_id'] !== 'All') {
            $reservable_id = $_GET['reservable_id'];
        } else {
            $reservable_id = NULL;
        }
        $reservables = Reservable::where('reservation_schedule_id',$id)->get(); // Get reservable items
        $reservations = Reservation::where('reservation_schedule_id',$id)->where('date',date('Y-m-d',strtotime($date)))->get(); // Get reservations
        if ($reservable_id !== NULL) $reservations = Reservation::where('reservation_schedule_id',$id)->where('date',date('Y-m-d',strtotime($date)))->where('reservable_id','=',$reservable_id)->get(); // Get reservations for given reservable id
        return view('reservations.schedule.index',[
            'schedule'=>$schedule,
            'date'=>$date,
            'reservables'=>$reservables,
            'reservations'=>$reservations,
            'reservable_id'=>$reservable_id,
        ]);
    }

    /**
     * Show reservation schedule
     */
    public function showPublicReservationSchedule($slug)
    {
        $schedule = ReservationSchedule::where('slug',$slug)->first();
        if (count($schedule) == 0) abort(404);
        if (!$schedule->public) return redirect()->to('/')->withErrors('The schedule you are trying to view is set to private and must be viewed from the dashboard.');
        if (!isset($_GET['date'])) {
            $date = date('m/d/y'); // Default to today
        } else {
            $date = $_GET['date'];
        }
        if (isset($_GET['reservable_id']) && $_GET['reservable_id'] !== 'All') {
            $reservable_id = $_GET['reservable_id'];
        } else {
            $reservable_id = NULL;
        }
        $reservables = Reservable::where('reservation_schedule_id',$schedule->id)->get(); // Get reservable items
        $reservations = Reservation::where('reservation_schedule_id',$schedule->id)->where('date',date('Y-m-d',strtotime($date)))->get(); // Get reservations
        if ($reservable_id !== NULL) $reservations = Reservation::where('reservation_schedule_id',$schedule->id)->where('date',date('Y-m-d',strtotime($date)))->where('reservable_id','=',$reservable_id)->get(); // Get reservations for given reservable id
        return view('reservations.schedule.public',[
            'schedule'=>$schedule,
            'date'=>$date,
            'reservables'=>$reservables,
            'reservations'=>$reservations,
            'reservable_id'=>$reservable_id,
        ]);
    }


    /**
     * Updates a reservation schedule of given id.
     * 
     */
    public function editReservationSchedule($id)
    {
        $schedule = ReservationSchedule::findOrfail($id);
        return view('reservations.schedule.edit',['schedule'=>$schedule]);
    }

    /**
     * Show reservables for given schedule.
     */
    public function showReservables($id)
    {
        $schedule = ReservationSchedule::findOrfail($id);
        $reservables = Reservable::orderBy('created_at','DESC')->where('reservation_schedule_id',$id)->paginate(25);
        return view('reservations.reservable.index',['schedule'=>$schedule,'reservables'=>$reservables]);
    }


    /**
     * Show reservations for given schedule.
     */
    public function showReservations($id)
    {
        $schedule = ReservationSchedule::findOrfail($id);
        $reservations = Reservation::where('reservation_schedule_id',$id)->get(); // Get reservations
        return view('reservations.reservation.index',['schedule'=>$schedule,'reservations'=>$reservations]);
    }

    /**
     * Updates a reservation of given id.
     * 
     */
    public function editReservation($id)
    {
        $reservation = Reservation::findOrfail($id);
        return view('reservations.reservation.edit',['reservation'=>$reservation]);
    }

    /**
     * Delete reservation of given id.
     */
    public function deleteReservation(Request $request, $id)
    {
        try {
            $reservation = Reservation::findOrfail($id);
            $reservation_schedule_id = $reservation->schedule->id;
            $reservation->delete();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        return response()->json([
            'closeDetails' => true,
            'loadView' => 'reservations/schedule/'.$reservation_schedule_id,
        ]);
    }


    /**
     * Update reservable of given id.
     */
    public function updateReservable(Request $request, $id)
    {
        // Require fields
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);
        // Create example record
        try {
            $reservable = Reservable::findOrFail($id);
            $reservable->name = $request->name;
            $reservable->details = $request->details;
            $reservable->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        // Return json response
        return response()->json([
            'success' => 'Successfully updated',
            'closeModal' => true,
            'loadView' => 'reservations/schedule/'.$reservable->reservation_schedule_id.'/reservables',
        ]); 
    }

    /**
     * Update reservation schedule of given id.
     */
    public function updateReservationSchedule(Request $request, $id)
    {
        // Require fields
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);
        // Make sure schedule name isn't already taken
        $schedule_name_check = ReservationSchedule::where('id','!=',$id)->where('slug',create_slug($request->name))->count();
        if ($schedule_name_check) return response()->json([
            'error' => 'The name "'.$request->name.'" is already taken. Please choose another.',
        ]);
        try {
            $schedule = ReservationSchedule::findOrfail($id);
            $schedule->name = $request->name;
            $schedule->slug = create_slug($request->name);
            // If both start time and and time are set
            if (strlen($request->available_start_time) > 0 && strlen($request->available_end_time) > 0) {
                if (date('H:i:s',strtotime($request->available_end_time)) > date('H:i:s',strtotime($request->available_start_time))) {
                    // A valid time range was set
                    $schedule->available_start_time = date('H:i:s',strtotime($request->available_start_time));
                    $schedule->available_end_time = date('H:i:s',strtotime($request->available_end_time));
                } else {
                    return response()->json([
                        'error' => 'Please select a valid available time range.',
                    ]);
                }
            } else {
                $schedule->available_start_time = NULL;
                $schedule->available_end_time = NULL;
            }
            $schedule->user_id = Auth::id(); // Store who the schedule was created by just in case we need to query this later
            $schedule->public = ($request->public == 'on') ? 1 : 0;
            $schedule->mondays = ($request->mondays == 'on') ? 1 : 0;
            $schedule->tuesdays = ($request->tuesdays == 'on') ? 1 : 0;
            $schedule->wednesdays = ($request->wednesdays == 'on') ? 1 : 0;
            $schedule->thursdays = ($request->thursdays == 'on') ? 1 : 0;
            $schedule->fridays = ($request->fridays == 'on') ? 1 : 0;
            $schedule->saturdays = ($request->saturdays == 'on') ? 1 : 0;
            $schedule->sundays = ($request->sundays == 'on') ? 1 : 0;
            $schedule->require_login = ($request->require_login == 'on') ? 1 : 0;
            $schedule->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        return response()->json([
            'success' => 'Successfully updated!',
            'loadView' => 'reservations/schedule/'.$schedule->id,
            'closeModal' => true,
        ]);
    }

    /**
     * Update reservation of given id.
     */
    public function updateReservation(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required',
            'email' => 'bail|required|email',
            'start_time' => 'bail|required',
            'date' => 'bail|required',
            'end_time' => 'bail|required',
            'reservable_id' => 'bail|required',
        ]);
        if ($validator->fails()) {
            if (!isset($request->reservable_id)) {
                return response()->json([
                    'error' => 'What do you want to reserve? Please make a selection.',
                ]);
            }
            return response()->json([
                'error' => $validator->errors()->first(),
            ]);
        }
        $schedule = ReservationSchedule::findOrfail($id);
        $day = strtolower(date('l',strtotime($request->date))).'s';
        if ($schedule->$day == 0) {
            // Schedule is not allowed on Mondays for example
            return response()->json([
                'error' => date('l',strtotime($request->date)).'\'s are not open for reservations. Please choose another day.',
            ]);
        }
        // If available times are restricted on the schedule then make sure the selected time falls within the avaible times range
        if ($schedule->available_start_time !== NULL && $schedule->available_end_time !== NULL) {
            $schedule_start_unix = strtotime(date("h:i A", strtotime($schedule->available_start_time)));
            $schedule_end_unix = strtotime(date("h:i A", strtotime($schedule->available_end_time)));
            if ((strtotime($request->start_time) < $schedule_start_unix) || (strtotime($request->end_time) > $schedule_end_unix)) {
                return response()->json([
                    'error' => 'The available reservation hours are '.date('g:i A', strtotime($schedule->available_start_time)).'-'.date('g:i A', strtotime($schedule->available_end_time)).'. Please choose a time within this time range.',
                ]);
            }
        }
        // Make sure a valid timespan is set
        if (date('H:i:s',strtotime($request->end_time)) > date('H:i:s',strtotime($request->start_time))) {
            // A valid time range was set
            $start_time = date('H:i:s',strtotime($request->start_time));
            $end_time = date('H:i:s',strtotime($request->end_time));
        } else {
            return response()->json([
                'error' => 'Please select a valid time range for the selected day.',
            ]);
        }
        try {
            $reservation = Reservation::findOrfail($id);
            $reservation->name = $request->name;
            $reservation->email = $request->email;
            $reservation->purpose = $request->purpose;
            $reservation->start_time = date('H:i:s',strtotime($request->start_time));;
            $reservation->end_time = date('H:i:s',strtotime($request->end_time));;
            $reservation->date = date('Y-m-d',strtotime($request->date));
            $reservation->purpose = $request->purpose;
            $reservation->reservable_id = $request->reservable_id;
            $reservation->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        return response()->json([
            'success' => 'Reservation updated.',
            'loadView' => 'reservations/schedule/'.$reservation->schedule->id.'?date='.$request->date.'&reservable_id='.$request->reservable_id,
        ]);
    }

    /**
     * Store reservation in the database.
     */
    public function storeReservation(Request $request, $reservation_schedule_id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required',
            'email' => 'bail|required|email',
            'start_time' => 'bail|required',
            'date' => 'bail|required|date',
            'end_time' => 'bail|required',
            'reservable_id' => 'bail|required',
        ]);
        if ($validator->fails()) {
            if (!isset($request->reservable_id)) {
                return response()->json([
                    'error' => 'What would you like to reserve? Please make a selection.',
                ]);
            }
            return response()->json([
                'error' => $validator->errors()->first(),
            ]);
        }
        $schedule = ReservationSchedule::findOrfail($reservation_schedule_id);
        $day = strtolower(date('l',strtotime($request->date))).'s';
        if ($schedule->$day == 0) {
            // Schedule is not allowed on Mondays for example
            return response()->json([
                'error' => date('l',strtotime($request->date)).'\'s are not open for reservations. Please choose another day.',
            ]);
        }
        // If available times are restricted on the schedule then make sure the selected time falls within the avaible times range
        if ($schedule->available_start_time !== NULL && $schedule->available_end_time !== NULL) {
            $schedule_start_unix = strtotime(date("h:i A", strtotime($schedule->available_start_time)));
            $schedule_end_unix = strtotime(date("h:i A", strtotime($schedule->available_end_time)));
            if ((strtotime($request->start_time) < $schedule_start_unix) || (strtotime($request->end_time) > $schedule_end_unix)) {
                return response()->json([
                    'error' => 'The available reservation hours are '.date('g:i A', strtotime($schedule->available_start_time)).'-'.date('g:i A', strtotime($schedule->available_end_time)).'. Please choose a time within this time range.',
                ]);
            }
        }
        // Make sure a valid timespan is set
        if (date('H:i:s',strtotime($request->end_time)) > date('H:i:s',strtotime($request->start_time))) {
            // A valid time range was set
            $start_time = date('H:i:s',strtotime($request->start_time));
            $end_time = date('H:i:s',strtotime($request->end_time));
        } else {
            return response()->json([
                'error' => 'Please select a valid time range for the selected day.',
            ]);
        }
        try {
            $reservations = Reservation::where('reservable_id',$request->reservable_id)->where('reservation_schedule_id',$reservation_schedule_id)->where('date',date('Y-m-d',strtotime($request->date)))->get();
            foreach ($reservations as $reservation) {      
                $reservation_start_unix = strtotime(date("h:i A", strtotime($reservation->start_time)));
                $reservation_end_unix = strtotime(date("h:i A", strtotime($reservation->end_time)));
                if (($reservation_end_unix >= strtotime($start_time)) && (strtotime($end_time) >= $reservation_start_unix)) {
                    return response()->json([
                        'error' => 'There\'s already a reservation at that time - please choose another time.',
                    ]);
                }
            }
            $reservation = new Reservation;
            if (Auth::user() !== NULL) $reservation->user_id = $request->name;
            $reservation->name = $request->name;
            $reservation->email = $request->email;
            $reservation->purpose = $request->purpose;
            $reservation->start_time = $start_time;
            $reservation->end_time = $end_time;
            $reservation->date = date('Y-m-d',strtotime($request->date));
            $reservation->purpose = $request->purpose;
            $reservation->reservable_id = $request->reservable_id;
            $reservation->reservation_schedule_id = $reservation_schedule_id;
            $reservation->save();
            // Add feed item
            $feed_item = new FeedItem;
            $feed_item->text = $reservation->name .' reserved '.$reservation->reservable->name .' from '.$request->start_time.'-'.$request->end_time. ' on ' . $request->date;
            $feed_item->details = 'reservations/reservation/'.$reservation->id.'/edit';
            $feed_item->icon = 'icon-circle-calendar';
            $feed_item->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        // If $request->public is set refresh insted of loading the admin view
        if ($request->public) {
            return response()->json([
                'success' => 'Reservation confirmed!',
                'refresh' => true,
            ]);
        }
        return response()->json([
            'success' => 'Reservation confirmed!',
            'closeModal' => true,
            'loadView' => 'reservations/schedule/'.$reservation_schedule_id.'?date='.$request->date.'&reservable_id='.$request->reservable_id,
        ]);
    }

    public function deleteReservationSchedule($id)
    {
        $schedule = ReservationSchedule::findOrFail($id);
        // $reservations = Reservation::where('reservation_schedule_id',$id)->get();
        // Delete reservations
        foreach ($schedule->reservations as $reservation) {
            $reservation = Reservation::find($reservation->id);
            if ($reservation !== NULL) $reservation->delete();
        }
        // Delete reservables
        foreach ($schedule->reservables as $reservable) {
            $reservable = Reservable::find($reservable->id);
            if ($reservable !== NULL) $reservable->delete();
        }

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $schedule->name.' was removed from reservation schedules.';
        $feed_item->icon = 'icon-circle-calendar';
        $feed_item->href = 'dashboard#reservations';
        $feed_item->save();

        // Finally delete the schedule itself
        $schedule->delete();

        return response()->json([
            'success' => 'The schedule was removed.',
            'refreshDynamicView' => true,
            'closeModal' => true,
        ]);
    }


    /**
     * Show create reservation form
     */
    public function createReservation($id)
    {
        $schedule = ReservationSchedule::findOrfail($id);
        if (isset($_GET['date'])) {
            $date = $_GET['date'];
        } else {
            $date = NULL;
        }
        if (isset($_GET['reservable_id'])) {
            $reservable_id = $_GET['reservable_id'];
        } else {
            $reservable_id = NULL;
        }
        if (isset($_GET['public'])) {
            $public = $_GET['public'];
        } else {
            $public = NULL;
        }
        // If login is required redirect them to the route that requires authentication
        if ($schedule->require_login) {
            if (Auth::user() == NULL) return redirect()->to('reservations/schedule/'.$id.'/reservation/create/auth?date='.$date.'&reservable_id='.$reservable_id.'&public='.$public);
        }
        return view('reservations.reservation.create',['schedule'=>$schedule,'date'=>$date,'reservable_id'=>$reservable_id,'public'=>$public]);
    }

    /**
     * Edit a reservable of given id.
     * 
     */
    public function editReservable($id)
    {
        $reservable = Reservable::findOrfail($id);
        return view('reservations.reservable.edit',['reservable'=>$reservable]);
    }

    /**
     * Show form to create a reservable
     */
    public function createReservable($id)
    {
        $schedule = ReservationSchedule::findOrfail($id);
        if (isset($_GET['date'])) {
            $date = $_GET['date'];
        } else {
            $date = NULL;
        }
        // if (isset($_GET['reservable_id'])) {
        //     $reservable_id = $_GET['reservable_id'];
        // } else {
        //     $reservable_id = NULL;
        // }
        return view('reservations.reservable.create',['schedule'=>$schedule,'date'=>$date]);
    }

    /**
     * Store reservable
     *
     * @param request $request
     * @return redirect
     *
     */
    public function storeReservable(Request $request,$reservation_schedule_id) 
    {
        // Require fields
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);
        // Create example record
        try {
            $reservable = new Reservable;
            $reservable->name = $request->name;
            $reservable->details = $request->details;
            $reservable->reservation_schedule_id = $request->reservation_schedule_id;
            $reservable->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        // Return json response
        return response()->json([
            'success' => 'Successfully added',
            'closeModal' => true,
            'loadView' => 'reservations/schedule/'.$reservation_schedule_id.'/reservables',
        ]); 
    }

    /*
    |--------------------------------------------------------------------------
    | Form Methods
    |--------------------------------------------------------------------------
    */

    public function showForms()
    { 
        // if (url()->previous() !== (env('APP_URL').'/dashboard')) die('dea');
        $forms = Form::orderBy('created_at','DESC')->paginate(25);
        return view('form.index',['forms'=>$forms]);
    }

    public function deleteForm($id)
    { 
        $form = Form::findOrfail($id)->delete();
        // Return json response
        return response()->json([
            'refreshDynamicView' => true,
        ]); 
    }

    public function showFormResponse($id)
    {
        $response = FormResponse::findOrFail($id);
        return view('form.response.view',['response'=>$response,'questions'=>json_decode($response->questions),'answers'=>json_decode($response->answers)]);
    }

    public function deleteFormResponse($id)
    {
        $response = FormResponse::findOrFail($id);
        $response->delete();
        // Return json response
        return response()->json([
            'refreshDynamicView' => true,
            'closeModal' => true,
        ]); 
    }

    public function showFormResponses($form_slug,$form_version)
    {
        $form = Form::where('slug',$form_slug)->firstOrFail();
        $responses = FormResponse::where('form_id',$form->id)->where('form_version',$form_version)->orderBy('created_at','DESC')->paginate(25);
        return view('form.response.responses',['responses'=>$responses,'form'=>$form,'form_version'=>$form_version]);
    }

    // Export spreadsheet
    public function exportFormResponses($form_id,$form_version)
    {
        $this->form = Form::findOrfail($form_id);
        $this->responses = FormResponse::where('form_id',$form_id)->where('form_version',$form_version)->get();

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = Auth::user()->name .' downloaded ' . $this->form->name.' responses.';
        $feed_item->href = 'dashboard#form/'.$this->form->slug.'/'.$form_version.'/responses';
        $feed_item->icon = 'icon-circle-form';
        $feed_item->save();

        Excel::create($this->form->name, function($excel) {
            // Set the title
            $excel->setTitle($this->form->name);
            $excel->sheet($this->form->name, function($sheet) {
                $sheet->setOrientation('landscape');
                // Create columns using the questions from the first response (they are the same for each response)
                $columns = json_decode($this->responses->first()->questions);
                $sheet->rows([
                    $columns, // add columns to sheet
                ]);
                foreach ($this->responses as $reponse) {
                    $row_data = json_decode($reponse->answers);
                    $sheet->rows([
                        $row_data
                    ]);
                }
                // $rows_data = json_decode($rows_data);
            });
        })->export('xls');
    }

    public function showFormResponseSets()
    {
        $forms = Form::orderBy('created_at','DESC')->get();
        $response_sets = array();
        foreach ($forms as $form) {
            // Loop through version number of times to create array of response sets
            for ($i = 0; $i < $form->version; $i++) {
                // If there is more than one response for each form version that has existed, loop through each that has at least one response to add to the array
                if (FormResponse::where('form_version',$i + 1)->where('form_id',$form->id)->count() > 0) {                
                    $form_array = array();
                    $form_array['version'] = $i + 1;
                    $form_array['responses'] = FormResponse::where('form_version',$i + 1)->where('form_id',$form->id)->count();
                    $form_array['form_name'] = $form->name;
                    $form_array['form_slug'] = $form->slug;
                    array_push($response_sets,$form_array);
                }
            }
        }
        // dd($response_sets);
        return view('form.response.index',['response_sets'=>array_reverse($response_sets)]);
    }

    public function createForm()
    {
        return view('form.create');
    }

    public function createField($id)
    {
        $form = Form::findOrFail($id);
        return view('form.field.create',['form'=>$form]);
    }

    public function generateField(Input $input)
    {
        return view('form.field.generate',[
            'type'=>$input->get('type'),
            'label'=>$input->get('label'),
            'placeholder'=>$input->get('placeholder'),
            'hint'=>$input->get('hint'),
        ]);
    }

    // public function storeField(Request $request, $id)
    // {
    //     if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');

    //     // Require fields
    //     $validator = Validator::make($request->all(), [
    //         'label' => 'bail|required',
    //     ]);

    //     $form = Form::findOrFail($id);

    //     // Return validation error
    //     if ($validator->fails()) return response()->json([
    //         'error' => $validator->errors()->first(),
    //     ]);

    //     try {
    //         $form_item = new FormItem;
    //         $form_item->label = $request->label;
    //         $form_item->type = $request->type;
    //         $form_item->hint = $request->hint;
    //         $form_item->options = isset($request->options) ? $request->options : '';
    //         $form_item->form_id = $form->id;
    //         $form_item->save();
    //     } catch(Exception $e){
    //         return response()->json([
    //             'error' => $e->getMessage(),
    //         ]);
    //     }

    //     // Return json response
    //     return response()->json([
    //         'success' => 'Field added.',
    //         'refreshDetailsView' => true,
    //         'closeModal' => true,
    //     ]); 
    // }

    public function storeForm(Request $request)
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');

        // Require fields
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required',
        ]);

        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        try {
            // Check if name is already takem
            $form_exists = Form::where('name',$request->name)->first();
            if ($form_exists !== null) return response()->json([
                'error' => 'A form with that name already exists. Please choose another name.',
            ]);
            $form = new Form;
            $form->name = $request->name;
            $form->slug = create_slug($request->name);
            $form->version = 0; // First set to zero since it has no form items yet and will be incremented to 1 on first save
            $form->save();
            $recipient = new FormAlert;
            $recipient->form_id = $form->id;
            $recipient->email = Auth::user()->email;
            $recipient->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Return json response
        return response()->json([
            'success' => 'Successfully added',
            // 'refreshDynamicView' => true,
            'loadDetailsView' => 'form/'.$form->id.'/edit',
            'closeModal' => true,
        ]); 
    }

    // public function storeField(Request $request)
    // {
    //     // Require fields
    //     $validator = Validator::make($request->all(), [
    //         'label' => 'bail|required',
    //         'type' => 'bail|required',
    //     ]);

    //     // Return validation error
    //     if ($validator->fails()) return response()->json([
    //         'error' => $validator->errors()->first(),
    //     ]);

    //     try {
    //         $form_item = new Form;
    //         $form_item->label = $request->label;
    //         $form_item->label = $request->label;
    //         $form_item->save();
    //     } catch(Exception $e){
    //         return response()->json([
    //             'error' => $e->getMessage(),
    //         ]);
    //     }

    //     // Return json response
    //     return response()->json([
    //         'success' => 'Successfully added',
    //         'refreshDetailsView' => true,
    //         'closeModal' => true,
    //     ]);
    // }

    public function editForm($id)
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        $form = Form::findOrFail($id);
        return view('form.edit',['form'=>$form]);
    }

    public function showFormPage(Request $request, $slug) 
    {
        $form = Form::where('slug',$slug)->firstOrFail();
        return view('form.view',['form' => $form]);
    }

    public function updateForm(Request $request, $id) 
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        try {
            $form = Form::findOrFail($id);
            // Check if name is already takem
            $form_exists = Form::where('name',$request->name)->first();
            if ($form_exists !== null && $form_exists->id !== $form->id) return response()->json([
                'error' => 'A form with that name already exists. Please choose another name.',
            ]);
            if (isset($request->name) && $request->name !== '') {            
                $form->name = $request->name;
                $form->slug = create_slug($request->name);
                $form->save();
            }

            // See if there are any form responses for the current version
            $form_version_responses = FormResponse::where('form_version',$form->version)->get();
            if (count($form_version_responses) > 0 || $form->version == 0) {
                $form->version = $form->version + 1;
                $form->save();
            }

            // First delete the old form items
            $form_items = FormItem::where('form_id',$id)->get();
            foreach ($form_items as $form_item) {
                $form_item = FormItem::findOrFail($form_item->id);
                $form_item->delete();
            }
            if (isset($_POST['field_type'])) {
                foreach ($_POST['field_type'] as $key => $type) {
                    $field = new FormItem;
                    $field->order = $key;
                    $field->form_id = $id;
                    if (isset($_POST['field_label'][$key]))         $field->label = $_POST['field_label'][$key];
                    if (isset($_POST['field_hint'][$key]))          $field->hint = $_POST['field_hint'][$key];
                    if (isset($_POST['field_placeholder'][$key]))   $field->placeholder = $_POST['field_placeholder'][$key];
                    if (isset($_POST['field_type'][$key]))          $field->type = $_POST['field_type'][$key];
                    if (isset($_POST['field_options'][$key]))       $field->options = $_POST['field_options'][$key];
                    $field->save();
                }
            }

        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Return json response
        return response()->json([
            'success' => 'Successfully updated',
            'refreshDetailsView' => true,
        ]); 
    }

    public function updateFormAlerts(Request $request, $form_id) 
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        $form = Form::findOrFail($form_id);
        foreach($form->recipients as $old_recipient) {
            // Delete old recipients
            $old_recipient = FormAlert::where('form_id',$old_recipient->form_id)->first();
            if ($old_recipient !== NULL) $old_recipient->delete();
        }
        // Add new recipients
        $emails = explode(',', $request->emails);
        if (count($emails) > 0 && strlen($request->emails) > 1) {        
            // Validate input
            foreach ($emails as $email) {
                $email = preg_replace('/\s+/', '', $email); // Remove white spaces
                if (!filter_var( $email ,FILTER_VALIDATE_EMAIL )) {
                    return response()->json([
                        'error' => 'One or more of the emails you entered were not valid.',
                    ]);
                }
            }
            foreach ($emails as $email) {
                $email = preg_replace('/\s+/', '', $email); // Remove white spaces
                // Add new recipient
                $recipient = new FormAlert;
                $recipient->form_id = $form_id;
                $recipient->email = $email;
                $recipient->save();
            }
        }
        return response()->json([
            'success' => 'Alert settings successfully updated',
            'closeModal' => true,
            'refreshDetailsView' => true,
        ]);
    }

    public function showFormAlerts($form_id) 
    {
        $form = Form::findOrFail($form_id);
        return view('form.response.alerts',[
            'form' => $form,
        ]);
    }

    public function storeFormResponse(Request $request, $slug) 
    {
        if (!isset($_POST['field'])) return response()->json([
            'success' => 'There are no fields on this form.',
        ]); 
        $form = Form::where('slug',$slug)->firstOrFail();
        $questions = FormItem::where('form_id',$form->id)->get();
        // // dd($questions);
        $answers = array();
        // // dd($_POST['field']);
        // Get the answer for each question posted
        foreach ($questions as $key => $question) {
            // $answer = $_POST['field'][$question->id];
            if (isset($_POST['field'][$question->id])) {
                array_push($answers, $_POST['field'][$question->id]);
            } else {
                array_push($answers, "");
            }
        }
        // dd($_POST['field']);
        // dd($_POST['field']);
        $form = Form::where('slug',$slug)->firstOrFail();
        $form_response = new FormResponse;
        $form_response->questions = json_encode($_POST['question']);
        $form_response->answers = json_encode($answers);
        $form_response->form_id = $form->id;
        $form_response->form_version = $form->version;
        $form_response->save();

        // Alert recipients
        $this->user = Auth::user();
        $this->org_name = $this->settings->organization_name;
        $this->form = $form;
        $this->html = 'A new form submission was recieved – here\'s the summary. <br><a href="'.url('dashboard#forms').'">Visit your dashboard</a> to view all form responses.';
        $this->html .= '<br><br><table>';
        foreach(json_decode($form_response->questions) as $key => $question) {
            $this->html .= '<tr><td><b>'.$question.': </b> </td>';
            $this->html .= '<td>'.json_decode($form_response->answers)[$key].'</td></tr>';
        }
        $this->html .= '</table>';
        
        foreach ($form->recipients as $recipient) {
            $this->recipient = $recipient;
            $this->user = User::where('email',$recipient->email)->first();
            $mail = Mail::send('emails.default', [
                'subject'=>'New '.$this->form->name.' Form Submission',
                'content' => $this->html,
            ], function ($mail) {
                if (env('APP_ENV') == 'local') {
                    $mail->from('support@nebulasuite.com', $this->org_name);
                    if (isset($this->user->name)) {
                        $mail->to($this->user->email, $this->user->name);
                    } else {
                        $mail->to($this->recipient->email);
                    }
                    $mail->subject('New '.$this->form->name.' Form Submission');
                } else {
                    $mail->from('support@'.env('APP_DOMAIN'), $this->org_name);
                    if (isset($this->user->name)) {
                        $mail->to($this->user->email, $this->user->name);
                    } else {
                        $mail->to($this->recipient->email);
                    }
                    $mail->subject('New '.$this->form->name.' Form Submission');
                }
            });
            if (!$mail) return response()->json(['error' => 'Failed to send mail to '.$email]);
        }
        return response()->json([
            'success' => 'Thanks! Your response has been recorded.',
        ]); 
    }

    /*
    |--------------------------------------------------------------------------
    | CRM Methods
    |--------------------------------------------------------------------------
    */

    public function showCRM($current_pipeline_key = NULL) {
        $module = Module::where('type','crm')->firstOrfail();
        $crm = Crm::firstOrfail();
        if (isset($current_pipeline_key) && $current_pipeline_key !== NULL)  {
            if($current_pipeline_key == 'no-go') {
                $records = CrmRecord::where('pipeline_key','-1')->orderBy('updated_at','DESC')->paginate(25);
            } else {
                $records = CrmRecord::where('pipeline_key',$current_pipeline_key)->orderBy('updated_at','DESC')->paginate(25);
            }
        }
        else {
            $records = CrmRecord::where('pipeline_key','0')->orderBy('updated_at','DESC')->paginate(25);
        }
        if ($current_pipeline_key == NULL) $current_pipeline_key = 0;
        return view('crm.index',[
            'module' => $module,
            'records' => $records,
            'crm' => $crm,
            'current_pipeline_key' => $current_pipeline_key,
        ]);
    }

    public function exportCrm($pipeline_key = NULL) {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        $this->crm = Crm::firstOrFail();
        $this->pipeline_key = $pipeline_key;
        $this->module = Module::where('slug','crm')->firstOrfail();
        Excel::create($this->module->label . ' ' . json_decode($this->crm->pipeline)[$this->pipeline_key], function($excel) {
            $excel->setTitle($this->module->label . ' ' . json_decode($this->crm->pipeline)[$this->pipeline_key]); 
            $excel->sheet($this->module->label, function($sheet) {
                $sheet->setOrientation('landscape');
                $columns = json_decode($this->crm->columns);
                $sheet->rows([
                    $columns, // add columns to sheet
                ]);
                $rows = CrmRecord::where('pipeline_key',$this->pipeline_key)->get();
                foreach ($rows as $row) {
                    $row_data = json_decode($row->data);
                    $sheet->rows([
                        $row_data
                    ]);
                }                
            });
        })->export('csv');
    }

    public function showImportCrm($pipeline_key) {
        $crm = Crm::first();
        return view('crm.record.import',[
            'pipeline_key' => $pipeline_key,
            'crm' => $crm,
        ]);
    }

    public function importCrm(Request $request,$pipeline_key) {
        $this->request = $request;
        $this->pipeline_key = $pipeline_key;
        $this->sheet = NULL;
        $records = CrmRecord::where('pipeline_key',$pipeline_key)->get();
        if ($request->file->getClientOriginalExtension() !== 'xls' && $request->file->getClientOriginalExtension() !==  'xlsx' && $request->file->getClientOriginalExtension() !== 'csv') {
            // Validate fields
            $validator = Validator::make($request->all(), [
                'file' => 'bail|mimes:xls,xlsx,csv',
            ]);
            // Return validation error
            if ($validator->fails()) {
                return Redirect::back()->withInput()->withErrors($validator);
            }
        }
        Excel::load($request->file, function($reader) {
            $sheet = $reader->get()->toArray();
            $this->sheet = $sheet;
            if (count($sheet) > 0) {
                $unformated_columns = array_keys($sheet[0]);
                $formatted_columns = array();
                foreach ($unformated_columns as $column) {
                     $column = str_replace('_',' ',ucwords($column,'_')); // e.g. first_name to First Name because columns were formatted as array keys
                     array_push($formatted_columns,$column); 
                }
                $columns = $formatted_columns; // redefine columns as formatted_columns
                $this->columns = $columns;
                $crm = Crm::first();
                if (count($this->columns) > 1) {
                    // If number of columns match for both the import file and defined pipeline in preferences. Or if the crm columns ar not defined.
                    if (count($columns) == count(json_decode($crm->pipeline)) || count(json_decode($crm->columns)) == 0) {
                        $this->sheet = $sheet;
                        if (count(json_decode($crm->columns)) == 0) {
                            // Define CRM columns as defined in spreadsheet
                            $crm->columns = json_encode($columns);
                            $crm->save();
                        }
                    }
                }
            }
        });
        if ($this->sheet == NULL) return redirect()->to('crm/'.$this->pipeline_key.'/import')->withErrors('There was a problem with the import. Make sure the columns defined in your CRM preferences match the columns defined in your file.');
        try {
            // $i = 0;
            foreach ($this->sheet as $row) {
                // $i++;
                // if ($i !== 1) { // Skip first row      
                    $data_array = array();
                    foreach ($row as $row_cell) {
                        if (($row_cell !== 'null')) {
                            array_push($data_array,$row_cell);
                        } else {
                            array_push($data_array,'');
                        }
                    }
                    $crm_record = new CrmRecord;
                    $crm_record->pipeline_key = $request->pipeline_key;
                    $crm_record->data = json_encode($data_array);
                    $crm_record->save();
                // }
            }
        } catch(Exception $e){
            return Redirect::back()->withInput()->withErrors($e->getMessage());
        }
        $crm = Crm::firstOrfail();
        return Redirect::to('crm/'.$this->pipeline_key.'/import')->withSuccess(count($this->sheet) .' CRM records imported to '.strtolower(json_decode($crm->pipeline)[$pipeline_key]).'. Refresh the page to see new changes reflected.');
    }

    public function createCrmRecord($pipeline_key = NULL) {
        $crm = Crm::firstOrfail();
        if ($pipeline_key == NULL) $pipeline_key = json_decode($crm->pipeline)[0];
        return view('crm.record.create',[
            'crm' => $crm,
            'pipeline_key' => $pipeline_key,
        ]);
    }

    public function showCrmPreferences() {
        $crm = Crm::firstOrfail();
        return view('crm.preferences',[
            'crm' => $crm,
        ]);
    }

    public function updateCrmRecord(Request $request, $id) 
    {
        if (!Auth::user()->is_manager) return response()->json(['You must be logged in as a manager to perform this operation.' => $e->getMessage(),]);
        try {
            $record = CrmRecord::findOrFail($id);
            $record->data = json_encode($_POST['data']);
            $record->pipeline_key = isset($request->pipeline_key) ? $request->pipeline_key : NULL;
            $record->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        return response()->json([
            'success' => 'Changes saved!',
            'refreshDynamicView' => true,
            'closeDetails' => true,
        ]); 
    }


    public function updateCrmPreferences(Request $request) 
    {
        if (!Auth::user()->is_manager) return response()->json(['You must be logged in as a manager to perform this operation.' => $e->getMessage(),]);
        // Require fields
        $validator = Validator::make($request->all(), [
            'columns' => 'bail|required',
            'pipeline' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);
        try {
            $crm = Crm::firstOrfail();
            $crm->columns = json_encode(explode(', ',$_POST['columns']));
            $crm->pipeline = json_encode(explode(', ',$_POST['pipeline']));
            $crm->label_key = ($request->label_key !== '') ? $request->label_key : NULL;
            $crm->sublabel_key = ($request->sublabel_key !== '') ? $request->sublabel_key : NULL;
            $crm->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        $module = Module::where('slug','crm')->first();
        $feed_item = new FeedItem;
        $feed_item->text = 'CRM preferences were updated.';
        $feed_item->details = 'dashboard#crm';
        $feed_item->icon = $module->icon;
        $feed_item->save();
        return response()->json([
            'success' => 'Preferences saved.',
            'loadView' => 'crm',
            'closeModal' => true,
        ]); 
    }

    public function editCrmRecord(Request $request, $id) 
    {
        $record = CrmRecord::findOrFail($id);
        $crm = Crm::firstOrfail();
        $module = Module::where('slug','crm')->first();
        return view('crm.record.edit',[
            'crm' => $crm,
            'record' => $record,
            'module' => $module,
        ]);
    }

    public function storeCrmRecord(Request $request, $pipeline_key) 
    {
        if (!Auth::user()->is_manager) return response()->json(['You must be logged in as a manager to perform this operation.' => $e->getMessage(),]);

        // Require fields
        $validator = Validator::make($request->all(), [
            'data' => 'bail|required',
        ]);

        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        // Store record
        try {
            $record = new CrmRecord;
            $record->data = json_encode($_POST['data']);
            $record->pipeline_key = strtolower(trim($pipeline_key));
            $record->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        $module = Module::where('slug','crm')->first();

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = 'A new CRM record was added.';
        $feed_item->details = 'dashboard#crm/records/'.$record->id.'/edit';
        $feed_item->icon = $module->icon;
        $feed_item->save();

        // Return json response
        return response()->json([
            'success' => 'Successfully added',
            'loadView' => 'crm/'.$pipeline_key,
            'closeModal' => true,
        ]); 
    }

    /**
     * Delete CRM record of given id
     */
    public function deleteCrmRecord(Request $request, $id)
    {
        if (!Auth::user()->is_manager) return response()->json(['You must be logged in as a manager to perform this operation.' => $e->getMessage(),]);
        try {
            $crm = CrmRecord::findOrfail($id);
            $crm->delete();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        return response()->json([
            'closeDetails' => true,
            'refreshDynamicView' => true,
        ]);
    }

    /**
     * Clear multiple crm records
     */
    public function clearCrmRecords(Request $request, $pipeline_key)
    {
        if (!Auth::user()->is_manager) return response()->json(['You must be logged in as a manager to perform this operation.' => $e->getMessage(),]);
        try {
            $crmRecords = CrmRecord::where('pipeline_key',$pipeline_key)->get();
            foreach ($crmRecords as $crmRecord) {
                CrmRecord::findOrfail($crmRecord->id)->delete();
            }
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        return response()->json([
            'closeDetails' => true,
            'refreshDynamicView' => true,
        ]);
    }

    /*
    |--------------------------------------------------------------------------
    | Record Methods
    |--------------------------------------------------------------------------
    */

    public function showRecordImport($module_id) {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        return view('records.import',['module_id'=>$module_id]);
    }

    public function exportRecords($record_set_id) 
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        $this->record_set = RecordSet::findOrFail($record_set_id);
        // $record_set->records = '';
        // dd($record_set->records);
        Excel::create($this->record_set->name, function($excel) {

            // Set the title
            $excel->setTitle($this->record_set->name);
            $excel->sheet($this->record_set->name, function($sheet) {
                $sheet->setOrientation('landscape');
                $columns = json_decode($this->record_set->columns);
                $sheet->rows([
                    $columns, // add columns to sheet
                ]);
                $rows = Record::where('record_set_id',$this->record_set->id)->get();
                // $rows_data = '';
                foreach ($rows as $row) {
                    // $row_data = $row;
                    // // add each row's data to the rows data string to be output
                    // $rows_data .= $row_data;
                    $row_data = json_decode($row->data);
                    $sheet->rows([
                        $row_data
                    ]);
                }
                // $rows_data = json_decode($rows_data);
            });
        })->export('csv');
    }

    public function importRecords(Request $request, $module_id) 
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        $this->module_id = $module_id;
        $this->request = $request;

        $module = Module::findOrFail($module_id);

        if ($request->file->getClientOriginalExtension() !== 'xls' && $request->file->getClientOriginalExtension() !==  'xlsx' && $request->file->getClientOriginalExtension() !== 'csv') {
            // Validate fields
            $validator = Validator::make($request->all(), [
                'file' => 'bail|mimes:xls,xlsx,csv',
            ]);
            // Return validation error
            if ($validator->fails()) {
                return Redirect::back()->withInput()->withErrors($validator);
            }
        }

        Excel::load($request->file, function($reader) {
            // $reader->each(function($sheet) {
            $sheet = $reader->get()->toArray();
            // $sheet = $reader->formatDates( true, 'm/d/Y' )->get()->toArray();

            $unformated_columns = array_keys($sheet[0]);
            $formatted_columns = array();
            foreach ($unformated_columns as $column) {
                 $column = str_replace('_',' ',ucwords($column,'_')); // e.g. first_name to First Name because columns were formatted as array keys
                 array_push($formatted_columns,$column); 
            }
            $columns = $formatted_columns; // redefine columns as formatted_columns
            $this->columns = $columns;
            if (count($columns) > 1) {
                // Create record set
                $record_set = new RecordSet;
                $record_set->name = ucfirst(pathinfo($this->request->file->getClientOriginalName(), PATHINFO_FILENAME));
                $record_set->module_id = $this->module_id;
                $record_set->icon = 'icon-circle-list';
                $record_set->columns = json_encode($columns);
                $record_set->label_key = 0; // First column
                $record_set->sublabel_key = 2; // Second column
                $record_set->save();
                $this->record_set = $record_set;
                // Add each row to the records table
                foreach ($sheet as $row) {
                    // If columns match
                    if (array_keys($row) == $unformated_columns) {
                        $row = array_values($row);
                        if (count(array_filter($row)) !== 0) { // If at least one column is set
                            $record = new Record;
                            $record->record_set_id = $record_set->id;
                            $record->data = json_encode($row);
                            $record->save();
                        }
                    } else {
                        continue; // skip records where the columns don't match
                    }
                }
            }
        });

        if (count($this->columns) > 1) {
            // Add feed item
            $feed_item = new FeedItem;
            $feed_item->text = $this->record_set->name.' was imported into '.$module->label.'.';
            $feed_item->href = 'dashboard#records/'.$module->slug.'/'.$this->record_set->id;
            $feed_item->icon = 'icon-circle-list';
            $feed_item->save();
            return redirect(url('record_set/'.$this->record_set->id.'/edit'));
        } else {
            return redirect()->back()->withErrors('Problem importing file. Ensure correct format and try again.');
        }
    }

    public function showSharedRecords(Request $request, $slug, $record_set_id = null)
    {
        // If shared with them already
        if (RecordSetShare::where('user_id',Auth::user()->id)->where('record_set_id',$record_set_id)->count() > 0) {        
            $module = Module::where('slug',$slug)->firstOrFail();
            if (isset($record_set_id)) $record_set = RecordSet::where('id',$record_set_id)->where('module_id',$module->id)->first();
            if (isset($record_set) && count($record_set) !== 0) { // Record set found
                $records = Record::where('record_set_id','=',$record_set->id)->orderBy('created_at','DESC')->paginate(25);
                return view('records.record_set.index',[
                    'module' => $module,
                    'record_set' => $record_set,
                    'records' => $records,
                ]);
            }
        } else {
            return redirect()->back()->withErrors('You do not have access to this record set.');
        }
    }

    public function showRecords(Request $request, $slug, $record_set_id = null)
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        $module = Module::where('slug',$slug)->firstOrFail();
        if (isset($record_set_id)) $record_set = RecordSet::where('id',$record_set_id)->where('module_id',$module->id)->first();
        if (isset($record_set) && count($record_set) !== 0) { // Record set found
            $records = Record::where('record_set_id','=',$record_set->id)->orderBy('created_at','DESC')->paginate(25);
            return view('records.index',[
                'module' => $module,
                'record_set' => $record_set,
                'records' => $records,
            ]);
        } else { // Record set id not set so show first list
            // $record_set = $module->record_sets->orderBy('created_at')->get();
            $record_set =  RecordSet::where('module_id',$module->id)->orderBy('created_at')->first();
            if(isset($record_set->id)) {
                $records = Record::where('record_set_id','=',$record_set->id)->orderBy('created_at','DESC')->paginate(25);
            } else {
                $records = null;
            }
            return view('records.index',[
                'module' => $module,
                'record_set' => $record_set,
                'records' => $records,
            ]);
        }
    }

    // public function showRecordSet()
    // {
    //     return view('records.record_set.index',[
    //         'record_set' => $record_set,
    //         'records' => $records,
    //     ]);
    // }

    public function editRecordSet($id) 
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        $record_set = RecordSet::findOrFail($id);
        return view('records.record_set.edit',[
            'record_set' => $record_set,
        ]);
    }

    public function editRecord($id) 
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        $record = Record::findOrFail($id);
        return view('records.record.edit',[
            'record' => $record,
        ]);
    }


    public function showRecord($id) 
    {
        // Check permissions
        $record = Record::findOrFail($id);
        return view('records.record.view',[
            'record' => $record,
        ]);
    }


    public function updateRecord(Request $request, $id) 
    {
        if (!Auth::user()->is_manager) return redirect()->back()->withErrors('You must be logged in as a manager to do that.');
        // Require fields
        $validator = Validator::make($request->all(), [
            'data' => 'bail|required',
            'record_set_id' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);
        // Update record
        try {
            $previous_record_set = RecordSet::findOrFail($request->input('record_set_id'));
            $record = Record::findOrFail($id);
            if($record->record_set->columns !== $previous_record_set->columns) {
                return response()->json([
                    'error' => 'Error: Columns in "'.$previous_record_set->name.'" do not match the columns in "'.$record->record_set->name.'"',
                    // 'refreshDynamicView' => true,
                ]);
            }
            $record->data = json_encode($_POST['data']);
            $record->record_set_id = $request->input('record_set_id');
            $record->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        // Return json response
        return response()->json([
            'success' => 'Successfully updated',
            'refreshDynamicView' => true,
            'closeDetails' => true,
        ]);
    }

    public function createRecordSet($module_id) {
        if (!Auth::user()->is_manager) return response()->json(['You must be logged in as a manager to perform this operation.' => $e->getMessage(),]);
        $module = Module::findOrFail($module_id);
        return view('records.record_set.create',['module'=>$module]);
    }

    public function createRecord($record_set_id) {
        if (!Auth::user()->is_manager) return response()->json(['You must be logged in as a manager to perform this operation.' => $e->getMessage(),]);
        $record_set = RecordSet::findOrFail($record_set_id);
        return view('records.record.create',['record_set'=>$record_set]);
    }

    public function deleteRecord($id) {
        // Delete record
        try {
            $record = Record::findOrFail($id);
            $record->delete();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Return json response
        return response()->json([
            'success' => 'Record deleted',
            'refreshDynamicView' => true,
        ]); 
    }

    public function storeRecord(Request $request, $record_set_id) 
    {
        if (!Auth::user()->is_manager) return response()->json(['You must be logged in as a manager to perform this operation.' => $e->getMessage(),]);

        // Require fields
        $validator = Validator::make($request->all(), [
            'data' => 'bail|required',
        ]);

        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        // Store record
        try {
            $record = new Record;
            $record->data = json_encode($_POST['data']);
            $record->record_set_id = $record_set_id;
            $record->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Add feed item
        // $feed_item = new FeedItem;
        // $feed_item->text = A  was created.';
        // $feed_item->href = 'dashboard#records/'.$record_set->module->slug.'/'.$record_set->id;
        // $feed_item->icon = 'icon-circle-list';
        // $feed_item->save();

        // Return json response
        return response()->json([
            'success' => 'Successfully added',
            'refreshDynamicView' => true,
            'closeModal' => true,
            //'loadView' => 'dashboard#records/'.$record->record_set->module->slug.'/'.$record_set_id,
        ]); 
    }

    public function showShareRecordSet($id) 
    {
        $record_set = RecordSet::findOrFail($id);
        return view('records.record_set.share',['record_set' => $record_set]);
    }

    public function shareRecordSet(Request $request, $id) {
        $record_set = RecordSet::findOrFail($id);
        $this->record_set = $record_set;
        $emails = explode(',', $request->emails);
        // Validate emails
        foreach ($emails as $email) {
            $email = preg_replace('/\s+/', '', $email); // Remove white spaces
            if (!filter_var( $email ,FILTER_VALIDATE_EMAIL )) {
                return response()->json([
                    'error' => 'Record set not shared. Enter only valid email addresses.',
                ]);
            }
            // Lookup whether or not the user exist
            $user = User::where('email',$email)->first();
            if ($user == null) {
                return response()->json([
                    'error' => 'List was not shared. '.$email . ' must first have an account before you can share a file with them.',
                ]);
            }
        }
        foreach ($emails as $email) {
            $email = preg_replace('/\s+/', '', $email); // Remove white spaces
            $settings = Setting::first();
            $user = User::where('email',$email)->first();
            $this->org_name = $settings->organization_name;
            // $this is used to keep in scope of mail
            $this->user = $user;
            // Add a record that the list was shared if the folder not already shared else continue
            if (RecordSetShare::where('user_id',$user->id)->where('record_set_id',$record_set->id)->count() > 0) {
                continue; // Already shared
            } else {                
                $share = new RecordSetShare;
                $share->record_set_id = $record_set->id;
                $share->user_id = $user->id;
                $share->save();
            }
            // Send an email that a file has been shared
            $mail = Mail::send('emails.default', [
                'subject'=>'A list was shared with you at '.env('APP_DOMAIN'),
                'content' => strtok($this->user->name , " "). ', <br/><br/>'.Auth::user()->name.' has shared a record set: "' . $this->record_set->name . '" with you. This is available under the Shared Lists section of <a href="'.url('account').'">'.'your account.</a>',
            ], function ($mail) {
                if (env('APP_ENV') == 'local') {
                    $mail->from('support@nebulasuite.com', $this->org_name);
                    $mail->to($this->user->email, $this->user->name);
                    $mail->subject('A list was shared with you at '.env('APP_DOMAIN'));
                } else {
                    $mail->from('support@'.env('APP_DOMAIN'), $this->org_name);
                    $mail->to($this->user->email, $this->user->name);
                    $mail->subject('A list was shared with you at '.env('APP_DOMAIN'));
                }
            });
            if (!$mail) return response()->json(['error' => 'Failed to send mail to '.$email]);
        }
        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $record_set->name . ' was shared with '.$this->user->name.'.';
        $feed_item->details = 'records/'.$record_set->module->slug;
        $feed_item->icon = 'icon-circle-list';
        $feed_item->save();
        return response()->json([
            'success' => 'List successfully shared',
            'closeModal' => true,
        ]);
    }

    public function storeRecordSet(Request $request) 
    {
        if (!Auth::user()->is_manager) return response()->json(['You must be logged in as a manager to perform this operation.' => $e->getMessage(),]);

        // Require fields
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required|max:31',
            'columns' => 'bail|required',
            'module_id' => 'bail|required',
        ]);

        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        // Store record_set
        try {
            $record_set = new RecordSet;
            $record_set->name = $request->name;
            $record_set->module_id = $request->module_id;
            $record_set->columns = json_encode(explode(',', $request->columns));
            $record_set->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Add feed item
        $feed_item = new FeedItem;
        $feed_item->text = $record_set->name .' was created.';
        $feed_item->href = 'dashboard#records/'.$record_set->module->slug;
        $feed_item->icon = 'icon-circle-list';
        $feed_item->save();

        // Return json response
        return response()->json([
            'success' => 'Successfully created',
            'refresh' => true,
        ]); 
    }

    public function updateRecordSet(Request $request, $id) 
    {
        // Require fields
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required|max:31',
            'columns' => 'bail|required',
        ]);

        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        // Update record set
        try {
            $columns = array();
            foreach (explode(',', $request->columns) as $key => $value) {
                $value = trim($value);
                array_push($columns,$value);
            }
            $record_set = RecordSet::findOrFail($id);
            $record_set->name = $request->name;
            $record_set->columns = json_encode($columns);
            $record_set->label_key = $request->label_key;
            $record_set->sublabel_key = ($request->sublabel_key !== '(optional)') ? $request->sublabel_key : NULL;
            $record_set->save();
            // Add feed item
            $feed_item = new FeedItem;
            $feed_item->text = $record_set->name.' was updated.';
            $feed_item->href = 'dashboard#records/'.$record_set->module->slug;
            $feed_item->icon = 'icon-circle-list';
            $feed_item->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Return json response
        return response()->json([
            'success' => 'Successfully updated',
            'refreshDynamicView' => true,
            'closeModal' => true,
        ]); 
    }

    public function deleteRecordSet(Request $request, $id) 
    {
        // Delete record
        try {
            $record_set = RecordSet::findOrFail($id);
            $records = Record::where('record_set_id',$record_set->id)->get();
            foreach ($records as $record) {
                $record->delete();
            }
            $record_set->delete();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Return json response
        return response()->json([
            'success' => 'Set deleted',
            'refresh' => true,
        ]); 
    }

    /*
    |--------------------------------------------------------------------------
    | Pages module methods
    |--------------------------------------------------------------------------
    */

    public function showPages() {
        $pages = Page::all();
        return view('page.index',[
            'pages' => $pages,
        ]);
    }


    public function showPageTemplates() {
        $templates = PageTemplate::all();
        return view('page.template.index',[
            'templates' => $templates,
        ]);
    }

    // public function editPage($page_slug) {
    //     $page = Page::where('slug',$page_slug)->firstOrFail();
    //     return view('page.edit',[
    //         'page' => $page,
    //     ]);
    // }

    public function showPage($page_slug) {
        $page = Page::where('slug',$page_slug)->first();
        $module = Module::where('type','pages')->first();
        if (isset($module->enabled) && $module->enabled) {
            if ($page !== null) {   
                $elements = $page->elements()->orderBy('order')->get();
                return view('page.view',[
                    'page' => $page,
                    'elements' => $elements,
                ]);
            } else {
                if (Auth::user() == null) {
                    return view('auth.login');
                } else {
                    abort(404);
                }
            }
        } else {
            abort(404);
        }
    }

    public function editPage($id) {
        $page = Page::findOrFail($id);
        return view('page.edit',[
            'page' => $page,
        ]);
    }

    public function pageEditor($id) {
        // dd(Auth::user()->is_manager);
        if (!Auth::user()->is_manager) return redirect('')->withErrors('Manager access required');
        $page = Page::findOrFail($id);
        $elements = $page->elements()->orderBy('order')->get();
        return view('page.editor',[
            'page' => $page,
            'elements' => $elements,
        ]);
    }


    public function showPagesSettings() {
        $menu_items = PageMenuItem::all();
        return view('page.settings',[
            'menu_items' => $menu_items,
        ]);
    }

    public function updatePagesSettings(Request $request) {
        $menu_items = PageMenuItem::all();
        // If there are menu items then edit each of them
        if (count($menu_items) > 0) {        
            foreach ($menu_items as $key => $menu_item) {
                $menu_item->label = $request->label[$key];
                $menu_item->href = $request->href[$key];
                $menu_item->save();
            }
        } else {
            // Create new page menu items
            foreach ($_POST['label'] as $key => $label) {
                $menu_item = new PageMenuItem;
                $menu_item->label = $label;
                $menu_item->href = $request->href[$key];
                $menu_item->save();
            }
        }
        $settings = Setting::first();
        $settings->script = $request->script;
        $settings->css = $request->css;
        $settings->save();
        return response()->json([
            'refresh' => true,
            'success' => 'Settings Saved',
        ]);
    }

    public function justifyElementText(Request $request, $id) {
        try {
            $element = PageElement::findOrFail($id);
            // $element->style = str_replace($request->style.';','',$element->style);
            $element->style = str_replace('text-align:center;','',$element->style);
            $element->style = str_replace('text-align:left;','',$element->style);
            $element->style = str_replace('text-align:right;','',$element->style);
            $element->style = 'text-align:'.$request->input('justify').';'. $element->style;
            $element->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
    }

    public function updatePage(Request $request, $id) {
        $page = Page::findOrFail($id);
        try {            
            if ($request->has('title')) $page->title = $request->input('title');
            // if ($request->has('layout')) $page->layout = $request->input('layout');
            if ($request->has('slug')) $page->slug = create_slug($request->input('slug'));
            $page->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);         
        }
        return response()->json([
            'refresh' => true,
            'success' => 'Settings Saved',
        ]);
    }


    public function createPage() {
        if (!Auth::user()->is_manager) return response()->json(['error' => 'Manager access required',]);
        return view('page.create');
    }

    public function createPageTemplate() {
        if (!Auth::user()->is_manager) return response()->json(['error' => 'Manager access required',]);
        $pages = Page::all();
        return view('page.template.create',['pages'=>$pages]);
    }

    public function storePage(Request $request) {
        // Validate fields
        $validator = Validator::make($request->all(), [
            'title' => 'bail|required',
            'slug' => 'bail|required',
            // 'layout' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);
        // Store a new record
        $page = new Page;
        if ($request->has('title')) $page->title = $request->input('title');
        if ($request->has('layout')) $page->layout = $request->input('layout');
        if ($request->has('slug')) $page->slug = create_slug($request->input('slug'));
        $page->save();
        return response()->json([
            'refresh' => 'true',
            'success' => 'Page Added',
        ]);
        // return response()->json([
        //     'redirect' => url($page->slug),
        // ]);
    }


    public function deletePage($id) 
    {
        if (Auth::user()->is_manager) {        
            $page = Page::findOrFail($id);
            // Delete each page element
            foreach ($page->elements as $element) {
                $element = PageElement::findOrFail($element->id);
                $element->delete();
            }
            $page->delete();
            return response()->json([
                'refresh' => 'true',
                'success' => 'Page Deleted',
            ]);
        } else {
            return response()->json([
                'error' => 'Unauthorized',
            ]);        
        }
    }

    public function createElement(Request $request) {
        return view('page.element.create');
    }

    public function deleteElement(Request $request, $id) {
        try {
            $element = PageElement::findOrFail($id);
            // Delete child elements if they exist
            $children = PageElement::where('parent_id',$element->id)->get();
            if (count($children) > 0) {            
                foreach ($children as $child) {
                   $child = PageElement::findOrFail($child->id);
                   $child->delete();
                }
            }
            $element->delete();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        return response()->json([
            'success' => 'Successfully deleted',
        ]);
    }


    // Show form to edit element
    public function editElement(Request $request, $id) {
        $element = PageElement::findOrFail($id);
        return view('page.element.edit',[
            'element' => $element,
        ]);
    }

    public function addPageElement($id) {
        return view('page.element.create',['block_id'=>$id]);
    }

    public function createPageElement($id, $type) {
        $forms = Form::all();
        $payment_forms = PaymentForm::orderBy('created_at','DESC')->get();
        return view('page.element.add',['block_id'=>$id,'type'=>$type, 'forms'=>$forms, 'payment_forms'=>$payment_forms]);
    }

    public function generatePageElement(Request $request,$id, $type) {
        // Include the parent as well
        $page_block = PageElement::findOrFail($id);
        $parent_elements = PageElement::where('parent_id',$page_block->id)->get();
        $order = 0;
        foreach ($parent_elements as $parent_element) {
            if ($order < $parent_element->order) $order = $parent_element->order + 1; // Make sure the order is greater than sibling elements show it show up last
        }
        // dd($order);
        // First create an element with appropriate specs
        $element = new PageElement;
        $element->order = $order;
        $element->type = $type;
        $element->parent_id = $id;
        $element->page_id = $page_block->page_id;
        $element->html = $request->html;
        $element->height = $request->height;
        $element->width = $request->width;
        $element->margin = $request->margin;
        $element->padding = $request->padding;
        $element->font_size = $request->font_size;
        $element->text_align = $request->text_align;
        $element->color = $request->color;
        $element->image = $request->image;
        $element->background_color = $request->background_color;
        $element->background_image = $request->background_image;
        $element->background_overlay = $request->background_overlay;
        $element->modal = $request->modal;
        $element->details = $request->details;
        $element->style = $request->styles;
        $element->class = $request->class;
        if ($request->type == 'form' || $request->type == 'payment-form')  {
            if ($request->form_id !== null && $request->form_id !== '') {
                $element->options = $request->form_id;
            } else {
                return response()->json([
                    'error' => 'You must first create a form in the dashboard before adding one to the page.',
                ]);
            }
        }
        $element->save();
        return view('page.element.generate',['element'=>$element,'page_block'=>$page_block]);
    }

    public function updatePageElement(Request $request, $id) {
        try {
            $element = PageElement::findOrFail($id);
            $element->html = $request->html;
            // $element->order = $request->order;
            $element->height = $request->height;
            $element->width = $request->width;
            $element->margin = $request->margin;
            $element->padding = $request->padding;
            $element->font_size = $request->font_size;
            $element->text_align = $request->text_align;
            $element->color = $request->color;
            $element->image = $request->image;
            $element->background_color = $request->background_color;
            $element->background_image = $request->background_image;
            $element->background_overlay = $request->background_overlay;
            $element->modal = $request->modal;
            $element->details = $request->details;
            $element->style = $request->style;
            $element->class = $request->class;
            $element->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
    // old public function updatePageElement(Request $request, $id) {
    //     try {
    //         $element = PageElement::findOrFail($id);
    //         $element->background_image = $request->input('background_image');
    //         if ($request->has('type')) $element->type = $request->input('type');
    //         $element->background_image = $request->input('background_image');
    //         // $response = $request->input('parallax');
    //         // $element->parallax = isset($_POST['parallax']) ? 1 : 0;
    //         $element->style = $request->input('style');
            
    //         $classes = explode(' ', $request->class);
    //         array_shift($classes);
    //         $classes = implode(' ', $classes);
    //         if (str_word_count($request->class) > 1) $element->class = $classes;
    //         $element->overlay = $request->input('overlay');
    //         // $element->attributes = $request->input('attributes');
    //         if (isset($request->modal) && $request->modal !== '') {
    //             $element->attributes = 'data-modal="'.$request->modal.'"';            }
    //         if (isset($request->details) && $request->details !== '') {
    //             $element->attributes = 'data-details="'.$request->details.'"';
    //         }
    //         // Both details and modal are set
    //         if ((isset($request->details) && $request->details !== '') && isset($request->modal) && $request->modal !== '') {
    //             $element->attributes = 'data-details="'.$request->input('details').'" data-modal="'.$request->input('modal').'"';
    //         }
    //         // Put attributes
    //         $element->html = $request->input('html');
    //         $element->save();
    //     } catch(Exception $e){
    //         return response()->json([
    //             'error' => $e->getMessage(),
    //         ]);
    //     }
    }

    public function updateElementOrder(Request $request, $id) {
        try {
            $element = PageElement::findOrFail($id);
            $element->order = $request->order;
            $element->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
    }

    // public function updateElement(Request $request, $id) {
    //     try {
    //         $element = PageElement::findOrFail($id);
    //         $element->background_image = $request->input('background_image');
    //         if ($request->has('type')) $element->type = $request->input('type');
    //         $element->background_image = $request->input('background_image');
    //         // $response = $request->input('parallax');
    //         $element->parallax = isset($_POST['parallax']) ? 1 : 0;
    //         // $element->parallax = isset($request->input('parallax')) ? 1 : '');
    //         $element->style = $request->input('style');
    //         $element->class = $request->input('class');
    //         $element->overlay = $request->input('overlay');
    //         $element->attributes = $request->input('attributes');
    //         $element->html = $request->input('html');
    //         $element->save();
    //     } catch(Exception $e){
    //         return response()->json([
    //             'error' => $e->getMessage(),
    //         ]);
    //     }
    //     return response()->json([
    //         'refresh' => 'true',
    //         'success' => 'Element updated...reloading',
    //     ]);
    // }

    public function generateSection(Request $request) {
        try {
            $page = Page::findOrFail($request->input('page_id'));
            $last_section_on_page = $page->elements()->where('type','page-section')->orderBy('order','DESC')->first();
            if (isset($last_section_on_page->order)) {
                 // the highest ordered element + 1 to append to bottom of page
                $section_order = $page->elements()->where('type','page-section')->orderBy('order','DESC')->first()->order;
            } else { // First section to be added
                 $section_order = 1;
            }
            $section = new PageElement;
            $section->type = 'page-section';
            $section->order = $section_order;
            $section->page_id = $request->input('page_id');
            $section->save();
            // dd($section->type);
            // Add blocks to the section
            $page_blocks = array();
            if ($request->input('type') == 'page-section-full') {
                $page_block = new PageElement();
                $page_block->parent_id = $section->id;
                $page_block->page_id = $request->input('page_id');
                $page_block->type = 'page-block-full';
                $page_block->save();
                // Push to page_blocks array
                array_push($page_blocks,$page_block);
            }
            if ($request->input('type') == 'page-section-halves') {
                $page_block = new PageElement();
                $page_block->parent_id = $section->id;
                $page_block->page_id = $request->input('page_id');
                $page_block->type = 'page-block-half';
                $page_block->save();
                array_push($page_blocks,$page_block);
                $page_block = new PageElement();
                $page_block->parent_id = $section->id;
                $page_block->page_id = $request->input('page_id');
                $page_block->type = 'page-block-half';
                $page_block->save();
                array_push($page_blocks,$page_block);
            }
            if ($request->input('type') == 'page-section-thirds') {
                $page_block = new PageElement();
                $page_block->parent_id = $section->id;
                $page_block->page_id = $request->input('page_id');
                $page_block->type = 'page-block-third';
                $page_block->save();
                array_push($page_blocks,$page_block);
                $page_block = new PageElement();
                $page_block->parent_id = $section->id;
                $page_block->page_id = $request->input('page_id');
                $page_block->type = 'page-block-third';
                $page_block->save();
                array_push($page_blocks,$page_block);
                $page_block = new PageElement();
                $page_block->parent_id = $section->id;
                $page_block->page_id = $request->input('page_id');
                $page_block->type = 'page-block-third';
                $page_block->save();
                array_push($page_blocks,$page_block);
            }
            if ($request->input('type') == 'page-section-fourths') {
                $page_block = new PageElement();
                $page_block->parent_id = $section->id;
                $page_block->page_id = $request->input('page_id');
                $page_block->type = 'page-block-fourth';
                $page_block->save();
                array_push($page_blocks,$page_block);
                $page_block = new PageElement();
                $page_block->parent_id = $section->id;
                $page_block->page_id = $request->input('page_id');
                $page_block->type = 'page-block-fourth';
                $page_block->save();
                array_push($page_blocks,$page_block);
                $page_block = new PageElement();
                $page_block->parent_id = $section->id;
                $page_block->page_id = $request->input('page_id');
                $page_block->type = 'page-block-fourth';
                $page_block->save();
                array_push($page_blocks,$page_block);
                $page_block = new PageElement();
                $page_block->parent_id = $section->id;
                $page_block->page_id = $request->input('page_id');
                $page_block->type = 'page-block-fourth';
                $page_block->save();
                array_push($page_blocks,$page_block);
            }
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        return view('page.section.generate',[
            'section'=>$section,
            'page_blocks'=>$page_blocks,
        ]);
    }

    // Show form to add an element to a page-block
    public function addElement(Request $request, $block_id)
    {
        return view('page.element.create',[
            'block_id'=>$block_id,
        ]);
    }

    // Add an element to given page-block
    public function addElementToBlock(Request $request, $block_id)
    {
        // Validate fields
        $validator = Validator::make($request->all(), [
            'type' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);
        try {
            $element = new PageElement;
            $element->type = $request->input('type');
            $element->html = $request->input('html');
            $element->class = $request->input('class');
            $element->url = $request->input('url');
            $element->image = $request->input('image');
            $element->overlay = $request->input('overlay');
            $element->parent_id = $block_id;
            $block = PageElement::findOrFail($block_id);
            $element->page_id = $block->page_id;
            $element->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
        return response()->json([
            'refresh' => 'true',
            'success' => 'Element added...reloading',
        ]);
    }

    private function isSubdomain($url) {
        $parsed = parse_url($url);
        $exploded = explode('.', $parsed["host"]);
        return (count($exploded) > 2);
    }

    /*
    |--------------------------------------------------------------------------
    | Helper methods
    |--------------------------------------------------------------------------
    */
    
    // Sets a session value
    public function setSessionValue(Request $request) {
        // dd($request->input('success'));
        if ($request->input('success') !== null) {
            session(['success' => $request->input('success')]);
        }
        // foreach ($request->input() as $key => $value) {
        //     session([$key => $value]);
        // }
    }
    
    /*
    |--------------------------------------------------------------------------
    | Reference methods for standard form
    |--------------------------------------------------------------------------
    */

    /**
     * Create example record
     *
     * @param request $request
     * @return redirect
     *
     */
    public function storeExample(Request $request) 
    {
        // Require fields
        $validator = Validator::make($request->all(), [
            'type' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        // Create example record
        try {
            $example = new Example;
            $example->type = $request->type;
            $example->save();
        } catch(Exception $e){
            return Redirect::back()->withInput()->withErrors($e->getMessage());
        }
        // Send back with success message
        return Redirect::back()->withInput()->withSuccess('Successfully added');
    }

    /**
     * Update example record
     *
     * @param request $request
     * @param integer $id
     * @return redirect
     *
     */
    public function updateExample(Request $request, $id) 
    {
        // Require fields
        $validator = Validator::make($request->all(), [
            'type' => 'bail|required',
        ]);
        // Return validation error
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        // Update example record
        try {
            $example = Example::findOrFail($id);
            $example->type = $request->type;
            $example->save();
        } catch(Exception $e){
            return Redirect::back()->withInput()->withErrors($e->getMessage());
        }

        // Send back with success message
        return Redirect::back()->withInput()->withSuccess('Successfully updated');
    }


    /**
     * Show form to edit example record
     *
     * @param integer $id
     * @return view
     *
     */
    public function editExample($id) 
    {
        $param = Example::all();

        return view('folder.blade',[
            'param' => $param,
        ]);
    }

    /*
    |--------------------------------------------------------------------------
    | Reference methods for [data-ajax-form]
    |--------------------------------------------------------------------------
    */

    /**
     * Create example record
     *
     * @param request $request
     * @return redirect
     *
     */
    public function storeExampleAjax(Request $request) 
    {
        // Require fields
        $validator = Validator::make($request->all(), [
            'type' => 'bail|required',
        ]);

        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        // Create example record
        try {
            $example = new Example;
            $example->type = $request->type;
            $example->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Return json response
        return response()->json([
            'success' => 'Successfully added',
            'refresh' => true,
        ]); 
    }


    /**
     * Update example record
     *
     * @param request $request
     * @param integer $id
     * @return redirect
     *
     */
    public function updateExampleAjax(Request $request, $id) 
    {
        // Require fields
        $validator = Validator::make($request->all(), [
            'type' => 'bail|required',
        ]);

        // Return validation error
        if ($validator->fails()) return response()->json([
            'error' => $validator->errors()->first(),
        ]);

        // Update example record
        try {
            $example = Example::findOrFail($id);
            $example->type = $request->type;
            $example->save();
        } catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }

        // Return json response
        return response()->json([
            'success' => 'Successfully updated',
            'refresh' => true,
        ]); 
    }
}
