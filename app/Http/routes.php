<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('pages/landing');
// });

// Route::get('ui', function () {
//     return view('reference.ui');
// });

// Route::post('/', function () {
//     return dd($_POST);
// });


// Route::group(['middlewareGroups' => 'web'], function() {
    Route::get('calendar', function () {
        return view('reservations.index');
        //
    });

    // Subdomain routes
    // Route::group(['domain' => '{subdomain}.{tld}'], function() {
        // $subdomain = Route::input('subdomain');
        // dd($subdomain);
    // Route::group(['domain' => '{domain}.{ext}'], function() {
    //     $subdomain = Route::input('subdomain');

        // Route::get('/', function ($subdomain, $domain, $ext) {
        //     dd($subdomain);
        //     dd($domain);
        //     dd($ext);
        // });

        Route::get('/',[
            'uses' => 'Controller@showLanding',
            'as' => 'pages.landing', // optional route name
            'middleware' =>'compress',
        ]);

        Route::post('/contact',[
            'uses' => 'Controller@contactNebula',
        ]);

        /*
        |--------------------------------------------------------------------------
        | Dashboard Routes
        |--------------------------------------------------------------------------
        */

        Route::get('/dashboard',[
            'uses' => 'Controller@showDashboard',
            'as' => 'dashboard', // optional route name
            'middleware' =>['auth','compress'],
            // 'middleware' => ['auth','compress'],
        ]);

        Route::get('/feed',[
            'uses' => 'Controller@showFeed',
            'as' => 'feed', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('/users',[
            'uses' => 'Controller@showUsers',
            'as' => 'users', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('/settings',[
            'uses' => 'Controller@showSettings',
            'as' => 'settings', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('/settings',[
            'uses' => 'Controller@storeSettings',
            'as' => 'settings.store', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('/settings/update',[
            'uses' => 'Controller@updateSettings',
            'as' => 'settings.update', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('/search',[
            'uses' => 'Controller@search',
            'as' => 'search', // optional route name
        ]);

        Route::get('module/add/{type}',[
            'uses' => 'Controller@addModule',
            'as' => 'module.add', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('module/add',[
            'uses' => 'Controller@storeModule',
            'as' => 'module.store', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::delete('module/{id}/delete',[
            'uses' => 'Controller@deleteModule',
            'as' => 'module.delete', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('modules/update',[
            'uses' => 'Controller@updateModules',
            'as' => 'module.update', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('module/{id}/edit',[
            'uses' => 'Controller@editModule',
            'as' => 'module.edit', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('module/{id}/update',[
            'uses' => 'Controller@updateModule',
            'as' => 'module.update', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        /*
        |--------------------------------------------------------------------------
        | Reservation Module Routes
        |--------------------------------------------------------------------------
        */

        Route::get('reservations',[
            'uses' => 'Controller@showReservationsIndex',
            'as' => 'reservations.index', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('reservations/schedule/create',[
            'uses' => 'Controller@createReservationSchedule',
            'as' => 'reservations.schedule.create', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('reservations/schedule/create',[
            'uses' => 'Controller@storeReservationSchedule',
            'as' => 'reservations.schedule.store', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('reservations/schedule/{id}',[
            'uses' => 'Controller@showReservationSchedule',
            'as' => 'reservations.schedule.show', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('reservations/schedule/{id}/edit',[
            'uses' => 'Controller@editReservationSchedule',
            'as' => 'reservations.schedule.edit', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('schedule/{slug}',[
            'uses' => 'Controller@showPublicReservationSchedule',
            'as' => 'reservations.schedule.public', // optional route name
        ]);

        Route::post('reservations/schedule/{id}/update',[
            'uses' => 'Controller@updateReservationSchedule',
            'as' => 'reservations.schedule.update', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::delete('schedule/{id}/delete',[
            'uses' => 'Controller@deleteReservationSchedule',
            'middleware' =>'auth',
        ]);

        Route::get('reservations/schedule/{id}/reservation/create',[
            'uses' => 'Controller@createReservation',
            'as' => 'reservations.reservation.create', // optional route name
        ]);

        Route::get('reservations/schedule/{id}/reservation/create/auth',[
            'uses' => 'Controller@createReservation',
            'middleware' =>['auth','compress'],
            'as' => 'reservations.reservation.create', // optional route name
        ]);

        Route::post('reservations/schedule/{reservation_schedule_id}/reservation/create',[
            'uses' => 'Controller@storeReservation',
            'as' => 'reservations.reservation.store', // optional route name
        ]);

        Route::get('reservations/schedule/{id}/reservable/create',[
            'uses' => 'Controller@createReservable',
            'as' => 'reservations.reservable.create', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('reservations/schedule/{reservation_schedule_id}/reservable/create',[
            'uses' => 'Controller@storeReservable',
            'as' => 'reservations.reservable.store', // optional route name
        ]);

        Route::get('reservations/reservable/{id}/edit',[
            'uses' => 'Controller@editReservable',
            'as' => 'reservations.reservable.edit', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('reservations/reservation/{id}/edit',[
            'uses' => 'Controller@editReservation',
            'as' => 'reservations.reservation.edit', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('reservations/reservation/{id}/update',[
            'uses' => 'Controller@updateReservation',
            'as' => 'reservations.reservation.update', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::delete('reservation/{id}/delete',[
            'uses' => 'Controller@deleteReservation',
            'as' => 'reservations.reservation.delete', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('reservations/schedule/{id}/reservables',[
            'uses' => 'Controller@showReservables',
            'as' => 'reservations.reservable.index', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('reservations/reservable/{id}/update',[
            'uses' => 'Controller@updateReservable',
            'as' => 'reservations.reservable.update', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('reservations/schedule/{id}/reservations',[
            'uses' => 'Controller@showReservations',
            'as' => 'reservations.reservation.index', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        /*
        |--------------------------------------------------------------------------
        | Files Module Routes
        |--------------------------------------------------------------------------
        */

        Route::get('files',[
            'uses' => 'Controller@showFiles',
            'as' => 'file.index', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('files/import',[
            'uses' => 'Controller@showFilesImport',
            'as' => 'file.index', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('files/import/{folder_id}',[
            'uses' => 'Controller@showFilesImport',
            'as' => 'file.index', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('account/files',[
            'uses' => 'Controller@showAccountFiles',
            'as' => 'files.account.show', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        /**
         * Shows a view that lists folders for each user containing their uploads. Viewed from within the dashboard.
         */
        Route::get('dashboard/files/users',[
            'uses' => 'Controller@showUsersFileFolders',
            'middleware' =>['auth','compress'],
        ]);

        /**
         * Shows a view that lists files for a given user id containing their uploads. Viewed from within the dashboard.
         */
        Route::get('dashboard/users/{id}/files',[
            'uses' => 'Controller@showUsersFiles',
            'middleware' =>['auth','compress'],
        ]);


        // /**
        //  * Shows files that are not in a folder for the authorized user from the dashboard
        //  */
        // Route::get('dashboard/files/uncategorized',[
        //     'uses' => 'Controller@showDashboardUncategorizedFiles',
        //     'middleware' =>['auth','compress'],
        // ]);

        /**
         * Shows uncategorized files for the authorized user on the dashboard 
         */
        Route::get('dashboard/files/uncategorized',[
            'uses' => 'Controller@showDashboardUncategorizedFiles',
            'middleware' =>['auth','compress'],
        ]);


        /**
         * Shows shared files for the authorized user on the dashboard 
         */
        Route::get('dashboard/files/shared',[
            'uses' => 'Controller@showDashboardSharedFiles',
            'middleware' =>['auth','compress'],
        ]);

        /**
         * Shows uncategorized shared files for the authorized user on the dashboard 
         */
        Route::get('files/dashboard/shared/uncategorized',[
            'uses' => 'Controller@showDashboardUncategorizedSharedFiles',
            'middleware' =>['auth','compress'],
        ]);

        Route::get('account/shared/files',[
            'uses' => 'Controller@showAccountSharedFiles',
            'middleware' =>['auth','compress'],
        ]);

        Route::get('file/{id}/download',[
            'uses' => 'Controller@downloadFile',
            'as' => 'file.download', // optional route name
            'middleware' =>['auth'], // require authentication
        ]);

        Route::delete('file/{id}/delete',[
            'uses' => 'Controller@deleteFile',
            'as' => 'file.delete', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('file/{id}/edit',[
            'uses' => 'Controller@editFile',
            'as' => 'file.edit', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('file/{id}/update',[
            'uses' => 'Controller@updateFile',
            'as' => 'file.update', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('file/{id}/share',[
            'uses' => 'Controller@showShareFile',
            'as' => 'file.share.show', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('file/{id}/share',[
            'uses' => 'Controller@shareFile',
            'as' => 'file.share', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::delete('file/{id}/delete',[
            'uses' => 'Controller@deleteFile',
            'as' => 'file.delete', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('files/list',[
            'uses' => 'Controller@showUserFiles',
            'as' => 'user.file.list', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('files/folder/{id}/share',[
            'uses' => 'Controller@showShareFilesFolder',
            'as' => 'file.folder.share.show', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('files/folder/{id}/share',[
            'uses' => 'Controller@shareFilesFolder',
            'as' => 'file.folder.share', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('files/import',[
            'uses' => 'Controller@importFiles',
            'as' => 'file.index', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('files/folder/create',[
            'uses' => 'Controller@createFilesFolder',
            'as' => 'file.folder.create', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('files/folder/{id}',[
            'uses' => 'Controller@showFilesFolder',
            'as' => 'file.folder.show', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        // View from account shares read only
        Route::get('files/folder/{id}/files',[
            'uses' => 'Controller@showFilesFolderFiles',
            'as' => 'file.folder.files.show', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('files/folder/store',[
            'uses' => 'Controller@storeFilesFolder',
            'as' => 'file.folder.store', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::get('files/folder/{id}/edit',[
            'uses' => 'Controller@editFilesFolder',
            'as' => 'file.folder.edit', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('files/folder/{id}/update',[
            'uses' => 'Controller@updateFilesFolder',
            'as' => 'file.folder.update', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::delete('files/folder/{id}/delete',[
            'uses' => 'Controller@deleteFilesFolder',
            'as' => 'file.folder.delete', // optional route name
            'middleware' =>['auth','compress'],
        ]);


        /*
        |--------------------------------------------------------------------------
        | Organization Manager Routes
        |--------------------------------------------------------------------------
        */

        Route::get('manager/create',[
            'uses' => 'Controller@createManager',
            'as' => 'manager.create', // optional route name
        ]);

        Route::post('manager/search',[
            'uses' => 'Controller@searchOrganizationManagers',
            'as' => 'search.managers', // optional route name
        ]);

        Route::post('manager/search/add',[
            'uses' => 'Controller@searchToAddOrganizationManagers',
            'as' => 'search.managers', // optional route name
        ]);

        Route::post('manager/{user_id}/add',[
            'uses' => 'Controller@addManager',
            'as' => 'manager.add', // optional route name
            'middleware' =>['auth','compress'],
        ]);

        Route::post('manager/{id}/toggle',[
            'uses' => 'Controller@toggleManagerStatus',
            'as' => 'manager.toggle', // optional route name
            'middleware' =>['auth','compress'],
        ]);

    /*
    |--------------------------------------------------------------------------
    | Page Routes
    |--------------------------------------------------------------------------
    */

    // Landing page
    Route::get('/',[
        'uses' => 'Controller@showLanding',
        'as' => 'pages.landing', // optional route name
        'middleware' =>'compress',
    ]);

    // Reference
    Route::get('reference/ui',[
        'uses' => 'Controller@showReferenceUI',
        'middleware' =>['auth','compress'],
    ]);



    /*
    |--------------------------------------------------------------------------
    | Uploader Routes
    |--------------------------------------------------------------------------
    */

    Route::get('uploader/images',[
        'uses' => 'Controller@chooseImage',
        'middleware' =>['auth','compress'],
    ]);

    Route::post('uploader/images',[
        'uses' => 'Controller@storeImage',
        'middleware' =>['auth'], // require authentication
    ]);

    Route::get('uploader',[
        'uses' => 'Controller@chooseFile',
        'middleware' =>['auth','compress'],
    ]);

    Route::post('uploader',[
        'uses' => 'Controller@storeUpload',
        'middleware' =>['auth'], // require authentication
    ]);

    Route::get('uploads/user/{user_id}/{filename}',[
        'uses' => 'Controller@showFile',
        // 'middleware' =>['auth','compress'],
    ]);
    /*
    |--------------------------------------------------------------------------
    | User Routes
    |--------------------------------------------------------------------------
    */

    // Display user search results
    Route::post('users/search',[
        'uses' => 'Controller@searchUsers',
        'middleware' =>['auth','compress'],
        // 'as' => 'users.search', // optional route name
    ]);

    // Create user record
    Route::get('user/create',[
        'uses' => 'Controller@createUser',
        'middleware' =>['auth','compress'],
    ]);

    Route::post('user/create',[
        'uses' => 'Controller@storeUser',
        'middleware' =>['auth','compress'],
    ]);

    // Show user record
    // Route::get('user/{id}',[
    //     'uses' => 'Controller@showUser',
    //     'middleware' =>['auth','compress'],
    //     // 'as' => 'user.show', // optional route name
    // ]);

    // Update user record
    Route::post('user/{id}',[
        'uses' => 'Controller@updateUser',
        'middleware' =>['auth','compress'],
        // 'as' => 'user.update', // optional route name
    ]);

    // Display form to edit user record
    Route::get('user/{id}/edit',[
        'uses' => 'Controller@editUser',
        'middleware' =>['auth','compress'],
        // 'as' => 'user.edit', // optional route name
    ]);

    // Update the user record
    Route::post('user/{id}/update',[
        'uses' => 'Controller@updateUser',
        'middleware' =>['auth','compress'],
    ]);

    Route::delete('user/{id}/delete',[
        'uses' => 'Controller@deleteUser',
        'middleware' =>['auth','compress'],
    ]);

    // Edit current user record
    Route::get('account',[
        'uses' => 'Controller@showAccount',
        'middleware' =>['auth','compress'],
        'as' => 'account.show', // optional route name
    ]);

    // Update current user record
    Route::post('account',[
        'uses' => 'Controller@updateAuthorizedUser',
        'middleware' =>['auth','compress'],
        // 'as' => 'account.update', // optional route name
    ]);

    /*
    |--------------------------------------------------------------------------
    | Error Routes
    |--------------------------------------------------------------------------
    */

    Route::get('errors',[
        'uses' => 'Controller@showError',

        'as' => 'error.show', // optional route name
    ]);

    /*
    |--------------------------------------------------------------------------
    | Billing Routes
    |--------------------------------------------------------------------------
    */

    Route::get('subscription/edit',[
        'uses' => 'Controller@editSubscription',
        'middleware' =>['auth','compress'],
        'as' => 'subscription.edit', // optional route name
    ]);

    Route::post('subscription/update',[
        'uses' => 'Controller@updateSubscription',
        'middleware' =>['auth','compress'],
        'as' => 'subscription.update', // optional route name
    ]);

    Route::post('subscription/{id}/cancel',[
        'uses' => 'Controller@cancelStripeSubscription',
        'as' => 'subscription.cancel', // optional route name
    ]);

    // Authentication routes
    Route::auth();

    /*
    |--------------------------------------------------------------------------
    | Form Module Routes
    |--------------------------------------------------------------------------
    */

    Route::get('forms',[
        'uses' => 'Controller@showForms',
        'as' => 'forms.index', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('form/create',[
        'uses' => 'Controller@createForm',
        'as' => 'form.create', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('forms/{slug}',[
        'uses' => 'Controller@showFormPage',
        'as' => 'form.show', // optional route name
        'middleware' =>['compress'],

    ]);

    Route::post('forms/{slug}',[
        'uses' => 'Controller@storeFormResponse',
        'as' => 'form.response.store', // optional route name
    ]);

    Route::post('form/create',[
        'uses' => 'Controller@storeForm',
        'as' => 'form.store', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('form/{id}/field/create',[
        'uses' => 'Controller@createField',
        'as' => 'form.field.create', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('field/edit',[
        'uses' => 'Controller@editField',
        'as' => 'form.field.edit', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('form/{id}/field/create',[
        'uses' => 'Controller@storeField',
        'as' => 'form.field.store', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('field/generate',[
        'uses' => 'Controller@generateField',
        'as' => 'form.field.generate', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('form/{id}/edit',[
        'uses' => 'Controller@editForm',
        'as' => 'form.store', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('form/{id}/alerts',[
        'uses' => 'Controller@updateFormAlerts',
        'as' => 'form.alerts.update', // optional route name
        'middleware' =>['auth'], // require authentication
    ]);

    Route::get('form/{id}/alerts',[
        'uses' => 'Controller@showFormAlerts',
        'as' => 'form.alerts', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('form/{id}/edit',[
        'uses' => 'Controller@updateForm',
        'as' => 'form.update', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::delete('form/{id}/delete',[
        'uses' => 'Controller@deleteForm',
        'as' => 'form.delete', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('form/responses',[
        'uses' => 'Controller@showFormResponseSets',
        'as' => 'form.responses.index', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('form/{form_id}/{form_version}/responses/export',[
        'uses' => 'Controller@exportFormResponses',
        'as' => 'form.responses.export', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('form/{form_slug}/{form_version}/responses',[
        'uses' => 'Controller@showFormResponses',
        'as' => 'form.responses.index', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('form/response/{id}',[
        'uses' => 'Controller@showFormResponse',
        'as' => 'form.response.index', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::delete('form/response/{id}/delete',[
        'uses' => 'Controller@deleteFormResponse',
        'as' => 'form.response.delete', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    /*
    |--------------------------------------------------------------------------
    | CRM Module Routes
    |--------------------------------------------------------------------------
    */

    Route::get('crm',[
        'uses' => 'Controller@showCRM',
        'as' => 'crm.index',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('crm/preferences',[
        'uses' => 'Controller@showCrmPreferences',
        'middleware' =>['auth','compress'],
    ]);

    Route::post('crm/preferences',[
        'uses' => 'Controller@updateCrmPreferences',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('crm/{pipeline_key}',[
        'uses' => 'Controller@showCRM',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('crm/{pipeline_key}/export',[
        'uses' => 'Controller@exportCRM',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('crm/{pipeline_key}/import',[
        'uses' => 'Controller@showImportCRM',
        'middleware' =>['auth','compress'],
    ]);

    Route::post('crm/{pipeline_key}/import',[
        'uses' => 'Controller@importCRM',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('crm/record/{pipeline_key}/create',[
        'uses' => 'Controller@createCrmRecord',
        'middleware' =>['auth','compress'],
    ]);

    Route::post('crm/record/{pipeline_key}/create',[
        'uses' => 'Controller@storeCrmRecord',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('crm/records/{id}/edit',[
        'uses' => 'Controller@editCrmRecord',
        'middleware' =>['auth','compress'],
    ]);

    Route::delete('crm/record/{id}/delete',[
        'uses' => 'Controller@deleteCrmRecord',
        'middleware' =>['auth','compress'],
    ]);

    Route::post('crm/record/{id}/update',[
        'uses' => 'Controller@updateCrmRecord',
        'middleware' =>['auth','compress'],
    ]);

    Route::delete('crm/{pipeline_key}/clear',[
        'uses' => 'Controller@clearCrmRecords',
        'middleware' =>['auth','compress'],
    ]);

    /*
    |--------------------------------------------------------------------------
    | Payment Module Routes
    |--------------------------------------------------------------------------
    */

    Route::get('payments/setup',[
        'uses' => 'Controller@redirectToSetupPayments',
        'as' => 'payment.setup.redirect',
    ]);

    Route::get('payments/setup/{stripe_id}',[
        'uses' => 'Controller@setupPayments',
        'as' => 'payment.setup',
    ]);

    Route::get('payments',[
        'uses' => 'Controller@showPayments',
        'as' => 'payment.index',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payment/create',[
        'uses' => 'Controller@createPayment',
        'as' => 'payment.create',
    ]);

    Route::get('charge/{id}/view',[
        'uses' => 'Controller@viewStripeCharge',
        'as' => 'stripe.charge.view',
    ]);

    Route::get('stripe/customer/{id}',[
        'uses' => 'Controller@viewStripeCustomer',
        'as' => 'stripe.customer.view',
    ]);

    Route::post('payment/create/dashboard',[
        'uses' => 'Controller@createStripeChargeFromDashboard',
        'as' => 'payment.dashboard.stripe.charge.create',
    ]);

    Route::post('payment/create',[
        'uses' => 'Controller@createStripeChargeFromPaymentForm',
        'as' => 'payment.stripe.charge.create',
    ]);

    Route::get('payments/customers',[
        'uses' => 'Controller@showCustomers',
        'as' => 'payment.customer.index',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payments/scheduled',[
        'uses' => 'Controller@showScheduledPayments',
        'as' => 'payments.scheduled.index',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payments/forms',[
        'uses' => 'Controller@showPaymentForms',
        'as' => 'payments.form.index',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payment/form/{id}/edit',[
        'uses' => 'Controller@editPaymentForm',
        'as' => 'payment.form.edit', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('payment/form/{id}/edit',[
        'uses' => 'Controller@updatePaymentForm',
        'as' => 'payment.form.update', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payment/form/create',[
        'uses' => 'Controller@createPaymentForm',
        'as' => 'payment.form.create', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payment/{id}/view',[
        'uses' => 'Controller@showPayment',
        'as' => 'payment.show', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::delete('payment/{id}/delete',[
        'uses' => 'Controller@cancelPayment',
        'as' => 'payment.cancel', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payment/forms/{slug}',[
        'uses' => 'Controller@showPaymentFormPage',
        'as' => 'payment.form.show', // optional route name
    ]);

    Route::post('payment/form/create',[
        'uses' => 'Controller@storePaymentForm',
        'as' => 'payment.form.store', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payment/form/{id}/field/create',[
        'uses' => 'Controller@createPaymentField',
        'as' => 'payment.field.create', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payments/invoices',[
        'uses' => 'Controller@showInvoices',
        'as' => 'payments.invoice.index',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payments/customers',[
        'uses' => 'Controller@showCustomers',
        'as' => 'payments.customer.index',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payments/subscriptions',[
        'uses' => 'Controller@showSubscriptions',
        'as' => 'payments.subscription.index',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('subscription/{subscription_id}/manage',[
        'uses' => 'Controller@showManageStripeSubscription',
        'as' => 'payment.subscription.manage.show',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payment/subscription/create',[
        'uses' => 'Controller@showCreateStripeSubscription',
        'as' => 'payments.stripe.subscription.create.show',
        'middleware' =>['auth','compress'],
    ]);

    Route::post('payment/subscription/create',[
        'uses' => 'Controller@createStripeSubscription',
        'as' => 'payments.stripe.subscription.create',
    ]);

    Route::get('stripe/subscription/{id}',[
        'uses' => 'Controller@viewStripeSubscription',
        'as' => 'payments.stripe.subscription.view',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payment/customer/create',[
        'uses' => 'Controller@showCreateStripeCustomer',
        'as' => 'payments.customer.create.show',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payment/plan/create',[
        'uses' => 'Controller@showCreateStripePlan',
        'as' => 'payment.plan.create.show',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('stripe/plan/{plan_id}',[
        'uses' => 'Controller@viewStripePlan',
        'as' => 'payment.plan.view',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payment/subscription/{subscription_id}/confirm',[
        'uses' => 'Controller@viewSubscriptionConfirmation',
        'as' => 'payment.subscription.confirmation',
    ]);

    Route::post('payment/plan/create',[
        'uses' => 'Controller@createStripePlan',
        'as' => 'payment.plan.create',
        'middleware' =>['auth','compress'],
    ]);

    // Lookup customer list from Stripe
    Route::post('stripe/customers/find',[
        'uses' => 'Controller@findStripeCustomer',
        'as' => 'payments.stripe.customer.find',
        'middleware' =>['auth','compress'],
    ]);

    Route::post('payment/customer/create',[
        'uses' => 'Controller@createStripeCustomer',
        'as' => 'payments.customer.create',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payment/{charge_id}/confirmation',[
        'uses' => 'Controller@showStripeCharge',
        'as' => 'payment.charge.confirmation',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('payments/plans',[
        'uses' => 'Controller@showPaymentPlans',
        'as' => 'payments.plan.index',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('plan/create',[
        'uses' => 'Controller@createPlan',
        'as' => 'payments.plan.create',
        'middleware' =>['auth','compress'],
    ]);

    Route::delete('payment/form/{id}/delete',[
        'uses' => 'Controller@deletePaymentForm',
        'as' => 'payment.form.delete', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('invoice/create',[
        'uses' => 'Controller@createInvoice',
        'as' => 'payment.invoice.create',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('invoice/settings',[
        'uses' => 'Controller@showInvoiceSettings',
        'as' => 'payment.invoice.settings',
        'middleware' =>['auth','compress'],
    ]);

    Route::post('invoice/settings/update',[
        'uses' => 'Controller@updateInvoiceSettings',
        'as' => 'payment.invoice.settings.update',
        'middleware' =>['auth','compress'],
    ]);

    Route::post('invoice/store',[
        'uses' => 'Controller@storeInvoice',
        'as' => 'payment.invoice.store',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('invoice/item/generate',[
        'uses' => 'Controller@addInvoiceItem',
        'as' => 'payment.invoice.item.add',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('invoice/{id}/edit',[
        'uses' => 'Controller@editInvoice',
        'as' => 'payment.invoice.edit',
        'middleware' =>['auth','compress'],
    ]);

    Route::post('invoice/{id}/update',[
        'uses' => 'Controller@updateInvoice',
        'as' => 'payment.invoice.update',
        'middleware' =>['auth','compress'],
    ]);

    Route::get('invoice/{id}/download',[
        'uses' => 'Controller@downloadInvoice',
        'as' => 'payment.invoice.download',
        'middleware' =>['auth'],
    ]);

    Route::get('invoice/{id}/share',[
        'uses' => 'Controller@showShareInvoice',
        'as' => 'payment.invoice.share.show',
        'middleware' =>['auth','compress'],
    ]);

    Route::post('invoice/{id}/share',[
        'uses' => 'Controller@shareInvoice',
        'as' => 'payment.invoice.share',
        'middleware' =>['auth','compress'],
    ]);
    /*
    |--------------------------------------------------------------------------
    | Record Module Routes
    |--------------------------------------------------------------------------
    */

    Route::get('record_set/{module_id}/create',[
        'uses' => 'Controller@createRecordSet',
        'as' => 'record_set.create', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('record_set/store',[
        'uses' => 'Controller@storeRecordSet',
        'as' => 'record_set.store', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('records/{module_id}/import',[
        'uses' => 'Controller@showRecordImport',
        'as' => 'record.import.show', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    // Route::post('records/import',[
    //     'uses' => 'Controller@importRecords',
    //     'as' => 'record.import', // optional route name
    //     'middleware' =>['auth','compress'],
    // ]);

    Route::get('record/{id}/view',[
        'uses' => 'Controller@showRecord',
        'as' => 'records.record.view', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('records/{module_slug}',[
        'uses' => 'Controller@showRecords',
        'as' => 'record.show', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('records/{record_set_id}/export',[
        'uses' => 'Controller@exportRecords',
        'as' => 'records.export', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('record/{record_set_id}/store',[
        'uses' => 'Controller@storeRecord',
        'as' => 'records.store', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('records/{module_slug}/{record_set_id}',[
        'uses' => 'Controller@showRecords',
        'as' => 'record_set.show', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('records/{module_slug}/{record_set_id}/shared',[
        'uses' => 'Controller@showSharedRecords',
        'as' => 'record_set.show.shared', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('records/{module_id}/import',[
        'uses' => 'Controller@importRecords',
        'as' => 'records.import', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('record/{record_set_id}/create',[
        'uses' => 'Controller@createRecord',
        'as' => 'record.create', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::delete('record/{id}/delete',[
        'uses' => 'Controller@deleteRecord',
        'as' => 'record.delete', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('record/{id}/edit',[
        'uses' => 'Controller@editRecord',
        'as' => 'record.edit', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('record/{id}/update',[
        'uses' => 'Controller@updateRecord',
        'as' => 'record.update', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('record_set/{id}/edit',[
        'uses' => 'Controller@editRecordSet',
        'as' => 'record_set.edit', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('record_set/{id}/share',[
        'uses' => 'Controller@showShareRecordSet',
        'as' => 'record_set.share.show', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('record_set/{id}/share',[
        'uses' => 'Controller@shareRecordSet',
        'as' => 'record_set.share', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('record_set/{id}/update',[
        'uses' => 'Controller@updateRecordSet',
        'as' => 'record_set.update', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::delete('record_set/{id}/delete',[
        'uses' => 'Controller@deleteRecordSet',
        'as' => 'record_set.delete', // optional route name
        'middleware' =>['auth','compress'],
    ]);
    // Route::get('records/{id}',[
    //     'uses' => 'Controller@showRecords',
    //     'as' => 'record.show', // optional route name
    //     'middleware' =>['auth','compress'],
    // ]);

    /*
    |--------------------------------------------------------------------------
    | Links Module Routes
    |--------------------------------------------------------------------------
    */

    Route::get('links',[
        'uses' => 'Controller@showLinks',
        'as' => 'links.index', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('link',[
        'uses' => 'Controller@storeLink',
        'as' => 'link.store', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('link/create',[
        'uses' => 'Controller@createLink',
        'as' => 'link.create', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('link/{id}/edit',[
        'uses' => 'Controller@editLink',
        'as' => 'link.edit', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('link/{id}/edit',[
        'uses' => 'Controller@updateLink',
        'as' => 'link.update', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::delete('link/{id}/delete',[
        'uses' => 'Controller@deleteLink',
        'as' => 'link.delete', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    /*
    |--------------------------------------------------------------------------
    | Page Module Routes
    |--------------------------------------------------------------------------
    */

    Route::get('pages',[
        'uses' => 'Controller@showPages',
        'as' => 'module.pages.index', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('page/create',[
        'uses' => 'Controller@storePage',
        'as' => 'page.store', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('page/{id}/editor',[
        'uses' => 'Controller@pageEditor',
        'as' => 'page.editor', // optional route name
        'middleware' =>['auth','compress'],
    ]);


    Route::post('justify/{id}',[
        'uses' => 'Controller@justifyElementText',
        'as' => 'page.justify.element', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    // Route::get('pages/{id}/edit',[
    //     'uses' => 'Controller@editPage',
    //     'as' => 'page.edit', // optional route name
    //     'middleware' =>['auth','compress'],
    // ]);

    Route::get('pages/settings',[
        'uses' => 'Controller@showPagesSettings',
        'as' => 'pages.settings', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('pages/settings',[
        'uses' => 'Controller@updatePagesSettings',
        'as' => 'pages.update', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('page/{id}/settings',[
        'uses' => 'Controller@editPage',
        'as' => 'page.settings', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('page/templates',[
        'uses' => 'Controller@showPageTemplates',
        'as' => 'page.templates', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('page/{id}/settings',[
        'uses' => 'Controller@updatePage',
        'as' => 'page.update', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::delete('page/{id}/delete',[
        'uses' => 'Controller@deletePage',
        'as' => 'page.delete', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('page/create',[
        'uses' => 'Controller@createPage',
        'as' => 'page.create', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('page/template/create',[
        'uses' => 'Controller@createPageTemplate',
        'as' => 'page.template.create', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('{page_slug}',[
        'uses' => 'Controller@showPage',
        'as' => 'page.index', // optional route name
        //'middleware' =>['auth','compress'],
    ]);

    // Preview page as non manager
    Route::get('{page_slug}/preview',[
        'uses' => 'Controller@showPage',
        'as' => 'page.index', // optional route name
        'middleware' =>'compress', // require authentication
    ]);

    Route::get('{page_slug}/edit',[
        'uses' => 'Controller@editPage',
        'as' => 'page.edit', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    // Edit a page element
    Route::get('element/{element_id}/edit',[
        'uses' => 'Controller@editElement',
        'as' => 'page.element.edit', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::delete('element/{element_id}/delete',[
        'uses' => 'Controller@deleteElement',
        'as' => 'page.element.delete', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    // Update page element order
    Route::post('element/{element_id}/order',[
    'uses' => 'Controller@updateElementOrder',
        'as' => 'page.element.order', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    // Update page element
    Route::post('element/{element_id}/update',[
        'uses' => 'Controller@updateElement',
        'as' => 'page.element.update', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    // Update page element from editor
    Route::post('page/element/{element_id}/update',[
        'uses' => 'Controller@updatePageElement',
        'as' => 'page.element.update', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('block/{id}/element/add',[
        'uses' => 'Controller@addPageElement',
        'as' => 'page.element.add', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('block/{id}/element/{type}/generate',[
        'uses' => 'Controller@generatePageElement',
        'as' => 'page.element.generate', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('block/{id}/element/add/{type}',[
        'uses' => 'Controller@createPageElement',
        'as' => 'page.element.add', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::get('block/{block_id}/add',[
        'uses' => 'Controller@addElement',
        'as' => 'page.element.add', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('block/{block_id}/add',[
        'uses' => 'Controller@addElementToBlock',
        'as' => 'page.element.add', // optional route name
        'middleware' =>['auth','compress'],
    ]);

    Route::post('section/generate',[
        'uses' => 'Controller@generateSection',
        'as' => 'page.section.generate', // optional route name
        'middleware' =>['auth','compress'],
    ]);


    /*
    |--------------------------------------------------------------------------
    | Helper Routes
    |--------------------------------------------------------------------------
    */
    Route::post('session',[
        'uses' => 'Controller@setSessionValue',
        'as' => 'session.set', // optional route name
        'middleware' =>['auth','compress'],
    ]);


// });

