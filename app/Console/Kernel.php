<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use Mail;
use App\Payment;
use App\Setting;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Payments schedule to charge any payments that are due to be charged on the current date. Run daily as a cron task.
        $this->schedule = $schedule;
        $schedule->call('App\Http\Controllers\Controller@chargeScheduledPayment')
            ->daily()->evenInMaintenanceMode();
            // ->everyMinute();
        // $this->schedule->call(function() {
        //     // Select scheduled payments
        //     $scheduled_payments = Payment::where('recurring',1)->get();
        //     if (count($scheduled_payments) > 0) {
        //         foreach ($scheduled_payments as $scheduled_payment) {
        //             // If payment next_charge matches todays date
        //             if($scheduled_payment->next_charge == date('Y-m-d')) {
        //                 // Update the number of days until the next charge
        //                 $scheduled_payment->next_charge =  date('Y-m-d',$scheduled_payment->next_charge + strtotime("+$scheduled_payment->interval days")); // Add days interval to date to calculate next charge date
        //                 $scheduled_payment->last_charge =  date('Y-m-d'); // last charge is today
        //                 $scheduled_payment->save();
        //                 // Set Stripe key
        //                 dd($stripe_secret);
        //                 \Stripe\Stripe::setApiKey($stripe_secret);
        //                 // Create Stripe charge for cutomer id on file
        //                 $stripe_charge = \Stripe\Charge::create(array(
        //                   "currency" => "usd",
        //                   "customer" => $scheduled_payment->stripe_customer_id,
        //                   "amount" => $scheduled_payment->amount * 100, // amount in cents
        //                   'description' => $scheduled_payment->description . ' ('. $scheduled_payment->email.')',
        //                   'destination' => Setting::firstOrFail()->stripe_id, // to connected account
        //                 ));
        //                 // Store a one time payment record for the charge
        //                 $payment = new Payment;
        //                 $payment->stripe_customer_id = $scheduled_payment->stripe_customer_id;
        //                 $payment->amount = $scheduled_payment->amount;
        //                 $payment->email = $scheduled_payment->email;
        //                 $payment->description = $scheduled_payment->description;
        //                 $payment->first_charge =  date('Y-m-d', $stripe_charge->created);
        //                 $payment->last_charge = date('Y-m-d'); // today
        //                 $payment->next_charge = NULL; // NULL as this is recording the one time payment for the day
        //                 $payment->save();

        //                 // TODO: send email reciept
        //             }
        //         }
        //     } // (count($scheduled_payments) > 0)
            // $mail = Mail::send('emails.default', [
            //         'subject'=>'A folder was shared with you at '.env('APP_DOMAIN'),
            //         'content' => 'dfx'
            // ], function ($mail) {
            //         $mail->from('support@nebulasuite.com', 'Schedule Test');
            //         $mail->to('geoffmilburn@gmail.com', 'Geoff Milburn');
            //         $mail->subject('Hello');
            // });
        // })->everyMinute(); // use everyMinute for testing with php artisan schedule:run
        // })->daily();
    }
}
