<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Session;
use Symfony\Component\HttpKernel\Exception\ErrorException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        // if ($e instanceof  \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
        //     // dd('404 error');
        //     return response()->view('errors.404',['error'=>$e], 404);
        // }

        // Custom token mismatch response
        if ($e instanceof TokenMismatchException) {
            // token mismatch (usually caused by session a timeout)
            if (!$request->ajax()) {
                return redirect('/')->with('error', 'You page session expired. Please try again.');
            } else {
                // Return json response
                return response()->json([
                    'error' => 'You page session expired. Please try again.',
                    'redirect' => url('dashboard'),
                ]); 
            }
        } elseif ($e instanceof  \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            // 404 not found
            return response()->view('errors.404',['error'=>$e], 404);
        } else { 
            // Is there an error in the session? If so send back and it should be automatically displayed
            if (session('errors') !== null) return redirect()->back();
            // all other errors
            return response()->view('errors.500',['error'=>$e], 500);
        }

        // Custom FatalErrorException response
        // if($e instanceof \Symfony\Component\Debug\Exception\FatalErrorException || $e instanceof \ErrorException) {
        //     // return redirect('/errors');
        //     return response()->view('errors.500',['error'=>$e], 500);
        //     // return View::make('errors.500',['error'=>$e]);
        // }

        // Below line is default by laravel commented out by GDM Dec 2016
        // return parent::render($request, $e);
    }
}
