<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrmRecord extends Model
{
    public function crm()
    {
        return $this->hasOne('App\Crm','id','1');
    }

    public function module()
    {
        return $this->belongsTo('App\Module','slug','crm');
    }
}
