<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationSchedule extends Model
{
    public function reservations()
    {
        return $this->hasMany('App\Reservation');
    }

    public function reservables()
    {
        return $this->hasMany('App\Reservable');
    }
}
