<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentForm extends Model
{
    public function fields()
    {
        return $this->hasMany('App\PaymentFormItem');
    }
}
