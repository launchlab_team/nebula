<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function form()
    {
        return $this->hasOne('App\PaymentForm','id','payment_form_id');
    }
}
