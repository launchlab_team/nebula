<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservable extends Model
{
    public function reservation_schedule()
    {
        return $this->hasMany('App\ReservationSchedule','id','reservation_schedule_id');
        // return $this->hasMany('App\ReservationSchedule','id','reservation_schedule_id');
    }

    public function reservation()
    {
        return $this->hasOne('App\Reservation','id');
    }
}
