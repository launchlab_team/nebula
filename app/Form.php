<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    public function fields()
    {
        return $this->hasMany('App\FormItem');
    }

    public function responses()
    {
        return $this->hasMany('App\FormResponse');
    }

    public function recipients()
    {
        return $this->hasMany('App\FormAlert');
    }
}
