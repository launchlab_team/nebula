<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileFolder extends Model
{
    public function files()
    {
        return $this->hasMany('App\Upload','folder_id');
    }
}
