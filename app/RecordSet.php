<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecordSet extends Model
{
    public function module()
    {
        return $this->belongsTo('App\Module');
    }

    public function records()
    {
        return $this->hasMany('App\Record');
    }
}
