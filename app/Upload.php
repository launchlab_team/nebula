<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function folder()
    {
        return $this->belongsTo('App\FileFolder','id');
    }

    public function file_shares()
    {
        return $this->hasMany('App\FileShare','file_id');
    }
}
