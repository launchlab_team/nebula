<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    public function reservable()
    {
        return $this->hasOne('App\Reservable','id','reservable_id');
    }

    public function schedule()
    {
        return $this->hasOne('App\ReservationSchedule','id','reservation_schedule_id');
    }
}
