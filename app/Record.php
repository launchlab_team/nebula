<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    public function record_set()
    {
        return $this->belongsTo('App\RecordSet');
    }
}
