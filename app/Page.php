<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

    protected $hidden = [
        'wasRecentlyCreated',
    ];

    public function module()
    {
        return $this->belongsTo('App\Module');
    }

    public function elements()
    {
        return $this->hasMany('App\PageElement');
    }
}
