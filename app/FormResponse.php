<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormResponse extends Model
{
    public function form()
    {
        return $this->hasOne('App\Form','id','form_id');
    }
}
