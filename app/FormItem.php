<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormItem extends Model
{
    public function form()
    {
        return $this->hasOne('App\Models\Form');
    }
}
