<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public function pages()
    {
        return $this->hasMany('App\Page');
    }

    public function record_sets()
    {
        return $this->hasMany('App\RecordSet');
    }
}
