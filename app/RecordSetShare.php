<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecordSetShare extends Model
{
    public function record_set()
    {
        return $this->hasOne('App\RecordSet','id','record_set_id');
    }
}
