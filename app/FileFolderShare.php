<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileFolderShare extends Model
{

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function folder()
    {
        return $this->hasOne('App\FileFolder','id','folder_id');
    }

    // /**
    //  * Return shared files
    //  */
    // public function uncategorized_files()
    // {
    //     // return $this->hasOne('App\FileFolder','id','');
    //     // return Upload::where('folder_id',NULL)->get();
    //     return 0;
    // }
}
