<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormAlert extends Model
{
    public function form()
    {
        return $this->hasOne('App\Form');
    }
}
