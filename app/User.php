<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable as Billable;


class User extends Authenticatable
{
    use Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function file_folder_shares()
    {
        return $this->hasMany('App\FileFolderShare')->orderBy('created_at','DESC');
    }

    public function record_set_shares()
    {
        return $this->hasMany('App\RecordSetShare');
    }

    public function stripe_subscriptions()
    {
        return $this->hasMany('App\UserStripeSubscription')->orderBy('created_at','DESC');
    }

    public function file_shares()
    {
        // return $this->hasMany('App\FileShare')->orderBy('created_at','DESC');
        return $this->hasMany('App\FileShare')->orderBy('created_at','DESC');
        // return $this->hasMany('App\FileShare','id')->orderBy('created_at','DESC');
    }

    public function uncategorized_files()
    {
        return $this->hasMany('App\Upload','id',NULL);
    }

    public function file_folders()
    {
        return $this->hasMany('App\FileFolder');
    }

    public function files()
    {
        return $this->hasMany('App\Upload')->orderBy('created_at','DESC');
    }

    public function manager()
    {
        return $this->hasMany('App\Manager');
    }

    public function organizations()
    {
        return $this->hasMany('App\Organization');
    }

    // public function subscriptions()
    // {
    //     return $this->hasMany('App\Subscription');
    // }
}
