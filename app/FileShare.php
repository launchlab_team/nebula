<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileShare extends Model
{

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function file()
    {
        return $this->hasOne('App\Upload','id','file_id');
    }

}
