var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// Compile scss to public css folder
elixir(function(mix) {
    // // Set the asset path
    this.config.assetsPath = 'resources/assets';
    // Compile the sass to css
    mix.sass('main.scss','public/css/main.css');
});

// Mix all scripts into app.js
elixir(function(mix) {
    mix.scriptsIn('resources/assets/js','public/js/app.js');
    mix.scriptsIn('resources/assets/js/core','public/js/app.js');
    mix.scriptsIn('resources/assets/js/component','public/js/app.js');
    mix.scriptsIn('resources/assets/js/module','public/js/app.js');
    mix.scriptsIn('resources/assets/js/vendor','public/js/vendor.js');
});