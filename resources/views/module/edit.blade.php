@extends('layouts.partial')

@section('middle')
    <div class="modal">
        <form method="post" action="{{url("module/$module->id/update")}}" data-ajax-form>

            <h2 class="modal-heading">Edit Tab</h2>
                {{csrf_field()}} 
                @include('component.form-item',[
                    'name' => 'label',
                    'type' => 'text',
                    'label' => 'label',
                    'value' => $module->label,
                ])  
                @include('component.form-item',[
                    'name' => 'icon',
                    'type' => 'icons',
                    'label' => 'Choose an icon',
                    'value' => $module->icon,
                ])   
                {{-- <div class="radios icon-radio-set" data-uk-margin>
                    <div class="form-item">
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-layout">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-layout"></span></div>
                        </label>
                    </div>
                    <div class="form-item">         
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-menu">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-menu"></span></div>
                        </label>
                    </div>
                    <div class="form-item">    
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-user">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-user"></span></div>
                        </label>
                    </div>
                    <div class="form-item">         
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-unit">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-unit"></span></div>
                        </label>
                    </div>
                    <div class="form-item">         
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-payment">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-payment"></span></div>
                        </label>
                    </div>
                    <div class="form-item">         
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-lead">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-lead"></span></div>
                        </label>
                    </div>
                    <div class="form-item">         
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-list">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-list"></span></div>
                        </label>
                    </div>
                    <div class="form-item">         
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-cloud">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-cloud"></span></div>
                        </label>
                    </div>
                    <div class="form-item">         
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-image">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-image"></span></div>
                        </label>
                    </div>
                    <div class="form-item">         
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-inbox">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-inbox"></span></div>
                        </label>
                    </div>
                </div> --}}
                @include('component.form-item',[
                    'name' => 'enabled',
                    'type' => 'checkbox',
                    'label' => 'Enabled',
                    'value' => $module->enabled,
                ])
                <button class="icon-button right red" title="Delete tab and all associated data" type="button" data-delete="{{url('module/'.$module->id.'/delete')}}"><span class="icon-trash-alt"></span></button>
                <button class="modal-button">Save Changes</button>
            {{-- </div> --}}
        </form>
    </div>
@endsection