@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Add Tab<span class="icon icon-plus"></div>
        <form method="post" enctype="multipart/form-data" data-ajax-form action="{{url('module/add')}}">
            {{csrf_field()}}
            @include('component.form-item',[
                'type' => 'text',
                'name' => 'label',
                'label' => 'Label',
            ])
            {{-- @include('component.form-item',[
                'type' => 'icons',
                'name' => 'icon',
                'label' => 'Icon',
            ]) --}}
            {{-- @include('component.form-item',[
                'type' => 'select',
                'name' => 'type',
                'label' => 'Type',
                'options' => 'Records,CRM,Links,Files,Pages,Forms,Payments,Reservations',
            ]) --}}
            <div class="form-item icons">
                <label>Icon</label>
                <div class="radios icon-radio-set uk-margin-remove" data-uk-margin="">

                    <div class="form-item">         
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-menu">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-menu"></span></div>
                        </label>
                    </div>
                    <div class="form-item">    
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-user">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-user"></span></div>
                        </label>
                    </div>
                    <div class="form-item">         
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-unit">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-unit"></span></div>
                        </label>
                    </div>

                    <div class="form-item">         
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-cloud">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-cloud"></span></div>
                        </label>
                    </div>
                    <div class="form-item">         
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-image">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-image"></span></div>
                        </label>
                    </div>
                    <div class="form-item uk-margin-small-top">         
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-inbox">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-inbox"></span></div>
                        </label>
                    </div>
                    <div class="form-item uk-margin-small-top">         
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-settings">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-settings"></span></div>
                        </label>
                    </div>

                    <div class="form-item uk-margin-small-top">         
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-calendar">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-calendar"></span></div>
                        </label>
                    </div>

                    <div class="form-item uk-margin-small-top">         
                        <label class="radio-label">
                            <input type="radio" name="icon" value="icon-circle-announcement">
                            <span class="icon icon-circle"></span>
                            <div class="radio-text"><span class="icon icon-circle-announcement"></span></div>
                        </label>
                    </div>


                    @if($type == 'records')
                        <div class="form-item">  
                            <label class="radio-label checked">
                                <input type="radio" name="icon" value="icon-circle-list" checked>
                                <span class="icon icon-radio-checked"></span>
                                <div class="radio-text"><span class="icon icon-circle-list icon-radio-checked"></span></div>
                            </label>
                        </div>
                    @else
                        <div class="form-item uk-margin-small-top">         
                            <label class="radio-label">
                                <input type="radio" name="icon" value="icon-circle-list">
                                <span class="icon icon-circle"></span>
                                <div class="radio-text"><span class="icon icon-circle-list"></span></div>
                            </label>
                        </div>
                    @endif

                    @if($type == 'pages')
                        <div class="form-item">
                            <label class="radio-label checked">
                                <input type="radio" name="icon" value="icon-circle-layout" checked>
                                <span class="icon icon-radio-checked"></span>
                                <div class="radio-text"><span class="icon icon-circle-layout icon-radio-checked"></span></div>
                            </label>
                        </div>
                    @else
                        <div class="form-item">
                            <label class="radio-label">
                                <input type="radio" name="icon" value="icon-circle-layout">
                                <span class="icon icon-circle"></span>
                                <div class="radio-text"><span class="icon icon-circle-layout"></span></div>
                            </label>
                        </div>
                    @endif

                    @if($type == 'payments')
                        <div class="form-item">         
                            <label class="radio-label checked">
                                <input type="radio" name="icon" value="icon-circle-payment" checked>
                                <span class="icon icon-radio-checked"></span>
                                <div class="radio-text"><span class="icon icon-circle-payment icon-radio-checked"></span></div>
                            </label>
                        </div>
                    @else
                        <div class="form-item">         
                            <label class="radio-label">
                                <input type="radio" name="icon" value="icon-circle-payment">
                                <span class="icon icon-circle"></span>
                                <div class="radio-text"><span class="icon icon-circle-payment"></span></div>
                            </label>
                        </div>
                    @endif

                    @if($type == 'crm')
                        <div class="form-item">         
                            <label class="radio-label checked">
                                <input type="radio" name="icon" value="icon-circle-lead" checked>
                                <span class="icon icon-radio-checked"></span>
                                <div class="radio-text"><span class="icon icon-circle-lead icon-radio-checked"></span></div>
                            </label>
                        </div>
                    @else
                        <div class="form-item">         
                            <label class="radio-label">
                                <input type="radio" name="icon" value="icon-circle-lead">
                                <span class="icon icon-circle"></span>
                                <div class="radio-text"><span class="icon icon-circle-lead"></span></div>
                            </label>
                        </div>
                    @endif

                    @if($type == 'files')
                        <div class="form-item uk-margin-small-top">     
                            <label class="radio-label checked">
                                <input type="radio" name="icon" value="icon-circle-upload" checked>
                                <span class="icon icon-radio-checked"></span>
                                <div class="radio-text"><span class="icon icon-circle-upload icon-radio-checked"></span></div>
                            </label>
                        </div>
                    @else
                        <div class="form-item uk-margin-small-top">         
                            <label class="radio-label">
                                <input type="radio" name="icon" value="icon-circle-upload">
                                <span class="icon icon-circle"></span>
                                <div class="radio-text"><span class="icon icon-circle-upload"></span></div>
                            </label>
                        </div>
                    @endif

                    @if($type == 'forms')
                        <div class="form-item uk-margin-small-top">
                            <label class="radio-label checked">
                                <input type="radio" name="icon" value="icon-circle-form" checked>
                                <span class="icon icon-radio-checked"></span>
                                <div class="radio-text"><span class="icon icon-circle-form icon-radio-checked"></span></div>
                            </label>
                        </div>
                    @else
                        <div class="form-item uk-margin-small-top">         
                            <label class="radio-label">
                                <input type="radio" name="icon" value="icon-circle-form">
                                <span class="icon icon-circle"></span>
                                <div class="radio-text"><span class="icon icon-circle-form"></span></div>
                            </label>
                        </div>
                    @endif

                    @if($type == 'links')
                        <div class="form-item uk-margin-small-top">
                            <label class="radio-label checked">
                                <input type="radio" name="icon" value="icon-circle-link" checked>
                                <span class="icon icon-radio-checked"></span>
                                <div class="radio-text"><span class="icon icon-circle-link icon-radio-checked"></span></div>
                            </label>
                        </div>
                    @else
                        <div class="form-item uk-margin-small-top">         
                            <label class="radio-label">
                                <input type="radio" name="icon" value="icon-circle-link">
                                <span class="icon icon-circle"></span>
                                <div class="radio-text"><span class="icon icon-circle-link"></span></div>
                            </label>
                        </div>
                    @endif

                </div>
            </div>
            <input type="hidden" name="type" value="{{$type}}">
            <button class="modal-button">Add Tab</button>
        </form>
    </div>
@endsection