@extends('layouts.partial')

@section('middle')
    <form method="post" action="{{url("user/$user->id/edit")}}" data-ajax-form>
        <div class="detail-view">

            <span class="detail-view-icon icon icon-circle-user"></span>
            <h2 class="detail-view-heading">Edit User</h2>

            <div class="detail-view-menu">
                <button type="button" title="Delete user" data-delete="{{url('user/'.$user->id.'/delete')}}"><span class="icon uk-icon-trash-o"></span></button>
                <button title="Save Changes"><span class="icon uk-icon-save"></span></button>
                <span onClick="hideDetails()" class="icon icon-x"></span>
            </div>

            <div class="detail-view-content">
                @include('component.form-item',[
                    'name' => 'name',
                    'type' => 'text',
                    'label' => 'Label',
                    //'value' => $data,
                ])
            </div>
        </div>
    </form>
@endsection