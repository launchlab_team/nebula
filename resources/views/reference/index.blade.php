{{-- Reference index file for given folder --}}
@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        Heading Label
        <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span class="icon icon-menu cursor-pointer"></span>
            <div class="uk-dropdown">
                <ul>
                    @include('component.list-item',[
                        'text'=>'Create New Set',
                        'icon_left'=>'icon-plus',
                        'modal' => "record_set/id/create",
                    ])
                </ul>
            </div>
        </div>
    </h1>
@endsection
