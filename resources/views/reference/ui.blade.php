@extends('layouts.partial')

@section('main')
    <div class="uk-text-left">
        <h1>Nebula UI</h1>
        <h2>heading</h2>
        <h3>heading</h3>
        <h4>heading</h4>
        <h3>Toggles</h3>
        @include('component.button',['text'=>'Open Modal','icon'=>'icon-modal','modal'=>'dashboard'])
        @include('component.button',['text'=>'Load View','icon'=>'uk-icon-view','view'=>'dashboard'])
        @include('component.button',['text'=>'Load Details View','icon'=>'uk-icon-details','details'=>'dashboard'])
        <hr/>
        <h3>Methods</h3>
        @include('component.button',['text'=>'Create','icon'=>'uk-icon-plus','view'=>'organization/create'])
        @include('component.button',['text'=>'Edit','icon'=>'uk-icon-edit','view'=>'organization/1/edit'])
        @include('component.button',['text'=>'Read','icon'=>'uk-icon-eye','view'=>'organization/1'])
        <hr/>
        <h3>Buttons</h3>
        @include('component.button',[
            'text' => 'Button',
        ])
        @include('component.button',[
            'text' => 'Button',
            'icon' => 'icon-logout',
        ])
        @include('component.button',[
            'text' => 'Button',
            'icon_right' => 'uk-icon-circle',
        ])
        @include('component.button',[
            'text' => 'Dropdown Button',
            'dropdown' => 'partials.dropdown',
            'dropdown_position' => 'top-left',
            'icon_right' => 'uk-icon-circle',
        ])
        <hr/>
        <h3>Form</h3>
        <form method="post" action="/">
            {{csrf_field()}}
            @include('component.form-item',[
                'type' => 'checkbox',
                'name' => 'checkbox',
                'value' => 'checkbox',
                'label' => 'I agree to the <a href="terms" target="_blank">terms and conditions</a>.',
            ])
            <div class="radios">
                <label>Choose one:</label>
                @include('component.form-item',[
                    'type' => 'radio',
                    'name' => 'radio',
                    'value' => 'radio',
                    'label' => 'Radio label',
                ])
                @include('component.form-item',[
                    'type' => 'radio',
                    'name' => 'radio',
                    'value' => 'radio two',
                    'label' => 'Radio label',
                ])
            </div>
            @include('component.form-item',[
                'type' => 'select',
                'name' => 'select',
                'placeholder' => 'Select',
                'options' => 'one,two', // comma seperated
            ])
            @include('component.form-item',[
                'type' => 'text',
                'name' => 'text',
                'placeholder' => 'Text',
            ])
            @include('component.form-item',[
                'type' => 'phone',
                'name' => 'phone',
                'label' => 'Phone',
                'placeholder' => 'Phone',
            ])
            @include('component.form-item',[
                'type' => 'image',
                'name' => 'image',
                'label' => 'Image',
                'placeholder' => 'Attach image',
            ])
            @include('component.form-item',[
                'type' => 'file',
                'name' => 'file',
                'label' => 'File',
                'placeholder' => 'Attach file',
            ])
            @include('component.form-item',[
                'type' => 'date',
                'name' => 'date',
                'placeholder' => 'Date',
            ])
            <button>Submit Form</button>
        </form>
        <hr/>
        <h3>Icons</h3>
            @include('component.icon',[
                'icon_class' => 'uk-icon-heart',
                'text' => 'icon',
            ])    
            @include('component.icon',[
                'icon_class' => 'uk-icon-heart',
            ])
        <hr/>
        <h3>Box</h3>
            <div class="box">
                <div class="box-heading">.box-heading <span class="icon uk-icon-heart"></span></div>
                Box content
                <button class="box-button">.box-button</button>
            </div>
        <hr/>
        <h3>List item</h3>
            @include('component.list-item',[
                'icon_left' => 'uk-icon-heart',
                'icon_right' => 'icon-angle-right',
                'text' => 'hello',
            ])
            @include('component.list-item',[
                'icon_left' => 'uk-icon-heart',
                'icon_right' => 'icon-angle-right',
                'text' => 'hello',
            ])
        <hr/>

        <h3>Searchable list</h3>
            @include('component.searchable-list',['action' => 'users/search','results'=>'user.list'])

            @include('component.form-item',[ 'icon_right' => 'uk-icon-heart','label'=>'Hello','name'=>'hello', 'type'=>'text' ])
        <hr/>


        <h3>Section</h3>
        @include('component.section',[
            'heading' => 'Welcome',
            'subheading' => 'subheading',
            'parallax' => 'true',
            'include' => 'partials.intro',
        ])
        <div class="uk-grid uk-margin-remove">
            @include('component.section',[
                'heading' => 'Welcome',
                'subheading' => 'subheading',
                'parallax' => 'false',
                'class' => 'uk-width-1-2',
            ])
            @include('component.section',[
                'heading' => 'Welcome',
                'subheading' => 'subheading',
                'parallax' => 'true',
                'class' => 'uk-width-1-2 text-white',
                'background' => url('https://upload.wikimedia.org/wikipedia/commons/d/d6/Hs-2009-25-e-full_jpg.jpg'),
            ])
        </div>
    </div>
@endsection