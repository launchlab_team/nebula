@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        Links
        <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span class="icon icon-menu cursor-pointer"></span>
            <div class="uk-dropdown">
                <ul>
                    @include('component.list-item',[
                        'text'=>'Add a link',
                        'icon_left'=>'icon-plus',
                        'modal' => "link/create",
                    ])
                </ul>
            </div>
        </div>
    </h1>

    @if(isset($links) && count($links) !== 0)
        @if(isset($links) && $links !== null)
            @foreach($links as $link)
                @include('component.list-item',[
                    'text' => $link->title,
                    'subtext' => $link->url,
                    'modal' => 'link/'.$link->id.'/edit',
                    'icon_left' => isset($link->icon) ? $link->icon : 'icon-circle-link',
                    'class' => 'has-circle-icon',
                ])
            @endforeach
            @include('component.paginate',['records'=>$links])
        @endif
    @else
       <span class="message-center">Add links to external websites here</span>
    @endif
@endsection