@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">New Link <span class="icon icon-link"></div>
        <form method="post" enctype="multipart/form-data" action="{{url('link')}}" data-ajax-form>
            {{csrf_field()}}
            @include('component.form-item',[
                'name' => 'title',
                'type' => 'text',
                'label' => 'Link Title',
            ])
{{--             @include('component.form-item',[
                'name' => 'icon',
                'type' => 'icons',
                'label' => 'Choose an icon (optional):',
            ]) --}}
            @include('component.form-item',[
                'name' => 'url',
                'type' => 'text',
                'label' => 'URL',
            ])
            <button class="modal-button">Add Link</button>
        </form>
    </div>
@endsection