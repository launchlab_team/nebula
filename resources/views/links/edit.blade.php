@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">{{ucfirst($link->title)}}<span class="icon icon-link"></div>
        @include('component.button',[
            'text' => 'View Link',
            'icon_left' => 'icon-external-link',
            'target' => '_blank',
            'class' => 'full',
            'href' => $link->url,
        ])
        <form method="post" enctype="multipart/form-data" action="{{url('link/'.$link->id.'/edit')}}" data-ajax-form style="margin-top:10px">
            {{csrf_field()}}
            @include('component.form-item',[
                'name' => 'title',
                'type' => 'text',
                'label' => 'Link Title',
                'value' => $link->title,
            ])
            @include('component.form-item',[
                'name' => 'url',
                'type' => 'text',
                'label' => 'URL',
                'value' => $link->url,
            ])
           {{--  @include('component.form-item',[
                'name' => 'icon',
                'type' => 'icons',
                'label' => 'Choose an icon (optional):',
                'value' => $link->icon,
            ]) --}}
            <br/>
            <button type="button" class="icon-button right red" title="Delete tab and all associated data" type="button" data-delete="{{url('link/'.$link->id.'/delete')}}"><span class="icon-trash-alt"></span></button>
            <button class="modal-button">Add Link</button>
        </form>
    </div>
@endsection