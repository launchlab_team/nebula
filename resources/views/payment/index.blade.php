@extends('layouts.partial')

@section('main')
    {{-- <h1 class="heading">
        Payments  --}}
        {{--<div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
             @if(isset($settings->stripe_id) && $settings->stripe_id !== '' && $settings->stripe_id !== NULL)
                    <span class="icon-select-arrow icon" style="    font-size: 15px;
            padding-top: 6px;
            padding-left: 5px;"></span> 
            @endif--}}
            {{-- @if(isset($settings->stripe_id) && $settings->stripe_id != '' && $settings->stripe_id != NULL)
            <div class="uk-dropdown" style="margin-top: 10px">
                <ul >
                    @include('component.list-item',[
                        'text'=>'Scheduled Payments',
                        'href' => "payments/scheduled",
                        'class' => 'uk-text-left',
                    ])
                    @include('component.list-item',[
                        'text'=>'Payment Forms',
                        'href' => "payments/forms",
                        'class' => 'uk-text-left',
                    ])
                    @include('component.list-item',[
                        'text'=>'Invoices',
                        'class' => 'uk-text-left',
                        'href' => "payments/invoices",
                    ])
                </ul>
            </div>
            @endif
        </div> --}}
        {{-- @if(isset($settings->stripe_id) && $settings->stripe_id != '' && $settings->stripe_id != NULL)
            <span data-modal="payment/create" class="icon icon-plus cursor-pointer"></span>
        @endif
    </h1> --}}
    @include('payment.header')

    @if(!isset($settings->stripe_id) || $settings->stripe_id == '' || $settings->stripe_id == NULL)
    @else {{-- if not connected to stripe --}}
        <div class="horizontal-tabs">
            <a href="{{url('payments')}}"><div class="tab selected">Charges</div></a>
            <a href="{{url('payments/subscriptions')}}"><div class="tab">Subscriptions</div></a>
            <a href="{{url('payments/plans')}}"><div class="tab">Plans</div></a>
            <a href="{{url('payments/customers')}}"><div class="tab">Customers</div></a>
            <a href="{{url('payments/invoices')}}"><div class="tab">Invoices</div></a>
            <a href="{{url('payments/forms')}}"><div class="tab">Forms</div></a>
        </div> 
    @endif

    @if(!isset($settings->stripe_id) || $settings->stripe_id == '' || $settings->stripe_id == NULL)
        <div class="message-center">
            <a href="{{$url}}" target="_blank">Connect with Stripe</a> to accept recurring payments, send invoices and more.            
        </div>
    @else
        @if($payments !== null && count($payments) > 0)
            @foreach($payments as $payment)
                @include('component.list-item',[
                    //'text'=> $payment->description . ' ('.  '$'.$payment->amount.')',
                    'text'=> '$'.money_format("%.2n", $payment->amount/100),
                    //'subtext'=> date('m/d/y',$payment->created),
                    'subtext'=> date('F jS',$payment->created),
                    'details'=>'charge/'.$payment->id.'/view',
                    'class'=>'has-circle-icon',
                    'icon_left'=>'icon-circle-payment',
                ])
            @endforeach
        @else
            <div class="message-center">No payments yet. Add a <a href="payments/forms">payment form</a> or click <span class="icon-plus"></span> to start collecting payments today.</div>
        @endif
    @endif
@endsection