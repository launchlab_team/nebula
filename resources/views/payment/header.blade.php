<h1 class="heading">
    Payments
    @if(!isset($settings->stripe_id) || $settings->stripe_id == '' || $settings->stripe_id == NULL)
    @else
        <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span title="Add New" class="icon cursor-pointer icon-plus"></span>
            <div class="uk-dropdown" style="margin-top: 10px">
                <ul>
                    @include('component.list-item',[
                        'text'=>'New Charge',
                        'modal' => "payment/create",
                        'class' => 'uk-text-left'
                    ])
                    @include('component.list-item',[
                        'text'=>'New Invoice',
                        'modal' => "invoice/create",
                        'class' => 'uk-text-left'
                    ])
                    @include('component.list-item',[
                        'text'=>'New Customer',
                        'modal' => "payment/customer/create",
                        'class' => 'uk-text-left'
                    ])
                    @include('component.list-item',[
                        'text'=>'New Subscription',
                        'modal' => "payment/subscription/create",
                        'class' => 'uk-text-left'
                    ])
                    @include('component.list-item',[
                        'text'=>'New Payment Plan',
                        'modal' => "payment/plan/create",
                        'class' => 'uk-text-left'
                    ])
                    @include('component.list-item',[
                        'text'=>'New Payment Form',
                        'modal' => "payment/form/create",
                        'class' => 'uk-text-left'
                    ])
                </ul>
            </div>
        </div>
    @endif
</h1>