@extends('layouts.partial')

@section('middle')
    <div class="detail-view">
        <div class="detail-view-menu">
            <span onClick="hideDetails()" class="icon icon-x"></span>
        </div>

        <div class="detail-view-content animated slideInUp">
            <span class="detail-view-icon icon icon-circle-calendar"></span>
            <h1 class="detail-view-heading">Payment Plan</h1>
            <div class="block uk-text-left">

            <h3 class="uk-text-center">{{$plan->name}}</h3>
             <hr/>

                <label>Created</label>
                <div class="answer">
                    {{date("m/d/y h:i A",$plan->created)}} 
                </div>
                <br/>


                <label>Plan Details</label>
                <div class="answer">
                    ${{money_format("%.2n", $plan->amount/100) . '/'.  $plan->interval }}
                </div>
                <br/>

                <label>Plan ID</label>
                <div class="answer">
                    {{$plan->id}}
                </div>
                <br/>
<hr>
                <a href="https://dashboard.stripe.com/plans/{{$plan->id}}" target="_blank"><button class="full"><span class="button-icon-left uk-icon-cc-stripe"></span> View in Stripe</button></a>
                <span class="text-light uk-text-center uk-display-block"></span>
            </div> 
        </div>
    </div>
@endsection