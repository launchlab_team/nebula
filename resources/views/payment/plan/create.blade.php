@extends('layouts.partial')

@section('main')
    <div class="modal">
        <h1 class="modal-heading">New Payment Plan</h1>
        <form method="post" action="{{url('payment/plan/create')}}" data-ajax-form>
            {{csrf_field()}}
            @include('component.form-item',[
                'type' => 'text',
                'name' => 'name',
                'label' => 'Name',
                'placeholder' => 'Golden Plan',
            ])
            @include('component.form-item',[
                'type' => 'text',
                'name' => 'id',
                'label' => 'ID',
                'placeholder' => 'golden-plan',
            ])
            @include('component.form-item',[
                'type' => 'text',
                'name' => 'amount',
                'label' => 'Amount',
            ])
            @include('component.form-item',[
                'type' => 'select',
                'name' => 'interval',
                'label' => 'Interval',
                'sublabel' => 'Specifies billing frequency.',
                'options' => 'Day,Week,Month,Year',
            ])
            <button class="modal-button">Create Plan</button>
        </form>
    </div>
@endsection
