@extends('layouts.partial')

@section('main')
    @include('payment.header')

    <div class="horizontal-tabs">
        <a href="{{url('payments')}}"><div class="tab">Charges</div></a>
        <a href="{{url('payments/subscriptions')}}"><div class="tab">Subscriptions</div></a>
        <a href="{{url('payments/plans')}}"><div class="tab selected">Plans</div></a>
        <a href="{{url('payments/customers')}}"><div class="tab">Customers</div></a>
        <a href="{{url('payments/invoices')}}"><div class="tab">Invoices</div></a>
        <a href="{{url('payments/forms')}}"><div class="tab">Forms</div></a>
    </div> 

    @if($plans !== null && count($plans) > 0)
        @foreach($plans as $plan)
        {{-- {{dd($plan->name)}} --}}
            @include('component.list-item',[
                'text'=>$plan->name,
                'subtext'=> '$'.money_format("%.2n", $plan->amount/100) . '/'.  $plan->interval,
                'details'=>'stripe/plan/'.$plan->id,
                'class'=>'has-circle-icon',
                'icon_left'=>'icon-circle-calendar',
            ])
        @endforeach
    @else
        <div class="message-center">No plans yet. Click <span data-modal="payment/plan/create" class="icon-plus"></span> to create your first plan.</div>
    @endif
@endsection