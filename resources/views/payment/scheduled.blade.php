{{-- The scheduled payments index page --}}
@extends('layouts.partial')

@section('main')
    <h1 class="heading cursor-pointer">
        <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
            Scheduled Payments <span class="icon-select-arrow icon" style="    font-size: 15px;
                        padding-top: 6px;
                        padding-left: 5px;"></span>
            <div class="uk-dropdown" style="margin-top: 10px">
                <ul >
                    @include('component.list-item',[
                        'text'=>'Payments',
                        'href' => "payments",
                        'class' => 'uk-text-left',
                    ])
                    @include('component.list-item',[
                        'text'=>'Payment Forms',
                        'href' => "payments/forms",
                        'class' => 'uk-text-left',
                    ])
                    @include('component.list-item',[
                        'text'=>'Invoices',
                        'class' => 'uk-text-left',
                        'href' => "payments/invoices",
                    ])
                </ul>
            </div>
        </div>
        <span data-modal="payment/create" class="icon icon-plus cursor-pointer"></span>
    </h1>
    @if($payments !== null && count($payments) !== 0)
        @foreach($payments as $payment)
            @include('component.list-item',[
                'text'=>$payment->description,
                'subtext'=>'$'.$payment->amount,
                'details'=>'payment/'.$payment->id.'/view',
                'class'=>'has-circle-icon',
                'icon_left'=>'icon-circle-payment',
            ])
        @endforeach
        @include('component.paginate',['records'=>$payments])
    @else
        <div class="message-center">No scheduled payments. Add a <a href="{{url('payments/forms')}}">payment form</a> or click <span class="icon-plus"></span> to create recurring payments.</div>
    @endif
@endsection