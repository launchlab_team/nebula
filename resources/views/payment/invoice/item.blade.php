<tr class="magictime vanishIn">
    <td style="width:160px">
        @include('component.form-item',[
            'name' => 'item[]',
            'type' => 'text',
            'label' => 'Item',
        ])
    </td>
    <td>
        @include('component.form-item',[
            'name' => 'rate[]',
            'type' => 'text',
            'label' => 'Rate',
        ])
    </td>
    <td>
        @include('component.form-item',[
            'name' => 'quantity[]',
            'type' => 'text',
            'label' => 'Qty',
        ])
    </td>
    <td>
        @include('component.form-item',[
            'name' => 'total[]',
            'type' => 'text',
            'label' => 'Total',
            'attributes' => 'readonly',
        ])
    </td>
    <td>
        <span class="icon icon-x" data-remove-invoice-item style="cursor:pointer;margin-right:-15px"></span>
    </td>
</tr>