@extends('layouts.partial')

@section('main')
    {{-- <h1 class="heading">
        Payments
        <span data-modal="invoice/create" class="icon icon-plus cursor-pointer"></span>
        <span data-details="invoice/settings" title="Menu Settings" class="icon cursor-pointer icon-settings"></span>
    </h1> --}}
    @include('payment.header')

    <div class="horizontal-tabs">
        <a href="{{url('payments')}}"><div class="tab">Charges</div></a>
        <a href="{{url('payments/subscriptions')}}"><div class="tab">Subscriptions</div></a>
        <a href="{{url('payments/plans')}}"><div class="tab">Plans</div></a>
        <a href="{{url('payments/customers')}}"><div class="tab">Customers</div></a>
        <a href="{{url('payments/invoices')}}"><div class="tab selected">Invoices</div></a>
        <a href="{{url('payments/forms')}}"><div class="tab">Forms</div></a>
    </div> 

    @include('component.list-item',[
        'text'=>'Preferences',
        'subtext'=>'Logo and default settings for invoices',
        'details'=>'invoice/settings',
        'class'=>'has-circle-icon',
        'icon_left'=>'icon-circle-settings',
    ])

    {{-- @include('component.list-item',[
        'text'=>'Invoice Items',
        'details'=>'invoice/settings',
        'class'=>'has-circle-icon',
        'icon_left'=>'icon-circle-list',
    ]) --}}
    <hr class="uk-margin-small" style="margin-bottom: 10px;" />
    @if($invoices !== null && count($invoices) > 0)
        @foreach($invoices as $invoice)
        {{-- {{dd($invoice->name)}} --}}
            @include('component.list-item',[
                'text'=>$invoice->name,
                'subtext'=>'$'.$invoice->amount_due,
                'details'=>'invoice/'.$invoice->id.'/edit',
                'class'=>'has-circle-icon',
                'icon_left'=>'icon-circle-form',
            ])
        @endforeach
    @else
        <div class="message-center">No invoices yet. Click <span data-modal="invoice/create" class="icon-plus"></span> to create your first invoice.</div>
    @endif
@endsection