@extends('layouts.partial')

@section('middle')
    <form method="post" action="{{url("invoice/settings/update")}}" data-ajax-form>
        {{csrf_field()}}
        <div class="detail-view">
            <span class="detail-view-icon icon icon-circle-settings"></span>
            <h1 class="detail-view-heading">Invoice Settings</h1>

            <div class="detail-view-menu">
                <button title="Save Changes"><span class="icon uk-icon-save"></span></button>
                <span onClick="hideDetails()" class="icon icon-x"></span>
            </div>

            <div class="detail-view-content">
                @include('component.form-item',[
                    'type'=>'image',
                    'name'=>'logo',
                    'label'=>'logo',
                    'value'=>isset($invoice_settings->logo) ? $invoice_settings->logo : '',
                ])
                @include('component.form-item',[
                    'type'=>'textarea',
                    'name'=>'from',
                    'label'=>'from',
                    'sublabel'=>'Company Name & Contact Information',
                    'value'=>isset($invoice_settings->from) ? $invoice_settings->from : '',
                ])
                @include('component.form-item',[
                    'type'=>'textarea',
                    'name'=>'terms',
                    'label'=>'Terms',
                    'sublabel'=>'Default Terms and Conditions',
                    'value'=>isset($invoice_settings->terms) ? $invoice_settings->terms : '',
                ])
            </div>
        </div>
    </form>
@endsection