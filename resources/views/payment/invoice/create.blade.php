@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">New Invoice <span class="icon icon-plus"></div>
        <form method="post" action="{{url('invoice/store')}}" data-ajax-form>
            {{csrf_field()}}
{{--             <div class="form-item">
                <label>Invoice Logo</label>
                <input type="file" name="logo"/>
            </div> --}}
            @include('component.form-item',[
                'name' => 'name',
                'type' => 'text',
                'label' => 'Invoice Name',
            ])
            @include('component.form-item',[
                'name' => 'number',
                'type' => 'text',
                'label' => 'Invoice #',
            ])
            @include('component.form-item',[
                'name' => 'from',
                'type' => 'textarea',
                'label' => 'From',
                'value'=>isset($invoice_settings->from) ? $invoice_settings->from : '',
            ])
            @include('component.form-item',[
                'name' => 'bill_to',
                'type' => 'textarea',
                'label' => 'Bill To',
            ])
            @include('component.form-item',[
                'name' => 'due_date',
                'type' => 'date',
                'label' => 'Due Date',
                'value' => date("m/d/y", strtotime("+2 weeks")),
            ])

            <table>
                <tr>
                    <td style="width:160px">
                        @include('component.form-item',[
                            'name' => 'item[]',
                            'type' => 'text',
                            'label' => 'Item',
                        ])
                    </td>
                    <td>
                        @include('component.form-item',[
                            'name' => 'rate[]',
                            'type' => 'text',
                            'label' => 'Rate',
                        ])
                    </td>
                    <td>
                        @include('component.form-item',[
                            'name' => 'quantity[]',
                            'type' => 'text',
                            'label' => 'Qty',
                        ])
                    </td>
                    <td>
                        @include('component.form-item',[
                            'name' => 'total[]',
                            'type' => 'text',
                            'label' => 'Total',
                            'attributes' => 'readonly',
                        ])
                    </td>
                </tr>
            </table>
            <a class="text-light uk-text-small uk-display-block uk-text-center" data-add-invoice-item>Add Item <span class="icon-plus"></span></a>

            @include('component.form-item',[
                'name' => 'tax',
                'type' => 'text',
                'label' => 'Tax',
                'placeholder'=>'(optional)',
            ])
            @include('component.form-item',[
                'name' => 'total_due',
                'type' => 'text',
                'label' => 'Total Amount Due',
            ])
            @include('component.form-item',[
                'type'=>'textarea',
                'name'=>'terms',
                'label'=>'Terms',
                'sublabel'=>'Default Terms and Conditions',
                'value'=>isset($invoice_settings->terms) ? $invoice_settings->terms : '',
            ])
            @include('component.form-item',[
                'name' => 'notes',
                'type' => 'textarea',
                'label' => 'Notes',
            ])
            <button class="modal-button">Generate Invoice</button>
        </form>
    </div>
@endsection