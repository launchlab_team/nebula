@extends('layouts.partial')

@section('middle')
    <form method="post" action="{{url("invoice/$invoice->id/update")}}" data-ajax-form>
        {{csrf_field()}}
        <div class="detail-view">
            <span class="detail-view-icon icon icon-circle-payment"></span>
            <h1 class="detail-view-heading">Invoice</h1>

            <div class="detail-view-menu">
                <button type="button" title="Delete Invoice" data-delete="{{url('invoice/'.$invoice->id.'/delete')}}"><span class="icon uk-icon-trash-o"></span></button>
                <button title="Save Changes"><span class="icon uk-icon-save"></span></button>
                <span onClick="hideDetails()" class="icon icon-x"></span>
            </div>
            <div class="detail-view-content">
                <hr style="margin-bottom:8px" />
                @include('component.button',[
                    'name' => 'name',
                    'type' => 'button',
                    'class' => 'full',
                    'text' => 'Download PDF',
                    'href' => 'invoice/'.$invoice->id.'/download',
                    'attributes' => 'target="_blank"',
                    'icon_left' => 'icon-download',
                    'label' => 'Download Invoice',
                ])
                @include('component.button',[
                    'name' => 'name',
                    'type' => 'button',
                    'class' => 'full',
                    'text' => 'Email PDF',
                    'modal' => 'invoice/'.$invoice->id.'/share',
                    'icon_left' => 'uk-icon-paper-plane-o',
                    'label' => 'Download Invoice',
                ])
                <hr/>
                @if(isset($invoice->shared_with) && count(json_decode($invoice->shared_with)) > 0)
                    <div class="form-item" style="margin-bottom:-8px"><label>Shared with:</label></div>
                    <div class="message-box uk-scrollable-box" style="height:50px">
                        @foreach(json_decode($invoice->shared_with) as $email)
                            {{$email}}&nbsp;
                        @endforeach
                    </div>
                <hr/>
                @endif

                @include('component.form-item',[
                    'name' => 'name',
                    'type' => 'text',
                    'label' => 'Invoice Name',
                    'value' => $invoice->name,
                ])
                @include('component.form-item',[
                    'name' => 'number',
                    'type' => 'text',
                    'label' => 'Invoice #',
                    'value' => $invoice->number,
                ])
                @include('component.form-item',[
                    'name' => 'from',
                    'type' => 'textarea',
                    'label' => 'From',
                    'value' => $invoice->from,
                ])
                @include('component.form-item',[
                    'name' => 'bill_to',
                    'type' => 'textarea',
                    'label' => 'Bill To',
                    'value' => $invoice->bill_to,
                ])
                @include('component.form-item',[
                    'name' => 'due_date',
                    'type' => 'date',
                    'label' => 'Due Date',
                    'value' => $invoice->due_date,
                ])

                <table>
                    @if(count(json_decode($invoice->items)) > 0)
                        <?php $i = 0; ?>
                        @foreach(json_decode($invoice->items) as $item)
                        <?php $i++; ?>
                            <tr>
                                <td style="width:160px">
                                    @include('component.form-item',[
                                        'name' => 'item[]',
                                        'type' => 'text',
                                        'label' => 'Item',
                                        'value' => isset($item[0]) ? $item[0] : NULL,
                                    ])
                                </td>
                                <td>
                                    @include('component.form-item',[
                                        'name' => 'rate[]',
                                        'type' => 'text',
                                        'label' => 'Rate',
                                        'value' => isset($item[1]) ? $item[1] : NULL,
                                    ])
                                </td>
                                <td>
                                    @include('component.form-item',[
                                        'name' => 'quantity[]',
                                        'type' => 'text',
                                        'label' => 'Qty',
                                        'value' => isset($item[2]) ? $item[2] : NULL,
                                    ])
                                </td>
                                <td>
                                    @include('component.form-item',[
                                        'name' => 'total[]',
                                        'type' => 'text',
                                        'label' => 'Total',
                                        'attributes' => 'readonly',
                                        'value' => (isset($item[2]) && isset($item[1])) ? ($item[2] * $item[1]) : NULL,
                                    ])
                                </td>
                                @if($i !== 1) {{-- Skip first row --}}
                                <td>
                                    <span class="icon icon-x" data-remove-invoice-item style="cursor:pointer;margin-right:-15px"></span>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    @endif
                </table>
                <a class="text-light uk-text-small uk-display-block uk-text-center" data-add-invoice-item>Add Item <span class="icon-plus"></span></a>
                @include('component.form-item',[
                    'name' => 'tax',
                    'type' => 'text',
                    'label' => 'Tax',
                    'placeholder' => '(optional)',
                    'value'=> $invoice->tax,
                ])
                @include('component.form-item',[
                    'name' => 'total_due',
                    'type' => 'text',
                    'label' => 'Total Amount Due',
                    'value'=> $invoice->amount_due,
                ])

                @include('component.form-item',[
                    'type'=>'textarea',
                    'name'=>'terms',
                    'label'=>'Terms',
                    'sublabel'=>'Terms and Conditions',
                    'placeholder'=>'(optional)',
                    'value'=>$invoice->terms,
                ])

                @include('component.form-item',[
                    'name' => 'notes',
                    'type' => 'textarea',
                    'label' => 'Notes',
                    'value'=> $invoice->notes,
                ])
            </div>
        </div>
    </form>
@endsection