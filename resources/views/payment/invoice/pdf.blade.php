<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Invoice</title>
    <style>
    body,html {
        font-family:Helvetica;
        font-size: 12px;
        line-height: 18px;
        font-weight: normal;
        background: #fff;
    }
    .page-break {
        page-break-after: always;
    }

    th {
        /*font-weight:100;*/
        /*color:#999;*/
        /*text-transform:uppercase;*/
        /*font-size:16px;*/
        text-align: left;
        /*line-height:22px;*/
/*        padding:0 10px;*/
    }
    td {
        vertical-align: top;
        padding:5px 0;
    }
    </style>
</head>
<body>

<table width="100%">
  <tr>
  {{-- {{file_get_contents(url($invoice_settings->logo))}} --}}
    <td> @if($logo !== '') <img style="max-width:150px;margin-bottom:10px" src="{{$logo}}"> @endif <br/></td>
    <td style="text-align:right;vertical-align: top"><h1 style="font-weight:100;text-transform: uppercase;margin-bottom:0px">Invoice</h1><h2>#{{$invoice->number}}</h2></td>
  </tr>
</table>
<table width="100%" style="border:1px solid #eee;background:#f8f8f8;border-radius:3px;padding:10px">
  <tr>
    <td><b>Date:</b><br/>{{date('d/m/Y',strtotime($invoice->updated_at))}}</td>
    <td><b>Date Due:</b><br/>{!! nl2br($invoice->due_date) !!}</td>
  </tr>
  <tr>
    <td><b>From:</b><br/>{!! nl2br($invoice->from) !!}</td>
    <td><b>To:</b><br/>{!! nl2br($invoice->bill_to) !!}</td>
  </tr>
</table>
<br/>
<table width="100%" style="border:1px solid #eee;border-radius:3px;padding:10px">
  <tr>
    <th style="width:400px">Items</th>
    <th>Rate</th>
    <th>Quantity</th>
    <th style="text-align:right">Total</th>
  </tr>
  <tr>
      <td colspan="4"><hr style="border:0;border-top:1px solid #eee" class="uk-margin-remove"/></td>
  </tr>
  @foreach(json_decode($invoice->items) as $item)
  <tr>
    <td>{{isset($item[0]) ? $item[0] : '' }}</td>
    {{-- <td>${{isset($item[1]) && is_float($item[1]) ? number_format($item[1], 2,'.', ',') : $item[1] }}</td> --}}
    <td>${{number_format((float) $item[1], 2,'.', ',') }}</td>
    <td>{{isset($item[2]) ? $item[2] : '0'}}</td>
    <td style="text-align:right">${{(isset($item[2]) && isset($item[1])) ? money_format("%.2n",(float) $item[1]*(float) $item[2]) : ''}}</td>
  </tr>
  @endforeach
  <tr>
      <td colspan="4"><hr style="border:0;border-top:1px solid #eee" class="uk-margin-remove"/></td>
  </tr>
  @if($invoice->tax !== '')
      <tr>
        <th  colspan="3" style="text-align:right">Tax:</th>
        <td  style="text-align:right">
          ${!! number_format((float) $invoice->tax, 2,'.', ',') !!}
        </td>
      </tr>
  @endif
  <tr>
    <th colspan="3" style="text-align:right;"">Total Due:</th>
    <th style="text-align:right">${!! $invoice->amount_due !!}</th>
  </tr>
</table>
<table width="100%" style="margin-top:25px">
  @if($invoice->terms !== '')
  <tr>
    <th style="text-align:left">Terms:</th>
    <td colspan="3">
      {!! nl2br($invoice->terms) !!}
    </td>
  </tr>
  @endif
  <tr>
    <th style="text-align:left">Notes:</th>
    <td colspan="3">
      {!! nl2br($invoice->notes) !!}
    </td>
  </tr>
</table> 