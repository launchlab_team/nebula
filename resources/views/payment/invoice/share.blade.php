@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Email an Invoice <span class="icon uk-icon-paper-plane-o"></div>
        <form method="post" action="{{url('invoice/'.$invoice->id.'/share')}}" data-ajax-form>
            {{csrf_field()}}
            @include('component.form-item',[
                'name' => 'emails',
                'type' => 'textarea',
                'label' => 'Share with:',
                'placeholder' => 'one@example.com, two@example.com',
                'sublabel' => 'Comma seperated email addresses',
            ])
            @include('component.form-item',[
                'name' => 'message',
                'type' => 'textarea',
                'placeholder' => '(optional)',
                'label' => 'Add message',
            ])
            <br>
            <div class="message-box">An email will be sent along with an attached PDF of the invoice titled "{{$invoice->name}}"</div>
            <button class="modal-button">Send Invoice</button>
        </form>
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        {{-- Reset the button on keyup if not submitting --}}
        $(document).on('keyup', $('form'), function(e) {
            if ($('.modal-button').is(':disabled')) {
            } else {
                $('.modal-button').html('Send Invoice');
            }
        });
        $(document).on('click', '.modal-button', function(e) {
            $('.modal-button').html('Sharing...Please wait');
        });
    </script>
@endsection