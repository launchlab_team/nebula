@extends('layouts.partial')

@section('middle')
    <div class="detail-view">
        <div class="detail-view-menu">
            <span onClick="hideDetails()" class="icon icon-x"></span>
        </div>

        <div class="detail-view-content animated slideInUp">
            <div class="block uk-text-left border-top-green">

            @if(isset($payment->form))
                 <h3 class="uk-text-center">{{$payment->form->title}}</h3>
            @else
                <h3 class="uk-text-center">{{'$'.($payment->amount/100)}} Payment</h3>
            @endif
             <hr/>

                <label>Charge Created</label>
                <div class="answer">
                    {{date("l M jS h:i A",$payment->created)}} 
                </div>
                <br/>
                {{-- {{dd($payment)}} --}}
                @if($payment->description !== NULL)
                    <label>Description</label>
                    <div class="answer">
                        {{$payment->description}} 
                    </div>
                    <br/>
                @endif
                
                @if(isset($customer) && $customer !== NULL)
                    <label>Email</label>
                    <div class="answer">
                        {{$customer->email}}
                    </div>
                    <br/>

                    <label>Stripe Customer ID</label>
                    <div class="answer">
                        {{$payment->customer}}
                    </div>
                    <br/>
                @endif

                @if(count(json_decode($payment->answers)) > 0)
                    <hr style="margin-top:0px" />
                    @foreach(json_decode($payment->answers) as $key => $data)
                        @if(array_key_exists($key, json_decode($payment->questions)))
                            <label>{{json_decode($payment->questions)[$key]}}</label>
                            <div class="answer">
                                {{$data}}
                            </div>
                            <br/>
                        @endif
                    @endforeach
                @endif
                <hr>

                <a href="https://dashboard.stripe.com/payments/{{$payment->id}}" target="_blank"><button class="full"><span class="button-icon-left uk-icon-cc-stripe"></span> View in Stripe</button></a>
                <span class="text-light uk-text-center uk-display-block"></span>
            </div> 
        </div>
    </div>
@endsection