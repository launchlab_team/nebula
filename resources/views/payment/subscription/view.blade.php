@extends('layouts.partial')

@section('middle')
    <div class="detail-view">
        <div class="detail-view-menu">
            <span onClick="hideDetails()" class="icon icon-x"></span>
        </div>

        <div class="detail-view-content animated slideInUp">
            <span class="detail-view-icon icon icon-circle-calendar"></span>
            <h1 class="detail-view-heading">Subscription</h1>
            <div class="block uk-text-left">

            <h3 class="uk-text-center">${{money_format("%.2n", $stripe_subscription->plan->amount/100) . '/'.  $stripe_subscription->plan->interval }}</h3>
             <hr/>
                <label>Status</label>
                <div class="answer">
                    {{ucfirst($stripe_subscription->status)}}
                </div>
                <br/>

                <label>Created</label>
                <div class="answer">
                    {{date("m/d/y h:i A",$stripe_subscription->created)}} 
                </div>
                <br/>
                
                <label>Plan</label>
                <div class="answer">
                    {{$stripe_subscription->plan->name}}
                </div>
                <br/>

                <label>Customer</label>
                <div class="answer">
                    {{$customer->description}}
                </div>
                <br/>

                <label>Customer Email</label>
                <div class="answer">
                    {{$customer->email}}
                </div>
                <br/>

                <label>Stripe Subscription ID</label>
                <div class="answer">
                    {{$stripe_subscription->id}}
                </div>
                <br/>

                {{-- {{dd($stripe_subscription)}} --}}
                <label>Subscription Quantity</label>
                <div class="answer">
                    {{$stripe_subscription->quantity}}
                </div>
                <br/>

                <label>Trial Ends</label>
                <div class="answer">
                    {{date("m/d/y",$stripe_subscription->trial_end)}} 
                </div>
                <br/>

                @if($subscription !== NULL)
                    @if(isset($subscription->answers) && count(json_decode($subscription->answers)) > 0)
                        <hr/>
                        @foreach(json_decode($subscription->answers) as $key => $data)
                            @if(array_key_exists($key, json_decode($subscription->questions)))
                                <label>{{json_decode($subscription->questions)[$key]}}</label>
                                <div class="answer">
                                    {{$data}}
                                </div>
                                <br/>
                            @endif
                        @endforeach
                    @endif
                @endif

                <hr/>
                <a href="https://dashboard.stripe.com/subscriptions/{{$stripe_subscription->id}}" target="_blank"><button class="full"><span class="button-icon-left uk-icon-cc-stripe"></span> View in Stripe</button></a>
                <span class="text-light uk-text-center uk-display-block"></span>
            </div> 
        </div>
    </div>
@endsection