@extends('layouts.partial')

@section('main')
    <div class="modal">
        <h2 class="modal-heading">Manage Subscription</h2>
        <table width="100%">
             <tr>
                 <td style="height:35px"><b>Details</b></td>
                 <td>${{money_format("%.2n", $stripe_subscription->plan->amount/100) . '/'.  $stripe_subscription->plan->interval }}</td>
             </tr>
             <tr>
                 <td style="height:35px"><b>Status</b></td>
                 <td>{{$stripe_subscription->status}}</td>
             </tr>
             <tr>
                 <td style="height:35px"><b>Created</b></td>
                 <td>{{date("m/d/y h:i A",$stripe_subscription->created)}} </td>
             </tr>
             <tr>
                 <td style="height:35px"><b>Plan</b></td>
                 <td>{{$stripe_subscription->plan->name}}</td>
             </tr>
        @if(isset($subscription->answers) && count(json_decode($subscription->answers)) > 0)
            @foreach(json_decode($subscription->answers) as $key => $data)
                @if(array_key_exists($key, json_decode($subscription->questions)))
                 <tr>
                     <td style="height:35px"><b>{{json_decode($subscription->questions)[$key]}}</b></td>
                     <td>{{$data}}</td>
                 </tr>
                @endif
            @endforeach
        @endif
        </table>
        <form method="post" action="{{url('subscription/'.$stripe_subscription->id.'/cancel')}}" data-ajax-form>
            <br/>
            <button class=" right" style="color:#cd0000">Cancel Subscription</button>
        </form>
        <button class="modal-button" onclick="closeModal()">Close</button>
    </div>
@endsection