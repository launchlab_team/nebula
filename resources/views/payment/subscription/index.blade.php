@extends('layouts.partial')

@section('main')
    @include('payment.header')
    
    <div class="horizontal-tabs">
        <a href="{{url('payments')}}"><div class="tab">Charges</div></a>
        <a href="{{url('payments/subscriptions')}}"><div class="tab selected">Subscriptions</div></a>
        <a href="{{url('payments/plans')}}"><div class="tab">Plans</div></a>
        <a href="{{url('payments/customers')}}"><div class="tab">Customers</div></a>
        <a href="{{url('payments/invoices')}}"><div class="tab">Invoices</div></a>
        <a href="{{url('payments/forms')}}"><div class="tab">Forms</div></a>
    </div> 

{{--     @include('component.list-item',[
        'text'=>'Payment Plans',
        'href'=>'payments/plans',
        'class'=>'has-circle-icon',
        'icon_left'=>'icon-circle-payment',
    ])
    <hr class="uk-margin-small" style="margin-bottom: 10px;" />
 --}}
    @if($subscriptions !== null && count($subscriptions) > 0)
        @foreach($subscriptions as $subscription)

        {{-- {{dd($subscription)}} --}}
            @include('component.list-item',[
                //'subtext'=>$subscription->plan->name,
                'subtext'=> Carbon\Carbon::createFromTimestamp($subscription->created)->diffForHumans(),
                'text'=> '$'.money_format("%.2n", $subscription->plan->amount/100) . '/'.  $subscription->plan->interval,
                'details'=>'stripe/subscription/'.$subscription->id.'',
                'class'=>'has-circle-icon',
                'icon_left'=>'icon-circle-calendar',
                //'icon_left'=>'icon-circle-form',
            ])
        @endforeach
    @else
        <div class="message-center">No subscriptions yet. First create a plan to subscribe customers to.</div>
    @endif
@endsection