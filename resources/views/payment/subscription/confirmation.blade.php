@extends('layouts.default')

@section('main')
    <div class="detail-view">
{{--         <div class="detail-view-menu">
            <span onClick="hideDetails()" class="icon icon-x"></span>
        </div> --}}

        <div class="detail-view-content animated slideInUp">
            <span class="detail-view-icon icon icon-check text-green" style="margin-bottom:-10px;color: #70a588;"></span>
            <h1 class="detail-view-heading">Subscription Confirmed</h1>
            <div class="block uk-text-left" style="margin-top:20px">

            <h3 class="uk-text-center">${{money_format("%.2n", $subscription->plan->amount/100) . '/'.  $subscription->plan->interval }}</h3>
             <hr/>
                <label>Created</label>
                <div class="answer">
                    {{date("m/d/y h:i A",$subscription->created)}} 
                </div>
                <br/>

                <label>Name</label>
                <div class="answer">
                    {{$customer->description}}
                </div>
                <br/>

                <label>Email</label>
                <div class="answer">
                    {{$customer->email}}
                </div>
                <br/>

                <label>Plan</label>
                <div class="answer">
                    {{$subscription->plan->name}}
                </div>
                <br/>

                {{-- <label>Amount</label>
                <div class="answer">
                    {{$subscription->plan->amount}}
                </div>
                <br/>

                <label>Interval</label>
                <div class="answer">
                    {{$subscription->plan->interval}}
                </div> --}}
                <hr/>
                @if(Auth::user() == NULL)
                    <div class="uk-text-center">Continue to <a href="{{url('login')}}"><u>login</u></a> or <a href="{{url('register')}}"><u>register</u></a> to manage this subscription.</div>
                @else
                    <div class="uk-text-center">Continue to <a href="{{url('account')}}"><u>your account</u></a> to manage your subscriptions.</div>
                @endif
            </div> 
        </div>
    </div>
@endsection