@extends('layouts.partial')

@section('main')
    <div class="modal">
        <h1 class="modal-heading">New Subscription </h1>
        <form method="post" action="{{url('payment/subscription/create')}}" data-ajax-form>
            {{csrf_field()}}
            @include('component.form-item',[
                'attributes' => 'data-find-customer',
                'type' => 'text',
                'placeholder' => 'Find customer',
                'icon_right' => 'icon-search',
                'name' => 'findCustomer', // name not used
            ])
            <div data-customer-list>
                {{-- Shows selected customers --}}
            </div>
            <div class="form-item select">
                <span class="icon form-item-icon form-item-icon-right icon-select-arrow"></span>
                <select name="plan">
                    <option disabled selected>Choose Plan</option>
                    @if($plans !== NULL)
                    @foreach(array_reverse($plans->data) as $plan)
                        <option value="{{$plan->id}}">{{$plan->name}} (${{money_format("%.2n", $plan->amount/100) . '/'.  $plan->interval }})</option>
                    @endforeach
                    @endif
                </select>
            </div>
            @if(count($plans) == 0)
                <div class="message-box">You must first create a payment plan to subscribe your customers to.</div>
            @endif
            <table width="100%">
                <tr>
                    <td>
                        @include('component.form-item',['type'=>'text','name'=>'quantity','placeholder'=>'Qty (optional)'])
                    </td>
                    <td>
                        @include('component.form-item',['type'=>'text','name'=>'tax_percent','placeholder'=>'Tax % (optional)'])
                    </td>
                </tr>
            </table>
            @include('component.form-item',['type'=>'date','name'=>'trial_end','placeholder'=>'Trial End'])
            <button class="modal-button">Create Subscription</button>
        </form>
    </div>
@endsection
