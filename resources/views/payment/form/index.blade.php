@extends('layouts.partial')

@section('main')
{{--     <h1 class="heading cursor-pointer">
        <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
            Payment Forms <span class="icon-select-arrow icon" style="    font-size: 15px;
                        padding-top: 6px;
                        padding-left: 5px;"></span>
            <div class="uk-dropdown" style="margin-top: 10px">
                <ul >
                    @include('component.list-item',[
                        'text'=>'Payments',
                        'href' => "payments",
                        'class' => 'uk-text-left',
                    ])
                    @include('component.list-item',[
                        'text'=>'Scheduled Payments',
                        'href' => "payments/scheduled",
                        'class' => 'uk-text-left',
                    ])
                    @include('component.list-item',[
                        'text'=>'Invoices',
                        'class' => 'uk-text-left',
                        'href' => "payments/invoices",
                    ])
                </ul>
            </div>
        </div>
        <span data-modal="payment/form/create" class="icon icon-plus cursor-pointer"></span>
    </h1> --}}
    @include('payment.header')

    <div class="horizontal-tabs">
        <a href="{{url('payments')}}"><div class="tab">Charges</div></a>
        <a href="{{url('payments/subscriptions')}}"><div class="tab">Subscriptions</div></a>
        <a href="{{url('payments/plans')}}"><div class="tab">Plans</div></a>
        <a href="{{url('payments/customers')}}"><div class="tab">Customers</div></a>
        <a href="{{url('payments/invoices')}}"><div class="tab">Invoices</div></a>
        <a href="{{url('payments/forms')}}"><div class="tab selected">Forms</div></a>
    </div> 

    @if($payment_forms !== null)
        @foreach($payment_forms as $payment_form)
            @include('component.list-item',[
                'text'=>$payment_form->title,
                'subtext'=>$payment_form->created_at->diffForHumans(),
                'details'=> 'payment/form/'.$payment_form->id.'/edit',
                'class'=>'has-circle-icon',
                'icon_left'=>'icon-circle-form',
            ])
        @endforeach
        @include('component.paginate',['records'=>$payment_forms])
    @endif
@endsection