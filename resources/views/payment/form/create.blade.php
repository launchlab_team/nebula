@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">New Payment Form <span class="icon icon-form"></div>
        <form method="post" enctype="multipart/form-data" action="{{url('payment/form/create')}}" data-ajax-form>
            {{csrf_field()}}
            @include('component.form-item',[
                'name' => 'name',
                'type' => 'text',
                'label' => 'Form Name',
            ])
            <button class="modal-button">Add Form</button>
        </form>
    </div>
@endsection