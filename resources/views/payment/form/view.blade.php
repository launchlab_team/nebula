@extends('layouts.default')

@section('middle')
    <div class="focus-width center">
        @if(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' || env('APP_ENV') == 'local')
            <div class="block full" style="border-color:#cedece;background:#f8fff8;margin-bottom:10px;margin-top:15px">
                @if($form->recurring)
                    <?php 
                    \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                    $plan = \Stripe\Plan::retrieve($form->plan_id);
                    $plan_amount = $plan->amount;
                    ?>
                    <b>${{$plan->amount/100}} Payment</b><br/>
                    <span class="uk-text-small">Your card will be charged each {{$plan->interval}}.</span>
                @else
                    <b>${{$form->amount}} Payment</b><br/>
                    <span class="uk-text-small">One time charge</span>
                @endif
            </div>
            @if($form->fields !== null)
                <div class="block" style="padding:0.5em 2em 1.5em 2em">
                    <div style="padding: 13px 0;border-bottom: 1px solid #f3f3f3;font-size: 1.5em;line-height: 35px;margin-bottom:10px;">
                        {{$form->title}} <span style="font-size:1.5em" class="right icon-circle-payment"></span>
                    </div>
                    <form method="post" action="{{url('payment/create')}}"  id="payment_form">
                        {{csrf_field()}} 
                        @if(count($form->fields) > 0)
                            @foreach($form->fields as $field)
                                @include('component.form-item',[
                                    'type'=>$field->type,
                                    //'name'=>'field['.$field->id.']',
                                    'name'=>'answer[]',
                                    'label'=>$field->label,
                                    'sublabel'=>$field->hint,
                                    'options'=>$field->options,
                                    'placeholder'=>$field->placeholder,
                                ])                        
                                <input type="hidden" name="question[]" value="{{$field->label}}"/>
                            @endforeach
                            <hr/>
                        @endif
                @if($form->recurring)
                        @include('payment.form.generate',['form'=>$form,'plan_amount'=>$plan_amount])
                @else
                        @include('payment.form.generate',['form'=>$form])
                @endif
                        <hr style="border-top: 1px solid #f3f3f3;">
                        <button class="center full">
                            <span class="button-icon-left icon icon-lock"></span>
                            Submit Payment
                        </button>
                    </form>
                </div>
            @endif
        @else
            <div class="message-box">Secure protocol (https) is required to accept payments. Contact <a href="mailto:team@nebulasuite.com">Nebula support</a> to install an SSL certificate for your domain.</div>
        @endif
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        $(function() {
            Stripe.setPublishableKey("{{env('STRIPE_KEY')}}");
        });
        $(document).on('submit','#payment_form',function(e) {
            {{-- var $form = $('#payment_form'); --}}
            $form = $(this); {{-- since there may be more than one on payment form on a page --}}
            {{-- Disable the submit button to prevent repeated clicks: --}}
            $form.find('button').prop('disabled', true);
            {{-- Request a token from Stripe: --}}
            Stripe.card.createToken($form, stripeResponseHandler);
            {{-- Prevent the form from being submitted: --}}
            return false;
        });
        function stripeResponseHandler(status, response) {
          {{-- Grab the form: --}}
          {{-- var $form = $('#payment_form'); --}}
          if (response.error) { {{-- Problem! --}}
            {{-- Show the errors on the form: --}}
            alert(response.error.message);
            $('button').removeAttr("disabled"); {{-- Re-enable submission --}}
          } else { {{-- Token was created! --}}
            {{-- Get the token ID: --}}
            var token = response.id;
            {{-- Insert the token ID into the form so it gets submitted to the server: --}}
            $form.append($('<input type="hidden" name="stripeToken">').val(token));
            {{-- Submit the form: --}}
            $form.get(0).submit();
          }
        };
    </script>
@overwrite