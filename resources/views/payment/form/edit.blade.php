@extends('layouts.partial')

@section('middle')
    <form method="post" action="{{url("payment/form/$form->id/edit")}}" data-ajax-form>
        <div class="detail-view">

            <span class="detail-view-icon icon icon-circle-form"></span>
            <h2 class="detail-view-heading">Edit Payment Form</h2>

            <div class="detail-view-menu">
                <button type="button" title="Delete Form" data-delete="{{url('payment/form/'.$form->id.'/delete')}}"><span class="icon uk-icon-trash-o"></span></button>
                <button title="Save Changes"><span class="icon uk-icon-save"></span></button>
                <span onClick="hideDetails()" class="icon icon-x"></span>
            </div>

            <div class="detail-view-content">
                @include('component.form-item',[
                    'name' => 'title',
                    'type' => 'text',
                    'label' => 'Name',
                    'value' => $form->title,
                ])
                <hr style="margin:5px 0" />
                @include('component.form-item',[
                    'type' => 'checkbox',
                    'label' => 'Form link is active',
                    'name' => 'active',
                    'value' => $form->active,
                ])  
                @include('component.form-item',[
                    'type' => 'checkbox',
                    'label' => 'Recurring subscription',
                    'name' => 'recurring',
                    'class' => 'recurring',
                    'value' => $form->recurring,
                ])
               @if($form->recurring == 0)
                    <div class="amount-wrapper"> 
                        @include('component.form-item',[
                            'type' => 'text',
                            'label' => 'Payment Amount',
                            'name' => 'amount',
                            'class' => 'amount',
                            'value' => $form->amount,
                        ])
                    </div>
                    <div class="interval-wrapper" style="display:none"> 
                        <div class="form-item select">
                            <span class="icon form-item-icon form-item-icon-right icon-select-arrow"></span>
                            <select name="plan">
{{--                                 @if($form->plan_id > 0)
                                    <option value="{{$form->plan_id}}" selected>{{$form->plan_name}}</option>
                                @else --}}
                                    <option disabled selected>Choose plan</option>
                                {{-- @endif --}}
                                @if($plans !== NULL)
                                @foreach($plans->data as $plan)
                                    <option @if($plan->id == $form->plan_id) selected @endif value="{{$plan->id}}">{{$plan->name}} - ${{money_format("%.2n", $plan->amount/100) . '/'.  $plan->interval}}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                @else
                    <div class="amount-wrapper" style="display:none"> 
                        @include('component.form-item',[
                            'type' => 'text',
                            'label' => 'Payment Amount',
                            'name' => 'amount',
                            'class' => 'amount',
                            'value' => $form->amount,
                        ])
                    </div>
                    <div class="interval-wrapper"> 
                        <div class="form-item select">
                            <span class="icon form-item-icon form-item-icon-right icon-select-arrow"></span>
                            <select name="plan">
                                @if($form->plan_id !== NULL)
                                    {{-- <option value="{{$form->plan_id}}" disabled selected>{{$form->plan_name}}</option> --}}
                                @else
                                    <option disabled selected>Choose plan</option>
                                @endif
                                @if($plans !== NULL)
                                @foreach($plans->data as $plan)
                                    <option @if($plan->id == $form->plan_id) selected @endif value="{{$plan->id}}">{{$plan->name}} - ${{money_format("%.2n", $plan->amount/100) . '/'.  $plan->interval}}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                @endif
                <hr style="margin:10px 0" />
                @include('component.list-item',[
                    'text' => 'Shareable Link',
                    'subtext' => url('payment/forms/'.$form->slug),
                    'icon_left' => 'icon-circle-link',
                    'class' => 'uk-text-left has-circle-icon',
                    'href' => url('payment/forms/'.$form->slug),
                    'target' => '_blank',
                ])
                <hr style="margin:10px 0" />
                <div id="form_fields">
                @if(isset($form->fields) && count($form->fields) !== 0)
                    @foreach($form->fields as $field)
                        <div class="form-item-draggable-field" id="field_{{$field->id}}">
                            @include('component.form-item',[
                                'label' => $field->label,
                                'sublabel' => $field->hint,
                                'placeholder' => $field->placeholder,
                                'type' => $field->type,
                                'options' => $field->options,
                                'draggable' => true,
                                'editable' => true,
                                'name' => $field->id, // name not used
                            ])
                            <input type="hidden" name="field_type[]" value="{{$field->type}}">
                            <input type="hidden" name="field_label[]" value="{{$field->label or ''}}">
                            <input type="hidden" name="field_hint[]" value="{{$field->hint or ''}}">
                            <input type="hidden" name="field_options[]" value="{{$field->options or ''}}">
                            <input type="hidden" name="field_placeholder[]" value="{{$field->placeholder or ''}}">
                        </div>
                    @endforeach
                @endif
                </div>

                <div class="uk-button-dropdown full" data-uk-dropdown="{mode:'click'}">
                    @include('component.button',[
                        'class' => 'full',
                        'name' => 'name',
                        'icon_left' => 'icon-plus',
                        'text' => 'Add a Field',
                        'type' => 'button',
                        //'modal' => 'form/'.$form->id.'/field/create'
                    ])
                    <div class="uk-dropdown uk-dropdown-small">
                        <ul class="uk-nav uk-nav-dropdown">
                            @include('component.list-item',[
                                'text'=>'Short Answer',
                                'icon_left'=>'uk-icon-font',
                                'modal'=>'payment/form/'.$form->id.'/field/create?type=text',
                                'dropdown' => null // Set dropdown to null if it's set in the parent view 
                            ])

                            @include('component.list-item',[
                                'text'=>'Long Answer',
                                'icon_left'=>'uk-icon-paragraph',
                                'modal'=>'payment/form/'.$form->id.'/field/create?type=textarea',
                                'dropdown' => null // Set dropdown to null if it's set in the parent view 
                            ])

                            @include('component.list-item',[
                                'text'=>'Choose Multiple',
                                'icon_left'=>'icon-square-check',
                                'modal'=>'payment/form/'.$form->id.'/field/create?type=checkboxes',
                                'dropdown' => null // Set dropdown to null if it's set in the parent view 
                            ])

                            @include('component.list-item',[
                                'text'=>'Choose One',
                                'icon_left'=>'icon-radio-checked',
                                'modal'=>'payment/form/'.$form->id.'/field/create?type=radios',
                                'dropdown' => null // Set dropdown to null if it's set in the parent view 
                            ])

                            @include('component.list-item',[
                                'text'=>'Select One',
                                'icon_left'=>'icon-select-arrow',
                                'modal'=>'payment/form/'.$form->id.'/field/create?type=select',
                                'dropdown' => null // Set dropdown to null if it's set in the parent view 
                            ])
                        </ul>
                    </div>
                </div>
            </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        $(function() {
            Stripe.setPublishableKey("{{env('STRIPE_KEY')}}");
        });
        $(document).on('submit','#payment_form',function(e) {
            var $form = $('#payment_form');
            {{-- Disable the submit button to prevent repeated clicks: --}}
            $form.find('.modal-button').prop('disabled', true);
            {{-- Request a token from Stripe: --}}
            Stripe.card.createToken($form, stripeResponseHandler);
            {{-- Prevent the form from being submitted: --}}
            return false;
        });
        function stripeResponseHandler(status, response) {
          {{-- Grab the form: --}}
          var $form = $('#payment_form');
          if (response.error) { {{-- Problem! --}}
            {{-- Show the errors on the form: --}}
            alert(response.error.message);
            $('.modal-button').removeAttr("disabled"); {{-- Re-enable submission --}}
          } else { {{-- Token was created! --}}
            {{-- Get the token ID: --}}
            var token = response.id;
            {{-- Insert the token ID into the form so it gets submitted to the server: --}}
            $form.append($('<input type="hidden" name="stripeToken">').val(token));
            {{-- Submit the form: --}}
            $form.get(0).submit();
          }
        };

        {{-- Show the interval input depending on status of recurring checkbox --}}
        $(document).on('click','.checkbox-label',function(e) {
            if ($(this).find('input').attr('name') == "recurring") {
                $('.interval-wrapper').slideToggle();
                $('.amount-wrapper').slideToggle();
                {{-- $(document).find('[name=interval]').slideToggle(''); --}}
                {{-- $(document).find('[name=amount]').slideToggle(''); --}}
            }
        });

    </script>
@overwrite