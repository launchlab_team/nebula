<input name="payment_form_id" type="hidden" value="{{$form->id or ''}}"/>
@if($form->recurring)
    <input name="amount" type="hidden" value="{{$plan_amount or ''}}"/>
@else
    <input name="amount" type="hidden" value="{{$form->amount or ''}}"/>
@endif
<input name="recurring" type="hidden" value="{{$form->recurring or ''}}"/>
<input name="plan" type="hidden" value="{{$form->plan_id or ''}}"/>
<input name="interval" type="hidden" value="{{$form->interval or ''}}"/>
<table width="100%" cellpadding="3px" style="margin-bottom: 10px;">
    <tr>
        <td class="form-item">
            <input data-stripe="name" name="cardholder" type="text" @if(App::environment('local')) value="Cardholder Name" @endif placeholder="Cardholder Name">
        </td>
        <td colspan="2" class="form-item">
            <input data-stripe="email" name="email" type="text" @if(App::environment('local')) value="team@launchlab.us" @endif placeholder="Email">
        </td>
    </tr>
    <tr>
        <td colspan="3" class="form-item">
            <input name="card_number" @if(App::environment('local')) value="4242 4242 4242 4242" @endif placeholder="Card Number" type="text" size="20" data-stripe="number">
        </td>
    </tr>
    <tr>
        <td colspan="2" class="uk-text-small form-item"><label style="margin-bottom:-8px">Expiration</label></td>
        <td class="uk-text-small form-item"><label style="margin-bottom:-8px">Security code</label></td>
    </tr>
    <tr>
        <td><input size="2" data-stripe="exp_month" type="text" placeholder="MM" @if(App::environment('local')) value="10" @endif></td>
        <td>
          <input size="2" data-stripe="exp_year" type="text" placeholder="YY" @if(App::environment('local')) value="19" @endif>
        </td>
        <td><input size="2" data-stripe="cvc" type="text" placeholder="•••" @if(App::environment('local')) value="123" @endif></td>
    </tr>
</table>