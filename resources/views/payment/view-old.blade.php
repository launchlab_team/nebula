@extends('layouts.partial')

@section('middle')
    <div class="detail-view">
        <span class="detail-view-icon icon icon-circle-payment"></span>
        <h2 class="detail-view-heading" style="margin-bottom:25px">Payment</h2>
        <div class="detail-view-menu">
            <span onClick="hideDetails()" class="icon icon-x"></span>
        </div>

        <div class="detail-view-content">
            <div class="block uk-text-left">

            @if(isset($payment->form))
                 <h3 class="uk-text-center">{{$payment->form->title}}</h3>
            @else
                 @if(strlen($payment->payment_form_id) > 0)
                     <h3 class="uk-text-center">Submitted via deleted payment form</h3>
                 @else
                     <h3 class="uk-text-center">Manually created charge</h3>
                 @endif
             @endif
             <hr/>

                {{-- List of responses from the payment form --}}
                @if(isset($payment->questions))
                    @foreach(json_decode($payment->questions) as $key => $question)
                        <label>{{$question}}</label>
                        <div class="answer">
                            {{json_decode($payment->answers)[$key]}}
                        </div>
                        <br/>
                    @endforeach
                <hr style="margin-top:0px" />
                @endif

                <label>Amount</label>
                <div class="answer">
                    {{'$'.$payment->amount}}
                </div>
                <br/>

                <label>Charge Created</label>
                <div class="answer">
                    {{date("m/d/Y h:i:s A",strtotime($payment->created_at))}} 
                </div>
                <br/>

                <label>Payment Type</label>
                <div class="answer">
                    @if($payment->recurring)
                        Recurring payment
                    @else
                        One time payment
                    @endif
                </div>
                <br/>
                
                <label>Cardholder</label>
                <div class="answer">
                    {{$payment->description}} 
                </div>
                <br/>
                
               {{--  <label>Stripe Customer ID</label>
                <div class="answer">
                    {{$payment->stripe_customer_id}} 
                </div>
                <br/> --}}


                @if($payment->recurring)
                    <label>Days between charges</label>
                    <div class="answer">
                        {{$payment->interval}}
                    </div>
                    <br/>

                    <label>Last Charge Date</label>
                    <div class="answer">
                        {{date("m/d/Y",strtotime($payment->last_charge))}} 
                    </div>
                    <br/>

                    <label>Next Charge Date</label>
                    <div class="answer">
                        {{date("m/d/Y",strtotime($payment->next_charge))}} 
                    </div>
                    <br/>
                @endif

                <label>Email</label>
                <div class="answer">
                    {{$payment->email}}
                </div>
                <hr class="uk-margin-bottom-remove" />
                @if($payment->recurring)
                    <button class="full" data-cancel-payment="{{url("payment/$payment->id/delete")}}"><span class="button-icon-left uk-icon-trash-o"></span> Cancel Plan</button>
                @endif
                <a href="https://dashboard.stripe.com" target="_blank"><button class="full"><span class="button-icon-left uk-icon-cc-stripe"></span> Go to Stripe</button></a>
                <hr style="margin-top:10px" />
                <span class="text-light uk-text-center uk-display-block">{{$payment->created_at->diffForHumans()}}</span>
            </div> 
        </div>
    </div>
@endsection