@extends('layouts.partial')

@section('main')
    <h1 class="heading cursor-pointer">
        <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
            Customers <span class="icon-select-arrow icon" style="    font-size: 15px;
                        padding-top: 6px;
                        padding-left: 5px;"></span>
            <div class="uk-dropdown" style="margin-top: 10px">
                <ul >
                    @include('component.list-item',[
                        'text'=>'Scheduled Payments',
                        'href' => "payments/scheduled",
                        'class' => 'uk-text-left',
                    ])
                    @include('component.list-item',[
                        'text'=>'Payment Forms',
                        'href' => "payments/forms",
                        'class' => 'uk-text-left',
                    ])
                    @include('component.list-item',[
                        'text'=>'Invoices',
                        'class' => 'uk-text-left',
                        'href' => "payments/invoices",
                    ])
                </ul>
            </div>
        </div>
        <span data-modal="charge/create" class="icon icon-plus cursor-pointer"></span>
    </h1>

    @if($customers !== null)
        @foreach($customers as $customer)
        {{var_dump($customer)}}
            @include('component.list-item',[
                'text'=>$customer->description,
                'subtext'=>$customer->amount,
                'class'=>'has-circle-icon',
                'icon_left'=>'icon-circle-user',
            ])
        @endforeach
        @include('component.paginate',['records'=>$paginator])
    @endif
@endsection