@extends('layouts.partial')

@section('main')
    {{-- <h1 class="heading">
        Payments
        <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span title="Add New" class="icon cursor-pointer icon-plus"></span>
            <div class="uk-dropdown" style="margin-top: 10px">
                <ul>
                    @include('component.list-item',[
                        'text'=>'New Charge',
                        'modal' => "payments/create",
                        'class' => 'uk-text-left'
                    ])
                    @include('component.list-item',[
                        'text'=>'Add Subscription',
                        'modal' => "payments/subscription/create",
                        'class' => 'uk-text-left'
                    ])
                    @include('component.list-item',[
                        'text'=>'Create Payment Plan',
                        'modal' => "payments/plan/create",
                        'class' => 'uk-text-left'
                    ])
                </ul>
            </div>
        </div>
    </h1> --}}
    @include('payment.header')

    <div class="horizontal-tabs">
        <a href="{{url('payments')}}"><div class="tab">Charges</div></a>
        <a href="{{url('payments/subscriptions')}}"><div class="tab">Subscriptions</div></a>
        <a href="{{url('payments/plans')}}"><div class="tab">Plans</div></a>
        <a href="{{url('payments/customers')}}"><div class="tab selected">Customers</div></a>
        <a href="{{url('payments/invoices')}}"><div class="tab">Invoices</div></a>
        <a href="{{url('payments/forms')}}"><div class="tab">Forms</div></a>
    </div> 

    @if($customers !== null && count($customers) > 0)
        @foreach($customers as $customer)
        {{-- {{dd($customer->name)}} --}}
            @include('component.list-item',[
                'text'=>$customer->description,
                'subtext'=>$customer->email,
                'details'=>'stripe/customer/'.$customer->id,
                'class'=>'has-circle-icon',
                'icon_left'=>'icon-circle-user',
            ])
        @endforeach
    @else
        <div class="message-center">No customers yet.</div>
    @endif
@endsection