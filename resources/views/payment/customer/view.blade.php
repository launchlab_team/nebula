@extends('layouts.partial')

@section('middle')
    <div class="detail-view">
        <div class="detail-view-menu">
            <span onClick="hideDetails()" class="icon icon-x"></span>
        </div>

        <div class="detail-view-content animated slideInUp">
            <span class="detail-view-icon icon icon-circle-user"></span>
            <h1 class="detail-view-heading">Customer</h1>
            <div class="block uk-text-left">

            <h3 class="uk-text-center">{{$customer->description}}</h3>
             <hr/>

                <label>Created</label>
                <div class="answer">
                    {{date("m/d/y h:i A",$customer->created)}} 
                </div>
                <br/>
                
                <label>Email</label>
                <div class="answer">
                    {{$customer->email}}
                </div>
                <br/>

                <label>Stripe Customer ID</label>
                <div class="answer">
                    {{$customer->id}}
                </div>
                <br/>

                <a href="https://dashboard.stripe.com/customers/{{$customer->id}}" target="_blank"><button class="full"><span class="button-icon-left uk-icon-cc-stripe"></span> View in Stripe</button></a>
                <span class="text-light uk-text-center uk-display-block"></span>
            </div> 
        </div>
    </div>
@endsection