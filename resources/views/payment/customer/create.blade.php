@extends('layouts.partial')

@section('main')
    <div class="modal">
        <h1 class="modal-heading">New Customer <span class="icon icon-user"></h1>
        <form method="post" action="{{url('payment/customer/create')}}"  id="payment_form">
            {{csrf_field()}} 
            <table width="100%" cellpadding="3px" style="margin-bottom: 10px;">
{{--                 <tr>
                    <td colspan="3" class="form-item">
                        <input name="amount" type="text" @if(App::environment('local')) value="10.99" @endif placeholder="Amount">
                    </td>
                </tr> --}}
                <tr>
                    <td class="form-item">
                        <input data-stripe="name" name="cardholder" type="text" @if(App::environment('local')) value="Cardholder Name" @endif placeholder="Cardholder Name">
                    </td>
                    <td colspan="2" class="form-item">
                        <input name="email" type="text" @if(App::environment('local')) value="team@launchlab.us" @endif placeholder="Email">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="form-item">
                        <input name="card_number" @if(App::environment('local')) value="4242 4242 4242 4242" @endif placeholder="Card Number" type="text" size="20" data-stripe="number">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="uk-text-small form-item"><label style="margin-bottom:-10px">Expiration (MM, YY)</label></td>
                    <td class="uk-text-small form-item"><label style="margin-bottom:-10px">Security code</label></td>
                </tr>
                <tr>
                    <td>
                        {{-- <input size="2" data-stripe="exp_month" type="text" placeholder="MM" @if(App::environment('local')) value="10" @endif> --}}
                        <select data-stripe="exp_month">
                            <option value="1">01</option>
                            <option value="2">02</option>
                            <option value="3">03</option>
                            <option value="4">04</option>
                            <option value="5">05</option>
                            <option value="6">06</option>
                            <option value="7">07</option>
                            <option value="8">08</option>
                            <option value="9">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>
                    </td>
                    <td style="min-width: 50px;">
                      {{-- <input size="2" data-stripe="exp_year" type="text" placeholder="YY" @if(App::environment('local')) value="19" @endif> --}}
                        <select data-stripe="exp_year">
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27" @if(App::environment('local')) selected @endif>27</option>
                        </select>
                    </td>
                    <td><input size="2" data-stripe="cvc" type="text" placeholder="•••" @if(App::environment('local')) value="123" @endif></td>
                </tr>
            </table>
            {{-- @include('component.form-item',['type'=>'checkbox','label'=>'Make this a recurring payment','name'=>'recurring'])
            <div class="form-interval-wrapper" style="display:none;margin-top:-5px;">
                @include('component.form-item',[
                    'type'=>'text',
                    'label'=>'Interval between charges in days',
                    'placeholder'=>'30',
                    'name'=>'interval',
                    'attributes'=>'maxlength="3"',
                ])
            </div> --}}
            <button class="modal-button">Create Customer</button>
        </form>
    </div>

@endsection

@section('scripts')
    @parent
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        $(function() {
            Stripe.setPublishableKey("{{env('STRIPE_KEY')}}");
        });
        $(document).on('submit','#payment_form',function(e) {
            var $form = $('#payment_form');
            {{-- Disable the submit button to prevent repeated clicks:  --}}
            $form.find('button').prop('disabled', true);
            {{-- Request a token from Stripe: --}}
            Stripe.card.createToken($form, stripeResponseHandler);
            {{-- Prevent the form from being submitted: --}}
            return false;
        });
        function stripeResponseHandler(status, response) {
          {{-- Grab the form: --}}
          var $form = $('#payment_form');
          if (response.error) { {{-- Problem! --}}
            {{-- Show the errors on the form: --}}
            alert(response.error.message);
            $('button').removeAttr("disabled"); {{-- Re-enable submission --}}
          } else { {{-- Token was created! --}}
            {{-- Get the token ID: --}}
            var token = response.id;
            {{-- Insert the token ID into the form so it gets submitted to the server: --}}
            $form.append($('<input type="hidden" name="stripeToken">').val(token));
            {{-- Submit the form: --}}
            $form.get(0).submit();
          }
        };

        $(document).on('click','.checkbox-label',function(e) {
            $('.form-interval-wrapper').slideToggle();
            $(document).find('input[name=interval]').val('');
            if ($(document).find('input[type=checkbox]').val() !== 'on') {
                {{-- Clear interval --}}
            }
        });

    </script>
@overwrite
