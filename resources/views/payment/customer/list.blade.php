{{-- This view shows the customer results e.g. when searching a customers to create a subscription for --}}
@if(count($customers) > 0)
    @foreach($customers as $customer)
        @include('component.list-item',[
            'text' => $customer->name,
            'subtext'=>$customer->email,
            'icon_right'=>'icon-plus',
            'attributes'=>'data-add-customer-to-form="'.$customer->customer_id.'"', // payments.js
        ])
    @endforeach
@else
    <div class="message-box">No customers found under that term.</div>
@endif