@extends('layouts.partial')

@section('main')
    <h1 class="heading">
       <a href="{{url('dashboard/files/users')}}">User Files</a> <span class="icon-angle-right" style="font-size:14px"></span> {{ucwords($user->name)}}'s Files
{{--         <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span class="icon icon-menu cursor-pointer"></span>
            <div class="uk-dropdown">
                <ul>
                    @include('component.list-item',[
                        'text'=>'New Upload',
                        'icon_left'=>'icon-upload',
                        'modal' => "files/import",
                    ])
                    @include('component.list-item',[
                        'text'=>'New Folder',
                        'icon_left'=>'uk-icon-folder-o',
                        'modal' => "files/folder/create",
                    ])
                </ul>
            </div>
        </div> --}}
    </h1>


    @if($user->files !== null)
        @foreach($user->files as $file)
            @include('component.list-item',[
                'text' => $file->name,
                'class' => 'has-circle-icon',
                'icon_left' => getIconFromType($file->type),
                'subtext' => humanFileSize($file->size) . ' | '.$file->type,
                'details' => 'file/'.$file->id.'/edit',
            ])
        @endforeach
        @include('component.paginate',['records'=>$user->files])
    @endif
@endsection
