@extends('layouts.partial')

@section('main')
    <h1 class="heading">
       Shared with Me
        <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span class="icon icon-menu cursor-pointer"></span>
            <div class="uk-dropdown">
                <ul>
                    @include('component.list-item',[
                        'text'=>'New Upload',
                        'icon_left'=>'icon-upload',
                        'modal' => "files/import",
                    ])
                    @include('component.list-item',[
                        'text'=>'New Folder',
                        'icon_left'=>'uk-icon-folder-o',
                        'modal' => "files/folder/create",
                    ])
                </ul>
            </div>
        </div>
    </h1>

    <div class="horizontal-tabs">
        <a href="{{url('files')}}"><div class="tab">My Files</div></a>
        <a href="{{url('dashboard/files/shared')}}"><div class="tab selected">Shared with Me</div></a>
        <a href="{{url('dashboard/files/users')}}"><div class="tab">User Uploads</div></a>
    </div>

    @if(isset($user) && count($user->file_shares) > 0)
        @if(count($user->file_shares) > 0)
            @include('component.list-item',[
                'text' => 'Uncategorized',
                'class' => 'has-circle-icon',
                'subtext' =>  (count($user->file_shares) == 1) ? count($user->file_shares) . ' ' . ' Total' : count($user->file_shares) . ' ' . ' Total',
                'icon_left' => 'icon-circle-folder',
                'href' => 'files/dashboard/shared/uncategorized',
            ])
        @endif
        <hr style="margin: 0px 0 9px 0;">
    @endif
    @if(isset($user) && count($user->file_folder_shares) > 0)
        @foreach($user->file_folder_shares as $file_folder_share)
            @if(isset($file_folder_share->folder))
                @include('component.list-item',[
                    'text' => $file_folder_share->folder->name,
                    'class' => 'has-circle-icon',
                    'icon_left' => 'icon-circle-folder',
                    'subtext' => (count($file_folder_share->folder->files) == 1) ? '1 File' :  count($file_folder_share->folder->files) . ' Files',
                    'href' => 'files/folder/'.$file_folder_share->folder->id,
                ])
            @endif
        @endforeach
    @endif

@endsection
