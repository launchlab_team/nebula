@extends('layouts.partial')

@section('main')
    <h1 class="heading">
       User Files
        <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span class="icon icon-menu cursor-pointer"></span>
            <div class="uk-dropdown">
                <ul>
                    @include('component.list-item',[
                        'text'=>'New Upload',
                        'icon_left'=>'icon-upload',
                        'modal' => "files/import",
                    ])
                    @include('component.list-item',[
                        'text'=>'New Folder',
                        'icon_left'=>'uk-icon-folder-o',
                        'modal' => "files/folder/create",
                    ])
                </ul>
            </div>
        </div>
    </h1>

    <div class="horizontal-tabs">
        <a href="{{url('files')}}"><div class="tab">My Files</div></a>
        <a href="{{url('dashboard/files/shared')}}"><div class="tab">Shared with Me</div></a>
        <a href="{{url('dashboard/files/users')}}"><div class="tab selected">User Uploads</div></a>
    </div>

{{--     @if(isset($uncategorized_files) && $uncategorized_files !== null)
        @include('component.list-item',[
            'text' => 'Uncategorized',
            'class' => 'has-circle-icon',
            'subtext' =>  (count($uncategorized_files) == 1) ? count($uncategorized_files) . ' ' . ' File' : count($uncategorized_files) . ' ' . ' Files',
            'icon_left' => 'icon-circle-user',
            'href' => 'dashboard/users/files/uncategorized',
        ])
    @endif --}}

    {{-- Loop through user folders --}}
    @foreach($users as $user)
        @if(count($user->files) > 0)
            @include('component.list-item',[
                'text' => $user->name,
                'class' => 'has-circle-icon',
                'subtext' =>  (count($user->files) == 1) ? count($user->files) . ' ' . ' File' : count($user->files) . ' ' . ' Files',
                'icon_left' => 'icon-circle-user',
                'href' => 'dashboard/users/'.$user->id.'/files',
            ])
        @endif
    @endforeach

@endsection
