{{-- This view displays uncategorized files for the authorized user --}}
@extends('layouts.partial')

@section('main')
    <h1 class="heading">
       <a href="{{url('dashboard/files/shared')}}">Shared with Me</a> <span class="icon-angle-right" style="font-size:14px"></span> Uncategorized
        <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span class="icon icon-menu cursor-pointer"></span>
            <div class="uk-dropdown">
                <ul>
                    @include('component.list-item',[
                        'text'=>'Upload File',
                        'icon_left'=>'icon-upload',
                        'modal' => "files/import",
                    ])
                    @include('component.list-item',[
                        'text'=>'New Folder',
                        'icon_left'=>'uk-icon-folder-o',
                        'modal' => "files/folder/create",
                    ])
                </ul>
            </div>
        </div>
    </h1>

    <div class="horizontal-tabs">
        <a href="{{url('files')}}"><div class="tab">My Files</div></a>
        <a href="{{url('dashboard/files/shared')}}"><div class="tab selected">Shared with Me</div></a>
        <a href="{{url('dashboard/files/users')}}"><div class="tab">User Uploads</div></a>
    </div>

    @if($files !== null)
        @foreach($files as $file)
            @include('component.list-item',[
                'text' => $file->name,
                'class' => 'has-circle-icon',
                'icon_left' => getIconFromType($file->type),
                'subtext' => humanFileSize($file->size) . ' | '.$file->type,
                'details' => 'file/'.$file->id.'/edit',
            ])
        @endforeach
        @include('component.paginate',['records'=>$files])
    @endif
@endsection
