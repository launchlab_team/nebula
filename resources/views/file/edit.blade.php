@extends('layouts.partial')

@section('middle')
    <form method="post" action="{{url("file/$file->id/update")}}" data-ajax-form>
        <div class="detail-view">

            <span class="detail-view-icon icon {{getIconFromType($file->type)}}"></span>
            <h2 class="detail-view-heading">{{$file->type}}</h2>

            <div class="detail-view-menu">
                <button type="button" title="Delete file" data-delete="{{url('file/'.$file->id.'/delete')}}"><span class="icon uk-icon-trash-o"></span></button>
                <button title="Save Changes"><span class="icon uk-icon-save"></span></button>
                <span onClick="hideDetails()" class="icon icon-x"></span>
            </div>

            <div class="detail-view-content">
                @include('component.form-item',[
                    'name' => 'name',
                    'type' => 'text',
                    'label' => 'Name',
                    'value' => $file->name,
                ])

                <div class="form-item">
                    <label>Folder</label>
                    <span class="icon form-item-icon form-item-icon-right icon-select-arrow"></span>
                    <select name="folder_id">
                        @if (isset($file->folder_id) && $file->folder_id !== null)
                            <option value="">No Folder</option>
                        @else
                            <option disabled selected>Move to folder</option> {{-- if no folder set --}}
                        @endif
                        @if($folders !== null)
                            @foreach($folders as $folder)
                                {{-- {{$folder->id}} --}} 
                                <option @if($file->folder_id !== null) @if($folder->id == $file->folder_id) selected @endif @endif value="{{$folder->id}}">{{$folder->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div> 

                <hr class="uk-margin-small" />
                @include('component.button',[
                    'href' => url('file/'.$file->id.'/download'),
                    'icon_left' => 'icon-download',
                    'text' => 'Download',
                    'class' => 'full',
                    'target' => '_blank',
                    'download' => ($file->type == 'image') ? NULL : $file->name,
                ])
                <br/>
                @include('component.button',[
                    'icon_left' => 'uk-icon-share',
                    'text' => 'Share File',
                    'class' => 'full',
                    'attributes' => 'type="button"',
                    'modal' => 'file/'.$file->id.'/share',
                ])
            </div>
    </div>
@endsection