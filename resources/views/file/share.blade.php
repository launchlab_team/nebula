@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Share File <span class="icon icon-upload"></div>
        <form method="post" enctype="multipart/form-data" action="{{url('file/'.$file->id.'/share')}}" data-ajax-form>
            {{csrf_field()}}
            @include('component.form-item',[
                'name' => 'emails',
                'type' => 'textarea',
                'label' => 'Share with:',
                'placeholder' => 'one@example.com, two@example.com',
                'sublabel' => 'Comma seperated email addresses',
            ])
            <div class="message-box">{{$file->name}} <br>will be made available under "My Files" in each user's account</div>
            <button class="modal-button">Share File</button>
        </form>
    </div>
@endsection