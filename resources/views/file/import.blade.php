@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Upload File(s) <span class="icon icon-upload"></div>
        <form method="post" id="upload_form" enctype="multipart/form-data" action="{{url('files/import')}}">
            {{csrf_field()}}
            <input type="file" name="file[]" id="upload_input" multiple/>
            @if(isset($folder_id) && $folder_id !== NULL)
                <input type="hidden" name="folder_id" value="{{$folder_id}}"/>
            @endif
            <span class="icon icon-upload"></span>
        </form>
        <div style="margin-top:10px" class="message-box">Hint: Compress (.zip) large files before uploading for better performance.</div>
    </div>
@endsection