@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        {{ucfirst($settings->organization_name)}} Files
        <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span class="icon icon-menu cursor-pointer"></span>
            <div class="uk-dropdown">
                <ul>
                    @include('component.list-item',[
                        'text'=>'New Upload',
                        'icon_left'=>'icon-upload',
                        'modal' => "files/import",
                    ])
                    @include('component.list-item',[
                        'text'=>'New Folder',
                        'icon_left'=>'uk-icon-folder-o',
                        'modal' => "files/folder/create",
                    ])
                </ul>
            </div>
        </div>
    </h1>

    <div class="horizontal-tabs">
        <a href="{{url('files')}}"><div class="tab selected">My Files</div></a>
        <a href="{{url('dashboard/files/shared')}}"><div class="tab">Shared with Me</div></a>
        <a href="{{url('dashboard/files/users')}}"><div class="tab">User Uploads</div></a>
        <a href="https://drive.google.com" target="_blank"><div class="tab right uk-hidden-small"><span class="uk-icon-google"></span> Drive &nbsp;<span style="font-size: .8em" class="uk-hidden-small icon-external-link"></span></div></a>
    </div>

    @if(isset($uncategorized_files) && $uncategorized_files !== NULL && count($uncategorized_files) > 0)
        {{-- Upload new file --}}
        @include('component.list-item',[
            'text' => 'Uncategorized',
            'class' => 'has-circle-icon',
            'icon_left' => 'icon-circle-folder',
            'subtext' => count($uncategorized_files) . ' Total',
            'view' => 'dashboard/files/uncategorized',
        ])
    @endif

    <hr style="margin: 0px 0 9px 0;">

    {{-- Loop through folders --}}
    @if(count($folders) > 0)
    @foreach($folders as $folder)
        @include('component.list-item',[
            'text' => $folder->name,
            'class' => 'has-circle-icon',
            'icon_left' => 'icon-circle-folder',
            'subtext' => count($folder->files) . ' Files',
            'href' => 'files/folder/'.$folder->id,
        ])
    @endforeach
    @endif
    {{-- @if($files !== null)
        @foreach($files as $file)
            @include('component.list-item',[
                'text' => $file->name,
                'class' => 'has-circle-icon',
                'icon_left' => getIconFromType($file->type),
                'subtext' => humanFileSize($file->size) . ' | '.$file->type,
                'details' => 'file/'.$file->id.'/edit',
            ])
        @endforeach
        @include('component.paginate',['records'=>$files])
    @endif --}}
@endsection
