@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        My Uploads
        <span class="icon-cloud right"></span>
    </h1>

    @if($files !== null)
        {{-- Loop through files --}}
        @foreach($files as $file)
            @include('component.list-item',[
                'text' => $file->name,
                'class' => 'has-circle-icon',
                'icon_left' => getIconFromType($file->type),
                'subtext' => humanFileSize($file->size) . ' | '.$file->type,
                'href' => 'file/'.$file->id.'/download',
                //'download'=>$file->name,
                'target'=>'_blank',
            ])
        @endforeach
        @include('component.paginate',['records'=>$files])
    @endif
@endsection
