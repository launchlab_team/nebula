@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        Shared with Me
    </h1>

    {{-- Loop through files --}}
    @if($files !== null)
    @foreach($files as $file)
        @include('component.list-item',[
            'text' => $file->name,
            'class' => 'has-circle-icon',
            'icon_left' => getIconFromType($file->type),
            'subtext' => humanFileSize($file->size) . ' | '.$file->type,
            'href' => 'file/'.$file->id.'/download',
            //'target' => '_blank',
            'download'=>$file->name,
        ])
    @endforeach
    @endif

@endsection
