@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        <a href="{{url('files')}}">{{-- <span class="left icon-back"></span> --}}My Files</a>  <span class="icon-angle-right" style="font-size:14px"></span> {{$folder->name}}
        <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span class="icon icon-menu cursor-pointer"></span>
            <div class="uk-dropdown">
                <ul>
                    @include('component.list-item',[
                        'text'=>'Add File',
                        'icon_left'=>'icon-upload',
                        'modal' => "files/import/".$folder->id,
                    ])
                    @include('component.list-item',[
                        'text'=>'Edit Folder',
                        'icon_left'=>'uk-icon-folder-o',
                        'modal' => "files/folder/".$folder->id."/edit",
                    ])
                    @include('component.list-item',[
                        'text'=>'Share Folder',
                        'icon_left'=>'uk-icon-share',
                        'modal' => "files/folder/".$folder->id."/share",
                    ])
                    @include('component.list-item',[
                        'text'=>'Delete Folder',
                        'icon_left'=>'icon-trash',
                        'attributes' => 'data-delete="'.$folder->id.'/delete"',
                    ])
                </ul>
            </div>
        </div>
    </h1>

    {{-- Loop through files --}}
    @if($folder->files !== null)
    @foreach($folder->files as $file)
        @include('component.list-item',[
            'text' => $file->name,
            'class' => 'has-circle-icon',
            'icon_left' => getIconFromType($file->type),
            'subtext' => humanFileSize($file->size) . ' | '.$file->type,
            'details' => 'file/'.$file->id.'/edit',
        ])
    @endforeach
    @endif

@endsection
