@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">New Folder <span class="icon uk-icon-folder-o"></div>
        <form method="post" enctype="multipart/form-data" action="{{url('files/folder/store')}}" data-ajax-form>
            {{csrf_field()}}
            @include('component.form-item',[
                'name' => 'name',
                'type' => 'text',
                'label' => 'Folder Name',
            ])
            <button class="modal-button">Create Folder</button>
        </form>
    </div>
@endsection