@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        {{$folder->name}}
    </h1>

    {{-- Loop through files --}}
    @if($folder->files !== null)
    @foreach($folder->files as $file)
        @include('component.list-item',[
            'text' => $file->name,
            'class' => 'has-circle-icon',
            'icon_left' => getIconFromType($file->type),
            'subtext' => humanFileSize($file->size) . ' | '.$file->type,
            'href' => 'file/'.$file->id.'/download',
            //'target' => '_blank',
            'download'=>$file->name,
        ])
    @endforeach
    @endif

@endsection
