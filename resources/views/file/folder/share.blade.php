@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Share Folder <span class="icon uk-icon-folder-o"></div>
        <form method="post" enctype="multipart/form-data" action="{{url('files/folder/'.$folder->id.'/share')}}" data-ajax-form>
            {{csrf_field()}}
            @include('component.form-item',[
                'name' => 'emails',
                'type' => 'textarea',
                'label' => 'Share with:',
                'placeholder' => 'one@example.com, two@example.com',
                'sublabel' => 'Comma seperated email addresses',
            ])
            <div class="message-box"><u>{{$folder->name}}</u><br>will be made available under "My Files" in each user's account</div>
            <button class="modal-button">Share Folder</button>
        </form>
    </div>
@endsection