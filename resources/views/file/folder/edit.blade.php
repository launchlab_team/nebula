@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Edit Folder <span class="icon uk-icon-folder-o"></div>
        <form method="post" enctype="multipart/form-data" action="{{url('files/folder/'.$folder->id.'/update')}}" data-ajax-form>
            {{csrf_field()}}
            @include('component.form-item',[
                'name' => 'name',
                'type' => 'text',
                'label' => 'Folder Name',
                'value' => $folder->name,
            ])
            <button class="modal-button">Save Changes</button>
        </form>
    </div>
@endsection