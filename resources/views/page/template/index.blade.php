@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        Page Templates
        <span data-modal="page/template/create" title="Create Template" class="icon cursor-pointer icon-plus"></span>
    </h1>
    @include('component.list-item',[
        'text' => 'Pages',
        'class' => 'has-circle-icon',
        'icon_left' => 'icon-circle-folder',
        'href' => url('dashboard#pages'),
        'attributes' =>'onclick="window.history.back()"',
    ])
    @if(isset($templates) && $templates !== null)
        @foreach($templates as $template)
            @include('component.list-item',[
                'text' => $template->title,
                'class' => 'has-circle-icon',
                'icon_left' => 'icon-circle-layout',
                'subtext' => url($template->page->slug),
                'details' => 'template/'.$template->id.'/edit',
            ])
        @endforeach
    @endif
@endsection