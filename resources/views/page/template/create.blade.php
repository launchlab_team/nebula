@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">New Template</div>
        <form method="post" action="{{url('page/create')}}" data-ajax-form>
            {{csrf_field()}} 
            @include('component.form-item',[
                'name' => 'title',
                'type' => 'text',
                'label' => 'Title',
            ])
            <div class="form-item">
                <label>Choose page</label>
                <select name="page_id">
                    <option disabled selected>Choose a page...</option>
                </select>
            </div>
            <button class="modal-button">Create Template</button>
        </form>

    </div>
@endsection