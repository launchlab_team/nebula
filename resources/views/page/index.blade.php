@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        Pages
        <span data-modal="page/create" title="New Page" class="icon cursor-pointer icon-plus"></span>
        <span data-modal="pages/settings" title="Menu Settings" class="icon cursor-pointer icon-settings"></span>
    </h1>
    {{-- @include('component.list-item',[
        'text' => 'Page Templates',
        'class' => 'has-circle-icon',
        'icon_left' => 'icon-circle-folder',
        'href' => 'page/templates',
    ]) --}}
    @if(isset($pages) && $pages !== null)
        @foreach($pages as $page)
            @include('component.list-item',[
                'text' => $page->title,
                'class' => 'has-circle-icon',
                'icon_left' => 'icon-circle-layout',
                'subtext' => url($page->slug),
                'details' => 'page/'.$page->id.'/settings',
            ])
        @endforeach
    @endif
@endsection