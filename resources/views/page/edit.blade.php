@extends('layouts.partial')

@section('middle')
<form method="post" action="{{url("page/$page->id/settings")}}" data-ajax-form>
    <div class="detail-view">

        <span class="detail-view-icon icon icon-circle-layout"></span>
        <h2 class="detail-view-heading">Page Settings</h2>

        <div class="detail-view-menu">
            @if($page->slug !== '')
                {{-- don't allow delete on homepage --}}
                <button type="button" title="Delete Page" data-delete="{{url('page/'.$page->id.'/delete')}}"><span class="icon uk-icon-trash-o"></span></button>
            @endif
            <button title="Save Changes"><span class="icon uk-icon-save"></span></button>
            <span onClick="hideDetails()" class="icon icon-x"></span>
            <a href="{{url($page->slug)}}" target="_blank"><button type="button" title="Open Page"><span class="icon uk-icon-eye"></span></button></a>
            <a href="{{url('page/'.$page->id.'/editor')}}" target="_blank"><button type="button" title="Edit Page"><span class="icon icon-edit"></span></button></a>
        </div>

        <div class="detail-view-content">
            {{csrf_field()}} 
            @include('component.form-item',[
                'name' => 'title',
                'type' => 'text',
                'label' => 'Page Title',
                'value' => $page->title,
            ])
            @if($page->slug !== '')
                @include('component.form-item',[
                    'name' => 'slug',
                    'type' => 'text',
                    'label' => 'Slug',
                    'sublabel' => env('APP_DOMAIN').'/',
                    'value' => $page->slug,
                ])
            @endif
{{--             @include('component.form-item',[
                'name' => 'layout',
                'type' => 'select',
                'label' => 'Layout',
                'options' => 'Default,Partial,Sidebar',
                'placeholder' => ucfirst($page->layout),
            ]) --}}
        </div>

    </div>
</form>
@endsection

@section('scripts')
    @parent
    {{-- <script type="text/javascript">    
        $(document).on('keyup', '[name=title]', function() {
            var title = $(this).val();
            // Generate url ready slug
            var slug = title.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]+/g, "-").toLowerCase();
            $('[name=slug]').val(slug);
            $('[name=slug]').parent().find('.form-item-sublabel').html('{{ getenv('APP_DOMAIN') }}/'+slug);
        });
        $(document).on('keyup', '[name=slug]', function() {
            var title = $(this).val();
            // Generate url ready slug
            var slug = title.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]+/g, "-").toLowerCase();
            $('[name=slug]').val(slug);
            $(this).parent().find('.form-item-sublabel').html('{{ getenv('APP_DOMAIN') }}/'+slug);
        });
    </script> --}}
@endsection