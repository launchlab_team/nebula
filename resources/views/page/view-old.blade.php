@extends('layouts.default')

{{-- @section('header')
    @include('dashboard.header')
@overwrite --}}

@section('links')
    @parent
    <style type="text/css">
    {!! $settings->css or '' !!}
    body {
        padding-bottom: 0px !important;
    }
    </style>
@overwrite

@section('title',$page->title)

@section('subheader')
    @if(Auth::user() !== null && Auth::user()->is_manager)
        <a data-modal="pages/settings"><span class="pages-settings-icon uk-icon-cog"></span></a>
    @endif
@endsection

@section('main')


    <div id="page">
    @if(Auth::user() !== null && Auth::user()->is_manager)
        @include('component.add-button')
    @endif
    {{csrf_field()}}
    @foreach($page->elements as $element)
            @if($element->type == 'page-section')
                <div class="page-section {{$element->class or ''}}"
                    style="{{$element->style or ''}}">
                    @if(Auth::user() !== null && Auth::user()->is_manager)
                        {{--Show edit tools if admin--}}
                        <div class="page-section-toolbar" data-delete-section="element/{{$element->id}}/delete">
                            <span class="icon uk-icon-trash-o" title="Delete Section"></span>
                        </div>
                    @endif
                    <div class="uk-grid uk-grid-collapse" data-uk-grid-match>
                        @foreach($page->elements as $page_block)
                            @if($page_block->parent_id == $element->id) 
                                {{-- Must be a page block --}}
                                @if($page_block->type == 'page-block-full')
                                    <div class="background-cover uk-width-1-1" @if($page_block->parallax !== null && $page_block->parallax != 0) data-uk-parallax="{bg: '-200'}" @endif style="display:table;{{$page_block->style or ''}}@if($page_block->background_image !== null)
                                       ;background-image:url({{urlencode($page_block->background_image)}}); @endif">
                                @endif
                                @if($page_block->type == 'page-block-half')
                                    <div class="background-cover uk-width-medium-1-2" @if($page_block->parallax !== null && $page_block->parallax != 0) data-uk-parallax="{bg: '-200'}" @endif  style="display:table;{{$page_block->style or ''}};@if($page_block->background_image !== null)background-image:url({{urlencode($page_block->background_image)}}); @endif">
                                @endif
                                @if($page_block->type == 'page-block-third')
                                    <div class="background-cover uk-width-medium-1-3" @if($page_block->parallax !== null && $page_block->parallax != 0) data-uk-parallax="{bg: '-200'}" @endif  style="display:table;{{$page_block->style or ''}};@if($page_block->background_image !== null)background-image:url({{urlencode($page_block->background_image)}}); @endif">
                                @endif
                                @if($page_block->type == 'page-block-fourth')
                                    <div class="background-cover uk-width-medium-1-4"  @if($page_block->parallax !== null && $page_block->parallax != 0) data-uk-parallax="{bg: '-200'}" @endif  style="display:table;{{$page_block->style or ''}};@if($page_block->background_image !== null)background-image:url({{urlencode($page_block->background_image)}}); @endif">
                                @endif
                                @if($page_block->overlay !== '' && $page_block->overlay !== null)
                                    <div class="page-block-overlay" style="{{$page_block->overlay}}"></div>
                                @endif
                                    <div class="page-block @if(Auth::user() !== null && Auth::user()->is_admin) page-block-editable @endif">
                                        {{-- <div class="page-block-toolbar">
                                            <span data-details="block/{{$page_block->id}}/add" class="icon icon-plus" title="Add Element to Block"></span>
                                        </div> --}} 
                                        @if(Auth::user() !== null && Auth::user()->is_manager)
                                            <div class="page-block-toolbar">
                                                <span class="icon uk-icon-align-left" data-left-justify-block="{{$page_block->id}}"></span>
                                                <span class="icon uk-icon-align-center" data-center-justify-block="{{$page_block->id}}"></span>
                                                <span class="icon uk-icon-align-right" data-right-justify-block="{{$page_block->id}}"></span>
                                                &nbsp;
                                                &nbsp;
                                                &nbsp;
                                                <span class="icon icon-edit" data-details="element/{{$page_block->id}}/edit" title="Edit Block"></span>
                                                <span class="icon icon-plus" data-details="block/{{$page_block->id}}/add" title="Add Element to Block"></span>
                                            </div>
                                        @endif
                                        {{-- Include child elements --}}
                                        @foreach($page->elements as $page_element)
                                            @if($page_element->parent_id == $page_block->id)
                                                <div id="{{$page_element->id}}"  @if(Auth::user() !== null && Auth::user()->is_manager) data-details="element/{{$page_element->id}}/edit" @endif class="element
                                                    @if($page_element->class !== null) 
                                                        {{$page_element->class}} 
                                                    @endif
                                                    @if(Auth::user() !== null && Auth::user()->is_manager) element-editable @endif"
                                                    style="@if($page_element->style !== null) 
                                                        {{$page_element->style}} 
                                                    @endif"
                                                    ">
                                                    {{-- <div class="element-toolbar">
                                                        <span class="icon uk-icon-trash-o" data-delete="element/{{$element->id}}/delete" title="Delete Section"></span>
                                                        <span class="icon icon-edit" data-modal="element/{{$element->id}}/edit" title="Edit Section"></span>
                                                    </div> --}}
                                                    @if($page_element->type == 'paragraph')
                                                        <p class="page-paragraph 
                                                        @if($page_element->alignment !== null) 
                                                            uk-text-{{$page_element->alignment}} 
                                                        @endif 
                                                        {{$page_element->class or ''}}">{!! html_entity_decode($page_element->html) !!}</p>
                                                    @endif
                                                    @if($page_element->type == 'h1')
                                                        <h1 class="page-heading {{$page_element->class or ''}}" style="{{$page_element->style or ''}}">{!!$page_element->html or ''!!}</h1>
                                                    @endif
                                                    @if($page_element->type == 'h2')
                                                        <h2 class="page-heading {{$page_element->class or ''}}" style="{{$page_element->style or ''}}">{!!$page_element->html or ''!!}</h2>
                                                    @endif
                                                    @if($page_element->type == 'h3')
                                                        <h3 class="page-heading {{$page_element->class or ''}}" style="{{$page_element->style or ''}}">{!!$page_element->html or ''!!}</h3>
                                                    @endif
                                                    @if($page_element->type == 'h4')
                                                        <h4 class="page-heading {{$page_element->class or ''}}" style="{{$page_element->style or ''}}">{!!$page_element->html or ''!!}</h4>
                                                    @endif
                                                    @if($page_element->type == 'h5')
                                                        <h5 class="page-heading {{$page_element->class or ''}}" style="{{$page_element->style or ''}}">{!!$page_element->html or ''!!}</h5>
                                                    @endif
                                                    @if($page_element->type == 'image')
                                                        <img src="{{url($page_element->image or '')}}" alt="{{$page_element->alt or ''}}"/>
                                                    @endif
                                                    @if($page_element->type == 'button')
                                                        <button class="page-button {{$page_element->class or ''}}">{!! html_entity_decode($page_element->html) !!}</button>
                                                    @endif
                                                    @if($page_element->type == 'slider')
                                                        @foreach($page_element->options as $image_location)
                                                            {{$image_location}}
                                                        @endforeach
                                                    @endif
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            @endif
    @endforeach
    </div> {{-- #page --}}
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">    
        page_id = {{$page->id}};
        {!! $settings->script or '' !!}
    </script>
@overwrite

@section('footer')
@overwrite