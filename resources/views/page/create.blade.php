@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">New Page</div>
        <form method="post" action="{{url('page/create')}}" data-ajax-form>
            {{csrf_field()}} 
            @include('component.form-item',[
                'name' => 'title',
                'type' => 'text',
                'label' => 'Page Title',
            ])
            @include('component.form-item',[
                'name' => 'slug',
                'type' => 'text',
                'label' => 'Slug',
                'sublabel' => getenv('APP_DOMAIN'),
            ])
            {{-- @include('component.form-item',[
                'name' => 'layout',
                'type' => 'select',
                'label' => 'Layout',
                'options' => 'Default,Partial,Sidebar',
            ]) --}}
            <button class="modal-button">Add Page</button>
        </form>

    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">    
        $(document).on('keyup', '[name=title]', function() {
            var title = $(this).val();
            {{-- Generate url ready slug --}}
            var slug = title.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]+/g, "-").toLowerCase();
            $('[name=slug]').val(slug);
            $('[name=slug]').parent().find('.form-item-sublabel').html('{{ getenv('APP_DOMAIN') }}/'+slug);
        });
        $(document).on('keyup', '[name=slug]', function() {
            var title = $(this).val();
            {{-- Generate url ready slug --}}
            var slug = title.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]+/g, "-").toLowerCase();
            $('[name=slug]').val(slug);
            $(this).parent().find('.form-item-sublabel').html('{{ getenv('APP_DOMAIN') }}/'+slug);
        });
    </script>
@endsection