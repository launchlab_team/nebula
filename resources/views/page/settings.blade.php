@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Pages Settings <span class="icon icon-settings"></span></div>
        <form method="post" action="{{url('pages/settings')}}" data-ajax-form>
            {{csrf_field()}}
            <label>Resources</label>
            @include('component.form-item',[
                'name' => 'css',
                'type' => 'textarea',
                'label' => 'CSS',
                'id' => 'css',
                'placeholder' => 'CSS',
                'sublabel' => 'Applies to each page',
                'value' => $settings->css,
            ])
            @include('component.form-item',[
                'name' => 'script',
                'type' => 'textarea',
                'placeholder' => 'js',
                'label' => 'Scripts',
                'sublabel' => 'Applies to each page',
                'value' => $settings->script,
            ])
            <br>
            <label>Header Menu</label>
            @include('component.form-item',[
                'name' => 'label[]',
                'type' => 'text',
                'placeholder' => 'Label',
                'value' => (count($menu_items) !== 0) ? $menu_items[0]->label : '',
            ])
            @include('component.form-item',[
                'name' => 'href[]',
                'type' => 'text',
                'placeholder' => 'Links to',
                'value' => (count($menu_items) !== 0) ? $menu_items[0]->href : '',
            ])
            <hr/>
            @include('component.form-item',[
                'name' => 'label[]',
                'type' => 'text',
                'placeholder' => 'Label',
                'value' => (count($menu_items) !== 0) ? $menu_items[1]->label : '',
            ])
            @include('component.form-item',[
                'name' => 'href[]',
                'type' => 'text',
                'placeholder' => 'Links to',
                'value' => (count($menu_items) !== 0) ? $menu_items[1]->href : '',
            ])
            <hr/>
            @include('component.form-item',[
                'name' => 'label[]',
                'type' => 'text',
                'placeholder' => 'Label',
                'value' => (count($menu_items) !== 0) ? $menu_items[2]->label : '',
            ])
            @include('component.form-item',[
                'name' => 'href[]',
                'type' => 'text',
                'placeholder' => 'Links to',
                'value' => (count($menu_items) !== 0) ? $menu_items[2]->href : '',
            ])
            <hr/>
            @include('component.form-item',[
                'name' => 'label[]',
                'type' => 'text',
                'placeholder' => 'Label',
                'value' => (count($menu_items) !== 0) ? $menu_items[3]->label : '',
            ])
            @include('component.form-item',[
                'name' => 'href[]',
                'type' => 'text',
                'placeholder' => 'Links to',
                'value' => (count($menu_items) !== 0) ? $menu_items[3]->href : '',
            ])
            <hr/>
            @include('component.form-item',[
                'name' => 'label[]',
                'type' => 'text',
                'placeholder' => 'Label',
                'value' => (count($menu_items) !== 0) ? $menu_items[4]->label : '',
            ])
            @include('component.form-item',[
                'name' => 'href[]',
                'type' => 'text',
                'placeholder' => 'Links to',
                'value' => (count($menu_items) !== 0) ? $menu_items[4]->href : '',
            ])
            <hr/>
            @include('component.form-item',[
                'name' => 'label[]',
                'type' => 'text',
                'placeholder' => 'Label',
                'value' => (count($menu_items) !== 0) ? $menu_items[5]->label : '',
            ])
            @include('component.form-item',[
                'name' => 'href[]',
                'type' => 'text',
                'placeholder' => 'Links to',
                'value' => (count($menu_items) !== 0) ? $menu_items[5]->href : '',
            ])
            <button class="modal-button">Save Changes</button>
        </form>

    </div>
@endsection

