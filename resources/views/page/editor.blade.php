@extends('layouts.default')

{{-- @section('header')
    @include('dashboard.header')
@overwrite --}}

@section('links')
    @parent
    <style type="text/css">
    {!! $settings->css or '' !!}
    body {
        padding-bottom: 0px !important;
        height:100%;
    }
    </style>
@overwrite

@section('title',$page->title)

@section('subheader')
    <a data-modal="pages/settings"><span class="pages-settings-icon uk-icon-cog"></span></a>
@endsection

@section('main')


    <div id="page">
        <div class="edit-element-menu-wrapper" data-uk-sticky="{boundary: true}">
        </div>
        @include('component.add-button')

    @foreach($elements as $element)
            @if($element->type == 'page-section')
                <div class="page-section {{$element->class or ''}}"
                    style="
                        @if(isset($element->height) && $element->height !== '')
                            height:{{$element->height}};
                        @endif
                        @if(isset($element->width) && $element->width !== '')
                            width:{{$element->width}};
                        @endif
                        @if(isset($element->margin) && $element->margin !== '')
                            margin:{{$element->margin}};
                        @endif
                        @if(isset($element->padding) && $element->padding !== '')
                            padding:{{$element->padding}};
                        @endif
                        @if(isset($element->font_size) && $element->font_size !== '')
                            font-size:{{$element->font_size}};
                        @endif
                        @if(isset($element->text_align) && $element->text_align !== '')
                            text-align:{{$element->text_align}};
                        @endif
                        @if(isset($element->color) && $element->color !== '')
                            color:{{$element->color}};
                        @endif
                        @if(isset($element->background_color) && $element->background_color !== '')
                            background-color:{{$element->background_color}};
                        @endif
                        @if(isset($element->background_image) && $element->background_image !== '')
                            background-image:{{'url('.url(str_replace(' ', '%20', $element->background_image)).')'}};
                        @endif
                        {{$element->style or ''}}" id="{{$element->id}}">
                    <div class="page-editor-section-toolbar">
                        <span class="icon uk-icon-edit" title="Edit Section" data-edit-element="{{$element->id}}"></span>
                        &nbsp;
                        <span class="icon uk-icon-arrows-v" title="Move Section"></span>
                    </div>
                    <div class="uk-grid uk-grid-collapse" data-uk-grid-match>
                        @foreach($elements as $page_block)
                            @if($page_block->parent_id == $element->id) 
                                {{-- Page blocks start --}}
                                @if($page_block->type == 'page-block-full')
                                    <div  id="{{$page_block->id}}" class="page-block-wrapper uk-width-1-1" @if($page_block->parallax !== null && $page_block->parallax != 0) data-uk-parallax="{bg: '-200'}" @endif style="display:table;position:relative;
                                        @if(isset($page_block->height) && $page_block->height !== '')
                                            height:{{$page_block->height}};
                                        @endif
                                        @if(isset($page_block->width) && $page_block->width !== '')
                                            width:{{$page_block->width}};
                                        @endif
                                        @if(isset($page_block->margin) && $page_block->margin !== '')
                                            margin:{{$page_block->margin}};
                                        @endif
                                        @if(isset($page_block->padding) && $page_block->padding !== '')
                                            padding:{{$page_block->padding}};
                                        @endif
                                        @if(isset($page_block->font_size) && $page_block->font_size !== '')
                                            font-size:{{$page_block->font_size}};
                                        @endif
                                        @if(isset($page_block->text_align) && $page_block->text_align !== '')
                                            text-align:{{$page_block->text_align}};
                                        @endif
                                        @if(isset($page_block->color) && $page_block->color !== '')
                                            color:{{$page_block->color}};
                                        @endif
                                        @if(isset($page_block->background_color) && $page_block->background_color !== '')
                                            background-color:{{$page_block->background_color}};
                                        @endif
                                        @if(isset($page_block->background_image) && $page_block->background_image !== '')
                                            background-image:url({{url($page_block->background_image)}});
                                        @endif
                                        {{$page_block->style or ''}}">
                                @endif
                                @if($page_block->type == 'page-block-half')
                                    <div  id="{{$page_block->id}}" class="page-block-wrapper uk-width-medium-1-2" @if($page_block->parallax !== null && $page_block->parallax != 0) data-uk-parallax="{bg: '-200'}" @endif  style="display:table;position:relative;
                                        @if(isset($page_block->height) && $page_block->height !== '')
                                            height:{{$page_block->height}};
                                        @endif
                                        @if(isset($page_block->width) && $page_block->width !== '')
                                            width:{{$page_block->width}};
                                        @endif
                                        @if(isset($page_block->margin) && $page_block->margin !== '')
                                            margin:{{$page_block->margin}};
                                        @endif
                                        @if(isset($page_block->padding) && $page_block->padding !== '')
                                            padding:{{$page_block->padding}};
                                        @endif
                                        @if(isset($page_block->font_size) && $page_block->font_size !== '')
                                            font-size:{{$page_block->font_size}};
                                        @endif
                                        @if(isset($page_block->text_align) && $page_block->text_align !== '')
                                            text-align:{{$page_block->text_align}};
                                        @endif
                                        @if(isset($page_block->color) && $page_block->color !== '')
                                            color:{{$page_block->color}};
                                        @endif
                                        @if(isset($page_block->background_color) && $page_block->background_color !== '')
                                            background-color:{{$page_block->background_color}};
                                        @endif
                                        @if(isset($page_block->background_image) && $page_block->background_image !== '')
                                            background-image:url({{url($page_block->background_image)}});
                                        @endif
                                        {{$page_block->style or ''}}">
                                @endif
                                @if($page_block->type == 'page-block-third')
                                    <div  id="{{$page_block->id}}" class="page-block-wrapper uk-width-medium-1-3" @if($page_block->parallax !== null && $page_block->parallax != 0) data-uk-parallax="{bg: '-200'}" @endif  style="display:table;position:relative;
                                        @if(isset($page_block->height) && $page_block->height !== '')
                                            height:{{$page_block->height}};
                                        @endif
                                        @if(isset($page_block->width) && $page_block->width !== '')
                                            width:{{$page_block->width}};
                                        @endif
                                        @if(isset($page_block->margin) && $page_block->margin !== '')
                                            margin:{{$page_block->margin}};
                                        @endif
                                        @if(isset($page_block->padding) && $page_block->padding !== '')
                                            padding:{{$page_block->padding}};
                                        @endif
                                        @if(isset($page_block->font_size) && $page_block->font_size !== '')
                                            font-size:{{$page_block->font_size}};
                                        @endif
                                        @if(isset($page_block->text_align) && $page_block->text_align !== '')
                                            text-align:{{$page_block->text_align}};
                                        @endif
                                        @if(isset($page_block->color) && $page_block->color !== '')
                                            color:{{$page_block->color}};
                                        @endif
                                        @if(isset($page_block->background_color) && $page_block->background_color !== '')
                                            background-color:{{$page_block->background_color}};
                                        @endif
                                        @if(isset($page_block->background_image) && $page_block->background_image !== '')
                                            background-image:url({{url($page_block->background_image)}});
                                        @endif
                                        {{$page_block->style or ''}}">
                                @endif
                                @if($page_block->type == 'page-block-fourth')
                                    <div  id="{{$page_block->id}}" class="page-block-wrapper uk-width-medium-1-4"  @if($page_block->parallax !== null && $page_block->parallax != 0) data-uk-parallax="{bg: '-200'}" @endif  style="display:table;position:relative;
                                        @if(isset($page_block->height) && $page_block->height !== '')
                                            height:{{$page_block->height}};
                                        @endif
                                        @if(isset($page_block->width) && $page_block->width !== '')
                                            width:{{$page_block->width}};
                                        @endif
                                        @if(isset($page_block->margin) && $page_block->margin !== '')
                                            margin:{{$page_block->margin}};
                                        @endif
                                        @if(isset($page_block->padding) && $page_block->padding !== '')
                                            padding:{{$page_block->padding}};
                                        @endif
                                        @if(isset($page_block->font_size) && $page_block->font_size !== '')
                                            font-size:{{$page_block->font_size}};
                                        @endif
                                        @if(isset($page_block->text_align) && $page_block->text_align !== '')
                                            text-align:{{$page_block->text_align}};
                                        @endif
                                        @if(isset($page_block->color) && $page_block->color !== '')
                                            color:{{$page_block->color}};
                                        @endif
                                        @if(isset($page_block->background_color) && $page_block->background_color !== '')
                                            background-color:{{$page_block->background_color}};
                                        @endif
                                        @if(isset($page_block->background_image) && $page_block->background_image !== '')
                                            background-image:url({{url($page_block->background_image)}});
                                        @endif
                                        {{$page_block->style or ''}}">
                                @endif
                                {{-- @if($page_block->overlay !== '' && $page_block->overlay !== null) --}}
                                    <div class="page-block-overlay" style="background-color:{{$page_block->background_overlay or ''}}"></div>
                                {{-- @endif --}}
                                    <div class="page-block {{$page_block->class or ''}}">
                                        {{-- <div class="page-block-overlay" style="{{$page_block->background_overlay or ''}}"></div> --}}
                                        <div class="page-editor-block-toolbar">
                                            @if ($page_block->type !== 'page-block-full')
                                            <span class="icon uk-icon-arrows-h" title="Move Block"></span>
                                            &nbsp;
                                            @endif
                                            <span class="icon uk-icon-edit" title="Edit Block" data-edit-element="{{$page_block->id}}"></span>
                                            <span class="icon icon-plus" data-add-element="{{$page_block->id}}" title="Add Element to Block"></span>
                                        </div>
                                        {{-- @endif --}}
                                        {{-- Include child elements --}}
                                        @foreach($elements as $page_element)
                                             @include('page.element.generate',['element'=>$page_element])
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            @endif
    @endforeach
    </div> {{-- #page --}}
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">    
        page_id = {{$page->id}};
        {!! $settings->script or '' !!}
    </script>
    {{-- Script for payment forms also included on page editor blade. --}}
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        $(function() {
            Stripe.setPublishableKey("{{env('STRIPE_KEY')}}");
        });
        $(document).on('submit','#payment_form',function(e) {
            $form = $(this); {{-- since there may be more than one on payment form on a page --}}
            {{-- Disable the submit button to prevent repeated clicks:  --}}
            $form.find('.modal-button').prop('disabled', true);
            {{-- Request a token from Stripe:  --}}
            Stripe.card.createToken($form, stripeResponseHandler);
            {{-- Prevent the form from being submitted:  --}}
            return false;
        });
        function stripeResponseHandler(status, response) {
          {{-- Grab the form:  --}}
          {{-- var $form = $('#payment_form');  --}}
          if (response.error) { {{-- Problem!  --}}
            {{-- Show the errors on the form:  --}}
            alert(response.error.message);
            $('.modal-button').removeAttr("disabled"); {{-- Re-enable submission  --}}
          } else { {{-- Token was created!  --}}
            {{-- Get the token ID:  --}}
            var token = response.id;
            {{-- Insert the token ID into the form so it gets submitted to the server:  --}}
            $form.append($('<input type="hidden" name="stripeToken">').val(token));
            {{-- Submit the form:  --}}
            $form.get(0).submit();
          }
        };
    </script>
@overwrite

@section('footer')
@overwrite
