{{-- The page where they select the element type to add --}}
<div class="close-add-element-menu icon-x"></div>
<div class="add-element-menu">
    <h2>Add an Element</h2>
    <form method="post" id="add_element_menu_form"> {{-- submitted via ajax --}}
        <div class="list-divider">Text</div>
        @include('component.list-item',[
            'text' => 'Paragraph',
            'attributes' => 'data-create-element="block/'.$block_id.'/element/add/paragraph"',
        ])
        @include('component.list-item',[
            'text' => 'Heading 1',
            'attributes' => 'data-create-element="block/'.$block_id.'/element/add/h1"',
        ])
        @include('component.list-item',[
            'text' => 'Heading 2',
            'attributes' => 'data-create-element="block/'.$block_id.'/element/add/h2"',
        ])
        @include('component.list-item',[
            'text' => 'Heading 3',
            'attributes' => 'data-create-element="block/'.$block_id.'/element/add/h3"',
        ])
        @include('component.list-item',[
            'text' => 'Heading 4',
            'attributes' => 'data-create-element="block/'.$block_id.'/element/add/h4"',
        ])
        <div class="list-divider">Media</div>
        @include('component.list-item',[
            'text' => 'Image',
            'attributes' => 'data-create-element="block/'.$block_id.'/element/add/image"',
        ])
        {{-- @include('component.list-item',[
            'text' => 'Slider',
            'attributes' => 'data-create-element="block/'.$block_id.'/element/add/slider"',
        ]) --}}
        <div class="list-divider">Advanced</div>
        @include('component.list-item',[
            'text' => 'HTML',
            'attributes' => 'data-create-element="block/'.$block_id.'/element/add/html"',
        ])
        @include('component.list-item',[
            'text' => 'Form',
            'attributes' => 'data-create-element="block/'.$block_id.'/element/add/form"',
        ])
        @include('component.list-item',[
            'text' => 'Payment Form',
            'attributes' => 'data-create-element="block/'.$block_id.'/element/add/payment-form"',
        ])
    </form>
</div>
{{-- <button class="add-element-save-button">Continue</button> --}}