@if($element->parent_id == $page_block->id)
<div class="page-element-wrapper">
    @if(!isset($editing) || $editing !== false) {{-- hide on view page --}}
    <div class="page-editor-element-toolbar">
        <span class="icon uk-icon-arrows" title="Move Element"></span>
        <span class="icon uk-icon-edit" title="Edit Element"  data-edit-element="{{$element->id}}"></span>
    </div>
    @else
        <div> </div>
        {{-- Spacer so live page matches spacing of editor page - replace eventually --}}
    @endif
    @if($element->type == 'paragraph')
    <p id="{{$element->id}}" class="page-element page-paragraph 
        {{$element->class or ''}}"
        style="
            @if(isset($element->height) && $element->height !== '')
                height:{{$element->height}};
            @endif
            @if(isset($element->width) && $element->width !== '')
                width:{{$element->width}};
            @endif
            @if(isset($element->margin) && $element->margin !== '')
                margin:{{$element->margin}};
            @endif
            @if(isset($element->padding) && $element->padding !== '')
                padding:{{$element->padding}};
            @endif
            @if(isset($element->font_size) && $element->font_size !== '')
                font-size:{{$element->font_size}};
            @endif
            @if(isset($element->text_align) && $element->text_align !== '')
                text-align:{{$element->text_align}};
            @endif
            @if(isset($element->color) && $element->color !== '')
                color:{{$element->color}};
            @endif
            @if(isset($element->background_color) && $element->background_color !== '')
                background-color:{{$element->background_color}};
            @endif
            @if(isset($element->background_image) && $element->background_image !== '')
                background-image:url({{url($element->background_image)}})
            @endif
            ;{{$element->style or ''}}">
        {!! $element->html !!}
    </p>
    @endif
    @if($element->type == 'h1')
        <h1 id="{{$element->id}}" class="page-element page-heading" style="
    @if(isset($element->height) && $element->height !== '')
        height:{{$element->height}};
    @endif
    @if(isset($element->width) && $element->width !== '')
        width:{{$element->width}};
    @endif
    @if(isset($element->margin) && $element->margin !== '')
        margin:{{$element->margin}};
    @endif
    @if(isset($element->padding) && $element->padding !== '')
        padding:{{$element->padding}};
    @endif
    @if(isset($element->font_size) && $element->font_size !== '')
        font-size:{{$element->font_size}};
    @endif
    @if(isset($element->text_align) && $element->text_align !== '')
        text-align:{{$element->text_align}};
    @endif
    @if(isset($element->color) && $element->color !== '')
        color:{{$element->color}};
    @endif
    @if(isset($element->background_color) && $element->background_color !== '')
        background-color:{{$element->background_color}};
    @endif
    @if(isset($element->background_image) && $element->background_image !== '')
        background-image:url({{url($element->background_image)}})
    @endif
    ;{{$element->style or ''}}">{!!$element->html or ''!!}</h1>
    @endif
    @if($element->type == 'h2')
        <h2 id="{{$element->id}}" class="page-element page-heading" style="
    @if(isset($element->height) && $element->height !== '')
        height:{{$element->height}};
    @endif
    @if(isset($element->width) && $element->width !== '')
        width:{{$element->width}};
    @endif
    @if(isset($element->margin) && $element->margin !== '')
        margin:{{$element->margin}};
    @endif
    @if(isset($element->padding) && $element->padding !== '')
        padding:{{$element->padding}};
    @endif
    @if(isset($element->font_size) && $element->font_size !== '')
        font-size:{{$element->font_size}};
    @endif
    @if(isset($element->text_align) && $element->text_align !== '')
        text-align:{{$element->text_align}};
    @endif
    @if(isset($element->color) && $element->color !== '')
        color:{{$element->color}};
    @endif
    @if(isset($element->background_color) && $element->background_color !== '')
        background-color:{{$element->background_color}};
    @endif
    @if(isset($element->background_image) && $element->background_image !== '')
        background-image:url({{url($element->background_image)}})
    @endif
    ;{{$element->style or ''}}">{!!$element->html or ''!!}</h2>
    @endif
    @if($element->type == 'h3')
        <h3 id="{{$element->id}}" class="page-element page-heading" style="
    @if(isset($element->height) && $element->height !== '')
        height:{{$element->height}};
    @endif
    @if(isset($element->width) && $element->width !== '')
        width:{{$element->width}};
    @endif
    @if(isset($element->margin) && $element->margin !== '')
        margin:{{$element->margin}};
    @endif
    @if(isset($element->padding) && $element->padding !== '')
        padding:{{$element->padding}};
    @endif
    @if(isset($element->font_size) && $element->font_size !== '')
        font-size:{{$element->font_size}};
    @endif
    @if(isset($element->text_align) && $element->text_align !== '')
        text-align:{{$element->text_align}};
    @endif
    @if(isset($element->color) && $element->color !== '')
        color:{{$element->color}};
    @endif
    @if(isset($element->background_color) && $element->background_color !== '')
        background-color:{{$element->background_color}};
    @endif
    @if(isset($element->background_image) && $element->background_image !== '')
        background-image:url({{url($element->background_image)}})
    @endif
    ;{{$element->style or ''}}">{!!$element->html or ''!!}</h3>
    @endif
    @if($element->type == 'h4')
        <h4 id="{{$element->id}}" class="page-element page-heading" style="
            @if(isset($element->height) && $element->height !== '')
                height:{{$element->height}};
            @endif
            @if(isset($element->width) && $element->width !== '')
                width:{{$element->width}};
            @endif
            @if(isset($element->margin) && $element->margin !== '')
                margin:{{$element->margin}};
            @endif
            @if(isset($element->padding) && $element->padding !== '')
                padding:{{$element->padding}};
            @endif
            @if(isset($element->font_size) && $element->font_size !== '')
                font-size:{{$element->font_size}};
            @endif
            @if(isset($element->text_align) && $element->text_align !== '')
                text-align:{{$element->text_align}};
            @endif
            @if(isset($element->color) && $element->color !== '')
                color:{{$element->color}};
            @endif
            @if(isset($element->background_color) && $element->background_color !== '')
                background-color:{{$element->background_color}};
            @endif
            @if(isset($element->background_image) && $element->background_image !== '')
                background-image:url({{url($element->background_image)}})
            @endif
            ;{{$element->style or ''}}">{!!$element->html or ''!!}</h4>
            @endif
            @if($element->type == 'h5')
                <h5 id="{{$element->id}}" class="page-element page-heading" style="
            @if(isset($element->height) && $element->height !== '')
                height:{{$element->height}};
            @endif
            @if(isset($element->width) && $element->width !== '')
                width:{{$element->width}};
            @endif
            @if(isset($element->margin) && $element->margin !== '')
                margin:{{$element->margin}};
            @endif
            @if(isset($element->padding) && $element->padding !== '')
                padding:{{$element->padding}};
            @endif
            @if(isset($element->font_size) && $element->font_size !== '')
                font-size:{{$element->font_size}};
            @endif
            @if(isset($element->text_align) && $element->text_align !== '')
                text-align:{{$element->text_align}};
            @endif
            @if(isset($element->color) && $element->color !== '')
                color:{{$element->color}};
            @endif
            @if(isset($element->background_color) && $element->background_color !== '')
                background-color:{{$element->background_color}};
            @endif
            @if(isset($element->background_image) && $element->background_image !== '')
                background-image:url({{url($element->background_image)}})
            @endif
            ;{{$element->style or ''}}">{!!$element->html or ''!!}</h5>
            @endif
    @if($element->type == 'image')
    {{-- {{dd($element)}} --}}
        <img id="{{$element->id}}"  class="page-element page-image"  src="@if(isset($element->image) && $element->image !== null) {{url($element->image)}}@endif"  alt="{{$element->alt or ''}}" style="
        @if(isset($element->height) && $element->height !== '')
            height:{{$element->height}};
        @endif
        @if(isset($element->width) && $element->width !== '')
            width:{{$element->width}};
        @endif
        @if(isset($element->margin) && $element->margin !== '')
            margin:{{$element->margin}};
        @endif
        @if(isset($element->padding) && $element->padding !== '')
            padding:{{$element->padding}};
        @endif
        @if(isset($element->font_size) && $element->font_size !== '')
            font-size:{{$element->font_size}};
        @endif
        @if(isset($element->text_align) && $element->text_align !== '')
            text-align:{{$element->text_align}};
        @endif
        @if(isset($element->color) && $element->color !== '')
            color:{{$element->color}};
        @endif
        @if(isset($element->background_color) && $element->background_color !== '')
            background-color:{{$element->background_color}};
        @endif
        @if(isset($element->background_image) && $element->background_image !== '')
            background-image:url({{url($element->background_image)}})
        @endif
        ;{{$element->style or ''}}" />
        @endif
    @if($element->type == 'button')
        <button id="{{$element->id}}" class="page-element page-button" style="
            @if(isset($element->height) && $element->height !== '')
                height:{{$element->height}};
            @endif
            @if(isset($element->width) && $element->width !== '')
                width:{{$element->width}};
            @endif
            @if(isset($element->margin) && $element->margin !== '')
                margin:{{$element->margin}};
            @endif
            @if(isset($element->padding) && $element->padding !== '')
                padding:{{$element->padding}};
            @endif
            @if(isset($element->font_size) && $element->font_size !== '')
                font-size:{{$element->font_size}};
            @endif
            @if(isset($element->text_align) && $element->text_align !== '')
                text-align:{{$element->text_align}};
            @endif
            @if(isset($element->color) && $element->color !== '')
                color:{{$element->color}};
            @endif
            @if(isset($element->background_color) && $element->background_color !== '')
                background-color:{{$element->background_color}};
            @endif
            @if(isset($element->background_image) && $element->background_image !== '')
                background-image:url({{url($element->background_image)}})
            @endif
            ;{{$element->style or ''}}">{!! html_entity_decode($element->html) !!}</button>
    @endif
    @if($element->type == 'html')
    <div id="{{$element->id}}" class="page-element page-html 
        {{$element->class or ''}}"
        style="
            @if(isset($element->height) && $element->height !== '')
                height:{{$element->height}};
            @endif
            @if(isset($element->width) && $element->width !== '')
                width:{{$element->width}};
            @endif
            @if(isset($element->margin) && $element->margin !== '')
                margin:{{$element->margin}};
            @endif
            @if(isset($element->padding) && $element->padding !== '')
                padding:{{$element->padding}};
            @endif
            @if(isset($element->font_size) && $element->font_size !== '')
                font-size:{{$element->font_size}};
            @endif
            @if(isset($element->text_align) && $element->text_align !== '')
                text-align:{{$element->text_align}};
            @endif
            @if(isset($element->color) && $element->color !== '')
                color:{{$element->color}};
            @endif
            @if(isset($element->background_color) && $element->background_color !== '')
                background-color:{{$element->background_color}};
            @endif
            @if(isset($element->background_image) && $element->background_image !== '')
                background-image:url({{url($element->background_image)}})
            @endif
            ;{{$element->style or ''}}">
        {!! $element->html !!}
    </div>
    @endif

    @if($element->type == 'form')
        @if(App\Form::find($element->options) !== NULL)
            <div id="{{$element->id}}" class="page-element page-form 
                {{$element->class or ''}}"
                style="
                    @if(isset($element->height) && $element->height !== '')
                        height:{{$element->height}};
                    @endif
                    @if(isset($element->width) && $element->width !== '')
                        width:{{$element->width}};
                    @endif
                    @if(isset($element->margin) && $element->margin !== '')
                        margin:{{$element->margin}};
                    @endif
                    @if(isset($element->padding) && $element->padding !== '')
                        padding:{{$element->padding}};
                    @endif
                    @if(isset($element->font_size) && $element->font_size !== '')
                        font-size:{{$element->font_size}};
                    @endif
                    @if(isset($element->text_align) && $element->text_align !== '')
                        text-align:{{$element->text_align}};
                    @endif
                    @if(isset($element->color) && $element->color !== '')
                        color:{{$element->color}};
                    @endif
                    @if(isset($element->background_color) && $element->background_color !== '')
                        background-color:{{$element->background_color}};
                    @endif
                    @if(isset($element->background_image) && $element->background_image !== '')
                        background-image:url({{url($element->background_image)}})
                    @endif
                    ;{{$element->style or ''}}">
                @include('form.include',['form'=>App\Form::findOrFail($element->options)]) {{-- options is equal to form id on form elements --}}
            </div>
        @endif
    @endif

    @if($element->type == 'payment-form')
    <div id="{{$element->id}}" class="page-element page-form 
        {{$element->class or ''}}"
        style="
            @if(isset($element->height) && $element->height !== '')
                height:{{$element->height}};
            @endif
            @if(isset($element->width) && $element->width !== '')
                width:{{$element->width}};
            @endif
            @if(isset($element->margin) && $element->margin !== '')
                margin:{{$element->margin}};
            @endif
            @if(isset($element->padding) && $element->padding !== '')
                padding:{{$element->padding}};
            @endif
            @if(isset($element->font_size) && $element->font_size !== '')
                font-size:{{$element->font_size}};
            @endif
            @if(isset($element->text_align) && $element->text_align !== '')
                text-align:{{$element->text_align}};
            @endif
            @if(isset($element->color) && $element->color !== '')
                color:{{$element->color}};
            @endif
            @if(isset($element->background_color) && $element->background_color !== '')
                background-color:{{$element->background_color}};
            @endif
            @if(isset($element->background_image) && $element->background_image !== '')
                background-image:url({{url($element->background_image)}})
            @endif
            ;{{$element->style or ''}}">
            @if(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' || env('APP_ENV') == 'local')
                @if(App\PaymentForm::find($element->options) !== NULL)
                    <form method="post" action="{{url('payment/create')}}"  id="payment_form" class="uk-text-left">
                        {{csrf_field()}} 
                        <div class="block full" style="border-color:#cedece;background:#f8fff8;margin-bottom:0px;margin-top:15px">
                            <b>${{App\PaymentForm::findOrFail($element->options)->amount}} Payment</b><br/>
                            @if(App\PaymentForm::findOrFail($element->options)->recurring)
                                <span class="uk-text-small">Your card will be charged again in {{App\PaymentForm::findOrFail($element->options)->interval}} days</span>
                            @else
                                <span class="uk-text-small">One time charge</span>
                            @endif
                        </div>
                        @if(count(App\PaymentForm::findOrFail($element->options)->fields) > 0)
                            <hr style="border-top:1px solid rgba(0, 0, 0, 0.09);margin-bottom:13px" />
                            @foreach(App\PaymentForm::findOrFail($element->options)->fields as $field)
                                @include('component.form-item',[
                                    'type'=>$field->type,
                                    //'name'=>'field['.$field->id.']',
                                    'name'=>'answer[]',
                                    'label'=>$field->label,
                                    'sublabel'=>$field->hint,
                                    'options'=>$field->options,
                                    'placeholder'=>$field->placeholder,
                                ])                        
                                <input type="hidden" name="question[]" value="{{$field->label}}"/>
                            @endforeach
                        @endif
                        <hr style="border-top:1px solid rgba(0, 0, 0, 0.09)" />
                        {{-- @include('payment.form.generate',['form'=>$form]) --}}
                        @include('payment.form.generate',['form'=>App\PaymentForm::findOrFail($element->options)]) {{-- options is equal to payment form id on form elements --}}
                        <hr style="border-top:1px solid rgba(0, 0, 0, 0.09)" />
                        <button class="center full">
                            <span class="button-icon-left icon icon-lock"></span>
                            Submit Payment
                        </button>
                    </form>
                @else
                    <div class="message-box">This payment form has been deleted.</div>
                @endif
            @else
                <div class="message-box">Secure protocol (https) is required to accept payments. Contact <a href="mailto:team@nebulasuite.com">Nebula support</a> to install an SSL certificate for your domain.</div>
            @endif
    </div>
    @endif

    {{-- @if($element->type == 'slider')
        @foreach($element->options as $image_location)
            {{$image_location}}
        @endforeach
    @endif --}}
</div> {{-- page-element-wrapper --}}
@endif
