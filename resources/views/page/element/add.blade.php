{{-- The page that actually adds the element to the page editor --}}
<div class="close-edit-element-menu icon-x"></div>
<div class="edit-element-menu">
    <h2>Adding an Element</h2>
    <form method="post" id="add_element_to_block_form" action="{{url('block/'.$block_id . '/element/' . $type . '/generate')}}">
        {{-- Show based on element type --}}
        @if($type == 'form')
            <div class="accordion-small">
                <div class="accordion-title">
                    Form
                    <div class="accordion-icon-left">
                        <span class="icon uk-icon-file-text-o"></span>
                    </div>
                    <div class="accordion-icon-right">
                        <span class="icon-minus"></span>
                    </div>
                </div>
                <div class="accordion-content" style="display: block">
                    <div class="form-item select">
                        <span class="icon form-item-icon form-item-icon-right icon-select-arrow"></span>
                        <select name="form_id">
                        @if(count($forms) > 0)
                            @foreach($forms as $form)
                            <option value="{{$form->id}}">{{$form->name}}</option>
                            @endforeach
                        @endif
                        </select>
                    </div>
                </div>
            </div>
        @endif

        @if($type == 'payment-form')
            <div class="accordion-small">
                <div class="accordion-title">
                    Payment Form
                    <div class="accordion-icon-left">
                        <span class="icon uk-icon-file-text-o"></span>
                    </div>
                    <div class="accordion-icon-right">
                        <span class="icon-minus"></span>
                    </div>
                </div>
                <div class="accordion-content" style="display: block">
                    <div class="form-item select">
                        <span class="icon form-item-icon form-item-icon-right icon-select-arrow"></span>
                        <select name="form_id">
                        @if(count($payment_forms) > 0)
                            @foreach($payment_forms as $payment_form)
                            <option value="{{$payment_form->id}}">{{$payment_form->title}}</option>
                            @endforeach
                        @endif
                        </select>
                    </div>
                </div>
            </div>
        @endif

        @if($type !== 'image' && $type !== 'slider' && $type !== 'form' && $type !== 'payment-form')
            <div class="accordion-small">
                <div class="accordion-title">
                    Content
                    <div class="accordion-icon-left">
                        <span class="icon icon-edit"></span>
                    </div>
                    <div class="accordion-icon-right">
                        <span class="icon-minus"></span>
                    </div>
                </div>
                <div class="accordion-content" style="display: block">
                    @include('component.form-item',[
                        'name' => 'html',
                        'type' => 'textarea',
                        'label' => 'Text/HTML',
                    ])
                </div>
            </div>
        @endif

        @if($type == 'image')
            <div class="accordion-small">
                <div class="accordion-title">
                    Image
                    <div class="accordion-icon-left">
                        <span class="icon uk-icon-picture-o"></span>
                    </div>
                    <div class="accordion-icon-right">
                        <span class="icon-minus"></span>
                    </div>
                </div>
                <div class="accordion-content" style="display: block">
                    @include('component.form-item',[
                        'name' => 'image',
                        'type' => 'image',
                        'label' => 'Choose image',
                    ])
                </div>
            </div>
        @endif

        <div class="accordion-small">
            <div class="accordion-title">
                Dimensions
                <div class="accordion-icon-left">
                    <span class="icon uk-icon-arrows-alt"></span>
                </div>
                <div class="accordion-icon-right">
                    <span class="icon-plus"></span>
                </div>
            </div>
            <div class="accordion-content">
                @include('component.form-item',[
                    'name' => 'height',
                    'type' => 'text',
                    'label' => 'height',
                    'sublabel' => 'e.g. "40vh" (40% view height) or "400px" (pixel height)',
                ])
                @include('component.form-item',[
                    'name' => 'width',
                    'type' => 'text',
                    'label' => 'width',
                ])
                @include('component.form-item',[
                    'name' => 'margin',
                    'type' => 'text',
                    'label' => 'Margin',
                ])
                @include('component.form-item',[
                    'name' => 'padding',
                    'type' => 'text',
                    'label' => 'Padding',
                ])
            </div>
        </div>

        <div class="accordion-small">
            <div class="accordion-title">
                Font
                <div class="accordion-icon-left">
                    <span class="icon uk-icon-font"></span>
                </div>
                <div class="accordion-icon-right">
                    <span class="icon-plus"></span>
                </div>
            </div>
            <div class="accordion-content">
                @include('component.form-item',[
                    'name' => 'font_size',
                    'type' => 'text',
                    'label' => 'Font Size',
                ])
                @include('component.form-item',[
                    'name' => 'color',
                    'type' => 'text',
                    'label' => 'Color',
                ])
                @include('component.form-item',[
                    'name' => 'text_align',
                    'type' => 'select',
                    'options' => 'Left,Center,Right',
                    'label' => 'Text Alignment',
                    'placeholder' => 'Choose',
                ])
            </div>
        </div>

        <div class="accordion-small">
            <div class="accordion-title">
                Background
                <div class="accordion-icon-left">
                    <span class="icon uk-icon-picture-o"></span>
                </div>
                <div class="accordion-icon-right">
                    <span class="icon-plus"></span>
                </div>
            </div>
            <div class="accordion-content">
                @include('component.form-item',[
                    'name' => 'background_color',
                    'type' => 'text',
                    'label' => 'Background Color',
                ])
                @include('component.form-item',[
                    'name' => 'background_image',
                    'type' => 'image',
                    'label' => 'Background Image',
                ])
                @include('component.form-item',[
                    'name' => 'overlay',
                    'type' => 'text',
                    'label' => 'Background Overlay',
                ])
            </div>
        </div>

        <div class="accordion-small">
            <div class="accordion-title">
                Advanced
                <div class="accordion-icon-left">
                    <span class="icon uk-icon-cog"></span>
                </div>
                <div class="accordion-icon-right">
                    <span class="icon-plus"></span>
                </div>
            </div>
            <div class="accordion-content">
                @include('component.form-item',[
                    'name' => 'styles',
                    'type' => 'textarea',
                    'label' => 'Inline Styles (CSS)',
                ])
                @include('component.form-item',[
                    'name' => 'modal',
                    'type' => 'text',
                    'label' => 'Modal URL',
                ])
                @include('component.form-item',[
                    'name' => 'details',
                    'type' => 'text',
                    'label' => 'Detail View URL',
                ])
                {{-- @include('component.form-item',[
                    'name' => 'classes',
                    'type' => 'text',
                    'label' => 'Classes',
                ]) --}}
            </div>
        </div>
        <br/>
        <br/>
        <br/>
    </form>
</div>
<button class="add-element-to-block-button" add-element-to-block="{{$block_id}}">Add to Page</button>