{{-- Page section is the uppermost element in the page builder --}}
@if($element->type == 'page-section')
    <div class="page-section">
        <div class="page-section-toolbar">
            <span class="icon uk-icon-align-left"></span>
            <span class="icon uk-icon-align-center"></span>
            <span class="icon uk-icon-align-right"></span>
            &nbsp;
            &nbsp;
            &nbsp;
            <span class="icon uk-icon-trash-o" title="Delete Block"></span>
            <span class="icon icon-edit" title="Edit Block"></span>
        </div>
    </div>
@endif

{{-- Page blocks are inside the page-section and have elements inside them --}}
@if($element->type == 'page-block')
    <div class="page-block">
        Add
    </div>
@else {{-- Page element --}}
    {{$element->type}}
@endif

