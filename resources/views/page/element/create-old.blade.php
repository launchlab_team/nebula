@extends('layouts.partial')


@section('middle')
<form method="post" action="{{url("block/$block_id/add")}}" data-ajax-form>
    <div class="detail-view">

        <span class="detail-view-icon icon icon-circle-layout"></span>
        <h2 class="detail-view-heading">Add an Element</h2>

        <div class="detail-view-menu">
            <button title="Add Element"><span class="icon icon-plus"></span></button>
            <span onClick="hideDetails()" class="icon icon-x"></span>
        </div>

        {{-- <div class="detail-view-content element-form-fields"> --}}
        <div class="detail-view-content">
            {{csrf_field()}} 
            @include('component.form-item',[
                'name' => 'type',
                'type' => 'select',
                'label' => 'Type',
                'class' => 'uk-display-block',
                'options' => 'Paragraph,Button,H1,H2,H3,H4,H5,Image,YouTube Embed,Vimeo Embed',
            ])
            {{-- @include('component.form-item',[
                'class' => 'show-for-',
                'name' => 'background_image',
                'type' => 'image',
                'label' => 'Background Image',
            ]) --}}
            @include('component.form-item',[
                'class' => 'show-for-paragraph',
                'name' => 'image',
                'type' => 'image',
                'label' => 'Image',
                'sublabel' => 'For Image Element',
            ])
            @include('component.form-item',[
                'name' => 'html',
                'type' => 'html',
                'label' => 'HTML',
            ])
            @include('component.form-item',[
                'name' => 'style',
                'type' => 'textarea',
                'label' => 'Styles',
                'sublabel' => 'Inline HTML Styles',
            ])
            @include('component.form-item',[
                'name' => 'overlay',
                'type' => 'textarea',
                'label' => 'Overlay Styles',
            ])
            @include('component.form-item',[
                'name' => 'url',
                'type' => 'text',
                'label' => 'URL',
                'sublabel' => 'For Vimeo or Youtube Embed',
            ])
        </div>

    </div>
</form>
@endsection

{{-- @section('middle')
<form method="post" action="{{url("page/$page->id/settings")}}" data-ajax-form>
    <div class="detail-view">

        <span class="detail-view-icon icon icon-circle-layout"></span>
        <h2 class="detail-view-heading">Add an Element</h2>

        <div class="detail-view-menu">
            <button type="button" title="Delete Element" onClick="rotate(this)" data-delete="page/{{$page->id}}/delete"><span class="icon uk-icon-trash-o"></span></button>

            <a href="{{url($page->slug)}}" target="_blank"><button type="button" title="Open Page"><span class="icon uk-icon-external-link"></span></button></a>

            <button title="Save Changes" onClick="rotate(this)"><span class="icon uk-icon-save"></span></button>
            <span onClick="hideDetails()" class="icon icon-x"></span>
        </div>

        <div class="detail-view-content">
            {{csrf_field()}} 
            @include('component.form-item',[
                'name' => 'title',
                'type' => 'text',
                'label' => 'Page Title',
                'value' => $page->title,
            ])
            @if($page->slug !== '')
                @include('component.form-item',[
                    'name' => 'slug',
                    'type' => 'text',
                    'label' => 'Slug',
                    'sublabel' => getenv('APP_DOMAIN'),
                    'value' => $page->slug,
                ])
            @endif
            @include('component.form-item',[
                'name' => 'layout',
                'type' => 'select',
                'label' => 'Layout',
                'options' => 'Default,Partial,Sidebar',
                'placeholder' => ucfirst($page->layout),
            ])
        </div>

    </div>
</form>
@endsection --}}