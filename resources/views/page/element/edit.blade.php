<div class="close-edit-element-menu icon-x"></div>
<div class="edit-element-menu">
    @if($element->type == 'page-section')
        <h2>Page Section <span class="cursor-pointer right uk-icon-trash-o" data-delete-section="element/{{$element->id}}/delete" style="line-height: 1.5;"></span></h2>
    @elseif($element->type == 'page-block-full' || $element->type == 'page-block-half' || $element->type == 'page-block-third' || $element->type == 'page-block-fourth')
        <h2>Edit Block</h2>
    @else
        <h2>Edit Element <span class="cursor-pointer right uk-icon-trash-o" data-delete-element="element/{{$element->id}}/delete" style="line-height: 1.5;"></span></h2>
    @endif
    <form method="post" id="edit_element_menu_form"> {{-- submitted via ajax --}}
        <input type="hidden" name="type" value="{{$element->type}}" /> 
       @if($element->type !== 'page-block-full' && $element->type !== 'page-block-half' && $element->type !== 'page-block-third' && $element->type !== 'page-block-fourth' && $element->type !== 'page-block-full' && $element->type !== 'page-section'  && $element->type !== 'image'  && $element->type !== 'slider'  && $element->type !== 'form'  && $element->type !== 'payment-form')
            {{csrf_field()}}
            <div class="accordion-small">
                <div class="accordion-title">
                    Content
                    <div class="accordion-icon-left">
                        <span class="icon icon-edit"></span>
                    </div>
                    <div class="accordion-icon-right">
                        <span class="icon-plus"></span>
                    </div>
                </div>
                <div class="accordion-content">
                    @include('component.form-item',[
                        'name' => 'html',
                        'type' => 'textarea',
                        'label' => 'Text/HTML',
                        'value' => $element->html,
                    ])
                </div>
            </div>
        @endif

        @if($element->type == 'image')
            <div class="accordion-small">
                <div class="accordion-title">
                    Image
                    <div class="accordion-icon-left">
                        <span class="icon uk-icon-picture-o"></span>
                    </div>
                    <div class="accordion-icon-right">
                        <span class="icon-minus"></span>
                    </div>
                </div>
                <div class="accordion-content" style="display: block">
                    @include('component.form-item',[
                        'name' => 'image',
                        'type' => 'image',
                        'label' => 'Choose image',
                        'value' => $element->image,
                    ])
                </div>
            </div>
        @endif

        @if($element->type !== 'page-section')
            <div class="accordion-small">
                <div class="accordion-title">
                    Dimensions
                    <div class="accordion-icon-left">
                        <span class="icon uk-icon-arrows-alt"></span>
                    </div>
                    <div class="accordion-icon-right">
                        <span class="icon-plus"></span>
                    </div>
                </div>
                <div class="accordion-content">
                    @include('component.form-item',[
                        'name' => 'height',
                        'sublabel' => 'e.g. "40vh" (40% view height) or "400px" (pixel height)',
                        'type' => 'text',
                        'label' => 'height',
                        'value' => $element->height,
                    ])
                    @include('component.form-item',[
                        'name' => 'width',
                        'type' => 'text',
                        'label' => 'width',
                        'value' => $element->width,
                    ])
                    @include('component.form-item',[
                        'name' => 'margin',
                        'type' => 'text',
                        'label' => 'Margin',
                        'value' => $element->margin,
                    ])
                    @include('component.form-item',[
                        'name' => 'padding',
                        'type' => 'text',
                        'label' => 'Padding',
                        'value' => $element->padding,
                    ])
                </div>
            </div>
        @endif

        <div class="accordion-small">
            <div class="accordion-title">
                Font
                <div class="accordion-icon-left">
                    <span class="icon uk-icon-font"></span>
                </div>
                <div class="accordion-icon-right">
                    <span class="icon-plus"></span>
                </div>
            </div>
            <div class="accordion-content">
                @include('component.form-item',[
                    'name' => 'font_size',
                    'type' => 'text',
                    'label' => 'Font Size',
                    'value' => $element->font_size,
                ])
                @include('component.form-item',[
                    'name' => 'color',
                    'type' => 'text',
                    'label' => 'Color',
                    'value' => $element->color,
                ])
                @include('component.form-item',[
                    'name' => 'text_align',
                    'type' => 'select',
                    'options' => 'Left,Center,Right',
                    'label' => 'Text Alignment',
                    'placeholder' => 'Choose',
                    'value' => $element->text_align,
                ])
            </div>
        </div>

        <div class="accordion-small">
            <div class="accordion-title">
                Background
                <div class="accordion-icon-left">
                    <span class="icon uk-icon-picture-o"></span>
                </div>
                <div class="accordion-icon-right">
                    <span class="icon-plus"></span>
                </div>
            </div>
            <div class="accordion-content">
                @include('component.form-item',[
                    'name' => 'background_color',
                    'type' => 'text',
                    'label' => 'Background Color',
                    'value' => $element->background_color,
                ])
                @include('component.form-item',[
                    'name' => 'background_image',
                    'type' => 'image',
                    'label' => 'Background Image',
                    'value' => $element->background_image,
                ])
                {{-- {{dd($element->type)}} --}}
                @if($element->type == 'page-block-full' || $element->type == 'page-block-third' || $element->type == 'page-block-half' || $element->type == 'page-block-fourth')
                    @include('component.form-item',[
                        'name' => 'background_overlay',
                        'type' => 'text',
                        'label' => 'Background Overlay',
                        'value' => $element->background_overlay,
                    ])
                @endif
            </div>
        </div>

        <div class="accordion-small">
            <div class="accordion-title">
                Advanced
                <div class="accordion-icon-left">
                    <span class="icon uk-icon-cog"></span>
                </div>
                <div class="accordion-icon-right">
                    <span class="icon-plus"></span>
                </div>
            </div>
            <div class="accordion-content">
                @include('component.form-item',[
                    'name' => 'styles',
                    'type' => 'textarea',
                    'label' => 'Inline Styles (CSS)',
                    'value' => $element->style,
                ])
                @include('component.form-item',[
                    'name' => 'modal',
                    'type' => 'text',
                    'label' => 'Modal URL',
                    'value' => $element->modal,
                ])
                @include('component.form-item',[
                    'name' => 'details',
                    'type' => 'text',
                    'label' => 'Detail View URL',
                    'value' => $element->details,
                ])
                @if($element->type !== 'page-block-full' && $element->type !== 'page-block-half' && $element->type !== 'page-block-third' && $element->type !== 'page-block-fourth' && $element->type !== 'page-block-full' && $element->type !== 'page-section')
                    @include('component.form-item',[
                        'name' => 'classes',
                        'type' => 'text',
                        'label' => 'Classes',
                        'value' => $element->class,
                    ])
                @endif
            </div>
        </div>
        <br/>
        <br/>
        <br/>
    </form>
</div>
<button class="edit-element-save-button">Save Changes</button>