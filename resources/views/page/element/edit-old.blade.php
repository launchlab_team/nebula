@extends('layouts.partial')

@section('middle')
<form method="post" action="{{url('element/'.$element->id.'/update')}}" data-ajax-form>
    <div class="detail-view">

        {{-- <span class="detail-view-icon icon icon-circle-layout"></span> --}}
        @if($element->type == 'page-section')
            <h2 class="detail-view-heading">Page Section</h2>
        @elseif($element->type == 'page-block-full' || $element->type == 'page-block-half' || $element->type == 'page-block-third' || $element->type == 'page-block-fourth')
            <h2 class="detail-view-heading">Edit Block</h2>
        @else
            <h2 class="detail-view-heading">Edit Element</h2>
        @endif

        <div class="detail-view-menu">
            <button type="button" title="Delete Element" data-delete="{{url("element/$element->id/delete")}}"><span class="icon uk-icon-trash-o"></span></button>
            <button title="Save Changes"><span class="icon uk-icon-save"></span></button>
            <span onClick="hideDetails()" class="icon icon-x"></span>
        </div>

        <div class="detail-view-content">
            {{csrf_field()}}
            {{-- Page blocks --}}
            @if($element->type == 'page-block-full' || $element->type == 'page-block-half' || $element->type == 'page-block-third' || $element->type == 'page-block-fourth')
                @include('component.form-item',[
                    'name' => 'background_image',
                    'type' => 'image',
                    'label' => 'Background Image',
                    'value' => $element->background_image,
                ])
                @include('component.form-item',[
                    'name' => 'overlay',
                    'type' => 'textarea',
                    'label' => 'Background Overlay Style',
                    'sublabel' => 'Inline CSS',
                    'value' => $element->overlay,
                ])
                @include('component.form-item',[
                    'name' => 'parallax',
                    'type' => 'checkbox',
                    'label' => 'Parallax',
                    'value' => $element->parallax,
                ])
                @include('component.form-item',[
                    'name' => 'style',
                    'type' => 'textarea',
                    'label' => 'Styles',
                    'sublabel' => 'Inline CSS',
                    'value' => $element->style,
                ])
                @include('component.form-item',[
                    'name' => 'class',
                    'type' => 'textarea',
                    'label' => 'Classes',
                    'value' => $element->class,
                ])
            @else
                @include('component.form-item',[
                    'name' => 'type',
                    'type' => 'select',
                    'label' => 'Type',
                    'placeholder' => ucfirst($element->type),
                    'options' => 'Paragraph,Button,H1,H2,H3,H4,H5,Image,YouTube Embed,Vimeo Embed',
                ])
            @endif

            @if($element->type == 'youtube_embed')
                @include('component.form-item',[
                    'name' => 'url',
                    'type' => 'text',
                    'label' => 'URL',
                    'value' => $element->url,
                ])
            @endif
            
            @if($element->type == 'vimeo_embed')
                @include('component.form-item',[
                    'name' => 'url',
                    'type' => 'text',
                    'label' => 'URL',
                    'value' => $element->url,
                ])
            @endif

            {{-- Basic Elements --}}
            @if($element->type == 'paragraph' || $element->type == 'h1' || $element->type == 'h2' || $element->type == 'h3' || $element->type == 'h4' || $element->type == 'h5' || $element->type == 'button')
                @include('component.form-item',[
                    'name' => 'html',
                    'type' => 'html',
                    'label' => 'HTML',
                    'value' => $element->html,
                ])

                @include('component.form-item',[
                    'name' => 'style',
                    'type' => 'textarea',
                    'label' => 'Styles',
                    'value' => $element->style,
                ])

                @include('component.form-item',[
                    'name' => 'class',
                    'type' => 'textarea',
                    'label' => 'Classes',
                    'value' => $element->class,
                ])

                @include('component.form-item',[
                    'name' => 'attributes',
                    'type' => 'textarea',
                    'label' => 'Attributes',
                    'value' => $element->attributes,
                ])
            @endif

            @if($element->type == 'image')
                @include('component.form-item',[
                    'name' => 'image',
                    'type' => 'image',
                    'label' => 'Image',
                    'value' => $element->image,
                ])
            @endif

        </div>

    </div>
</form>
@endsection

@section('styles')
    @parent
    <style type="text/css">
        {{$element->page->css}}
    </style>
@endsection
