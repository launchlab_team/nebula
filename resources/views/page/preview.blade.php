@extends('layouts.default')

@section('main')
    {{Auth::user()->is_manager == 0}}
    @include('view.blade.php')
@endsection