<div class="page-section" id="{{$section->id}}">
    <div class="page-editor-section-toolbar">
        <span class="icon uk-icon-edit" title="Edit Section" data-edit-element="{{$section->id}}"></span>
        &nbsp;
        <span class="icon uk-icon-arrows-v" title="Move Section"></span>
    </div>

    <div class="uk-grid uk-grid-collapse" data-uk-grid-match>
            @foreach($page_blocks as $page_block)
                <div class="
                    @if($page_block->type == 'page-block-full')
                        uk-width-medium-1-1
                    @endif
                    @if($page_block->type == 'page-block-half')
                        uk-width-medium-1-2
                    @endif
                    @if($page_block->type == 'page-block-third')
                        uk-width-medium-1-3
                    @endif
                    @if($page_block->type == 'page-block-fourth')
                        uk-width-medium-1-4
                    @endif
                    page-block-wrapper"
                    id="{{$page_block->id}}">
                    <div class="page-block-overlay"></div>

                    <div class="page-block">
                        <div class="page-editor-block-toolbar">
                            @if ($page_block->type !== 'page-block-full')
                            <span class="icon uk-icon-arrows-h" title="Move Block"></span>
                            &nbsp;
                            @endif
                            <span class="icon uk-icon-edit" title="Edit Block" data-edit-element="{{$page_block->id}}"></span>
                            <span class="icon icon-plus" data-add-element="{{$page_block->id}}" title="Add Element to Block"></span>
                        </div>
                    </div>
                </div>
            @endforeach
    </div>
</div>