<div class="page-section">
    <div class="page-editor-section-toolbar" data-delete-section="element/{{$section->id}}/delete" >
        <span class="icon uk-icon-trash-o" title="Delete Section"></span>
    </div>

    <div class="uk-grid" data-uk-grid-match>
            @foreach($page_blocks as $page_block)
                <div class="
                    @if($page_block->type == 'page-block-full')
                        uk-width-medium-1-2
                    @endif
                    @if($page_block->type == 'page-block-half')
                        uk-width-medium-1-2
                    @endif
                    @if($page_block->type == 'page-block-third')
                        uk-width-medium-1-3
                    @endif
                    @if($page_block->type == 'page-block-fourth')
                        uk-width-medium-1-4
                    @endif"
                    >
                    <div class="page-block" id="{{$page_block->id}}">
                        <div class="page-block-toolbar">
                            <span class="icon uk-icon-align-left" data-left-justify-block="{{$page_block->id}}"></span>
                            <span class="icon uk-icon-align-center" data-center-justify-block="{{$page_block->id}}"></span>
                            <span class="icon uk-icon-align-right" data-right-justify-block="{{$page_block->id}}"></span>
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            <span class="icon icon-edit" data-details="element/{{$page_block->id}}/edit" title="Edit Block"></span>
                            <span class="icon icon-plus" data-details="block/{{$page_block->id}}/add" title="Add Element to Block"></span>
                        </div>
                    </div>
                </div>
            @endforeach
    </div>
</div>