<form method="post" data-ajax-form action="{{url('forms/'.$form->slug)}}" class="uk-text-left">
    @if($form->fields !== null)
        @foreach($form->fields as $field)
            @include('component.form-item',[
                'type'=>$field->type,
                'name'=>'field['.$field->id.']',
                'label'=>$field->label,
                'sublabel'=>$field->hint,
                'options'=>$field->options,
                'placeholder'=>$field->placeholder,
            ])
            <input type="hidden" name="question[]" value="{{$field->label}}"/>
        @endforeach
        <hr style="border-top:1px solid rgba(0, 0, 0, 0.09)">
        <button class="center full">Submit Response</button>
    @endif
</form>