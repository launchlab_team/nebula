@extends('layouts.default')

@section('middle')
    {{-- <div class="container"> --}}
        <div class="focus-width center">
            <form method="post" data-ajax-form> {{-- Posts to itself --}}
                @if($form->fields !== null)
                    <div class="block" style="padding:0.5em 2em 1.5em 2em">
                    <div style="padding: 13px 0;border-bottom: 1px solid #f3f3f3;font-size: 1.5em;line-height: 35px;margin-bottom:10px;">
                        {{$form->name}} <span style="font-size:1.5em" class="right icon-circle-form"></span>
                    </div>
                    @foreach($form->fields as $field)
                        @include('component.form-item',[
                            'type'=>$field->type,
                            'name'=>'field['.$field->id.']',
                            'label'=>$field->label,
                            'sublabel'=>$field->hint,
                            'options'=>$field->options,
                            'placeholder'=>$field->placeholder,
                        ])
                        <input type="hidden" name="question[]" value="{{$field->label}}"/>
                    @endforeach
                    <hr style="border-top: 1px solid #f3f3f3;">
                    <button class="center full">Submit Response</button>
                    </div>
                @endif
            </form>
        </div>
    {{-- </div> --}}
@endsection


@section('scripts')
    @parent
    <script type="text/javascript">
        $(document).on('submit','form',function(e) {
            $('button.center.full').html('Submitting...');
        });
        $( document ).ajaxComplete(function() {
            $('button.center.full').html('Thank you!');
        });
    </script>
@overwrite