@extends('layouts.partial')

@section('middle')
    <form method="post" action="{{url("form/$form->id/edit")}}" data-ajax-form>
        <div class="detail-view">

            <span class="detail-view-icon icon icon-circle-form"></span>
            <h2 class="detail-view-heading">Edit Form</h2>

            <div class="detail-view-menu">
                <button type="button" title="Delete Form" data-delete="{{url('form/'.$form->id.'/delete')}}"><span class="icon uk-icon-trash-o"></span></button>
                <button title="Save Changes"><span class="icon uk-icon-save"></span></button>
                <span onClick="hideDetails()" class="icon icon-x"></span>
            </div>



            <div class="detail-view-content">
                @include('component.form-item',[
                    'name' => 'name',
                    'type' => 'text',
                    'label' => 'Name',
                    'value' => $form->name,
                ])

                @if (count($form->recipients) !== 0)
                    <div class="form-item" style="margin-bottom:-8px"><label>Notify:</label></div>
                    <div class="message-box uk-scrollable-box" style="height:50px">
                        @foreach($form->recipients as $recipient)
                          {{$recipient->email}}
                        @endforeach
                    </div>
                @endif
                @include('component.button',[
                    'name' => 'name',
                    'type' => 'button',
                    'class' => 'full',
                    'text' => 'Manage Notifications',
                    'modal' => 'form/'.$form->id.'/alerts',
                    'icon_left' => 'uk-icon-bell-o',
                ])
                {{-- @if(isset($invoice->shared_with) && count(json_decode($invoice->shared_with)) > 0)
                    <div class="form-item" style="margin-bottom:-8px"><label>Notify by email:</label></div>
                    <div class="message-box uk-scrollable-box" style="height:50px">
                        @foreach(json_decode($invoice->shared_with) as $email)
                            {{$email}}&nbsp;
                        @endforeach
                    </div>
                <hr/>
                @endif --}}
                <hr style="margin:10px 0" />
                @include('component.list-item',[
                    'text' => 'Shareable Link',
                    'subtext' => url('forms/'.$form->slug),
                    'icon_left' => 'icon-circle-form',
                    'class' => 'uk-text-left has-circle-icon',
                    'href' => url('forms/'.$form->slug),
                    'target' => '_blank',
                ])
                @if($form->responses !== null && count($form->responses) > 0)
                @include('component.list-item',[
                    'text' => 'View Responses',
                    'icon_left' => 'icon-circle-bubble',
                    'class' => 'uk-text-left has-circle-icon',
                    'view' => 'form/'.$form->slug.'/'.$form->version.'/responses',
                ])
                @endif
                <hr style="margin:5px 0" />

                <div id="form_fields">
                @if(isset($form->fields) && count($form->fields) !== 0)
                    @foreach($form->fields as $field)
                        <div class="form-item-draggable-field" id="field_{{$field->id}}">
                            @include('component.form-item',[
                                'label' => $field->label,
                                'sublabel' => $field->hint,
                                'placeholder' => $field->placeholder,
                                'type' => $field->type,
                                'options' => $field->options,
                                'draggable' => true,
                                'editable' => true,
                                'name' => $field->id, // name not used
                            ])
                            <input type="hidden" name="field_type[]" value="{{$field->type}}">
                            <input type="hidden" name="field_label[]" value="{{$field->label or ''}}">
                            <input type="hidden" name="field_hint[]" value="{{$field->hint or ''}}">
                            <input type="hidden" name="field_options[]" value="{{$field->options or ''}}">
                            <input type="hidden" name="field_placeholder[]" value="{{$field->placeholder or ''}}">
                        </div>
                    @endforeach
                @endif
                </div>

                <div class="uk-button-dropdown full" data-uk-dropdown="{mode:'click'}">
                    @include('component.button',[
                        'class' => 'full',
                        'name' => 'name',
                        'icon_left' => 'icon-plus',
                        'text' => 'Add a Field',
                        'type' => 'button',
                        //'modal' => 'form/'.$form->id.'/field/create'
                    ])
                    <div class="uk-dropdown uk-dropdown-small">
                        <ul class="uk-nav uk-nav-dropdown">
                            @include('component.list-item',[
                                'text'=>'Short Answer',
                                'icon_left'=>'uk-icon-font',
                                'modal'=>'form/'.$form->id.'/field/create?type=text',
                                'dropdown' => null // Set dropdown to null if it's set in the parent view 
                            ])

                            @include('component.list-item',[
                                'text'=>'Long Answer',
                                'icon_left'=>'uk-icon-paragraph',
                                'modal'=>'form/'.$form->id.'/field/create?type=textarea',
                                'dropdown' => null // Set dropdown to null if it's set in the parent view 
                            ])

                            @include('component.list-item',[
                                'text'=>'Choose Multiple',
                                'icon_left'=>'icon-square-check',
                                'modal'=>'form/'.$form->id.'/field/create?type=checkboxes',
                                'dropdown' => null // Set dropdown to null if it's set in the parent view 
                            ])

                            @include('component.list-item',[
                                'text'=>'Choose One',
                                'icon_left'=>'icon-radio-checked',
                                'modal'=>'form/'.$form->id.'/field/create?type=radios',
                                'dropdown' => null // Set dropdown to null if it's set in the parent view 
                            ])

                            @include('component.list-item',[
                                'text'=>'Select One',
                                'icon_left'=>'icon-select-arrow',
                                'modal'=>'form/'.$form->id.'/field/create?type=select',
                                'dropdown' => null // Set dropdown to null if it's set in the parent view 
                            ])
                        </ul>
                    </div>
                </div>
            </div>
    </div>

@endsection