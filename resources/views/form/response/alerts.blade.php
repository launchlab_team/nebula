@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Alert Settings <span class="icon uk-icon-bell-o"></div>
        <form method="post" enctype="multipart/form-data" action="{{url('form/'.$form->id.'/alerts')}}" data-ajax-form>
            {{csrf_field()}}
            <div class="form-item textarea ">
                <label>Alert the following users:</label> 
                <span class="form-item-sublabel">Comma seperated email addresses</span>
                <textarea name="emails" placeholder="one@example.com, two@example.com"><?php $i = 0; ?>@foreach($form->recipients as $recipient)<?php $i++; ?>{{$recipient->email}}@if($i !== count($form->recipients)), @endif
                @endforeach</textarea>
            </div>
            <button class="modal-button">Update Settings</button>
        </form>
    </div>
@endsection