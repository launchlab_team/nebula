@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        @if($form_version == 1)
            Forms / {{$form->name}}
        @else
             Forms / {{$form->name}} v{{$form_version}}
        @endif
        <a href="{{url('form/'.$form->id.'/'.$form_version.'/responses/export')}}" class="right"><span class="icon icon-download"></span></a>
    </h1>
    <div class="horizontal-tabs">
        <a href="{{url('forms')}}"><div class="tab">Recent</div></a>
        <a href="{{url('form/responses')}}"><div class="tab selected">Responses</div></a>
    </div> 
    
    @foreach($responses as $response)
        @include('component.list-item',[
            'details' => 'form/response/'.$response->id,
            'text' => json_decode($response->answers)[0],
            'subtext' => $response->created_at->diffForHumans(),
            'class' => 'has-circle-icon',
            'icon_left' => 'icon-circle-bubble',
        ])
    @endforeach

@endsection
