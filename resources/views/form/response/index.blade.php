@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        Forms
        {{-- <a href="{{URL::previous()}}" class="right"><span class="icon icon-back"></span></a> --}}
        <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span data-modal="form/create" class="icon icon-plus cursor-pointer"></span>
        </div>
    </h1>

    <div class="horizontal-tabs">
        <a href="{{url('forms')}}"><div class="tab">Recent</div></a>
        <a href="{{url('form/responses')}}"><div class="tab selected">Responses</div></a>
    </div> 
    @if (App\FormResponse::count() !== 0)
        @foreach($response_sets as $response_set)
            @if($response_set['responses'] > 0)
                @if($response_set['version'] == '1')
                    @include('component.list-item',[
                        'text' => $response_set['form_name'],
                        'class' => 'has-circle-icon',
                        'icon_left' => 'icon-circle-form',
                        'subtext' => ($response_set['responses'] !== 1) ? $response_set['responses'] . ' Responses' : '1 Response',
                        'href' => 'form/'.$response_set['form_slug'].'/'. $response_set['version'].'/responses'
                    ])
                @else
                    @include('component.list-item',[
                        'text' => $response_set['form_name'] . ' v'.$response_set['version'],
                        'class' => 'has-circle-icon',
                        'icon_left' => 'icon-circle-form',
                        'subtext' => ($response_set['responses'] !== 1) ? $response_set['responses'] . ' Responses' : '1 Response',
                        'href' => 'form/'.$response_set['form_slug'].'/'. $response_set['version'].'/responses'
                    ])
                @endif
            @endif
        @endforeach
    @else
        <div class="message-center">No responses to display</div>
    @endif

    {{-- @foreach($forms as $form)
         @include('component.list-item',[
            'text' => $form->name,
            'class' => 'has-circle-icon',
            'icon_left' => 'icon-circle-form',
            'subtext' => count($form->responses) . ' Responses',
            'href' => 'form/'.$form->id.'/responses'
        ])
    @endforeach --}}

@endsection
