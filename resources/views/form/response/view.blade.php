@extends('layouts.partial')

@section('middle')
    <div class="detail-view">
        <span class="detail-view-icon icon icon-circle-bubble"></span>
        <h2 class="detail-view-heading" style="margin-bottom:25px">Form Submission</h2>
        <div class="detail-view-menu">
            <button type="button" title="Delete Response" data-delete="{{url('form/response/'.$response->id.'/delete')}}"><span class="icon uk-icon-trash-o"></span></button>
            <span onClick="hideDetails()" class="icon icon-x"></span>
        </div>

        <div class="detail-view-content">
            <div class="block uk-text-left">
                <h3 class="uk-text-center">{{$response->form->name}}</h3>
                <hr class="uk-margin-top-remove" />
                @foreach($questions as $key => $question)
                    <label>{{$question}}</label>
                    <div class="answer">
                        {{$answers[$key]}}
                    </div>
                    <br/>
                @endforeach
                <hr class="uk-margin-top-remove" />
                <span class="text-light uk-text-center uk-display-block">{{$response->form->created_at->diffForHumans()}}</span>
            </div> 
        </div>
    </div>
@endsection