@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        Forms
        <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span data-modal="form/create" class="icon icon-plus cursor-pointer"></span>
        </div>
    </h1>
    
    <div class="horizontal-tabs">
        <a href="{{url('forms')}}"><div class="tab selected">Recent</div></a>
        <a href="{{url('form/responses')}}"><div class="tab">Responses</div></a>
    </div> 
    
    {{-- @if (App\FormResponse::count() !== 0)
        @include('component.list-item',[
            'text' => 'Responses',
            'subtext' => App\FormResponse::count() . ' Total',
            'class' => 'has-circle-icon',
            'icon_left' => 'icon-circle-bubble',
            'href' => 'form/responses',
        ])
        <hr style="margin:10px 0"/>
    @endif --}}

    @if(count($forms) == 0)
        <div class="message-center">No forms yet. Click <span class="icon icon-plus"></span> to get started.</div> 
    @endif

    @foreach($forms as $form)
        @include('component.list-item',[
            'text' => $form->name,
            'subtext' => $form->updated_at->diffForHumans(),
            'class' => 'has-circle-icon',
            'icon_left' => 'icon-circle-form',
            'details' => 'form/'.$form->id.'/edit'
        ])
    @endforeach

    @include('component.paginate',['records'=>$forms])

@endsection
