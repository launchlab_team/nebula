<div class="form-item-draggable-field"> 
@include('component.form-item',[
    'type'=>$_GET['type'],
    'name'=>'field[]',
    'label'=>$_GET['label'],
    'sublabel'=>$_GET['hint'],
    'options'=>$_GET['options'],
    'placeholder'=>$_GET['placeholder'],
    'draggable' => true,
    'editable' => true,
])
<input type="hidden" name="field_type[]" value="{{$_GET['type']}}">
<input type="hidden" name="field_label[]" value="{{$_GET['label'] or ''}}">
<input type="hidden" name="field_hint[]" value="{{$_GET['hint'] or ''}}">
<input type="hidden" name="field_options[]" value="{{$_GET['options'] or ''}}">
<input type="hidden" name="field_placeholder[]" value="{{$_GET['placeholder'] or ''}}">
</div>