@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">New Field <span class="icon icon-plus"></div>
        {{-- This form is not posted but rather handled via javascript in forms.js and used to add a field --}}
        <form id="create_field_form">
            <input type="hidden" name="type" value="{{$_GET['type']}}" data-field-type/> 
            @include('component.form-item',[
                'name' => 'label',
                'type' => 'text',
                'label' => 'Question/Prompt',
                'attributes' => 'data-field-label',
            ])
            @include('component.form-item',[
                'name' => 'hint',
                'type' => 'text',
                'label' => 'hint (optional)',
                'sublabel' => 'Example hint text',
                'attributes' => 'data-field-hint',
            ])
            @include('component.form-item',[
                'name' => 'placeholder',
                'type' => 'text',
                'label' => 'Placeholder (optional)',
                'placeholder' => 'Example',
                'attributes' => 'data-field-placeholder',
            ])
            @if(isset($_GET['type']) && ($_GET['type'] == 'select' || $_GET['type'] == 'radios' || $_GET['type'] == 'checkboxes'))
                @include('component.form-item',[
                    'name' => 'options',
                    'type' => 'textarea',
                    'label' => 'Options',
                    'sublabel' => 'Comma seperated values',
                    'placeholder' => 'e.g. one, two, three',
                    'attributes' => 'data-field-options',
                ])
            @endif
            <button class="modal-button">Add to Form</button>
        </form>
    </div>
@endsection