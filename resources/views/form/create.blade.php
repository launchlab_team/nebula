@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">New Form <span class="icon icon-form"></div>
        <form method="post" enctype="multipart/form-data" action="{{url('form/create')}}" data-ajax-form>
            {{csrf_field()}}
            @include('component.form-item',[
                'name' => 'name',
                'type' => 'text',
                'label' => 'Name',
            ])
            <button class="modal-button">Add Form</button>
        </form>
    </div>
@endsection