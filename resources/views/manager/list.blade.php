@if(isset($managers) && $managers !== null)
    @foreach($managers as $manager)
        @include('component.list-item',[
            'text' => $manager->name,
            'subtext' => $manager->email,
            'attributes' => 'user/'.$manager->id.'/edit',
        ])
    @endforeach
@else
    <div class="message-box">
        No managers found
    </div>
@endif