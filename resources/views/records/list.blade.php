@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        {{$module->label}}
        <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span class="icon icon-menu cursor-pointer"></span>
            <div class="uk-dropdown">
                <ul>
                    @include('component.list-item',[
                        'text'=>'Create List',
                        'icon_left'=>'icon-plus',
                        'modal' => "record_set/".$module->id."/create",
                    ])
                    @include('component.list-item',[
                        'text'=>'Import Sheet',
                        'icon_left'=>'icon-upload',
                        'attributes'=>'data-modal="records/'.$module->id.'/import"',
                    ])
                    @if(isset($record_set) && count($record_set) !== 0)
                        @include('component.list-item',[
                            //'text'=>'Download '. $record_set->name,
                            'text'=>'Export Sheet',
                            'icon_left'=>'icon-download',
                            'href'=>'records/'.$record_set->id.'/export',
                        ])
                        @include('component.list-item',[
                            'text'=>'Edit Current List',
                            'icon_left'=>'uk-icon-edit',
                            'attributes'=>'data-modal="record_set/'.$record_set->id.'/edit"',
                        ])
                        @include('component.list-item',[
                            'text'=>'Delete Current List',
                            'icon_left'=>'icon-trash-alt',
                            'attributes'=>'data-delete="'.url('record_set/'.$record_set->id.'/delete').'"',
                        ])
                    @endif
                </ul>
            </div>
        </div>
    </h1>

    @if(isset($module->record_sets) && count($module->record_sets) !== 0)
        <h2 class="record-set-toolbar"> 
            <div class="select-wrapper">
            {{-- {{dd(url('#records/'.$module->slug.'/'.$record_set->id))}} --}}
            <div class="form-item">
                @if(isset($module->record_sets) && count($module->record_sets) > 0)
                    <span class="icon form-item-icon form-item-icon-right icon-select-arrow"></span>
                @endif
                <select data-choose-record-set>
                    {{-- <option value="records/{{$module->slug}}/{{$record_set->id}}" disabled selected>{{$record_set->name}}</option> --}}
                    @foreach($module->record_sets as $module_record_set)
                        <option @if($module_record_set->id == $record_set->id) selected @endif value="records/{{$module->slug}}/{{$module_record_set->id}}">{{$module_record_set->name}}</option>
                    @endforeach
                </select>
            </div>
            </div>
            @if(isset($record_set) && count($record_set) > 0)
                <span class="icon icon-plus" title="New Record" data-details="record/{{$record_set->id}}/create"></span>
            @endif
        </h2>
        @if(isset($records) && $records !== null)
            @foreach($records as $record)
            {{-- {{dd(is_object($record->data))}} --}}
                {{-- Necessary to prevent error when data is imported as an object --}}
                @if(!is_object(json_decode($record->data)[$record->record_set->label_key]))
                    @include('component.list-item',[
                        'text' => json_decode($record->data)[$record->record_set->label_key],
                        'subtext' => isset($record->record_set->sublabel_key) ? json_decode($record->data)[$record->record_set->sublabel_key] : NULL,
                        'details' => 'record/'.$record->id.'/edit',
                        //'icon_left' => isset($record->record_set->icon) ? $record->record_set->icon : 'icon-circle-list',
                        'icon_left' => isset($record->record_set->module->icon) ? $record->record_set->module->icon : 'icon-circle-list',
                        'class' => 'has-circle-icon',
                    ])
                @endif

            @endforeach
            {{-- {{$records->links()}} --}}
            @include('component.paginate',['records'=>$records])
        @endif
    @else
       <span class="message-center">No lists found. 
       <a data-modal="records/{{$module->id}}/import">Import an existing sheet</a> or <a data-modal="record_set/{{$module->id}}/create">create a new list.</a></span>
    @endif
@endsection