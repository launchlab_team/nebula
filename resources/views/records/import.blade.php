@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Choose File <span class="icon uk-icon-table"></div>
        <form method="post" id="upload_form" enctype="multipart/form-data" action="{{url('records/'.$module_id.'/import')}}">
            {{csrf_field()}} 
            <input type="file" name="file" id="upload_input" accept=".csv,.xls,.xlsx">
            <span class="icon icon-upload"></span>
        </form>
        <div class="message-box" style="margin-top:10px">The first row will be interpreted as column titles.</div>
    </div>
@endsection