@extends('layouts.partial')

@section('middle')
    <form method="post" action="{{url("record/".$record_set->id."/store")}}" data-ajax-form>
        <div class="detail-view">
            <span class="detail-view-icon icon {{$record_set->module->icon}}"></span>
            <h1 class="detail-view-heading">{{$record_set->name}}</h1>
            <div class="detail-view-menu">
                <button title="Save Changes"><span class="icon uk-icon-save"></span></button>
                <span onClick="hideDetails()" class="icon icon-x"></span>
            </div>
            <div class="detail-view-content">
                {{csrf_field()}} 
                @foreach(json_decode($record_set->columns) as $key => $column)
                    @include('component.form-item',[
                        'name' => 'data[]',
                        'type' => 'text',
                        'label' => $column,
                    ])
                @endforeach
            </div>
        </div>
    </form>
@endsection