@extends('layouts.partial')

@section('main')
    <div class="modal">
        <h2 class="modal-heading">List Item <a href="javascript:history.back()" class="right"><span class="icon icon-back"></span></a></h2>
        <table width="100%">
        @foreach(json_decode($record->data) as $key => $data)
            @if(array_key_exists($key, json_decode($record->record_set->columns)))
             <tr>
                 <td style="height:35px"><b>{{json_decode($record->record_set->columns)[$key]}}</b></td>
                 <td>{{$data}}</td>
             </tr>
            @endif
        @endforeach
        </table>
        <button class="modal-button" onclick="closeModal()">Close Listing</button>
    </div>
@endsection