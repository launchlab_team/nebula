@extends('layouts.partial')

@section('middle')
    <form method="post" action="{{url("record/$record->id/update")}}" data-ajax-form>
        {{csrf_field()}}
        <div class="detail-view">
            <span class="detail-view-icon icon {{$record->record_set->module->icon}}"></span>
            <h1 class="detail-view-heading" style="margin-top:0px">{{$record->record_set->name}} Record</h1>

            <div class="detail-view-menu">
                <button type="button" title="Delete Record" data-delete="{{url('record/'.$record->id.'/delete')}}"><span class="icon uk-icon-trash-o"></span></button>
                <button title="Save Changes"><span class="icon uk-icon-save"></span></button>
                <span onClick="hideDetails()" class="icon icon-x"></span>
            </div>

            <div class="detail-view-content">
                <div class="form-item">
                    <label>List</label>
                    <span class="icon form-item-icon form-item-icon-right icon-select-arrow"></span>
                    @if ($record->record_set->module->record_sets !== null)
                        <select name="record_set_id">
                        @foreach($record->record_set->module->record_sets as $record_set)
                            <option @if($record->record_set->id == $record_set->id) selected @endif value="{{$record_set->id}}">{{$record_set->name}}</option>
                        @endforeach
                        </select>
                    @endif
                </div>
                <hr style="margin:5px" />
                @foreach(json_decode($record->record_set->columns) as $key => $column)
                    @include('component.form-item',[
                        'name' => 'data[]',
                        'type' => 'text',
                        'label' => $column,
                        'value' => isset(json_decode($record->data)[$key]) ? json_decode($record->data)[$key] : NULL,
                    ])
                @endforeach
{{--                 @foreach(json_decode($record->data) as $key => $data)
                    @if(isset(json_decode($record->record_set->columns)[$key]))
                    @include('component.form-item',[
                        'name' => 'data[]',
                        'type' => 'text',
                        'label' => isset(json_decode($record->record_set->columns)[$key]) ? json_decode($record->record_set->columns)[$key] : NULL,
                        'value' => $data,
                    ])
                    @endif
                @endforeach --}}
            </div>
        </div>
    </form>
@endsection