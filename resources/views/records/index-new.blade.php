@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        {{$module->label}}
        <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span class="icon icon-menu cursor-pointer"></span>
            <div class="uk-dropdown">
                <ul>
                    @include('component.list-item',[
                        'text'=>'Create New List',
                        'icon_left'=>'icon-plus',
                        'modal' => "record_set/".$module->id."/create",
                    ])
                    @include('component.list-item',[
                        'text'=>'Upload Spreadsheet',
                        'icon_left'=>'icon-upload',
                        'attributes'=>'data-modal="records/'.$module->id.'/import"',
                    ])
                </ul>
            </div>
        </div>
    </h1>

    @if(isset($module->record_sets) && count($module->record_sets) !== 0)
        @if(isset($records) && $records !== null)
            @foreach($records as $record)
                {{-- Necessary to prevent error when data is imported as an object --}}
                @if(!is_object(json_decode($record->data)[$record->record_set->label_key]))
                    @include('component.list-item',[
                        'text' => json_decode($record->data)[$record->record_set->label_key],
                        'subtext' => isset($record->record_set->sublabel_key) ? json_decode($record->data)[$record->record_set->sublabel_key] : NULL,
                        'details' => 'record/'.$record->id.'/edit',
                        //'icon_left' => isset($record->record_set->icon) ? $record->record_set->icon : 'icon-circle-list',
                        'icon_left' => isset($record->record_set->module->icon) ? $record->record_set->module->icon : 'icon-circle-list',
                        'class' => 'has-circle-icon',
                    ])
                @endif
            @endforeach
            @include('component.paginate',['records'=>$records])
        @endif
    @else
       <span class="message-center">No lists found. 
       <a data-modal="records/{{$module->id}}/import">Import an existing spreadsheet</a> or <a data-modal="record_set/{{$module->id}}/create">create a new list.</a></span>
    @endif
@endsection