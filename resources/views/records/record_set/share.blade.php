@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Share {{$record_set->name}} <span class="icon uk-icon-list" style="font-size:0.6em"></div>
        <form method="post" action="{{url("record_set/$record_set->id/share")}}" data-ajax-form>
            {{csrf_field()}}
            @include('component.form-item',[
                'name' => 'emails',
                'type' => 'textarea',
                'label' => 'Share with:',
                'placeholder' => 'one@example.com, two@example.com',
                'sublabel' => 'Enter email addresses seperated by a comma.',
            ])
            <div class="message-box"><u>{{$record_set->name}}</u><br/>will be made available under shared listings in each user's account.</div>
            <button class="modal-button">Share List</button>
        </form>
    </div>
@endsection