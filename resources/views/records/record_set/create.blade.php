@extends('layouts.partial')

@section('middle')
    <div class="modal">
        <form method="post" action="{{url("record_set/store")}}" data-ajax-form>

            <h2 class="modal-heading">Create a List</h2>
                {{csrf_field()}} 
                <input type="hidden" name="module_id" value="{{$module->id}}" />
                @include('component.form-item',[
                    'name' => 'name',
                    'type' => 'text',
                    'label' => 'List Name',
                ])
                @include('component.form-item',[
                    'name' => 'columns',
                    'type' => 'textarea',
                    'label' => 'Columns',
                    'sublabel' => 'Comma seperated',
                    'placeholder'=>"e.g. Name, Email, Address etc.",
                ])
                <button class="modal-button">Create List</button>
        </form>
    </div>
@endsection