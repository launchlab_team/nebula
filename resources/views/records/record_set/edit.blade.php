@extends('layouts.partial')

@section('middle')
    <div class="modal">
        <form method="post" action="{{url("record_set/$record_set->id/update")}}" data-ajax-form>

            <h2 class="modal-heading">Edit Record Set</h2>
                {{csrf_field()}} 
                <label>Display Settings</label>
                <hr class="uk-margin-remove" />
                @include('component.form-item',[
                    'name' => 'name',
                    'type' => 'text',
                    'label' => 'Record Set Name',
                    'value' => $record_set->name,
                ])
                <div class="form-item">
                    <label>List item Text</label>
                    <select name="label_key">       
                        <option disabled>Choose one:</option>                      
                        @foreach(json_decode($record_set->columns) as $key => $column)
                            <option @if($record_set->label_key == $key) selected @endif value="{{$key}}">{{$column}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-item">
                    <label>List item subtext</label>
                    <select name="sublabel_key">
                        <option>(optional)</option>             
                        @foreach(json_decode($record_set->columns) as $key => $column)
                            <option @if($record_set->sublabel_key !== null && $record_set->sublabel_key == $key) selected @endif value="{{$key}}">{{$column}}</option>
                        @endforeach
                    </select>
                </div>
                <br/>
                <label>Columns</label>
                <hr class="uk-margin-remove" />
                <div class="form-item select">
                    <label>Comma seperated</label>
                    <textarea name="columns" placeholder="e.g. Name, Email, Address etc.">@foreach(json_decode($record_set->columns) as $key => $column){{$column}}@if($key + 1 !== count(json_decode($record_set->columns))),@endif @endforeach</textarea>
                </div>
            <button class="modal-button">Update Record Set</button>
        </form>
    </div>
@endsection