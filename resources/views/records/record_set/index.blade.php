@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        {{$record_set->name}}
    </h1>
    @if(isset($records) && $records !== null)
        @foreach($records as $record)
            @if(isset($record->record_set->label_key))
                @include('component.list-item',[
                    'text' => array_key_exists($record->record_set->label_key, json_decode($record->data)) ? json_decode($record->data)[$record->record_set->label_key] : '',
                    'subtext' => array_key_exists($record->record_set->sublabel_key, json_decode($record->data)) ? json_decode($record->data)[$record->record_set->sublabel_key] : NULL,
                    'href' => 'record/'.$record->id.'/view',
                    'icon_left' => isset($record->record_set->module->icon) ? $record->record_set->module->icon : 'icon-circle-list',
                    'class' => 'has-circle-icon',
                ])
            @endif
        @endforeach
    @endif
@endsection
