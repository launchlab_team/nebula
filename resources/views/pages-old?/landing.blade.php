@extends('layouts.default')

@section('content')
    @include('component.section',[
        'heading' => 'Nebula Business Suite',
        'subheading' => 'The operating system for your business',
        'parallax' => 'true',
        'include' => 'pages.partials.landing-intro-section',
    ])
    @include('component.section',[
        'class' => 'background-white red',
        'include' => 'pages.partials.landing-text-section',
    ])
    {{-- <div class="uk-grid">
        @include('component.section',[
            'heading' => 'Welcome',
            'subheading' => 'subheading',
            'parallax' => 'false',
            'class' => 'uk-width-1-2',
        ])
        @include('component.section',[
            'heading' => 'Welcome',
            'subheading' => 'subheading',
            'parallax' => 'true',
            'class' => 'uk-width-1-2',
            'background' => url('https://upload.wikimedia.org/wikipedia/commons/d/d6/Hs-2009-25-e-full_jpg.jpg'),
        ])
    </div> --}}
@endsection