@extends('layouts.default')

@section('title',$page->title)

@section('main')
    @include('page.view',[
        'page'=>$page,
    ])
@endsection