@extends('layouts.partial')

@section('middle')
    <form method="post" action="{{url("user/$user->id/update")}}" data-ajax-form>
        <div class="detail-view">

            <span class="detail-view-icon icon icon-circle-user"></span>
            <h2 class="detail-view-heading">Edit User</h2>

            <div class="detail-view-menu">
                <button type="button" title="Delete user" data-delete="{{url('user/'.$user->id.'/delete')}}"><span class="icon uk-icon-trash-o"></span></button>

                {{-- <a href="{{url($user->slug)}}" target="_blank"><button type="button" title="Open user"><span class="icon icon-edit"></span></button></a> --}}

                <button title="Save Changes"><span class="icon uk-icon-save"></span></button>
                <span onClick="hideDetails()" class="icon icon-x"></span>
            </div>

            <div class="detail-view-content">
                {{csrf_field()}} 
                @include('component.form-item',[
                    'name' => 'name',
                    'type' => 'text',
                    'label' => 'Name',
                    'value' => $user->name,
                ])
                @include('component.form-item',[
                    'name' => 'email',
                    'type' => 'text',
                    'label' => 'Email',
                    'value' => $user->email,
                ])
                @include('component.form-item',[
                    'name' => 'password',
                    'type' => 'password',
                    'label' => 'password',
                    'placeholder' => 'Change Password',
                ])
                @include('component.form-item',[
                    'name' => 'photo',
                    'type' => 'image',
                    'label' => 'Profile photo',
                    'placeholder' => 'Choose photo',
                ])
                <button class="full" data-ajax-button="{{url('manager/'.$user->id.'/toggle')}}">
                    @if ($user->is_manager)
                        Revoke Manager Access
                    @else
                        Grant Manager Access
                    @endif
                </button>
            </div>
        </div>
    </form>
@endsection