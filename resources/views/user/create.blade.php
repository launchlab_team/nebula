@extends('layouts.partial')

@section('middle')
    <form method="post" action="{{url("user/create")}}" data-ajax-form>
        <div class="detail-view">

            <span class="detail-view-icon icon icon-circle-user"></span>
            <h2 class="detail-view-heading">Create User</h2>

            <div class="detail-view-menu">
                <button title="Save Changes"><span class="icon uk-icon-save"></span></button>
                <span onClick="hideDetails()" class="icon icon-x"></span>
            </div>

            <div class="detail-view-content">
                {{csrf_field()}} 
                @include('component.form-item',[
                    'name' => 'name',
                    'type' => 'text',
                    'label' => 'Name',
                ])
                @include('component.form-item',[
                    'name' => 'email',
                    'type' => 'text',
                    'label' => 'Email',
                ])
                @include('component.form-item',[
                    'name' => 'password',
                    'type' => 'password',
                    'label' => 'password',
                    'placeholder' => 'Change Password',
                ])
                @include('component.form-item',[
                    'name' => 'photo',
                    'type' => 'image',
                    'label' => 'Profile photo',
                    'placeholder' => 'Choose photo',
                ])
            </div>
        </div>
    </form>
@endsection