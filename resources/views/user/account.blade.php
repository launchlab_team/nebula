@extends('layouts.default')

@section('subheader')
    @parent
    <h1 class="heading-bar">
        <div class="focus-width">
        Welcome {{strtok(Auth::user()->name, " ")}}
        @if(isset(Auth::user()->photo) && Auth::user()->photo !== '')
            <div class="profile-image right" style="background-image:url({{Auth::user()->photo}})"></div>
        @else
            <span class="icon icon-circle-user right" style="font-size: 34px;opacity: 0.5"></span>
        @endif
        </div>
    </h1> 
@endsection

@section('main')
<div class="container focus-width magicTime uk-animation-slide-bottom" style="padding-top:0px;margin-top:-10px">
        <div class="uk-grid">
            <div class="uk-width-1-1">
                @if(isset($settings) && $settings !== null)
                    {{-- <div class="block organization-block">
                        <div class="uk-grid">
                            <div class="uk-width-medium-1-2">
                                <h2 class="organization-block-name">{{$settings->organization_name}}</h2><br>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="stat-row">Users: <span class="count">{{count($users)}}</span></div>
                                <div class="stat-row">Managers: <span class="count">{{count($users->where('is_manager','1'))}}</span></div>
                                <div class="stat-row">Storage used:<span class="count">{{$storage_used}}</span></div>
                            </div>
                        </div>
                        <a href="dashboard"><button class="organization-block-button">Continue to Dashboard</button></a>
                    </div> --}}
                @else
                    <div class="block organization-block">
                        <h2 class="organization-block-name">Get Started</h2>
                        <form method="post" action="{{url('settings')}}" data-ajax-form>
                            {{csrf_field()}} 
                            @include('component.form-item',[
                                'name' => 'organization_name',
                                'type' => 'text',
                                'label' => 'Organization Name',
                            ])
                            @include('component.form-item',[
                                'name' => 'logo',
                                'type' => 'image',
                                'label' => 'Logo',
                            ])
                            <button class="organization-block-button">Save &amp; Continue to Dashboard</button>
                        </form>
                    </div>
                @endif
            </div>       
        </div>
        @if(isset($settings) && $settings !== null)
        <br/>
        @endif

     @if(Auth::user()->is_manager)
        <a href="dashboard">
        <div class="accordion">
            <div class="accordion-title">
                <div class="accordion-icon-left">
                    <span class="icon icon-circle-home"></span>
                </div>
                Dashboard
                <div class="accordion-icon-right">
                    <span class="icon-angle-right"></span>
                </div>
            </div>
        </div>
        </a>
    @endif


        {{-- <div class="accordion">
            <div class="accordion-title">
                <div class="accordion-icon-left">
                    <span class="icon icon-circle-payment"></span>
                </div>
                Settings
                <div class="accordion-icon-right">
                    <span class="icon-minus"></span>
                </div>
            </div>
            <div class="accordion-content" style="display: block">
                details about account
            </div>
        </div> --}}

        <div class="accordion">
            <div class="accordion-title">
                <div class="accordion-icon-left">
                    <span class="icon icon-circle-user"></span>
                </div>
                Account Details
                <div class="accordion-icon-right">
                    <span class="icon-plus"></span>
                </div>
            </div>
            <div class="accordion-content">

                {{-- <div class="block uk-margin-bottom"> --}}
                    {{-- <h4>Account Information</h4> --}}
                    {{-- <hr/> --}}
                    <form method="post" action="{{url('account')}}">
                        {{csrf_field()}} 
                        @include('component.form-item',[
                            'name' => 'name',
                            'type' => 'text',
                            'label' => 'Name',
                            'value' => Auth::user()->name,
                        ])
                        @include('component.form-item',[
                            'name' => 'email',
                            'type' => 'text',
                            'label' => 'Email',
                            'value' => Auth::user()->email,
                        ])
                        @include('component.form-item',[
                            'name' => 'password',
                            'type' => 'password',
                            'label' => 'password',
                            'placeholder' => 'Change Password',
                        ])
                        @include('component.form-item',[
                            'name' => 'photo',
                            'type' => 'image',
                            'label' => 'Profile photo',
                            'placeholder' => 'Choose photo',
                        ])
                        <button>Update Account</button>
                    </form> 
                {{-- </div> --}}

            </div>
        </div>

        <div class="accordion">
            <div class="accordion-title">
                <div class="accordion-icon-left">
                    <span class="icon icon-circle-cloud"></span>
                </div>
                My Files
                <div class="accordion-icon-right">
                    <span class="icon-plus"></span>
                </div>
            </div>
            <div class="accordion-content">
                @include('component.list-item',[
                    'text' => 'Upload File(s)',
                    'class' => 'has-circle-icon',
                    'icon_left' => 'icon-circle-upload',
                    'icon_right' => 'icon-upload',
                    'modal' => 'files/import',
                ])
                <div class="list-section-title">My Uploads</div>
                {{-- {{dd($user->files)}} --}}
                {{-- First loop through "My Uploads" --}}
                @if(isset($user) && $user->files !== NULL)
                    {{-- Upload new file --}}
                    @include('component.list-item',[
                        'text' => 'My Uploads',
                        'class' => 'has-circle-icon',
                        'icon_left' => 'icon-circle-cloud',
                        'subtext' => count($user->files) . ' Total',
                        'modal' => 'account/files',
                    ])
                    {{-- Loop through the user's folders --}}
                    @foreach($user->file_folders as $file_folder)
                        {{-- @if($file_folder->files !== NULL) --}}
                            @include('component.list-item',[
                                'text' => $file_folder->name,
                                'class' => 'has-circle-icon',
                                'icon_left' => 'icon-circle-folder',
                                'subtext' => count($file_folder->files) . ' Files',
                                'modal' => 'files/folder/'.$file_folder->id.'/files',
                            ])
                       {{--  @else
                            @include('component.list-item',[
                                'text' => $file_folder->name,
                                'class' => 'has-circle-icon',
                                'icon_left' => 'icon-circle-folder',
                                'subtext' => 'No Files',
                                'modal' => 'files/folder/'.$file_folder->id.'/files',
                            ])
                        @endif --}}
                    @endforeach
                @endif

                @if(isset($user) && (count($user->file_shares) > 0 || count($user->file_folder_shares) > 0))
                    <div class="list-section-title">Shared with Me</div>
                @endif

                @if(isset($user) && count($user->file_shares) > 0)
                    @if(count($user->file_shares) > 0)
                        @include('component.list-item',[
                            'text' => 'Uncategorized',
                            'class' => 'has-circle-icon',
                            'subtext' =>  (count($user->file_shares) == 1) ? count($user->file_shares) . ' ' . ' Total' : count($user->file_shares) . ' ' . ' Total',
                            'icon_left' => 'icon-circle-cloud',
                            'modal' => 'account/shared/files',
                        ])
                    @endif
                @endif

                {{-- Loop through shared folders --}}
                @if(isset($user) && count($user->file_folder_shares) > 0)
                    @foreach($user->file_folder_shares as $file_folder_share)
                        @if(isset($file_folder_share->folder))
                            @include('component.list-item',[
                                'text' => $file_folder_share->folder->name,
                                'class' => 'has-circle-icon',
                                'icon_left' => 'icon-circle-folder',
                                'subtext' => (count($file_folder_share->folder->files) == 1) ? '1 File' :  count($file_folder_share->folder->files) . ' Files',
                                'modal' => 'files/folder/'.$file_folder_share->folder->id.'/files',
                            ])
                        @endif
                    @endforeach
                @endif

                @if(!isset($user) || (count($user->file_shares) == 0 && count($user->file_folder_shares) == 0))
                    <br/>
                    <div class="message-box">Files shared with you will appear here</div>
                @endif
            </div>
        </div>


        @if(isset($user) && count($user->record_set_shares) > 0)
            <div class="accordion">
                <div class="accordion-title">
                    <div class="accordion-icon-left">
                        <span class="icon icon-circle-list"></span>
                    </div>
                    Shared Lists
                    <div class="accordion-icon-right">
                        <span class="icon-plus"></span>
                    </div>
                </div>
                <div class="accordion-content">
                    @foreach($user->record_set_shares as $record_set_share)
                        @if(isset($record_set_share->record_set))
                            @include('component.list-item',[
                                'text' => $record_set_share->record_set->name,
                                'subtext' =>  (count($record_set_share->record_set->records) == 1) ? count($record_set_share->record_set->records) . ' ' . ' record' : count($record_set_share->record_set->records) . ' ' . ' records',
                                'icon_left' => $record_set_share->record_set->module->icon,
                                'class' => 'has-circle-icon',
                                'modal' => 'records/'.$record_set_share->record_set->module->slug.'/'.$record_set_share->record_set->id.'/shared',
                            ])
                        @endif
                    @endforeach
                </div>
            </div>
        @endif

        @if(isset($user) && count($user->stripe_subscriptions) > 0)
            <div class="accordion">
                <div class="accordion-title">
                    <div class="accordion-icon-left">
                        <span class="icon icon-circle-payment"></span>
                    </div>
                    My Subscriptions
                    <div class="accordion-icon-right">
                        <span class="icon-plus"></span>
                    </div>
                </div>
                <div class="accordion-content">
                    @foreach($user->stripe_subscriptions as $stripe_subscription)
                        @include('component.list-item',[
                            'text' => '$'.$stripe_subscription->description,
                            'icon_left' => 'icon-circle-calendar',
                            'class' => 'has-circle-icon',
                            'modal' => 'subscription/'.$stripe_subscription->subscription_id.'/manage',
                        ])
                    @endforeach
                </div>
            </div>
        @endif

        {{-- <div class="accordion">
            <div class="accordion-title" style="border: 0">
                <div class="accordion-icon-left">
                    <span class="icon icon-circle-payment"></span>
                </div>
                Billing Details
                <div class="accordion-icon-right">
                    <span class="icon-plus"></span>
                </div>
            </div>
            <div class="accordion-content">
                <form method="post" action="{{url('subscription/update')}}"  id="payment_form">
                    {{csrf_field()}} 
                    <table width="100%" cellpadding="3px">
                        <tr>
                            <td colspan="3" class="form-item">
                                <input name="cardholder" type="text" @if(App::environment('local')) value="Cardholder Name" @endif placeholder="Cardholder Name">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="form-item">
                                <input name="card_number" @if(App::environment('local')) value="4242 4242 4242 4242" @endif placeholder="Card Number" type="text" size="20" data-stripe="number">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="uk-text-small form-item"><label>Expiration</label></td>
                            <td class="uk-text-small form-item"><label>Security code</label></td>
                        </tr>
                        <tr>
                            <td><input size="2" data-stripe="exp_month" type="text" placeholder="MM" @if(App::environment('local')) value="10" @endif></td>
                            <td>
                              <input size="2" data-stripe="exp_year" type="text" placeholder="YY" @if(App::environment('local')) value="19" @endif>
                            </td>
                            <td><input size="2" data-stripe="cvc" type="text" placeholder="•••" @if(App::environment('local')) value="123" @endif></td>
                        </tr>
                    </table>
                    <br/>
                    <button>Update Payment Method</button>
                </form>
            </div>
        </div> --}}

    {{-- @if(Auth::user()->is_manager) --}}
    {{-- @endif --}}

    {{-- <div class="uk-grid" data-uk-grid-match="{target:'.block'}">
        <div class="uk-width-medium-1-2">        
            <div class="block uk-margin-bottom">
                <h4>Account Information</h4>
                <hr/>
                <form method="post" action="{{url('account')}}">
                    {{csrf_field()}} 
                    @include('component.form-item',[
                        'name' => 'name',
                        'type' => 'text',
                        'label' => 'Name',
                        'value' => Auth::user()->name,
                    ])
                    @include('component.form-item',[
                        'name' => 'email',
                        'type' => 'text',
                        'label' => 'Email',
                        'value' => Auth::user()->email,
                    ])
                    @include('component.form-item',[
                        'name' => 'password',
                        'type' => 'password',
                        'label' => 'password',
                        'placeholder' => 'Change Password',
                    ])
                    @include('component.form-item',[
                        'name' => 'photo',
                        'type' => 'image',
                        'label' => 'Profile photo',
                        'placeholder' => 'Choose photo',
                    ])
                    <button class="block-button white full">Update Account</button>
                </form> 
            </div>
        </div>
        <div class="uk-width-medium-1-2">        
            <div class="block">
                <h4>Billing Information <a data-modal="subscription/edit" class="right icon icon-pencil"></a></h4>
                <hr>
                @if (Auth::user()->card_brand !== null)
                    @include('component.list-item',[
                        'text' => 'Default',
                        'subtext' => Auth::user()->card_brand . ' ' . Auth::user()->card_last_four,
                        'modal' => 'subscription/edit',
                    ])
                @else
                    <div class="message-box uk-text-left">You don't have a card on file yet <a data-modal="subscription/edit">add a card</a>.</div>
                @endif
            </div>

            @if(isset(Auth::user()->card_brand) && Auth::user()->card_brand !== null)
                <div class="block">
                    <h4>My Cards <a data-modal="subscription/edit" class="right icon icon-edit"></a></h4>
                    @if(isset(Auth::user()->card_brand))
                        @include('component.list-item',[
                            'text' => Auth::user()->card_brand . ' ' . Auth::user()->card_last_four,
                            'modal' => 'subscription/edit',
                        ])
                    @endif
                </div>
            @endif
        </div>
    </div> --}}
</div>

@endsection