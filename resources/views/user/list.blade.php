@if(isset($users) && $users !== null)
    @foreach($users as $user)
        @include('component.list-item',[
            'text' => $user->name,
            'subtext' => $user->email,
            'details' => 'user/'.$user->id.'/edit',
            'icon_left' => 'icon-circle-user',
            'class' => 'has-circle-icon',
        ])
    @endforeach
@else
    <div class="message-box">
        No users found
    </div>
@endif