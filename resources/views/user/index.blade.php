@extends('layouts.partial')

@section('main')
    <h2 class="heading">Users ({{count($users) + count($admins)}})<span data-details="user/create" class="cursor-pointer icon icon-plus"></span></h2>
    @if($admins !== null && count($admins) > 0)
        @include('user.list',['users'=>$admins])
        <hr style="margin:10px 0" />
    @endif
    @include('user.list',['users'=>$users])
@endsection