{{-- @include('component.tab',[
    'href' => url('/feed'),
    'icon' => 'icon-circle-home',
    'text' => 'Home',
    'draggable' => true,
]) --}}
<div class="tab {{$class or ''}} @if(isset($href) && (basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) == (basename(parse_url($href, PHP_URL_PATH)))) selected @endif" @if(isset($modal)) data-modal="{{$modal}}" @endif @if(isset($view)) data-view="{{$view}}" @endif @if(isset($details)) data-details="{{$details}}" @endif {!! $attributes or '' !!}>
    @if(isset($href))
        <a href="{{$href}}"></a>
    @endif
    @if(isset($text))
        <div class="tab-text">{{$text}}</div>
    @endif
    @if(isset($icon))
        <div class="icon {{$icon}} tab-icon"></div>
    @endif
    @if(isset($draggable) && $draggable !== 'false')
        {{-- For deleting/editing modules --}}
        <span class="icon icon-draggable settings-visible"></span>
        @if(isset($module) && $module !== null)
            {{-- <span class="icon icon-edit edit-icon settings-visible" data-modal="module/{{$module->id}}/editp"></span> --}}
            <input type="hidden" name="id[]" value="{{$module->id}}">
            <input type="hidden" name="label[]" value="{{$module->label}}">
            <input type="hidden" name="icon[]" value="{{$module->icon}}">
        @endif
        <div data-modal="module/{{$module->id}}/edit" class="tab-editable settings-visible"></div>
    @endif
</div>
