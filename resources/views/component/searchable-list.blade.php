{{-- @include('component.searchable-list',[
    'action' => 'pages/search',
    'preload' => 'organization.manager.list', // preloaded results blade
]) --}}
<div class="searchable-list">
    <form method="post" action="{{url($action)}}" class="searchable-list-form">
        @include('component.form-item',[
            'name' => 'search',
            'type' => 'text',
            'placeholder' => isset($placeholder) ? $placeholder : '',
            'icon_left' => 'icon-search',
            'class' => 'searchable-list-input',
        ])
    </form>
    <ul class="searchable-list-results">
        @if(isset($preload))
            @include($preload)
        @endif
    </ul>
</div>