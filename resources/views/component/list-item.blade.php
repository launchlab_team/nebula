{{-- 
.list-item
@params: text, icon, html, attributes, modal
@example:
    @include('component.list-item',[
        'text' => 'Button Text',
        'icon_left' => 'uk-icon-heart',
        'icon_right' => 'uk-icon-heart',
        'href' => url(),
        'modal' => url(), // shows modal and loads #modal_iframe
        'attributes' => 'data-attribute', 
        'dropdown' => 'dropdown.blade', // includes a dropdown with a view on click
    ])
--}}
@if(isset($dropdown))
    <div class="uk-position-relative uk-display-inline-block" data-uk-dropdown="{mode:'{{isset($dropdown_mode)?$dropdown_mode:'click'}}' @if($dropdown_position !== null),pos:'{{$dropdown_position}}' @endif}" style="display: table-cell">
        {{-- The dropdown toggle is anything inside here --}}
@endif

<li @if(isset($target) && $target == '_blank') target="_blank" @endif class="list-item {{$class or ''}} @if(isset($icon_right)) has-icon-right @endif @if(isset($icon_left) || isset($icon)) has-icon-left @endif" {!!$attributes or ''!!} {{$id or ''}} @if(isset($modal) && $modal !== '') data-modal="{{$modal}}" @endif @if(isset($view)) data-view="{{$view}}" @endif @if(isset($details) && $details !== '') data-details="{{$details}}" @endif @if(isset($style)) style="{{$style}}" @endif>
    <div class="list-item-body">
        <span class="list-item-text">{{$text or ''}}</span>
        @if(isset($subtext))
            <span class="list-item-subtext">{{$subtext or ''}}</span>
        @endif
        {!!$html or ''!!}
        {{-- Icon included by default --}}
        @if(isset($icon))
            <span class="list-item-icon-left {{$icon}}"></span>
        @endif
        @if(isset($icon_left))
            <span class="list-item-icon-left {{$icon_left}}"></span>
        @endif
        @if(isset($icon_right))
            <span class="list-item-icon-right {{$icon_right}}"></span>
        @else

            <span class="list-item-icon-right icon-angle-right @if(isset($subtext)) has-subtext @endif"></span>
        @endif
        @if(isset($href))
            <a class="list-item-link" href="{{url($href)}}" @if(isset($target)) target="{{$target}}" @endif  @if(isset($download)) download="{{$download}}" @endif></a> {{-- expands to full width and height of list-item --}}
        @endif
    </div>
</li>


@if(isset($dropdown))
        {{-- The dropdown content --}}
        <div class="uk-dropdown {{isset($dropdown_class) ? $dropdown_class : ''}}">
            @include($dropdown)
        </div>
    </div>
@endif