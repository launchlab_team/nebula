{{-- Example: 
@include('component.section',[
    'heading' => 'Welcome',
    'subheading' => 'Section subheading',
    'include' => 'pages.partials.example',
    'parallax' => 'true',
])
--}}
<section @if(isset($parallax) && $parallax != 'false') {{$parallax}} data-uk-parallax="{bg: '-200'}" @endif class="{{$class or ''}}" id="{{$id or ''}}" @if(isset($background)) style="background-image: url({{$background}})"@endif @if(isset($height)) style="height:{{$height}}" @endif>
    <div class="section-content">
        @if(isset($heading))
            <h2 class="section-heading">{{$heading}}</h2>
        @endif
        @if(isset($subheading))
            <h3 class="section-subheading">{{$subheading}}</h3>
        @endif
        @if(isset($include))
            @include($include)
        @endif
    </div>
</section>