@if(session('message') !== null || session('status') !== null || session('errors') !== null|| session('success') !== null)
    <div id="message" class="animated puffIn @if(session('success') !== null) success @endif @if(session('errors') !== null) error @endif">
        <span class="message-text">
            @if(session('success') !== null)
                {{session('success')}}<br/>
            @endif
            @if(session('message') !== null)
                {{session('message')}}<br/>
            @endif
            {{-- Status is used by boilerplate Laravel authentication --}}
            @if(session('status') !== null)
                {{session('status')}}<br/>
            @endif
            @if(session('errors') !== null)
                @foreach(session('errors')->all() as $error)
                    {{$error}}<br/>
                @endforeach
            @endif
        </span>
        <span class="icon icon-close" data-slideToggle="#message"></span>
    </div>
    {{Session::forget('success')}}
    {{Session::forget('error')}}
    {{Session::forget('errors')}}
    {{Session::forget('message')}}
@endif