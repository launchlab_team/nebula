{{-- Only show on dashboard --}}
@if(basename($_SERVER['REQUEST_URI'], ".php") == 'dashboard')
    <button class="menu-toggle" data-uk-offcanvas="{target:'#menu'}">
        <span class="icon icon-menu-alt"></span>
        Menu
    </button>
@endif