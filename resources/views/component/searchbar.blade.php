<div id="searchbar" class="searchable-list">
    {{-- <span class="icon icon-search searchbar-icon"></span> --}}
    <span class="searchbar-close icon-x" data-close-search></span>
    {{-- <input type="text" class="searchbar-input"> --}}
    <form method="post" action="{{url('search')}}" class="searchable-list-form">
        @include('component.form-item', [
            'type' => 'text',
            'name' => 'search',
            'icon_left' => 'icon-search',
            'icon_right' => 'icon-x uk-hidden cursor-pointer',
            'class' => 'searchable-list-input',
            'id'=>'header_search_input',
        ])
    </form>
    <div class="searchable-list-results searchbar-results"></div>
</div>