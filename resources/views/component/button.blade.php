{{-- 
.button
@params: text, icon, html, attributes, modal
@example:
    @include('component.button',[
        'text' => 'Button Text',
        'icon_left' => 'uk-icon-heart',
        'icon_right' => 'uk-icon-heart',
        'href' => url(),
        'modal' => url(), // shows modal and loads #modal_iframe
        'attributes' => 'data-attribute', 
        'dropdown' => 'dropdown.blade', // includes a dropdown with a view on click
    ])
--}}
@if(isset($dropdown))
    <div class="uk-position-relative uk-display-inline-block" data-uk-dropdown="{mode:'{{isset($dropdown_mode)?$dropdown_mode:'click'}}' @if($dropdown_position !== null),pos:'{{$dropdown_position}}' @endif}" style="display: table-cell">
        {{-- The dropdown toggle is anything inside here --}}
@endif

<button @if(isset($type)) type="{{$type or ''}}" @endif class="{{$class or ''}} @if(isset($icon_right)) has-icon-right @endif @if(isset($icon_left) || isset($icon)) has-icon-left @endif" {!!$attributes or ''!!} {{$id or ''}} @if(isset($modal)) data-modal="{{$modal}}" @endif @if(isset($view)) data-view="{{$view}}" @endif @if(isset($details)) data-details="{{$details}}" @endif>
    {{$text or ''}} {!!$html or ''!!}
    {{-- Icon included by default --}}
    @if(isset($icon))
        <span class="button-icon-left {{$icon}}"></span>
    @endif
    @if(isset($icon_left))
        <span class="button-icon-left {{$icon_left}}"></span>
    @endif
    @if(isset($icon_right))
        <span class="button-icon-right {{$icon_right}}"></span>
    @endif
    @if(isset($href))
        <a href="{{url($href)}}" @if(isset($target)) target="{{$target}}" @endif  @if(isset($download)) download="{{$download}}" @endif></a> {{-- expands to full width and height of button --}}
    @endif
</button>

@if(isset($dropdown))
        {{-- The dropdown content --}}
        <div class="uk-dropdown {{isset($dropdown_class) ? $dropdown_class : ''}}">
            @include($dropdown)
        </div>
    </div>
@endif