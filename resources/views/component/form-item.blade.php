{{-- @include('component.form-item', [
    'label' => 'Item Label',
    'name' => 'name',
    'type' => 'text',
    'value' => 'val',
    'hint' => 'The name of your group or your name if independant',
]) --}}
<div class="form-item {{ $class or '' }} @if($errors !== null) @if($errors->first($name) !== null && $errors->first($name) !== '') uk-animation-shake @endif @endif
@if(isset($type)) {{$type}} @endif @if(isset($icon_left) || $type == 'image' || $type == 'file') has-icon-left @endif  @if(isset($icon_right)) has-icon-right @endif">
    {{-- Error text
    @if($errors !== null)
        @if ($errors->first($name) !== null)
            <span class="error magictime fadeIn">{{$errors->first($name)}}</span>
        @endif
    @endif --}}

    @if(isset($type) && $type == 'text')
        @if (isset($label) && $label !== '')
            <label>{{$label}}</label>
        @endif
        @if (isset($sublabel) && $sublabel !== '')
            <span class="form-item-sublabel">{{$sublabel or ''}}</span>
        @endif
        @if (isset($icon_left))
            <span class="icon form-item-icon form-item-icon-left {{$icon_left}}"></span>
        @endif
        @if(isset($icon_right))
            <span class="icon form-item-icon form-item-icon-right {{$icon_right}}"></span>
        @endif
        <input type="text" id="{{ $id or '' }}"  name="{{ $name or '' }}" value="{{$value or '' }}" @if(isset($placeholder)) placeholder="{{$placeholder}}" @endif {{$attributes or ''}} class="{{ $class or '' }}" />
    @endif

    @if(isset($type) && $type == 'password')
        @if (isset($label) && $label !== '')
            <label>{{$label}}</label>
        @endif
        @if (isset($sublabel) && $sublabel !== '')
            <span class="form-item-sublabel">{{$sublabel or ''}}</span>
        @endif
        @if (isset($icon_left))
            <span class="icon form-item-icon form-item-icon-left {{$icon_left}}"></span>
        @endif
        @if(isset($icon_right))
            <span class="icon form-item-icon form-item-icon-right {{$icon_right}}"></span>
        @endif
        <input type="password" id="{{ $id or '' }}"  name="{{ $name or '' }}" value="{{$value or '' }}" @if(isset($placeholder)) placeholder="{{$placeholder}}" @endif {{$attributes or ''}}/>
    @endif

    @if(isset($type) && $type == 'time')
        @if (isset($label) && $label !== '')
            <label>{{$label}}</label>
        @endif
        @if (isset($sublabel) && $sublabel !== '')
            <span class="form-item-sublabel">{{$sublabel or ''}}</span>
        @endif
        @if (isset($icon_left))
            <span class="icon form-item-icon form-item-icon-left {{$icon_left}}"></span>
        @endif
        @if(isset($icon_right))
            <span class="icon form-item-icon form-item-icon-right {{$icon_right}}"></span>
        @endif
        <input data-time type="text" id="{{ $id or '' }}"  name="{{ $name or '' }}" value="{{$value or '' }}" @if(isset($placeholder)) placeholder="{{$placeholder}}" @endif {{$attributes or ''}}/>
    @endif

    @if(isset($type) && $type == 'phone')
        @if (isset($label) && $label !== '')
            <label>{{$label}}</label>
        @endif
        @if (isset($sublabel) && $sublabel !== '')
            <span class="form-item-sublabel">{{$sublabel or ''}}</span>
        @endif
        @if (isset($icon_left))
            <span class="icon form-item-icon form-item-icon-left {{$icon_left}}"></span>
        @endif
        @if(isset($icon_right))
            <span class="icon form-item-icon form-item-icon-right {{$icon_right}}"></span>
        @endif
        <input data-phone type="text" id="{{ $id or '' }}"  name="{{ $name or '' }}" value="{{$value or '' }}" @if(isset($placeholder)) placeholder="{{$placeholder}}" @endif {{$attributes or ''}}/>
    @endif

    @if(isset($type) && $type == 'date')
        @if (isset($label) && $label !== '')
            <label>{{$label}}</label>
        @endif
        @if (isset($sublabel) && $sublabel !== '')
            <span class="form-item-sublabel">{{$sublabel or ''}}</span>
        @endif
        @if (isset($icon_left))
            <span class="icon form-item-icon form-item-icon-left {{$icon_left}}"></span>
        @endif
        @if(isset($icon_right))
            <span class="icon form-item-icon form-item-icon-right {{$icon_right}}"></span>
        @endif
        <input data-date type="text" id="{{ $id or '' }}"  name="{{ $name or '' }}" value="{{$value or '' }}" @if(isset($placeholder)) placeholder="{{$placeholder}}" @endif {{$attributes or ''}}/>
    @endif

    @if(isset($type) && $type == 'textarea')
        @if (isset($label) && $label !== '')
            <label>{{$label}}</label>
        @endif
        @if (isset($sublabel) && $sublabel !== '')
            <span class="form-item-sublabel">{{$sublabel or ''}}</span>
        @endif
        @if (isset($icon_left))
            <span class="icon form-item-icon form-item-icon-left {{$icon_left}}"></span>
        @endif
        @if(isset($icon_right))
            <span class="icon form-item-icon form-item-icon-right {{$icon_right}}"></span>
        @endif
        <textarea id="{{ $id or '' }}" name="{{ $name or '' }}" value="{{$value or '' }}" @if(isset($placeholder)) placeholder="{{$placeholder}}" @endif {{$attributes or ''}}>{{$value or ''}}</textarea>
    @endif

    @if(isset($type) && $type == 'html')
        @if (isset($label) && $label !== '')
            <label>{{$label}}</label>
        @endif
        @if (isset($sublabel) && $sublabel !== '')
            <span class="form-item-sublabel">{{$sublabel or ''}}</span>
        @endif
        <textarea data-uk-htmleditor id="{{ $id or '' }}" name="{{ $name or '' }}" value="{{$value or '' }}" @if(isset($placeholder)) placeholder="{{$placeholder}}" @endif {{$attributes or ''}}>{{$value or ''}}</textarea>
    @endif

    @if(isset($type) && $type == 'checkbox')
        <label class="checkbox-label @if(isset($value) && $value == 1) checked @endif">
            <input type="checkbox" class="{{ $class or '' }}" name="{{ $name or '' }}" @if(isset($value) && $value == 1) checked @endif/>
            <span class="icon @if(isset($value) && $value == 1) icon-square-check @else icon-square @endif"></span>
            <div class="checkbox-text">{!!$label!!}</div>
        </label>
    @endif

    @if(isset($type) && $type == 'radio')
        <label class="radio-label @if(isset($value) && $value == 1) checked @endif">
            <input type="radio" class="{{ $class or '' }}" id="{{ $id or '' }}" name="{{ $name or '' }}" @if(isset($value) && $value == 1) checked @endif>
            <span class="icon @if(isset($value) && $value == 1) icon-circle-check @else icon-circle @endif"></span>
            <div class="radio-text">{!!$label!!}</div>
        </label>
    @endif

    @if(isset($type) && $type == 'radios')
        @if (isset($label) && $label !== '')
            <label class="form-item-label">{{$label}}</label>
        @endif
        @if (isset($sublabel) && $sublabel !== '')
            <span class="form-item-sublabel">{{$sublabel or ''}}</span>
        @endif
        <div class="radios">
        @foreach (explode(',',$options) as $option)
            <label class="radio-label">
                <input type="radio" value="{{$option}}" class="{{ $class or '' }}" id="{{ $id or '' }}" name="{{ $name or '' }}" @if(isset($value) && $value == 1) checked @endif>
                <span class="icon-circle"></span>
                <div class="radio-text">{{$option}}</div>
            </label>
        @endforeach
        </div>
    @endif

    @if(isset($type) && $type == 'checkboxes')
        @if (isset($label) && $label !== '')
            <label class="form-item-label">{{$label}}</label>
        @endif
        @if (isset($sublabel) && $sublabel !== '')
            <span class="form-item-sublabel">{{$sublabel or ''}}</span>
        @endif
        <div class="checkboxes">
            <input type="hidden" class="{{ $class or '' }}" name="{{ $name or '' }}" value="{{ $value or '' }}"/>
            @foreach (explode(',',$options) as $option)
                <label class="checkbox-label">
                    <input type="checkbox" value="{{$option}}" class="{{ $class or '' }}" id="{{ $id or '' }}" @if(isset($value) && $value == 1) checked @endif>
                    <span class="icon-square"></span>
                    <div class="checkbox-text">{{$option}}</div>
                </label>
            @endforeach
        </div>
    @endif

    @if(isset($type) && $type == 'select')
        @if (isset($label) && $label !== '')
            <label>{{$label}}</label>
        @endif
        @if (isset($sublabel) && $sublabel !== '')
            <span class="form-item-sublabel">{{$sublabel or ''}}</span>
        @endif
        @if (isset($icon_left))
            <span class="icon form-item-icon form-item-icon-left {{$icon_left}}"></span>
        @endif
        @if(isset($icon_right))
            <span class="icon form-item-icon form-item-icon-right {{$icon_right}}"></span>
        @else
            <span class="icon form-item-icon form-item-icon-right icon-select-arrow"></span>
        @endif
        <select id="{{ $id or '' }}"  name="{{ $name or '' }}" value="{{$value or '' }}" @if(isset($placeholder)) @endif >
            @if(isset($placeholder) && $placeholder !== null)
                <option disabled selected>{{$placeholder or ''}}</option>
            @endif
            @foreach (explode(',',$options) as $option) {{-- comma seperated --}}
                <option value="{{create_slug($option)}}" @if(isset($value) && $value == create_slug($option)) selected @endif>{{$option}}</option>
            @endforeach
        </select>
    @endif

    @if(isset($type) && $type == 'file')
        @if (isset($label) && $label !== '')
            <label>{{$label}}</label>
        @endif
        @if (isset($sublabel) && $sublabel !== '')
            <span class="form-item-sublabel">{{$sublabel or ''}}</span>
        @else
            <span class="icon form-item-icon form-item-icon-left uk-icon-paperclip"></span>
        @endif
        @if (isset($icon_left))
            <span class="icon form-item-icon form-item-icon-left {{$icon_left}}"></span>
        @endif
        @if(isset($icon_right))
            <span class="icon form-item-icon form-item-icon-right {{$icon_right}}"></span>
        @endif
        <input accept="doc,docx,pdf,csv,xls,txt,zip" readonly type="text" data-file-input  data-modal="uploader" id="{{ $id or '' }}" value="{{$value or '' }}" @if(isset($placeholder)) placeholder="{{$placeholder}}" @endif {{$attributes or ''}}/>
        <input type="hidden" name="{{ $name or '' }}"/>
    @endif


    @if(isset($type) && $type == 'image')
        @if (isset($label) && $label !== '')
            <label>{{$label}}</label>
        @endif
        @if (isset($sublabel) && $sublabel !== '')
            <span class="form-item-sublabel">{{$sublabel or ''}}</span>
        @else
            <span class="icon form-item-icon form-item-icon-left uk-icon-image"></span>
        @endif
        @if (isset($icon_left))
            <span class="icon form-item-icon form-item-icon-left {{$icon_left}}"></span>
        @endif
        <span class="icon form-item-icon form-item-icon-right icon-x " @if(isset($value) && $value !== '') style="display:block" @endif title="Remove Image" data-remove-image></span>
        <input accept="image/*" readonly onfocus="blur()" type="text" data-image-input data-modal="uploader/images" id="{{ $id or '' }}" @if(isset($value) && $value !== '') value="Uploaded Image" @endif @if(isset($placeholder)) placeholder="{{$placeholder}}" @endif {{$attributes or ''}}/>
        <input type="hidden" name="{{ $name or '' }}" value="{{$value or '' }}"/>
    @endif


    @if(isset($type) && $type == 'icons')
        @if (isset($label) && $label !== '')
            <label>{{$label}}</label>
        @endif
        @if (isset($value))
            <input style="display:none" type="radio" name="icon" value="{{$value}}" checked>
        @endif
        <div class="radios icon-radio-set uk-margin-remove" data-uk-margin>
            <div class="form-item">
                <label class="radio-label">
                    <input type="radio" name="icon" value="icon-circle-layout">
                    <span class="icon icon-circle"></span>
                    <div class="radio-text"><span class="icon icon-circle-layout"></span></div>
                </label>
            </div>
            <div class="form-item">         
                <label class="radio-label">
                    <input type="radio" name="icon" value="icon-circle-menu">
                    <span class="icon icon-circle"></span>
                    <div class="radio-text"><span class="icon icon-circle-menu"></span></div>
                </label>
            </div>
            <div class="form-item">    
                <label class="radio-label">
                    <input type="radio" name="icon" value="icon-circle-user">
                    <span class="icon icon-circle"></span>
                    <div class="radio-text"><span class="icon icon-circle-user"></span></div>
                </label>
            </div>
            <div class="form-item">         
                <label class="radio-label">
                    <input type="radio" name="icon" value="icon-circle-unit">
                    <span class="icon icon-circle"></span>
                    <div class="radio-text"><span class="icon icon-circle-unit"></span></div>
                </label>
            </div>
            <div class="form-item">         
                <label class="radio-label">
                    <input type="radio" name="icon" value="icon-circle-payment">
                    <span class="icon icon-circle"></span>
                    <div class="radio-text"><span class="icon icon-circle-payment"></span></div>
                </label>
            </div>
            <div class="form-item">         
                <label class="radio-label">
                    <input type="radio" name="icon" value="icon-circle-lead">
                    <span class="icon icon-circle"></span>
                    <div class="radio-text"><span class="icon icon-circle-lead"></span></div>
                </label>
            </div>
            <div class="form-item">         
                <label class="radio-label">
                    <input type="radio" name="icon" value="icon-circle-list">
                    <span class="icon icon-circle"></span>
                    <div class="radio-text"><span class="icon icon-circle-list"></span></div>
                </label>
            </div>
            <div class="form-item">         
                <label class="radio-label">
                    <input type="radio" name="icon" value="icon-circle-cloud">
                    <span class="icon icon-circle"></span>
                    <div class="radio-text"><span class="icon icon-circle-cloud"></span></div>
                </label>
            </div>
            <div class="form-item">         
                <label class="radio-label">
                    <input type="radio" name="icon" value="icon-circle-image">
                    <span class="icon icon-circle"></span>
                    <div class="radio-text"><span class="icon icon-circle-image"></span></div>
                </label>
            </div>
            <div class="form-item">         
                <label class="radio-label">
                    <input type="radio" name="icon" value="icon-circle-inbox">
                    <span class="icon icon-circle"></span>
                    <div class="radio-text"><span class="icon icon-circle-inbox"></span></div>
                </label>
            </div>
            <div class="form-item">         
                <label class="radio-label">
                    <input type="radio" name="icon" value="icon-circle-settings">
                    <span class="icon icon-circle"></span>
                    <div class="radio-text"><span class="icon icon-circle-settings"></span></div>
                </label>
            </div>
            <div class="form-item">         
                <label class="radio-label">
                    <input type="radio" name="icon" value="icon-circle-upload">
                    <span class="icon icon-circle"></span>
                    <div class="radio-text"><span class="icon icon-circle-upload"></span></div>
                </label>
            </div>
            <div class="form-item">         
                <label class="radio-label">
                    <input type="radio" name="icon" value="icon-circle-calendar">
                    <span class="icon icon-circle"></span>
                    <div class="radio-text"><span class="icon icon-circle-calendar"></span></div>
                </label>
            </div>
            <div class="form-item">         
                <label class="radio-label">
                    <input type="radio" name="icon" value="icon-circle-form">
                    <span class="icon icon-circle"></span>
                    <div class="radio-text"><span class="icon icon-circle-form"></span></div>
                </label>
            </div>
            <div class="form-item">         
                <label class="radio-label">
                    <input type="radio" name="icon" value="icon-circle-announcement">
                    <span class="icon icon-circle"></span>
                    <div class="radio-text"><span class="icon icon-circle-announcement"></span></div>
                </label>
            </div>
            <div class="form-item">         
                <label class="radio-label">
                    <input type="radio" name="icon" value="icon-circle-link">
                    <span class="icon icon-circle"></span>
                    <div class="radio-text"><span class="icon icon-circle-link"></span></div>
                </label>
            </div>
        </div>
    @endif

    {{-- For form builder: --}}
    @if(isset($draggable) && $draggable == true)
    <div class="icon uk-icon-arrows form-item-icon-draggable"></div>
    @endif
    @if(isset($editable) && $editable == true)
    <div class="icon icon-edit form-item-icon-editable" data-edit-field></div>
    @endif
</div>