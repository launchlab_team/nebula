<div class="add-button-menu">
    <div class="uk-position-relative">
        <div data-uk-dropdown="{mode:'click',pos:'top-left'}" class="uk-display-inline uk-position-relative" id="add-button-dropdown">
            <button class="add-button">
                <span class="icon-plus"></span>
            </button>
            <div class="uk-dropdown">
                @include('component.list-item',[
                    'icon_right' => 'icon-plus',
                    'text' => '1 Column',
                    'attributes' => 'data-add-section-full',
                ])
                @include('component.list-item',[
                    'icon_right' => 'icon-plus',
                    'text' => '2 Columns',
                    'attributes' => 'data-add-section-halves',
                ])
                @include('component.list-item',[
                    'icon_right' => 'icon-plus',
                    'text' => '3 Columns',
                    'attributes' => 'data-add-section-thirds',
                ])
                @include('component.list-item',[
                    'icon_right' => 'icon-plus',
                    'text' => '4 Columns',
                    'attributes' => 'data-add-section-fourths',
                ])
            </div>
        </div>
    </div>
</div>