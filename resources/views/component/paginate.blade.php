@if ($records !== null && method_exists($records, "lastPage")  && $records->lastPage() > 1)
    <ul class="pagination">
        <table width="100%">
            <tr>
                <td>
                    <li class="{{ ($records->currentPage() == 1) ? ' disabled' : '' }} pagination-previous">
                        <a href="{{ $records->url($records->currentPage()-1) }}" >Prev</a>
                    </li>
                </td>
                <td width="100%" class="uk-text-center">
                    @for ($i = 1; $i <= $records->lastPage(); $i++)
                        <li class="{{ ($records->currentPage() == $i) ? ' active' : '' }} pagination-number">
                            <a href="{{ $records->url($i) }}">{{ $i }}</a>
                        </li>
                    @endfor                
                </td>
                <td class="uk-text-right">
                    <li class="{{ ($records->currentPage() == $records->lastPage()) ? ' disabled' : '' }} pagination-next">
                        <a href="{{ $records->url($records->currentPage()+1) }}" >Next</a>
                    </li>   
                </td>
            </tr>
        </table>
    </ul>
@endif