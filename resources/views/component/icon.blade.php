{{-- 
@component: icon
@params: icon_class, icon_text class, attributes
--}}
@if(isset($dropdown))
    <div class="uk-position-relative uk-display-inline-block" data-uk-dropdown="{mode:'{{isset($dropdown_mode)?$dropdown_mode:'click'}}' @if($dropdown_position !== null),pos:'{{$dropdown_position}}' @endif}" style="display: table-cell;{{$dropdown_style or ''}}">
        {{-- The dropdown toggle is anything inside here --}}
@endif

<div class="icon-wrapper {{$wrapper_class or ''}}">
    <div class="icon {{$icon_class}} {{$class or ''}}" {!!$attributes or ''!!}></div>
    @if(isset($text) && $text !== null)
        <span class="icon-text">{{$text}}</span>
    @endif
    @if(isset($href))
        <a href="{{url($href)}}"></a> {{-- expands to full width and height of button --}}
    @endif
</div>

@if(isset($dropdown))
        {{-- The dropdown content --}}
        <div class="uk-dropdown {{isset($dropdown_class) ? $dropdown_class : ''}}">
            @include($dropdown)
        </div>
    </div>
@endif