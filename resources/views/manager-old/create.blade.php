@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Add Managers</div>
        @include('component.searchable-list',[
            'action' => 'manager/search/add',
        ])
        <form method="post" action="{{url('organization/manager/')}}">
            {{csrf_field()}} 
            {{-- Searchable list here --}}
{{--             @include('component.form-item',[
                'name' => 'name',
                'type' => 'text',
                'label' => 'Organization Name',
            ])
            @include('component.form-item',[
                'name' => 'subdomain',
                'type' => 'text',
                'label' => 'Subdomain',
                'sublabel' => 'Subdomain.'. $_ENV['APP_DOMAIN'],
            ])
            @include('component.form-item',[
                'name' => 'logo',
                'type' => 'image',
                'label' => 'Logo',
                'placeholder' => 'Choose file',
            ]) --}}
            {{-- <button class="modal-button">Continue</button> --}}
        </form>

    </div>
@endsection