@if(isset($users) && $users !== null)
    @foreach($users as $user)
        <form method="post" action="{{url('manager/'.$user->id.'/add')}}">
            {{csrf_field()}}
            <input type="hidden" name="user_id" value="{{$user->id}}" /> 
            @include('component.list-item',[
                'text' => $user->name,
                'icon_right' => 'icon-plus',
                'class' => 'cursor-pointer',
                'attributes' => 'data-add-manager="'.$user->id.'"',
            ])
        </form>
    @endforeach
@else
    <div class="message-box">
        No users found under that term
    </div>
@endif