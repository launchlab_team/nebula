@if(isset($organization->managers) && $organization->managers !== null)
    @foreach($organization->managers as $manager)
        @include('component.list-item',[
            'text' => $manager->user->name,
        ])
    @endforeach
@else
    <div class="message-box">
        No managers found
    </div>
@endif