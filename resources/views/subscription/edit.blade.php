@extends('layouts.partial')

@section('main')
    <div class="modal">
        <h1 class="modal-heading">Update Subscription <span class="icon icon-credit-card"></h1>
        <form method="post" action="{{url('subscription/update')}}"  id="payment_form">
            {{csrf_field()}} 
            <table width="100%" cellpadding="3px">
                <tr>
                    <td colspan="3" class="form-item">
                        <input name="cardholder" type="text" @if(App::environment('local')) value="Cardholder Name" @endif placeholder="Cardholder Name">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="form-item">
                        <input name="card_number" @if(App::environment('local')) value="4242 4242 4242 4242" @endif placeholder="Card Number" type="text" size="20" data-stripe="number">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="uk-text-small form-item"><label>Expiration</label></td>
                    <td class="uk-text-small form-item"><label>Security code</label></td>
                </tr>
                <tr>
                    <td><input size="2" data-stripe="exp_month" type="text" placeholder="MM" @if(App::environment('local')) value="10" @endif></td>
                    <td>
                      <input size="2" data-stripe="exp_year" type="text" placeholder="YY" @if(App::environment('local')) value="19" @endif>
                    </td>
                    <td><input size="2" data-stripe="cvc" type="text" placeholder="•••" @if(App::environment('local')) value="123" @endif></td>
                </tr>
            </table>
            <button class="modal-button">Update Payment Method</button>
        </form>
    </div>

@endsection

@section('scripts')
    @parent
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        $(function() {
            Stripe.setPublishableKey("{{env('STRIPE_KEY')}}");
        });
        $(document).on('submit','#payment_form',function(e) {
            var $form = $('#payment_form');
            {{-- Disable the submit button to prevent repeated clicks: --}}
            $form.find('.modal-button').prop('disabled', true);
            {{-- Request a token from Stripe: --}}
            Stripe.card.createToken($form, stripeResponseHandler);
            {{-- Prevent the form from being submitted: --}}
            return false;
        });
        function stripeResponseHandler(status, response) {
          {{-- Grab the form: --}}
          var $form = $('#payment_form');
          if (response.error) { {{-- Problem! --}}
            {{-- Show the errors on the form: --}}
            alert(response.error.message);
            $('.modal-button').removeAttr("disabled"); {{-- Re-enable submission --}}
          } else { {{-- Token was created! --}}
            {{-- Get the token ID: --}}
            var token = response.id;
            {{-- Insert the token ID into the form so it gets submitted to the server: --}}
            $form.append($('<input type="hidden" name="stripeToken">').val(token));
            {{-- Submit the form: --}}
            $form.get(0).submit();
          }
        };
    </script>
@overwrite
