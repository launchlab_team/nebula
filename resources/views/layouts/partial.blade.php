@extends('layouts.default')

@section('links')
    @parent
    <style type="text/css">
        body{
            padding-bottom: 0 !important;
        }
        #message {
            position: absolute;
        }
    </style>
@overwrite

@section('header')
@overwrite

@section('modal')
@overwrite

@section('alert')
@overwrite

@section('subheader')
@overwrite

@section('content')
    <div id="main" class="uk-animation-fade">
        <div class="container">
            @include('component.loader')
            @section('main')
                <div class="vertical-container">
                    <div id="middle">
                        @yield('middle')
                    </div>
                </div>
            @show
        </div>
    </div>
@overwrite

@section('footer')
    <script type="text/javascript">
        // Redirect the page to home if shown as a page that is not inside a frame like
        // if (window.top.document == document) window.location.href = '/';
    </script>
@overwrite