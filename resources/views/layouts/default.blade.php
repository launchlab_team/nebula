<!DOCTYPE html>
<html>
    <head>
        @section('head')
            @section('meta')
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
                <meta http-equiv="cleartype" content="on">
                <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1, maximum-scale=1">
                <meta name="_token" content="{{ csrf_token() }}">
            @show
            <title>
                @if(isset($settings) && isset($settings->organization_name))
                    @yield('title',$settings->organization_name.' Suite')
                @else
                    @yield('title','Nebula Suite')
                @endif
            </title>
            @section('links')
                {{-- Sass files included in resources/assets/sass/app.scss are compiled to css/main.css --}}
                {{ HTML::style('css/main.css?v=1.35') }}
                {{-- <link rel="stylesheet" href="{{ elixir('css/main.css') }}"> --}}
                <link rel="icon" href="{{url('favicon.ico')}}" type="image/x-icon" />
                {{-- <link href="https://cdn.myfontastic.com/qighTNcgxC7SqNN2i76AE5/icons.css" rel="stylesheet"> --}}
            @show
        @show
    </head>

    <body>
        {{-- The message component is used to display notices via session('message'), session('errors'),  session('success'), session('status') in the session --}}
        @section('message')
            @include('component.message')
        @show

        @section('header')
            @include('layout.header')
        @show

        @section('subheader')
            {{-- The menu toggle component shows on mobile and toggles the offcanvas menu --}}
            @include('component.menu-toggle')
        @show
    

        @section('menu')
            {{-- menu is an uikit offcanvas component --}}
            @include('layout.menu')
        @show

        @section('content')
            <div id="main" class="uk-animation-fade">
                {{-- Content is loaded into the #dynamic_view via data-view="url" --}}
                <iframe id="dynamic_view" width="100%"></iframe>
                {{-- Content is loaded into the #details_view via data-detail-view="url" --}}
                <iframe id="details_view" width="100%" height="100%" class="animated slideInUp"></iframe>
                @include('component.loader')
                @section('main')
                    <div class="container">
                        @section('container')
                            <div class="vertical-container">
                                <div id="middle">
                                    @yield('middle')
                                </div>
                            </div>
                        @show
                    </div>
                @show
            </div>
        @show

        {{-- Modal is loaded with data-modal="url" --}}
        @section('modal')
            @include('layout.modal')
        @show

        {{-- Alerts are toggled via showAlert('message') --}}
        @include('component.alert')

        @section('footer')
            @include('layout.footer')
        @show

        @section('scripts')
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
            {{-- Javascript compiled from resources/assets/js/vendor --}}
            <script src="{{ URL::asset('js/vendor.js?v=1.35') }}"></script>
            {{-- Javascript compiled from resources/assets/js --}}
            <script src="{{ URL::asset('js/app.js?v=1.35') }}"></script>
        @show
    </body>
</html>