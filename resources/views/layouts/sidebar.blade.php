@extends('layouts.default')

@section('content')
    <div class="uk-grid uk-grid-collapse" style="max-width:100%" data-uk-grid-match>
        <aside id="sidebar" class="uk-width-medium-1-4 uk-width-large-1-5 uk-width-xlarge-1-6 uk-position-relative {{$settings->sidebar or ''}}"> 
            @section('sidebar')
                @include('layout.sidebar')
            @show
        </aside>
        <div class="uk-width-medium-3-4 uk-width-large-4-5 uk-width-xlarge-5-6 uk-position-relative">
            <div id="main" class="uk-animation-fade">
                {{-- Content is loaded into the #dynamic_view element via data-view="url" --}}
                <iframe id="dynamic_view" width="100%"></iframe>
                {{-- Content is loaded into the #detail_view element via data-detail-view="url" --}}
                <iframe id="details_view" width="100%" height="100%" class="animated slideInUp"></iframe>
                @include('component.loader')
                <div class="container">
                    @section('main')
                        <div class="vertical-container">
                            <div id="middle">
                                @yield('middle')
                            </div>
                        </div>
                    @show
                </div>
            </div>
        </div>
    </div>
@overwrite