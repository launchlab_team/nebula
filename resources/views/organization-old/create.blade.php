@extends('layouts.default')

@section('middle')
    <div class="box">
        <div class="box-heading">Create an Organization</div>
        <form method="post" action="{{url('organization/store')}}">
            {{csrf_field()}} 
            @include('component.form-item',[
                'name' => 'name',
                'type' => 'text',
                'label' => 'Organization Name',
            ])
            @include('component.form-item',[
                'name' => 'subdomain',
                'type' => 'text',
                'label' => 'Subdomain',
                'sublabel' => 'Subdomain.'. $_ENV['APP_DOMAIN'],
            ])
            @include('component.form-item',[
                'name' => 'logo',
                'type' => 'image',
                'label' => 'Logo',
                'placeholder' => 'Choose file',
            ])
            <button class="box-button">Continue</button>
        </form>
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">    
        $(document).on('keyup', '[name=name]', function() {
            var org_name = $(this).val();
            // Generate url ready slug
            var org_slug = org_name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]+/g, "-").toLowerCase();
            $('[name=subdomain]').val(org_slug);
            $('[name=subdomain]').parent().find('.form-item-sublabel').html(org_slug+'.{{ $_ENV['APP_DOMAIN'] }}');
        });
        $(document).on('keyup', '[name=subdomain]', function() {
            var org_name = $(this).val();
            // Generate url ready slug
            var org_slug = org_name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]+/g, "-").toLowerCase();
            $('[name=subdomain]').val(org_slug);
            $(this).parent().find('.form-item-sublabel').html(org_slug+'.{{ $_ENV['APP_DOMAIN'] }}');
        });
    </script>
@endsection