@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        Organization Settings
        <span class="icon icon-settings"></span>
    </h1> 

    <h2 class="block-heading">{{$organization->name}}</h2>
    <div class="block">
        <form method="post" action="{{url('organization/'.$organization->id.'/update')}}">
            {{csrf_field()}} 
            @include('component.form-item',[
                'name' => 'name',
                'type' => 'text',
                'label' => 'Organization Name',
                'value' => $organization->name,
            ])
            @include('component.form-item',[
                'name' => 'subdomain',
                'type' => 'text',
                'label' => 'Subdomain',
                'sublabel' => 'Subdomain.'.str_replace('www.','', $_SERVER['SERVER_NAME']),
                'value' => $organization->subdomain,
            ])
            @include('component.form-item',[
                'name' => 'logo',
                'type' => 'image',
                'label' => 'Logo',
                'placeholder' => 'Update logo',
            ])
            <button>Update Organization</button>
        </form>
    </div>

    <h2 class="block-heading">Managers ({{count($organization->managers)}}) <span data-modal="manager/create" title="Add Manager" class="icon cursor-pointer icon-plus"></h2>
    <div class="block">
        @include('component.searchable-list',[
            'action' => 'manager/search',
            'preload' => 'organization.manager.list',
            'placeholder' => 'Search Managers'
        ])
    </div>

    <h2 class="block-heading">Members ({{count($organization->members)}}) <span data-modal="member/create" title="Add Member" class="icon cursor-pointer icon-plus"></h2>
    <div class="block">
        @include('component.searchable-list',[
            'action' => 'member/search',
            'preload' => 'user.list',
            'placeholder' => 'Search Users'
        ])
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">    
        $(document).on('keyup', '[name=name]', function() {
            var org_name = $(this).val();
            // Generate url ready slug
            var org_slug = org_name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]+/g, "-").toLowerCase();
            $('[name=subdomain]').val(org_slug);
            $('[name=subdomain]').parent().find('.form-item-sublabel').html(org_slug+'.{{ getenv('APP_DOMAIN') }}');
        });
        $(document).on('keyup', '[name=subdomain]', function() {
            var org_name = $(this).val();
            // Generate url ready slug
            var org_slug = org_name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]+/g, "-").toLowerCase();
            $('[name=subdomain]').val(org_slug);
            $(this).parent().find('.form-item-sublabel').html(org_slug+'.{{ getenv('APP_DOMAIN') }}');
        });
    </script>
@endsection