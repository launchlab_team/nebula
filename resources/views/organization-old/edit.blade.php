@extends('layouts.sidebar')

@section('main')
    <div class="box">
        <h2 class="box-heading">{{$organization->name}} Settings</h2>
        <form method="post" action="{{url('organization/'.$organization->id.'/edit')}}">
            {{csrf_field()}} 
            @include('component.form-item',[
                'name' => 'name',
                'type' => 'text',
                'label' => 'Organization Name',
                'value' => $organization->name,
            ])
            <button>Update Organization</button>
        </form>
    </div>
@endsection

