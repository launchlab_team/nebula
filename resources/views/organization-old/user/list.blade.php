@if(isset($users) && $users !== null)
    @foreach($users as $user)
        {{$user}}
    @endforeach
@else
    <div class="message-box">
        No users found
    </div>
@endif