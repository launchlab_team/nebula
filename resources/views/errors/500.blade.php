@extends('layouts.partial')

@section('main')
<div class="message-box uk-text-left" style="color:#ad3a3a;">
    <b>Whoops, it looks like something went wrong.</b><br>
    {{-- Click <a href="mailto:mailto:team@nebulasuite.com?Subject=Issue%20Encountered%20on%20Nebula">here</a> to report an issue or <a href="{{url('/')}}">here</a> to return home. --}}
    @if(env('APP_DEBUG'))
        {{($error)}}
    @else
        Please try again. To report an issue or bug, send an email to team@nebulasuite.com along with a brief description of how you encountered the issue.
    @endif
</div>
@endsection

@section('scripts')
@overwrite
