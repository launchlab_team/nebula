@extends('layouts.partial')

@section('middle')
    <h1 class="uk-text-center uk-margin-remove">404</h1>
    <a target="_top" href="{{url('')}}" class="uk-text-center uk-display-block">Return Home</a>
@endsection