@extends('layouts.partial')

@section('main')
    <div class="modal">
        <form method="post" action="{{url("crm/preferences")}}" data-ajax-form>
            <h2 class="modal-heading">CRM Preferences</h2>
                {{csrf_field()}}
                <?php 
                $columns = NULL;
                $pipeline = NULL;
                ?>
                @if(count(json_decode($crm->columns)) > 0)
                    <?php $i = 0; ?>
                    @foreach(json_decode($crm->columns) as $column)
                        <?php
                        $i++;
                        $columns .= $column; 
                        if ($i !== count(json_decode($crm->columns))) $columns .= ', '; 
                        ?>
                    @endforeach
                @endif
                @if(count(json_decode($crm->pipeline)) > 0)
                    <?php $i = 0; ?>
                    @foreach(json_decode($crm->pipeline) as $pipeline_stage)
                        <?php 
                        $i++;
                        $pipeline .= $pipeline_stage; 
                        if ($i !== count(json_decode($crm->pipeline))) $pipeline .= ', '; 
                        ?>
                    @endforeach
                @endif
                @include('component.form-item',[
                    'name' => 'columns',
                    'type' => 'textarea',
                    'label' => 'Columns',
                    'sublabel' => 'comma seperated',
                    'placeholder' => 'Name, Email, Address',
                    'value' => isset($columns) ? $columns : NULL,
                ])
                @include('component.form-item',[
                    'name' => 'pipeline',
                    'type' => 'textarea',
                    'label' => 'Sales Pipeline',
                    'sublabel' => 'comma seperated',
                    'placeholder' => 'Leads, Opportunities, Customers',
                    'value' => isset($pipeline) ? $pipeline : NULL,
                ])
                @if(count(json_decode($crm->columns)) > 0)
                    <hr class="uk-margin-remove" />
                    <div class="form-item">
                        <label>List item text column:</label>
                        <select name="label_key">
                            @if(count(json_decode($crm->columns)) > 0)
                            @foreach(json_decode($crm->columns) as $key => $column)
                                <option @if($crm->label_key !== null && $crm->label_key == $key) selected @endif value="{{$key}}">{{$column}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-item">
                        <label>List item subtext column (optional):</label>
                        <select name="sublabel_key">
                            <option value="">(optional)</option>             
                            @if(count(json_decode($crm->columns)) > 0)
                            @foreach(json_decode($crm->columns) as $key => $column)
                                <option @if($crm->sublabel_key !== null && $crm->sublabel_key == $key) selected @endif value="{{$key}}">{{$column}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                @endif
            <button class="modal-button">Save Preferences</button>
        </form>
    </div>
@endsection