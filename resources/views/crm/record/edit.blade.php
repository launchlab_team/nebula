@extends('layouts.partial')

@section('middle')
    <form method="post" action="{{url("crm/record/$record->id/update")}}" data-ajax-form>
        {{csrf_field()}}
        <div class="detail-view">
            <span class="detail-view-icon icon {{$module->icon}}"></span>
            <h1 class="detail-view-heading" style="margin-top:0px">{{json_decode($crm->pipeline)[$record->pipeline_key]}}</h1>

            <div class="detail-view-menu">
                <button type="button" title="Delete Record" data-delete="{{url('crm/record/'.$record->id.'/delete')}}"><span class="icon uk-icon-trash-o"></span></button>
                <button title="Save Changes"><span class="icon uk-icon-save"></span></button>
                <span onClick="hideDetails()" class="icon icon-x"></span>
            </div>

            <div class="detail-view-content">
                <div class="form-item">
                    <label>Pipeline Stage</label>
                    <span class="icon form-item-icon form-item-icon-right icon-select-arrow"></span>
                    <select name="pipeline_key">
                        @if(count(json_decode($crm->pipeline)) > 0)
                        @foreach(json_decode($crm->pipeline) as $key => $pipeline)
                            <option @if($record->pipeline_key !== null && $record->pipeline_key == $key) selected @endif value="{{$key}}">{{$pipeline}}</option>
                        @endforeach
                        @endif
                        <option value="-1">No-go</option>
                    </select>
                </div>
                <hr style="margin:5px">
                @if(count(json_decode($crm->columns)) > 0)
                    @foreach(json_decode($crm->columns) as $key => $column)
                        {{-- @if(array_key_exists($key, json_decode($record->data))) --}}
                        @include('component.form-item',[
                            'name' => 'data[]',
                            'type' => strlen($column) > 100 ? 'textarea' : 'text',
                            'label' => $column,
                            'value' => array_key_exists($key, json_decode($record->data)) ? json_decode($record->data)[$key] : '',
                        ])
                        {{-- @endif --}}
                    @endforeach
                @endif
            </div>
        </div>
    </form>
@endsection