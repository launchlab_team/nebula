@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">New {{json_decode($crm->pipeline)[$pipeline_key]}} Record <span class="icon icon-plus"></span></div>
        <form method="post" action="{{url('crm/record/'.$pipeline_key.'/create')}}" data-ajax-form>
            {{csrf_field()}} 
            @if(isset($crm->columns) && $crm->columns !== '')
                @if(count(json_decode($crm->columns)) > 0)
                    @foreach(json_decode($crm->columns) as $label)
                        @include('component.form-item',[
                            'name' => 'data[]',
                            'label' => $label,
                            'type' => strlen($label) > 100 ? 'textarea' : 'text',
                        ])
                    @endforeach
                @endif
            @else
                No CRM columns have been defined yet. Head over to <a href="{{url('crm/preferences')}}"><u>preferences</u></a> to customize the columns for your CRM records or  <a href="{{url('crm/'.$pipeline_key.'/import')}}"><u>import</u></a> an existing spreadsheet. 
            @endif
            <button class="modal-button">Add to {{json_decode($crm->pipeline)[$pipeline_key]}}</button>
        </form>
    </div>
@endsection