@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Import Records (csv) <span class="icon icon-upload"></div>
        <div class="form-item">
            <label>Pipeline Stage:</label>
            <span class="icon form-item-icon form-item-icon-right icon-select-arrow"></span>
            <select name="pipeline_key">
                @if(count(json_decode($crm->pipeline)) > 0)
                    @foreach(json_decode($crm->pipeline) as $key => $pipeline_stage)
                        {{-- {{$pipeline->id}} --}} 
                        <option @if($pipeline_key !== null) @if($key == $pipeline_key) selected @endif @endif value="{{$key}}">{{$pipeline_stage}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="message-box">The first row of the file will be ignored as column titles.</div>
        <form method="post" id="upload_form" enctype="multipart/form-data" action="{{url('crm/'.$pipeline_key.'/import')}}" style="margin-top: 8px">
            {{csrf_field()}}
            <input type="file" name="file" id="upload_input" accept=".csv, .xls, .xlsx" />
            <input type="hidden" name="pipeline_key" value="{{$pipeline_key}}" />
            <span class="icon icon-upload"></span>
        </form>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        $(document).on('change','select[name=pipeline_key]',function() {
            $('input[name=pipeline_key]').val($('select[name=pipeline_key]').val());
        });
    </script>
@endsection
