@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        {{$module->label}} 
        <span data-modal="crm/record/{{$current_pipeline_key}}/create" class="icon icon-plus cursor-pointer"></span>
        <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span class="icon icon-menu cursor-pointer"></span>
            <div class="uk-dropdown" style="margin-top: 10px">
                <ul >
                    @if($current_pipeline_key !== 'no-go')
                        @include('component.list-item',[
                            'text'=>'Export '.json_decode($crm->pipeline)[$current_pipeline_key].' (.csv)',
                            'href' => "crm/".$current_pipeline_key."/export",
                            'attributes' => "download",

                        ])
                        @include('component.list-item',[
                            'text'=>'Import Spreadsheet/csv',
                            'modal' => "crm/".$current_pipeline_key."/import",
                        ])
                    @endif
                    @include('component.list-item',[
                        'text'=>'CRM Preferences',
                        'modal' => "crm/preferences",
                    ])
                    @if($current_pipeline_key !== 'no-go' && (App\CrmRecord::where('pipeline_key',$current_pipeline_key)->count() > 0))
                        @include('component.list-item',[
                            'text'=>'Clear '.App\CrmRecord::where('pipeline_key',$current_pipeline_key)->count().' '.json_decode($crm->pipeline)[$current_pipeline_key],
                            'attributes' => 'data-delete="'.url('crm/'.$current_pipeline_key.'/clear') .'" data-confirm="Are you sure you want to delete '.App\CrmRecord::where('pipeline_key',$current_pipeline_key)->count().' records? This cannot be undone."',
                        ])
                    @endif
                </ul>
            </div>
        </div>
    </h1>
 
    <div class="horizontal-tabs">
        <?php $i = 0; ?>
        @foreach(json_decode($crm->pipeline) as $key => $pipeline_stage)
            @if($i == 0)
                <a data-view="crm/{{$key}}"><div class="tab @if((basename($_SERVER['REQUEST_URI'], ".php") == $key || basename($_SERVER['REQUEST_URI'], ".php") == 'crm') && basename($_SERVER['REQUEST_URI'], ".php") !== 'no-go') selected @endif">{{$pipeline_stage}} ({{App\CrmRecord::where('pipeline_key',$key)->count()}})</div></a>
            @else
                <a data-view="crm/{{$key}}"><div class="tab @if(basename($_SERVER['REQUEST_URI'], ".php") ==  $key) selected @endif">{{$pipeline_stage}} ({{App\CrmRecord::where('pipeline_key',$key)->count()}})</div></a>
            @endif
            <?php $i++; ?>
            @if($i !== count(explode(',', $crm->pipeline)))
            <span class="icon icon-angle-right uk-display-inline" style="display: inline-block !important;"></span>
            @endif
        @endforeach
        <a href="{{url('crm/'.'no-go')}}" class="uk-display-inline right"><div class="tab @if(basename($_SERVER['REQUEST_URI'], ".php") == 'no-go') selected @endif">No-go <span class="icon uk-icon-trash-o"></span></div></a>
    </div> 

    @if($records !== null && count($records) > 0)
        @foreach($records as $record)
            @if(json_decode($record->data) > 0)
                @include('component.list-item',[
                    'text' => array_key_exists($crm->label_key, json_decode($record->data)) ? json_decode($record->data)[$crm->label_key] : '',
                    'subtext' => array_key_exists($crm->sublabel_key, json_decode($record->data)) ? json_decode($record->data)[$crm->sublabel_key] : NULL,
                    'details'=>'crm/records/'.$record->id.'/edit',
                    'class'=>'has-circle-icon',
                    'icon_left'=>$module->icon,
                ])
            @endif
        @endforeach
    @else
        @if($current_pipeline_key !== 'no-go')
            <div class="message-center">No {{trim(strtolower( json_decode($crm->pipeline)[$current_pipeline_key] ))}} to display. Click <span class="icon icon-plus"></span> to get started.</div>
        @else
            <div class="message-center">No-go's are records that have been qualified out of the sales pipeline.</div>
        @endif
    @endif
    @include('component.paginate',['records'=>$records])
@endsection