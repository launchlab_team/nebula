<ul>
    @if(Auth::user() && Auth::user()->is_manager && basename($_SERVER['REQUEST_URI'], ".php") != 'dashboard')
    @else {{-- if the user is a manager and is on dashboard page show customize option --}}
        @if(Auth::user() !== null && Auth::user()->is_manager)
            @include('component.list-item',[
                'text'=>'Customize',
                'attributes'=>'data-toggle-settings',
                'dropdown' => null // Set dropdown to null if it's set in the parent view 
            ])
        @endif
    @endif

    {{-- If not on the dashboard page then show the menu items that were set via the the pages module --}}
    @if(basename($_SERVER['REQUEST_URI'], ".php") != 'dashboard')
        @if(isset($menu_items) && $menu_items !== null)
            <div class="uk-visible-small">{{--  only visble on small screens --}}
                @foreach($menu_items as $menu_item)
                    @if($menu_item->label !== '')
                        @include('component.list-item',[
                            'text'=>$menu_item->label,
                            'href'=>url($menu_item->href),
                            'dropdown' => null // Set dropdown to null if it's set in the parent view 
                        ])
                    @endif
                @endforeach
            </div>
        @endif
    @endif



    {{-- @if(Auth::user() && Auth::user()->is_manager && basename($_SERVER['REQUEST_URI'], ".php") != 'dashboard')
        @include('component.list-item',[
            'text' => 'Dashboard',
            //'icon_left' => 'icon-settings',
            'href' => url('dashboard'),
            'dropdown' => null, // Set dropdown to null if it's set in the parent view 
        ])
    @endif

    @if(Auth::user() && Auth::user()->is_manager)
        @include('component.list-item',[
            'text'=>'Edit Menu',
            'modal'=>'menu/edit',
            'dropdown' => null // Set dropdown to null if it's set in the parent view 
        ])
    @endif --}}

    @if(Auth::user())
        @include('component.list-item',[
            'text' => 'Account',
            //'icon_left' => 'icon-user',
            'href' => url('account'),
            'dropdown' => null // Set dropdown to null if it's set in the parent view 
        ])
        @include('component.list-item',[
            'text'=>'Logout',
            'href'=>'logout',
            'dropdown' => null // Set dropdown to null if it's set in the parent view 
        ])
    @else
        @include('component.list-item',[
            'text' => 'Login',
            //'icon_left' => 'icon-user',
            'href' => url('login'),
            'dropdown' => null // Set dropdown to null if it's set in the parent view 
        ])
    @endif

</ul>