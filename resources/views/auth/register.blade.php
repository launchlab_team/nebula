@extends('layouts.default')

@section('middle')
    {{-- <div class="container"> --}}
        <div class="box">
            <h1 class="box-heading">Register {{-- <span class="icon-user right"></span> --}}</h1>     
            <form role="form" method="POST" action="{{ url('register') }}">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="POST">
                @include('component.form-item',[
                    'name' => 'name',
                    'type' => 'text',
                    'placeholder' => 'Full Name',
                ])
                @include('component.form-item',[
                    'name' => 'email',
                    'type' => 'text',
                    'placeholder' => 'Email',
                ])
                @include('component.form-item',[
                    'name' => 'password',
                    'type' => 'password',
                    'placeholder' => 'Password',
                ])
                @include('component.form-item',[
                    'name' => 'password_confirmation',
                    'type' => 'password',
                    'placeholder' => 'Confirm Password',
                ])
                <table width="100%" cellspacing="0" cellpadding="0" class="uk-text-small">
                    <tr>
                        <td class="uk-text-right"><a href="{{ url('/login') }}">Login</a></td>
                    </tr>
                </table>
                <button class="box-button">Create Account</button>
            </form>
        </div>
    {{-- </div> --}}
@endsection