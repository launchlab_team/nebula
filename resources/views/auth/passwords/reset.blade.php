@extends('layouts.default')

@section('middle')
    <div class="box">
        <h1 class="box-heading">Reset Password <span class="icon-lock right"></span></h1>     
        <form method="POST" action="{{ url('/password/reset') }}">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="POST"/>
            <input type="hidden" name="token" value="{{ $token }}"/>
            @include('component.form-item',[
                'name' => 'email',
                'type' => 'text',
                'value' => $email,
                'placeholder' => 'Email',
            ])
            @include('component.form-item',[
                'name' => 'password',
                'type' => 'password',
                'placeholder' => 'New Password',
            ])
            @include('component.form-item',[
                'name' => 'password_confirmation',
                'type' => 'password',
                'placeholder' => 'Confirm Password',
            ])
            <button class="box-button">Reset Password</button>
        </form>
    </div>
@endsection