@extends('layouts.partial')

@section('middle')
    <div class="box">
        <h1 class="box-heading">Reset Password <span class="icon-lock right"></span></h1>     
        <form method="POST" action="{{ url('/password/email') }}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="POST">
            @include('component.form-item',[
                'name' => 'email',
                'type' => 'text',
                'placeholder' => 'Email',
                'value' => old('email'),
            ])
            <button class="box-button">Send Password Reset Link</button>
        </form>
    </div>
@endsection