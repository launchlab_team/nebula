@extends('layouts.partial')

@section('middle')
    <div class="box @if(App\Setting::first() !== NULL && App\Setting::first()->logo !== NULL && App\Setting::first()->logo !== '') logo-box @endif">
        <h1 class="box-heading">
            @if(App\Setting::first() !== NULL && App\Setting::first()->logo !== NULL && App\Setting::first()->logo !== '')
                <img style="margin:auto;display:block" src="{{App\Setting::first()->logo}}">
            @else
                Login
            @endif
        </h1>
        <form method="POST" action="{{ url('login') }}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="POST">
            @include('component.form-item',[
                'name' => 'email',
                'type' => 'text',
                'placeholder' => 'Email',
                'value' => old('email'),
            ])
            @include('component.form-item',[
                'name' => 'password',
                'type' => 'password',
                'placeholder' => 'Password',
            ])
            <table width="100%" cellspacing="0" cellpadding="0" class="uk-text-small" style="margin-top:3px;">
                <tr>
                    <td><a style="color: #99a3ab" href="{{ url('/password/reset') }}">Forgot?</a></td>
                    <td class="uk-text-right"><a style="color: #99a3ab" href="{{ url('/register') }}">Register</a></td>
                </tr>
            </table>
            <button class="box-button">Sign In</button>
        </form>
    </div>
@endsection