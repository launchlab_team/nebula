{{-- Shows reservations and reservables for a given reservation schedule id --}}
@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        <a href="{{url('reservations')}}"><span class="icon icon-circle-list" style="float:none !important;padding-left:0px;font-size:1.2em"></span></a> 
        <span class="icon-angle-right" style="font-size:14px"></span> {{ucfirst($schedule->name)}}
        <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span class="icon icon-plus cursor-pointer"></span>
            <div class="uk-dropdown" style="width:270px">
                <ul>
                    @include('component.list-item',[
                        'text'=>'New Reservation',
                        'icon_left'=>'uk-icon-calendar',
                        'modal' => "reservations/schedule/".$schedule->id."/reservation/create?date=".$date."&reservable_id=".$reservable_id,
                    ])
                    @include('component.list-item',[
                        'text'=>'New Reservable',
                        'icon_left'=>'uk-icon-list',
                        'modal' => "reservations/schedule/".$schedule->id."/reservable/create?date=".$date,
                    ])
                </ul>
            </div>
        </div>
        <span title="{{ucfirst($schedule->name)}} Preferences" class="icon cursor-pointer icon-settings" data-modal="reservations/schedule/{{$schedule->id}}/edit"></span>
    </h1>

    <div class="horizontal-tabs">
        <a href=""><div class="tab selected">Schedule</div></a>
        <a href="{{url('reservations/schedule/'.$schedule->id.'/reservations')}}"><div class="tab">Reservations</div></a>
        <a href="{{url('reservations/schedule/'.$schedule->id.'/reservables')}}"><div class="tab">Reservables</div></a>
    </div> 

    @if($schedule->public)
    @include('component.list-item',[
        'text'=> 'Public Link',
        'subtext'=>url('schedule/'.$schedule->slug),
        'href'=>'schedule/'.$schedule->slug,
        'target'=>'_blank',
        'class'=>'has-circle-icon',
        'icon_left'=>'icon-circle-link',
        'style'=>'margin-bottom:18px;',
    ])
    @endif

    <div class="uk-grid">
        <div class="uk-width-medium-1-3">
            <div id="datepicker" class="animated vanishIn"></div>
            <div class="form-item text">
                <input type="text" name="date" id="date" style="text-align:center">
            </div>
            <div class="form-item select" style="margin-bottom: 10px">
                <span class="icon icon-select-arrow form-item-icon-right"></span>
                    {{-- {{dd($schedule->reservables)}} --}}
                <select name="reservable_id">
                    <option>All</option>
                    @if(isset($schedule->reservables) && $schedule->reservables !== null)
                        @foreach($schedule->reservables as $reservable)
                            <option @if($reservable->id == $reservable_id) selected @endif value="{{$reservable->id}}">{{$reservable->name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="uk-width-medium-2-3">
            <div class="schedule-day">
                <div class="schedule-day-header">
                    {{ date('l M jS', \DateTime::createFromFormat('m/d/y', $date)->getTimestamp()) }}
                    <span class="right uk-hidden-small">
                        @if(count($reservable_id) > 0) 
                            @if(count(App\Reservable::find($reservable_id)) > 0)
                            {{App\Reservable::find($reservable_id)->name}}
                            @endif
                        @endif
                    </span> 
                </div>
                <div class="schedule-day-body">
                    @if($schedule->available_start_time !== NULL && $schedule->available_end_time !== NULL)
                        @if(strtotime($schedule->available_start_time) <= strtotime('5:00 AM') && strtotime($schedule->available_end_time) >= strtotime('5:00 AM'))
                            {{-- If the blocked off (grey) area is within the schedule range --}}
                            <div class="schedule-unavailable-block" style="
                            top:{{getReservationStartValue($schedule->available_end_time)}}px;
                            height:{{1440-getReservationDurationValue(date('g:i A',strtotime($schedule->available_start_time)), date('g:i A',strtotime($schedule->available_end_time)))}}px;
                            "></div>
                        @else
                            {{-- If the grey area overlaps the schedule range --}}
                            <div class="schedule-unavailable-block" style="top:0px;height:{{getReservationStartValue($schedule->available_start_time)}}px"></div>
                            <div class="schedule-unavailable-block" style="top:{{getReservationStartValue($schedule->available_end_time)}}px;
                            height:{{getUnavailableBlockHeight($schedule->available_end_time)}}px;
                            "></div>
                        @endif
                    @endif
                    @if(count($reservations) > 0)
                        @foreach($reservations as $reservation)
                            <div title="{{ucwords($reservation->reservable->name)}} ({{date('g:i A', strtotime($reservation->start_time))}} - {{date('g:i A', strtotime($reservation->end_time))}})" 
                            data-details="reservations/reservation/{{$reservation->id}}/edit" class="reservation-time-block" style="
                            {{-- If the reservation is less than 30 minutes, set overflow to scroll so that they can view the reservation details. --}}
                            @if(getReservationDurationValue($reservation->start_time, $reservation->end_time) <= 30) overflow:scroll; @endif
                            line-height:{{getReservationDurationValue($reservation->start_time, $reservation->end_time)+15}}px;top:{{getReservationStartValue($reservation->start_time)}}px;height:{{getReservationDurationValue($reservation->start_time, $reservation->end_time)}}px">
                                @if(isset($reservation->reservable))
                                    <b>{{ucwords($reservation->reservable->name)}}</b> 
                                @endif
                                {{date('g:i A', strtotime($reservation->start_time))}} - {{date('g:i A', strtotime($reservation->end_time))}}
                            </div>
                            {{-- If the reservation begins before 5AM it will begin above the schedule on screen and needs to be wrapped around to cover the appropriate time blocks --}}
                            @if(getReservationStartValue($reservation->start_time) < 0)
                                <div title="{{ucwords($reservation->reservable->name)}} ({{date('g:i A', strtotime($reservation->start_time))}} - {{date('g:i A', strtotime($reservation->end_time))}})" 
                                    data-details="reservations/reservation/{{$reservation->id}}/edit" class="reservation-time-block" 
                                    style="
                                        {{-- If the reservation is less than 30 minutes, set overflow to scroll so that they can view the reservation details. --}}
                                        @if(getReservationDurationValue($reservation->start_time, $reservation->end_time) <= 30)
                                            overflow:scroll;
                                        @endif
                                        line-height:
                                        @if((getReservationDurationValue($reservation->start_time, $reservation->end_time)+15) < 300){{getReservationDurationValue($reservation->start_time, $reservation->end_time)+15}}px;
                                        @else 
                                            315px;border-bottom-left-radius:0px;border-bottom-right-radius:0px; 
                                        @endif
                                        top:{{(1440+getReservationStartValue($reservation->start_time))}}px;
                                        height:{{getReservationDurationValue($reservation->start_time, $reservation->end_time)}}px;
                                        max-height:300px;
                                        {{-- If reservation begins before 5am and ends after 5am than cut off the reservation block so it doesn't overflow past the bottom --}}
                                        @if(strtotime($reservation->start_time) < strtotime('5:00 AM') && strtotime($reservation->end_time) > strtotime('5:00 AM'))
                                            height:{{getReservationDurationValue($reservation->start_time, date('g:i A',strtotime('5:00 AM')))}}px;
                                            line-height:{{getReservationDurationValue($reservation->start_time, date('g:i A',strtotime('5:00 AM'))) + 15}}px;
                                            border-bottom-left-radius:0px;border-bottom-right-radius:0px;
                                        @endif
                                    ">
                                    @if(isset($reservation->reservable))
                                        <b>{{ucwords($reservation->reservable->name)}}</b> 
                                    @endif
                                        {{date('g:i A', strtotime($reservation->start_time))}} - {{date('g:i A', strtotime($reservation->end_time))}}
                                </div>
                            @endif
                        @endforeach
                    @endif
                    <div class="schedule-day-hour">5 AM</div>
                    <div class="schedule-day-hour">6 AM</div>
                    <div class="schedule-day-hour">7 AM</div>
                    <div class="schedule-day-hour">8 AM</div>
                    <div class="schedule-day-hour">9 AM</div>
                    <div class="schedule-day-hour">10 AM</div>
                    <div class="schedule-day-hour">11 AM</div>
                    <div class="schedule-day-hour">12 PM</div>
                    <div class="schedule-day-hour">1 PM</div>
                    <div class="schedule-day-hour">2 PM</div>
                    <div class="schedule-day-hour">3 PM</div>
                    <div class="schedule-day-hour">4 PM</div>
                    <div class="schedule-day-hour">5 PM</div>
                    <div class="schedule-day-hour">6 PM</div>
                    <div class="schedule-day-hour">7 PM</div>
                    <div class="schedule-day-hour">8 PM</div>
                    <div class="schedule-day-hour">9 PM</div>
                    <div class="schedule-day-hour">10 PM</div>
                    <div class="schedule-day-hour">11 PM</div>
                    <div class="schedule-day-hour">12 AM</div>
                    <div class="schedule-day-hour">1 AM</div>
                    <div class="schedule-day-hour">2 AM</div>
                    <div class="schedule-day-hour">3 AM</div>
                    <div class="schedule-day-hour">4 AM</div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        $(function() {
            $("#datepicker").datepicker('setDate', '{{$date}}');
        });
    </script>
@endsection