@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">New Reservation Schedule <span class="icon uk-icon-calendar"></div>
        <form method="post" action="{{url('reservations/schedule/create')}}" data-ajax-form>
            {{csrf_field()}}
            @include('component.form-item',[
                'name' => 'name',
                'type' => 'text',
                'placeholder' => 'e.g. Equipment, Staff, Rooms, etc.',
                'label' => 'Schedule Name',
            ])
            <table role="presentation" style="width:100%">
                <tr>
                    <td colspan="2">
                        <br>
                        <span class="form-item"><label class="label">Restrict Available Reservation Times (optional):</label></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        @include('component.form-item',[
                            'name' => 'available_start_time',
                            'type' => 'time',
                            'placeholder' => 'Start Time',
                        ])  
                    </td>
                    <td>
                        @include('component.form-item',[
                            'name' => 'available_end_time',
                            'type' => 'time',
                            'placeholder' => 'End Time',
                        ])  
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br>
                        <span class="form-item"><label class="label">Available Reservation Days:</label></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        @include('component.form-item',[
                            'label' => 'Mondays',
                            'name' => 'mondays',
                            'value' => 1,
                            'type' => 'checkbox',
                        ])
                        @include('component.form-item',[
                            'label' => 'Tuesdays',
                            'name' => 'tuesdays',
                            'value' => 1,
                            'type' => 'checkbox',
                        ])
                        @include('component.form-item',[
                            'label' => 'Wednesdays',
                            'name' => 'wednesdays',
                            'value' => 1,
                            'type' => 'checkbox',
                        ])
                        @include('component.form-item',[
                            'label' => 'Thursdays',
                            'name' => 'thursdays',
                            'value' => 1,
                            'type' => 'checkbox',
                        ])
                        @include('component.form-item',[
                            'label' => 'Fridays',
                            'name' => 'fridays',
                            'value' => 1,
                            'type' => 'checkbox',
                        ])
                    </td>
                    <td style="vertical-align: top">
                        @include('component.form-item',[
                            'label' => 'Saturdays',
                            'name' => 'saturdays',
                            'value' => 1,
                            'type' => 'checkbox',
                        ])
                        @include('component.form-item',[
                            'label' => 'Sundays',
                            'name' => 'sundays',
                            'value' => 1,
                            'type' => 'checkbox',
                        ])
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br>
                        <span class="form-item"><label class="label">Anonymous Reservations</label></span>
                        @include('component.form-item',[
                            'label' => 'Require user login to make a reservation',
                            'name' => 'require_login',
                            'value' => 0,
                            'type' => 'checkbox',
                        ])
                    </td>
                </tr>
            </table>
            <br>
            <span class="form-item"><label class="label">View settings </label></span>
            @include('component.form-item',[
                'label' => 'Publicly viewable',
                'name' => 'public',
                'type' => 'checkbox',
                'value' => 1,
            ])
            <button class="modal-button">Create Schedule</button>
        </form>
    </div>
@endsection