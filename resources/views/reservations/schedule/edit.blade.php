@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Schedule Preferences <span class="icon uk-icon-calendar"></div>
        <form method="post" action="{{url('reservations/schedule/'.$schedule->id.'/update')}}" data-ajax-form>
            {{csrf_field()}}
            @include('component.form-item',[
                'name' => 'name',
                'type' => 'text',
                'placeholder' => 'e.g. Equipment, Staff, Rooms, etc.',
                'label' => 'Schedule Name',
                'value' => $schedule->name,
            ])
            {{-- <span class="schedule-public-link" @if(!$schedule->public) style="display:none" @endif>
                @include('component.list-item',[
                    'text' => 'Sharable link',
                    'href' => 'schedule/'.$schedule->slug,
                    'subtext' => url('schedule/'.$schedule->slug),
                    'target' => '_blank',
                    'label' => $schedule->public,
                    'class' => 'has-circle-icon',
                    'icon_left' => 'icon-circle-link',
                ])
            </span> --}}
            <table role="presentation" style="width:100%">
                <tr>
                    <td colspan="2">
                        <br>
                        <span class="form-item"><label class="label">Restrict Available Reservation Times (optional):</label></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        @include('component.form-item',[
                            'name' => 'available_start_time',
                            'type' => 'time',
                            'placeholder' => 'Start Time',
                            'value' => isset($schedule->available_start_time) ? date('g:i a',strtotime($schedule->available_start_time)) : '',
                        ])  
                    </td>
                    <td>
                        @include('component.form-item',[
                            'name' => 'available_end_time',
                            'type' => 'time',
                            'placeholder' => 'End Time',
                            'value' => isset($schedule->available_end_time) ? date('g:i a',strtotime($schedule->available_end_time)) : '',
                        ])  
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br>
                        <span class="form-item"><label class="label">Available Reservation Days:</label></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        @include('component.form-item',[
                            'label' => 'Mondays',
                            'name' => 'mondays',
                            'value' => $schedule->mondays,
                            'type' => 'checkbox',
                        ])
                        @include('component.form-item',[
                            'label' => 'Tuesdays',
                            'name' => 'tuesdays',
                            'value' => $schedule->tuesdays,
                            'type' => 'checkbox',
                        ])
                        @include('component.form-item',[
                            'label' => 'Wednesdays',
                            'name' => 'wednesdays',
                            'value' => $schedule->wednesdays,
                            'type' => 'checkbox',
                        ])
                        @include('component.form-item',[
                            'label' => 'Thursdays',
                            'name' => 'thursdays',
                            'value' => $schedule->thursdays,
                            'type' => 'checkbox',
                        ])
                        @include('component.form-item',[
                            'label' => 'Fridays',
                            'name' => 'fridays',
                            'value' => $schedule->fridays,
                            'type' => 'checkbox',
                        ])
                    </td>
                    <td style="vertical-align: top">
                        @include('component.form-item',[
                            'label' => 'Saturdays',
                            'name' => 'saturdays',
                            'value' => $schedule->saturdays,
                            'type' => 'checkbox',
                        ])
                        @include('component.form-item',[
                            'label' => 'Sundays',
                            'name' => 'sundays',
                            'value' => $schedule->sundays,
                            'type' => 'checkbox',
                        ])
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br>
                        <span class="form-item"><label class="label">Anonymous Reservations</label></span>
                        @include('component.form-item',[
                            'label' => 'Require user login to make a reservation',
                            'name' => 'require_login',
                            'value' => $schedule->require_login,
                            'type' => 'checkbox',
                        ])
                    </td>
                </tr>
            </table>
            <br>
            <span class="form-item"><label class="label">View settings </label></span>
            @include('component.form-item',[
                'label' => 'Publicly viewable',
                'name' => 'public',
                'value' => $schedule->public,
                'type' => 'checkbox',
            ])
            <button type="button" class="icon-button right red" data-confirm="Are you sure you want to delete {{$schedule->name}} and all it's associated data? This cannot be undone." data-delete="{{url('schedule/'.$schedule->id . '/delete')}}"><span class="icon-trash-alt"></span></button>

            <button class="modal-button">Save Changes</button>
        </form>
    </div>
@endsection