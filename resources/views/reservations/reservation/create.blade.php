@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">New Reservation {{-- <span class="right">@if(isset($date)){{ date("M d, Y",strtotime($date)) }}@endif</span> --}}{{-- {{date('m/d/y',strtotime($date))}} --}} {{-- <span class="icon uk-icon-calendar"></span> --}}</div>
        <form method="post" action="{{url('reservations/schedule/'.$schedule->id.'/reservation/create')}}" data-ajax-form>
            {{csrf_field()}}
            @if(isset($public))
                <input type="hidden" name="public" value="1">
            @endif

            <table role="presentation" style="width:100%">
                <tr>
                    <td>            
                        <div class="form-item select">
                            <span class="icon icon-select-arrow form-item-icon-right"></span>
                            <select name="reservable_id">
                                <option disabled selected>Choose one</option>
                                @if(isset($schedule->reservables) && $schedule->reservables !== null)
                                    @foreach($schedule->reservables as $reservable)
                                        <option @if($reservable->id == $reservable_id) selected @endif value="{{$reservable->id}}">{{$reservable->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </td>
                    <td>
                        @if(!isset($date))
                            @include('component.form-item',[
                                'name' => 'date',
                                'type' => 'date',
                                'class' => 'uk-text-right',
                                'placeholder' => 'MM/DD/YY',
                            ])
                        @else
                            @include('component.form-item',[
                                'name' => 'date',
                                'type' => 'date',
                                'class' => 'uk-text-right',
                                'placeholder' => 'MM/DD/YY',
                                'value'=>$date
                            ])
                            {{-- <input type="hidden" name="date" value="{{$date}}"> --}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        @include('component.form-item',[
                            'name' => 'name',
                            'type' => 'text',
                            'placeholder' => 'Full Name',
                            'value' => (Auth::user() !== NULL) ? Auth::user()->name : '',
                        ])
                    </td>
                    <td>
                        @include('component.form-item',[
                            'name' => 'email',
                            'type' => 'text',
                            'placeholder' => 'Email',
                            'value' => (Auth::user() !== NULL) ? Auth::user()->email : '',
                        ])
                    </td>
                </tr>
                <tr>
                    <td>
                        @include('component.form-item',[
                            'name' => 'start_time',
                            'type' => 'time',
                            'placeholder' => 'Start Time',
                        ])  
                    </td>
                    <td>
                        @include('component.form-item',[
                            'name' => 'end_time',
                            'type' => 'time',
                            'placeholder' => 'End Time',
                        ])  
                    </td>
                </tr>
            </table>
            @include('component.form-item',[
                'name' => 'purpose',
                'type' => 'textarea',
                'placeholder' => 'Purpose',
            ])
            <button class="modal-button">Confirm Reservation</button>
        </form>
    </div>
@endsection