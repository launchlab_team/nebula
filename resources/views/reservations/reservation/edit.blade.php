@extends('layouts.partial')

@section('middle')
    <form method="post" action="{{url("reservations/reservation/$reservation->id/update")}}" data-ajax-form>
        <div class="detail-view">

            <span class="detail-view-icon icon icon-circle-calendar"></span>
            <h2 class="detail-view-heading">{{$reservation->reservable->name}}<br>({{date('g:i A', strtotime($reservation->start_time))}} - {{date('g:i A', strtotime($reservation->end_time))}})</h2>

            <div class="detail-view-menu">
                <button type="button" title="Delete Reservation" data-delete="{{url('reservation/'.$reservation->id.'/delete')}}"><span class="icon uk-icon-trash-o"></span></button>
                <button title="Save Changes"><span class="icon uk-icon-save"></span></button>
                <span onClick="hideDetails()" class="icon icon-x"></span>
            </div>

            <div class="detail-view-content">
                {{csrf_field()}} 
                <div class="form-item select">
                    <label class="label">Resource</label>
                    <span class="icon icon-select-arrow form-item-icon-right"></span>
                    <select name="reservable_id">
                        <option disabled selected>Choose one</option>
                        @if(isset($reservation->schedule->reservables))
                            @foreach($reservation->schedule->reservables as $reservable)
                                <option @if($reservable->id == $reservation->reservable_id) selected @endif value="{{$reservable->id}}">{{$reservable->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                @include('component.form-item',[
                    'name' => 'name',
                    'type' => 'text',
                    'label' => 'Reserved by',
                    'value' => $reservation->name,
                ])
                @include('component.form-item',[
                    'name' => 'email',
                    'type' => 'text',
                    'value' => $reservation->email,
                ])
                @include('component.form-item',[
                    'name' => 'purpose',
                    'type' => 'textarea',
                    'label' => 'purpose',
                    'value' => $reservation->purpose,
                ])
                @include('component.form-item',[
                    'label' => 'Date & Time',
                    'name' => 'date',
                    'type' => 'date',
                    'placeholder' => 'MM/DD/YY',
                    'value' => date('m/d/y',strtotime($reservation->date)),
                ])
                <table role="presentation" style="width:100%">
                    <tr>
                        <td>
                            @include('component.form-item',[
                                'name' => 'start_time',
                                'type' => 'time',
                                'placeholder' => 'Start Time',
                                'value' => date('g:i a',strtotime($reservation->start_time)),
                            ])  
                        </td>
                        <td>
                            @include('component.form-item',[
                                'name' => 'end_time',
                                'type' => 'time',
                                'placeholder' => 'End Time',
                                'value' => date('g:i a',strtotime($reservation->end_time)),
                            ])  
                        </td>
                    </tr>
                </table>
            {{-- {{dd($reservation->schedule)}} --}}
        </div>
    </form>
@endsection