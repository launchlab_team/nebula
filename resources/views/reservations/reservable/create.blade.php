@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">New Reservable</div>
        <form method="post" action="{{url('reservations/schedule/'.$schedule->id.'/reservable/create')}}" data-ajax-form>
            {{csrf_field()}}
            @include('component.form-item',[
                'name' => 'name',
                'type' => 'text',
                'placeholder' => 'Name',
            ])
            @include('component.form-item',[
                'name' => 'details',
                'type' => 'textarea',
                'placeholder' => 'Details',
            ])
            <button class="modal-button">Add to Reservables</button>
        </form>
    </div>
@endsection