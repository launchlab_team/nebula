@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        <a href="{{url('reservations')}}"><span class="icon icon-circle-list" style="float:none !important;padding-left:0px;font-size:1.2em"></span></a> 
        <span class="icon-angle-right" style="font-size:14px"></span> {{ucfirst($schedule->name)}}
        <div class="uk-button-dropdown right" data-uk-dropdown="{mode:'click'}">
            <span class="icon icon-plus cursor-pointer"></span>
            <div class="uk-dropdown" style="width:270px">
                <ul>
                    @include('component.list-item',[
                        'text'=>'New Reservation',
                        'icon_left'=>'uk-icon-calendar',
                        'modal' => "reservations/schedule/".$schedule->id."/reservation/create",
                    ])
                    @include('component.list-item',[
                        'text'=>'New Reservable',
                        'icon_left'=>'uk-icon-list',
                        'modal' => "reservations/schedule/".$schedule->id."/reservable/create",
                    ])
                </ul>
            </div>
        </div>
    </h1>

    <div class="horizontal-tabs">
        <a href="{{url('reservations/schedule/'.$schedule->id)}}"><div class="tab ">Schedule</div></a>
        <a href="{{url('reservations/schedule/'.$schedule->id.'/reservations')}}"><div class="tab">Reservations</div></a>
        <a href=""><div class="tab selected">Reservables</div></a>
    </div> 

    @if($schedule->reservables !== null && count($schedule->reservables) > 0)
        @foreach($schedule->reservables as $reservable)
            @include('component.list-item',[
                'text'=> $reservable->name,
                'modal'=>'reservations/reservable/'.$reservable->id.'/edit',
                'class'=>'has-circle-icon',
                'icon_left'=>'icon-circle-list',
            ])
        @endforeach
    @else
        <div class="message-center">No reservables yet. Click <a data-modal="reservations/schedule/{{$schedule->id}}/reservable/create"><span class="icon icon-plus"></span></a> to create a new reservable.</div>
    @endif
@endsection