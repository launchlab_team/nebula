@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Edit Reservable</div>
        <form method="post" action="{{url('reservations/reservable/'.$reservable->id.'/update')}}" data-ajax-form>
            {{csrf_field()}}
            @include('component.form-item',[
                'name' => 'name',
                'type' => 'text',
                'label' => 'Name',
                'value' => $reservable->name,
            ])
            @include('component.form-item',[
                'name' => 'details',
                'type' => 'textarea',
                'label' => 'Details',
                'value' => $reservable->details,
            ])
            <button class="modal-button">Save Changes</button>
        </form>
    </div>
@endsection