@extends('layouts.partial')

@section('main')
    <h1 class="heading">
        Reservation Schedules
        <span title="Add New" class="icon cursor-pointer icon-plus" data-modal="reservations/schedule/create"></span>
    </h1>
    @if($schedules !== null && count($schedules) > 0)
        @foreach($schedules as $schedule)
            @include('component.list-item',[
                'text'=> $schedule->name,
                'subtext'=> (count($schedule->reservations) == 1) ? '1 Reservation' : count($schedule->reservations) . ' Reservations',
                'href'=>'reservations/schedule/'.$schedule->id,
                'class'=>'has-circle-icon',
                'icon_left'=>'icon-circle-calendar',
            ])
        @endforeach
    @else
        <div class="message-center"><a data-modal="reservations/schedule/create">Create a schedule</a> to start accepting reservations for people, rooms, equipment etc.</div>
    @endif
@endsection