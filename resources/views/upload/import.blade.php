@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Choose File (csv) <span class="icon uk-icon-file-o"></div>
        <form method="post" id="upload_form" enctype="multipart/form-data" accept=".csv">
            {{csrf_field()}}
            <input type="file" name="file" id="upload_input"/>
            <span class="icon icon-upload"></span>
        </form>
    </div>
@endsection