@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Choose File <span class="icon uk-icon-file-o"></div>
        <form method="post" id="upload_form" enctype="multipart/form-data">
            {{csrf_field()}} 
            <input type="file" name="file" id="upload_input">
            <span class="icon icon-upload"></span>
        </form>
        <ul>
            <label class="label-small text-light">My Uploads</label>
            <hr class="uk-margin-top-remove uk-margin-bottom-small" />
            @foreach($uploads as $upload)
                <li class="list-item">
                    <div class="list-item-body">
                        <span class="list-item-text">{{$upload->name}}</span>
                        <a href="{{url($upload->source)}}" target="_blank"><span class="icon uk-icon-external-link text-light"></span></a>
                        <span class="uppercase right uk-text-small cursor-pointer text-light" data-choose-file="{{$upload->source}}">Choose</span>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
@endsection