@extends('layouts.partial')

@section('main')
    <div class="modal">
        <div class="modal-heading">Choose a Photo <span class="icon uk-icon-image"></div>
        <form method="post" id="upload_form" enctype="multipart/form-data">
            {{csrf_field()}} 
            <input type="file" name="file" id="upload_input">
            <span class="icon icon-upload"></span>
        </form>
        <ul>
            <label class="label-small text-light">My Images</label>
            <hr class="uk-margin-top-remove uk-margin-bottom-small" />
            @foreach($images as $image)
                <li class="list-item">
                    <div class="list-item-body">
                        <span class="list-item-text uk-display-inline">{{$image->name}}</span>
                        <a href='{{url("$image->source")}}' target="_blank"><span class="icon uk-icon-external-link text-light"></span></a>
                        <span class="uppercase right uk-text-small cursor-pointer text-light" data-choose-image="{{$image->source}}">Choose</span>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
@endsection