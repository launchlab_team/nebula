@extends('layouts.partial')

@section('main')
    {{-- <h2 class="heading">Feed</h2> --}}
    @if(isset($feed_items) && $feed_items !== null)
        @foreach($feed_items as $feed_item)
            {{-- <div class="feed-item">
                <a href="{{$feed_item->href}}">
                    <span class="feed-item-icon">{{$feed_item->icon}}</span>
                    <span class="feed-item-text">{{$feed_item->text}}</span>
                    <span class="feed-item-time">{{$feed_item->created_at->diffForHumans()}}</span>
                </a>
            </div> --}}
            @include('component.list-item',[
                'class'=>'has-circle-icon',
                'target'=>'_top',
                'href'=>$feed_item->href,
                'text'=>$feed_item->text,
                'modal'=>$feed_item->modal,
                'details'=> $feed_item->details,
                'icon_left'=>$feed_item->icon,
                'subtext'=>$feed_item->created_at->diffForHumans(),
            ])
        @endforeach
        @include('component.paginate',['records'=>$feed_items])
    @endif
@endsection