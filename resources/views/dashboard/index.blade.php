@extends('layouts.sidebar')

@section('header')
    @include('dashboard.header')
@overwrite

@section('scripts')
    @parent
    <script type="text/javascript">
        if (location.hash === '') {
            $('a[href="#feed"]').click();
            location.hash = '#feed';
        }
    </script>
@overwrite