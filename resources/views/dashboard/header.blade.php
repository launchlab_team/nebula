<header>

    <div class="header-left">
        @if(isset($settings) && $settings->logo !== '')
            <a href="{{url('/')}}"><img class="header-logo" src="{{url($settings->logo)}}"></a>
        @else
            <a href="{{url('/')}}"><img class="header-logo" src="{{url('img/logo-nebula.svg')}}"></a>
        @endif
    </div>

    <div class="header-searchbar">
        @if(Auth::user() && Auth::user()->is_manager && basename($_SERVER['REQUEST_URI'], ".php") == 'dashboard')
            @include('component.searchbar')
        @endif
    </div>

    <div class="header-right">

        @if(Auth::user() && Auth::user()->is_manager && basename($_SERVER['REQUEST_URI'], ".php") == 'dashboard')
            @include('component.icon',[
                'icon_class' => 'icon-circle-search',
                'wrapper_class' => 'outlined animated rotateIn uk-visible-small',
                'attributes' => 'data-show-search',
            ])
            @if(Auth::user() !== null && Auth::user()->is_manager)
                {{-- @include('component.icon',[
                    'icon_class' => 'icon-circle-settings',
                    'attributes' => 'data-toggle-settings',
                ]) --}}
            @endif
        @endif

        @if(Auth::user() && Auth::user()->is_manager)
            @include('component.icon',[
                'icon_class' => 'icon-circle-menu',
                'dropdown' => 'dropdown.header',
                'dropdown_position' => 'bottom-left',
                'dropdown_style' => 'right:10px',
                'wrapper_class' => 'uk-hidden-small',
            ])
            {{-- @include('component.icon',[
                'icon_class' => 'icon-circle-search',
                'wrapper_class' => 'outlined animated rotateIn uk-visible-small',
                'attributes' => 'data-show-search',
                'wrapper_class' => 'uk-visible-small',
            ]) --}}
        @endif

        @if(basename($_SERVER['REQUEST_URI'], ".php") != 'dashboard')
        @include('component.icon',[
            'icon_class' => 'icon-circle-menu',
            'dropdown' => 'dropdown.header',
            'dropdown_position' => 'bottom-left',
            'wrapper_class' => 'uk-visible-small',
        ])
        @endif


    </div>
</header>