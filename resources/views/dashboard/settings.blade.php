
@extends('layouts.partial')

@section('middle')
<form method="post" action="{{url('settings/update')}}" data-ajax-form>
    {{csrf_field()}}
    <div class="detail-view">

        <span class="detail-view-icon icon icon-circle-settings"></span>
        <h2 class="detail-view-heading">Settings</h2>

        <div class="detail-view-menu">
            <button title="Save Changes"><span class="icon uk-icon-save"></span></button>
        </div>

        <div class="detail-view-content">
            {{csrf_field()}} 
            @include('component.form-item',[
                'name' => 'organization_name',
                'type' => 'text',
                'label' => 'Organization Name',
                'value' => $settings->organization_name,
            ])
            @include('component.form-item',[
                'name' => 'logo',
                'type' => 'image',
                'label' => 'Logo',
                'placeholder' => 'Update logo',
                'value' => $settings->logo,
            ])
            {{-- {{$settings->sidebar}} --}}
            @include('component.form-item',[
                'name' => 'sidebar',
                'type' => 'select',
                'label' => 'Sidebar color',
                'options' => 'Default,Red,Orange,Green,Blue,Purple',
                'value'=> $settings->sidebar,
            ])
            {{-- <button>Update Settings</button> --}}
        </div>

    </div>
</form>
@endsection

@section('scripts')
    @parent
    {{-- <script type="text/javascript">    
        $(document).on('keyup', '[name=title]', function() {
            var title = $(this).val();
            // Generate url ready slug
            var slug = title.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]+/g, "-").toLowerCase();
            $('[name=slug]').val(slug);
            $('[name=slug]').parent().find('.form-item-sublabel').html('{{ getenv('APP_DOMAIN') }}/'+slug);
        });
        $(document).on('keyup', '[name=slug]', function() {
            var title = $(this).val();
            // Generate url ready slug
            var slug = title.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]+/g, "-").toLowerCase();
            $('[name=slug]').val(slug);
            $(this).parent().find('.form-item-sublabel').html('{{ getenv('APP_DOMAIN') }}/'+slug);
        });
    </script> --}}
@endsection