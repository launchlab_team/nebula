@if(count($records) > 0)
    <div class="list-divider">Records</div>
    <div class="uk-scrollable-text">
        @foreach($records as $record)
            @if($record->record_set->module->enabled)
                @include('component.list-item',[
                    'text' => json_decode($record->data)[$record->record_set->label_key],
                    'subtext' => $record->record_set->name,
                    //'subtext' => isset($record->sublabel_key) ? json_decode($record->data)[$record->sublabel_key] : NULL,
                    'details' => 'record/'.$record->id.'/edit',
                    'icon_left' => $record->record_set->module->icon,
                    'class' => 'has-circle-icon',
                ])
            @endif
        @endforeach
    </div>
@endif

@if(count($crm_records) > 0)
    <div class="list-divider">CRM</div>
    <div class="uk-scrollable-text">
        @foreach($crm_records as $record)
            {{-- @if(isset($record) && isset($crm)) --}}
            <?php $crm = App\Crm::first(); ?>
            <?php $module = App\Module::where('slug','crm')->first(); ?>
                @include('component.list-item',[
                    'text' => json_decode($record->data)[$crm->label_key],
                    'subtext' => array_key_exists($crm->sublabel_key, json_decode($record->data)) ? json_decode($record->data)[$crm->sublabel_key] : NULL,
                    'details' => 'crm/records/'.$record->id.'/edit',
                    'icon_left' => 'icon-circle-list',
                    'class' => 'has-circle-icon',
                ])
            {{-- @endif --}}
        @endforeach
    </div>
@endif

@if(count($users) > 0)
    <div class="list-divider">Users</div>
    <div class="uk-scrollable-text">
        @foreach($users as $user)
            @include('component.list-item',[
                'text' => $user->name,
                'subtext' => $user->email,
                'details' => 'user/'.$user->id.'/edit',
                'icon_left' => 'icon-circle-user',
                'class' => 'has-circle-icon',
            ])
        @endforeach
    </div>
@endif

@if(count($pages) > 0)
    <div class="list-divider">Pages</div>
    <div class="uk-scrollable-text">
        @foreach($pages as $page)
            @include('component.list-item',[
                'text' => $page->title,
                'subtext' => url('').'/'.$page->slug,
                'details' => 'page/'.$page->id.'/settings',
                'icon_left' => 'icon-circle-layout',
                'class' => 'has-circle-icon',
            ])
        @endforeach
    </div>
@endif

@if(count($forms) > 0)
    <div class="list-divider">Forms</div>
    <div class="uk-scrollable-text">
        @foreach($forms as $form)
            @include('component.list-item',[
                'text' => $form->name,
                'class' => 'has-circle-icon',
                'icon_left' => 'icon-circle-form',
                'subtext' => $form->updated_at->diffForHumans(),
                'details' => 'form/'.$form->id.'/edit',
            ])
        @endforeach
    </div>
@endif

@if(count($links) > 0)
    <div class="list-divider">Links</div>
    <div class="uk-scrollable-text">
        @foreach($links as $link)
            @include('component.list-item',[
                'text' => $link->title,
                'class' => 'has-circle-icon',
                'icon_left' => 'icon-circle-link',
                'subtext' => $link->href,
                'modal' => 'link/'.$link->id.'/edit',
            ])
        @endforeach
    </div>
@endif

@if(count($files) > 0)
    <div class="list-divider">Files</div>
    <div class="uk-scrollable-text">
        @foreach($files as $file)
            @include('component.list-item',[
                'text' => $file->name,
                'class' => 'has-circle-icon',
                'icon_left' => getIconFromType($file->type),
                'subtext' => humanFileSize($file->size) . ' | '.$file->type,
                'details' => 'file/'.$file->id.'/edit',
            ])
        @endforeach
    </div>
@endif

{{-- @if(count($payments) > 0)
    <div class="list-divider">Payments</div>
    <div class="uk-scrollable-text">
        @foreach($payments as $payment)
            @include('component.list-item',[
                'text' => $payment->description,
                'class' => 'has-circle-icon',
                'icon_left' => 'icon-circle-payment',
                'subtext' => '$'.$payment->amount,
                'details' => 'payment/'.$payment->id.'/view',
            ])
        @endforeach
    </div>
@endif --}}

@if(count($reservations) > 0)
    <div class="list-divider">Reservations</div>
    <div class="uk-scrollable-text">
        @foreach($reservations as $reservation)
            @if($reservation->reservable !== NULL)
                @include('component.list-item',[
                    'text' => $reservation->name.' | '.$reservation->reservable->name,
                    'class' => 'has-circle-icon',
                    'icon_left' => 'icon-circle-calendar',
                    'details' => 'reservations/reservation/'.$reservation->id.'/edit',
                    'subtext' => date('l m/d',strtotime($reservation->date)) .' ('. date('g:i A', strtotime($reservation->start_time)) .'-'. date('g:i A', strtotime($reservation->end_time)).')',
                ])
            @endif
        @endforeach
    </div>
@endif

@if(count($reservation_schedules) > 0)
    <div class="list-divider">Reservation Schedules</div>
    <div class="uk-scrollable-text">
        @foreach($reservation_schedules as $reservation_schedule)
            @include('component.list-item',[
                'text' => $reservation_schedule->name,
                'class' => 'has-circle-icon',
                'icon_left' => 'icon-circle-calendar',
                'href' => 'dashboard#reservations/schedule/36',
            ])
        @endforeach
    </div>
@endif

@if(count($payment_forms) > 0)
    <div class="list-divider">Payment Forms</div>
    <div class="uk-scrollable-text">
        @foreach($payment_forms as $payment_form)
            @include('component.list-item',[
                'text' => $payment_form->title,
                'class' => 'has-circle-icon',
                'icon_left' => 'icon-circle-form',
                'details' => 'payment/form/'.$payment_form->id.'/edit',
            ])
        @endforeach
    </div>
@endif

@if(count($stripe_customers) > 0) 
    <div class="list-divider">Customers</div>
    <div class="uk-scrollable-text">
{{--         {{dd($stripe_customers)}}
 --}}        @foreach($stripe_customers as $customer)
            @if(count($customer) > 0)
            @include('component.list-item',[
                'text' => $customer->name,
                'class' => 'has-circle-icon',
                'icon_left' => 'icon-circle-user',
                'details' => 'stripe/customer/'.$customer->customer_id,
            ])
            @endif
        @endforeach
    </div>
@endif

