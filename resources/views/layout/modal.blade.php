<div class="uk-modal" id="modal">
    <a href="#" class="uk-modal-close"><span class="icon icon-x"></span></a>
    <div class="uk-modal-dialog">
        <iframe id="modal_iframe" width="100%"></iframe>
    </div>
</div>