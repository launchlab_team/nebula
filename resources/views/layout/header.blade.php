<header>

    <div class="header-left">
        @if(isset($settings) && $settings->logo !== NULL && $settings->logo !== '')
            <a href="{{url('/')}}"><img class="header-logo" src="{{url($settings->logo)}}"/></a>
        @else
            <a href="{{url('/')}}"><img class="header-logo" src="{{url('img/logo-nebula.svg')}}"/></a>
        @endif
    </div>


    <div class="header-right">
        {{-- If not on the account page --}}
        @if(basename($_SERVER['REQUEST_URI'], ".php") != 'account')
            {{-- If menu items are set (originally set in the pages module) --}}
            @if(isset($menu_items) && $menu_items !== null)
                <div class="page-menu-items">
                    @foreach($menu_items as $menu_item)
                        @if($menu_item->label !== '')
                            @if(filter_var($menu_item->href, FILTER_VALIDATE_URL))
                            <a class="page-menu-item" href="{{$menu_item->href}}">{{$menu_item->label}}</a>
                            @else
                            <a class="page-menu-item" href="{{env('APP_URL') . '/' . $menu_item->href}}">{{$menu_item->label}}</a>
                            @endif
                        @endif
                    @endforeach
                </div>
            @endif
        @endif

        {{-- If not on the dashboard or the account page show a link to the account page
        {{-- @if(basename($_SERVER['REQUEST_URI'], ".php") != 'dashboard' && basename($_SERVER['REQUEST_URI'], ".php") != 'account')
            @include('component.icon',[
                'icon_class'=>'icon-circle-user',
                'href'=>'account',
                'class'=>'uk-hidden-small',
            ])
        @endif --}}

        @include('component.icon',[
            'icon_class' => 'icon-circle-menu',
            'dropdown' => 'dropdown.header',
            'dropdown_position' => 'bottom-left',
            // 'wrapper_class' => 'uk-visible-small', // only visble on small screens
        ])

    </div>

</header>