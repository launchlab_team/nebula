<div class="tabs draggable">
    @include('component.tab',[
        'href' => url('#feed'),
        'icon' => 'icon-circle-home',
        'text' => 'Feed',
    ])

    <form action="modules/update" id="sortable_sidebar" class="uk-margin-remove" method="post" enctype="multipart/form-data" data-ajax-form>
        {{csrf_field()}}
        @if($modules !== null)
            @foreach($modules as $module)
                @if($module->type == 'records')
                    @include('component.tab',[
                        //'href' => url('#records').'/'.$module->id,
                        'href' => url('#records').'/'.create_slug($module->label),
                        'icon' => $module->icon,
                        'text' => $module->label,
                        'draggable' => true,
                        'html'=> '<span class="icon-draggable settings-visible"></span>',
                        'class' => $module->enabled ? '' : 'settings-visible', // if not enabled only show on settings visible
                    ])
                @endif
                @if($module->type == 'crm')
                    @include('component.tab',[
                        'href' => url('#crm'),
                        'icon' => $module->icon,
                        'text' => $module->label,
                        'draggable' => true,
                        'html'=> '<span class="icon-draggable settings-visible"></span>',
                        'class' => $module->enabled ? '' : 'settings-visible', // if not enabled only show on settings visible
                    ])
                @endif
                @if($module->type == 'pages')
                    @include('component.tab',[
                        'href' => url('#pages'),
                        'icon' => 'icon-circle-layout',
                        'text' => $module->label,
                        'draggable' => true,
                        'html'=> '<span class="icon-draggable settings-visible"></span>',
                        'class' => $module->enabled ? '' : 'settings-visible', // if not enabled only show on settings visible
                    ])
                @endif
                @if($module->type == 'payments')
                    @include('component.tab',[
                        'href' => url('#payments'),
                        'icon' => 'icon-circle-payment',
                        'text' => $module->label,
                        'draggable' => true,
                        'html'=> '<span class="icon-draggable settings-visible"></span>',
                        'class' => $module->enabled ? '' : 'settings-visible', // if not enabled only show on settings visible
                    ])
                @endif
                @if($module->type == 'files')
                    @include('component.tab',[
                        'href' => url('#files'),
                        'icon' => 'icon-circle-upload',
                        'text' => $module->label,
                        'draggable' => true,
                        'html'=> '<span class="icon-draggable settings-visible"></span>',
                        'class' => $module->enabled ? '' : 'settings-visible', // if not enabled only show on settings visible
                    ])
                @endif
                @if($module->type == 'links')
                    @include('component.tab',[
                        'href' => '#links',
                        'icon' => $module->icon,
                        'text' => $module->label,
                        'draggable' => true,
                        'html'=> '<span class="icon-draggable settings-visible"></span>',
                        'class' => $module->enabled ? '' : 'settings-visible', // if not enabled only show on settings visible
                    ])
                @endif
                @if($module->type == 'forms')
                    @include('component.tab',[
                        'href' => url('#forms'),
                        'icon' => $module->icon,
                        'text' => $module->label,
                        'draggable' => true,
                        'html'=> '<span class="icon-draggable settings-visible"></span>',
                        'class' => $module->enabled ? '' : 'settings-visible', // if not enabled only show on settings visible
                    ])
                @endif
                @if($module->type == 'reservations')
                    @include('component.tab',[
                        'href' => url('#reservations'),
                        'icon' => $module->icon,
                        'text' => $module->label,
                        'draggable' => true,
                        'html'=> '<span class="icon-draggable settings-visible"></span>',
                        'class' => $module->enabled ? '' : 'settings-visible', // if not enabled only show on settings visible
                    ])
                @endif
            @endforeach
        @endif
    </form>

    {{-- The following only appear on the dashboard --}}
    @if(basename($_SERVER['REQUEST_URI'], ".php") == 'dashboard')
        @include('component.tab',[
            'href' => url('#users'),
            'icon' => 'icon-circle-user',
            'text' => 'Users',
        ])
        @include('component.tab',[
            'href' => url('#settings'),
            'icon' => 'icon-circle-settings',
            'text' => 'Settings',
            'class'=>'settings-visible',
        ])
    @endif

    {{-- @include('component.tab',[
        'icon' => 'icon-circle-add',
        'text' => 'Add Tab',
        'class'=> 'button-side-tab settings-visible',
        'html'=> '<span class="settings-visible"></span>',
        'attributes'=>'data-modal="module/add"'
    ]) --}}

    <div class="sidebar-toolbar-toggle">
        Toolbar
        <span class="icon sidebar-toolbar-toggle-arrow icon-angle-right"></span>
    </div>

    <div class="sidebar-toolbar uk-animation-fade" style="display: none">
        <div class="uk-grid uk-grid-collapse">
            @if($modules !== null)
                @foreach($modules as $module)
                    @if($module->type == 'records')
                        <?php $dashboard_has_records_tab = true ?>
                    @endif
                    @if($module->type == 'crm')
                        <?php $dashboard_has_crm_tab = true ?>
                    @endif
                    @if($module->type == 'pages')
                        <?php $dashboard_has_pages_tab = true ?>
                    @endif
                    @if($module->type == 'payments')
                        <?php $dashboard_has_payments_tab = true ?>
                    @endif
                    @if($module->type == 'files')
                        <?php $dashboard_has_files_tab = true ?>
                    @endif
                    @if($module->type == 'links')
                        <?php $dashboard_has_links_tab = true ?>
                    @endif
                    @if($module->type == 'forms')
                        <?php $dashboard_has_forms_tab = true ?>
                    @endif
                    @if($module->type == 'reservations')
                        <?php $dashboard_has_reservations_tab = true ?>
                    @endif
                @endforeach
            @endif

            {{-- @if(isset($dashboard_has_records_tab) && $dashboard_has_records_tab) --}}
                <div class="uk-width-1-2">
                    <div class="sidebar-toolbar-add-button">
                        {{-- <span class="icon uk-icon-question-circle-o"  data-modal="info/records"></span> --}}
                        <span class="icon icon-plus sidebar-toolbar-plus" data-modal="module/add/records"></span>
                        <span class="icon icon-circle-list" data-modal="module/add/records"></span>
                        <span class="sidebar-add-button-text" data-modal="module/add/records">Records</span>
                    </div>
                </div>
            {{-- @endif --}}

            @if(!isset($dashboard_has_payments_tab))
                <div class="uk-width-1-2">
                    <div class="sidebar-toolbar-add-button">
                        {{-- <span class="icon uk-icon-question-circle-o"  data-modal="info/payments"></span> --}}
                        <span class="icon icon-plus sidebar-toolbar-plus" data-modal="module/add/payments"></span>
                        <span class="icon icon-circle-payment" data-modal="module/add/payments"></span>
                        <span class="sidebar-add-button-text" data-modal="module/add/payments">Payments</span>
                    </div>
                </div>
            @endif

            @if(!isset($dashboard_has_crm_tab))
                <div class="uk-width-1-2">
                    <div class="sidebar-toolbar-add-button">
                        {{-- <span class="icon uk-icon-question-circle-o"  data-modal="info/crm"></span> --}}
                        <span class="icon icon-plus sidebar-toolbar-plus" data-modal="module/add/crm"></span>
                        <span class="icon icon-circle-lead" data-modal="module/add/crm"></span>
                        <span class="sidebar-add-button-text" data-modal="module/add/crm">CRM</span>
                    </div>
                </div>
            @endif

            @if(!isset($dashboard_has_forms_tab))
                <div class="uk-width-1-2">
                    <div class="sidebar-toolbar-add-button">
                        {{-- <span class="icon uk-icon-question-circle-o"  data-modal="info/forms"></span> --}}
                        <span class="icon icon-plus sidebar-toolbar-plus" data-modal="module/add/forms"></span>
                        <span class="icon icon-circle-calendar" data-modal="module/add/forms"></span>
                        <span class="sidebar-add-button-text" data-modal="module/add/forms">Forms</span>
                    </div>
                </div>
            @endif

            @if(!isset($dashboard_has_links_tab))
                <div class="uk-width-1-2">
                    <div class="sidebar-toolbar-add-button">
                        {{-- <span class="icon uk-icon-question-circle-o"  data-modal="info/links"></span> --}}
                        <span class="icon icon-plus sidebar-toolbar-plus" data-modal="module/add/links"></span>
                        <span class="icon icon-circle-link" data-modal="module/add/links"></span>
                        <span class="sidebar-add-button-text" data-modal="module/add/links">Links</span>
                    </div>
                </div>
            @endif

            @if(!isset($dashboard_has_files_tab))
                <div class="uk-width-1-2">
                    <div class="sidebar-toolbar-add-button">
                        {{-- <span class="icon uk-icon-question-circle-o"  data-modal="info/files"></span> --}}
                        <span class="icon icon-plus sidebar-toolbar-plus" data-modal="module/add/files"></span>
                        <span class="icon icon-circle-upload" data-modal="module/add/files"></span>
                        <span class="sidebar-add-button-text" data-modal="module/add/files">Files</span>
                    </div>
                </div>
            @endif

            @if(!isset($dashboard_has_reservations_tab))
                <div class="uk-width-1-2">
                    <div class="sidebar-toolbar-add-button">
                        {{-- <span class="icon uk-icon-question-circle-o"  data-modal="info/reservations"></span> --}}
                        <span class="icon icon-plus sidebar-toolbar-plus" data-modal="module/add/reservations"></span>
                        <span class="icon icon-circle-calendar" data-modal="module/add/reservations"></span>
                        <span class="sidebar-add-button-text" data-modal="module/add/reservations">Reservations</span>
                    </div>
                </div>
            @endif

            @if(!isset($dashboard_has_pages_tab))
                <div class="uk-width-1-2">
                    <div class="sidebar-toolbar-add-button">
                        {{-- <span class="icon uk-icon-question-circle-o"  data-modal="info/pages"></span> --}}
                        <span class="icon icon-plus sidebar-toolbar-plus" data-modal="module/add/pages"></span>
                        <span class="icon icon-circle-layout" data-modal="module/add/pages"></span>
                        <span class="sidebar-add-button-text" data-modal="module/add/pages">Pages</span>
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>