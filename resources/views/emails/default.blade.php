<!DOCTYPE html>
<html>
<head>
    <title>{{$subject or ''}}</title>
    <style type="text/css">
        body {
            font-size: 13px;
            line-height: 18px;
            color:#666;
            margin:0;
            padding:10px 15px;
            background:#f1f1f1;
        }
        .email-container {
            background:white;
            border-radius: 5px;
            border:1px solid #e8e8e8;
            margin:0 auto;
        }
        .email-header {
            padding:1em;
        }
        .email-header > a > img {
            max-width:160px;
        }
        .email-content {
            padding:1.5em;
            border-top:1px solid #eaeaea;
            border-bottom:1px solid #eaeaea;
        }
        .email-footer {
            padding:1em;
            min-height: 20px;
        }
    </style>
</head>
<body>
    <div class="email-container">
        <div class="email-header">
            @if(isset($settings) && $settings->logo !== NULL && $settings->logo !== '')
                <?php
                // Convert logo to base64
                // $path = storage_path('app/'.$settings->logo);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $data = file_get_contents($path);
                // $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                ?>
                <a href="{{url('/')}}"><img class="header-logo" src="{{url($settings->logo)}}"/></a>
                {{-- <a href="{{url('/')}}"><img class="header-logo" src="{{$base64}}"/></a> --}}
            @else
                <?php
                // Convert logo to base64
                // $path = $settings->logo;
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $data = file_get_contents($path);
                // $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                ?>
                {{-- <a href="{{url('/')}}"><img class="header-logo" src="{{$base64}}"/></a> --}}
                <a href="{{url('/')}}"><img class="header-logo" src="{{url('img/logo-nebula.svg')}}"/></a>
            @endif
        </div>
        <div class="email-content">
            {!! $content or 'No message' !!}
        </div>
        <div class="email-footer">
            {{$footer or ''}}
        </div>
    </div>
</body>
</html>