@if(isset($organization->members) && $organization->members !== null)
    @foreach($organization->members as $member)
        @include('component.list-item',[
            'text' => $member->user->name,
        ])
    @endforeach
@else
    <div class="message-box">
        No members found
    </div>
@endif