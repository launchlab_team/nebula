$(function() {
    // jQuery ui datepicker
    $( "#datepicker" ).datepicker({
        //firstDay: 1,
        dateFormat: "mm/dd/y",
        altField: "#date",
        onSelect: function(dateText) {
            var date = $('#date').val();
            var reservable_id = $('[name=reservable_id]').val();
            if (reservable_id === undefined) {
                reservable_id = '';
            }
            // reformat e.g. 12/16/16 to 12.16.16 so that it can be parsed as a date in the url
            // date = date.replace('/','.');
            // date = date.replace('/','.'); // repeat required
            // console.log( base_url + window.location.pathname +'/'+ date);
            window.location.href = base_url.slice(0,-1) + window.location.pathname +'?date='+ date + '&reservable_id=' + reservable_id;
        }
    });

    // Select the reservable if reservable_id is set
    if (URLGetValues()['reservable_id'] !== undefined && URLGetValues()['reservable_id'] !== '' && URLGetValues()['reservable_id'] !== 'All') {
        $('[name=reservable_id]').val(URLGetValues()['reservable_id']).change();
        // alert(URLGetValues()['reservable_id']);
    }
    function URLGetValues() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }

    // Set the reservable id on reservable_id select change
    $(document).on('change','[name=reservable_id]',function(e) {
        var date = $('#date').val();
        var reservable_id = $('[name=reservable_id]').val();
        if (reservable_id === undefined) {
            reservable_id = '';
        }
        if (date !== undefined) {
            window.location.href = base_url.slice(0,-1) + window.location.pathname +'?date='+ date + '&reservable_id=' + reservable_id;
        }
    });

    // Show the .schedule-public-link list item depending on public status
    $(document).on('click','.checkbox-label',function(e) {
        if ($(this).find('input').attr('name') == "public") {
            $('.schedule-public-link').slideToggle();
        }
    });

});