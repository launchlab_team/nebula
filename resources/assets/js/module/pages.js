// $(document).on('click','[data-add-element]',function() {

//     var form = $('#add-element-form');
//     $('.header-searchbar').addClass('magictime vanishOut');
//     $('.header-searchbar').fadeOut(1000);
//     setTimeout(function () {
//         $('.header-searchbar').removeClass('magictime vanishOut');
//     }, 1000);
// });

$(document).on('click','[data-add-section-full]',function() {
    // $('#main').load('section/generate', { type: 'page-section-full' }, function(response) {
    //       alert( response.success );
    // });
    $.ajax({
        url : base_url + 'section/generate',
        type: "POST",
        data: { type: 'page-section-full', page_id: page_id }, // page id is set on the 
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        },
        complete: function() {
            hideLoader();
        },
        beforeSend: function() {
            showLoader();
        },
        success: function(data) {
            $('#page').append(data);
            if (data.success !== undefined) { // Success
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('success');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('success');
                } else {
                    $('#alert').addClass('success');
                }
                showAlert(data.success);
            }
            if (data.error !== undefined) { // Error
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('error');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('error');
                } else {
                    $('#alert').addClass('error');
                }
                showAlert(data.error);
            }
            if (data.redirect !== undefined) { // Redirect
                if (parent !== undefined) {
                    parent.window.location.href = data.redirect;
                } else {
                    window.location.href = data.redirect;
                }
            }
        },
        error: function (data) {
            $('body').append(data.responseText);
            alert('An unknown error occured.');
        }
    });
});

$(document).on('click','[data-add-section-halves]',function() {
    $.ajax({
        url : base_url + 'section/generate',
        type: "POST",
        data: { type: 'page-section-halves', page_id: page_id }, // page id is set on the 
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        },
        complete: function() {
            hideLoader();
        },
        beforeSend: function() {
            showLoader();
        },
        success: function(data) {
            $('#page').append(data);
            if (data.success !== undefined) { // Success
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('success');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('success');
                } else {
                    $('#alert').addClass('success');
                }
                showAlert(data.success);
            }
            if (data.error !== undefined) { // Error
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('error');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('error');
                } else {
                    $('#alert').addClass('error');
                }
                showAlert(data.error);
            }
            if (data.redirect !== undefined) { // Redirect
                if (parent !== undefined) {
                    parent.window.location.href = data.redirect;
                } else {
                    window.location.href = data.redirect;
                }
            }
        },
        error: function (data) {
            $('body').append(data.responseText);
            alert('An unknown error occured.');
        }
    });
});

$(document).on('click','[data-add-section-thirds]',function() {
    $.ajax({
        url : base_url + 'section/generate',
        type: "POST",
        data: { type: 'page-section-thirds', page_id: page_id }, // page id is set on the 
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        },
        complete: function() {
            hideLoader();
        },
        beforeSend: function() {
            showLoader();
        },
        success: function(data) {
            $('#page').append(data);
            if (data.success !== undefined) { // Success
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('success');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('success');
                } else {
                    $('#alert').addClass('success');
                }
                showAlert(data.success);
            }
            if (data.error !== undefined) { // Error
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('error');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('error');
                } else {
                    $('#alert').addClass('error');
                }
                showAlert(data.error);
            }
            if (data.redirect !== undefined) { // Redirect
                if (parent !== undefined) {
                    parent.window.location.href = data.redirect;
                } else {
                    window.location.href = data.redirect;
                }
            }
        },
        error: function (data) {
            $('body').append(data.responseText);
            alert('An unknown error occured.');
        }
    });
});

$(document).on('click','[data-add-section-fourths]',function() {
    $.ajax({
        url : base_url + 'section/generate',
        type: "POST",
        data: { type: 'page-section-fourths', page_id: page_id }, // page id is set on the 
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        },
        complete: function() {
            hideLoader();
        },
        beforeSend: function() {
            showLoader();
        },
        success: function(data) {
            $('#page').append(data);
            if (data.success !== undefined) { // Success
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('success');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('success');
                } else {
                    $('#alert').addClass('success');
                }
                showAlert(data.success);
            }
            if (data.error !== undefined) { // Error
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('error');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('error');
                } else {
                    $('#alert').addClass('error');
                }
                showAlert(data.error);
            }
            if (data.redirect !== undefined) { // Redirect
                if (parent !== undefined) {
                    parent.window.location.href = data.redirect;
                } else {
                    window.location.href = data.redirect;
                }
            }
        },
        error: function (data) {
            $('body').append(data.responseText);
            alert('An unknown error occured.');
        }
    });
});

$(document).on('click','[data-left-justify-block]',function() {
    page_block_id = $(this).attr('data-left-justify-block');
    page_block = $(this).parent('.page-block-toolbar').parent('.page-block');
    $.ajax({
        url : base_url + 'justify/'+page_block_id,
        type: "POST",
        data: { justify: 'left', page_block_id: page_block_id },
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        },
        success: function(data) {
            page_block.css('text-align','left');
            if (data.success !== undefined) { // Success
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('success');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('success');
                } else {
                    $('#alert').addClass('success');
                }
                showAlert(data.success);
            }
            if (data.error !== undefined) { // Error
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('error');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('error');
                } else {
                    $('#alert').addClass('error');
                }
                showAlert(data.error);
            }
            if (data.redirect !== undefined) { // Redirect
                if (parent !== undefined) {
                    parent.window.location.href = data.redirect;
                } else {
                    window.location.href = data.redirect;
                }
            }
        },
        error: function (data) {
            $('body').append(data.responseText);
            alert('An unknown error occured.');
        }
    });
});

$(document).on('click','[data-right-justify-block]',function() {
    page_block_id = $(this).attr('data-right-justify-block');
    page_block = $(this).parent('.page-block-toolbar').parent('.page-block');
    $.ajax({
        url : 'justify/'+page_block_id,
        type: "POST",
        data: { justify: 'right', page_block_id: page_block_id },
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        },
        success: function(data) {
            page_block.css('text-align','right');
            if (data.success !== undefined) { // Success
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('success');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('success');
                } else {
                    $('#alert').addClass('success');
                }
                showAlert(data.success);
            }
            if (data.error !== undefined) { // Error
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('error');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('error');
                } else {
                    $('#alert').addClass('error');
                }
                showAlert(data.error);
            }
            if (data.redirect !== undefined) { // Redirect
                if (parent !== undefined) {
                    parent.window.location.href = data.redirect;
                } else {
                    window.location.href = data.redirect;
                }
            }
        },
        error: function (data) {
            $('body').append(data.responseText);
            alert('An unknown error occured.');
        }
    });
});

$(document).on('click','[data-center-justify-block]',function() {
    page_block_id = $(this).attr('data-center-justify-block');
    page_block = $(this).parent('.page-block-toolbar').parent('.page-block');
    $.ajax({
        url : 'justify/'+page_block_id,
        type: "POST",
        data: { justify: 'center', page_block_id: page_block_id },
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        },
        success: function(data) {
            page_block.css('text-align','center');
            if (data.success !== undefined) { // Success
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('success');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('success');
                } else {
                    $('#alert').addClass('success');
                }
                showAlert(data.success);
            }
            if (data.error !== undefined) { // Error
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('error');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('error');
                } else {
                    $('#alert').addClass('error');
                }
                showAlert(data.error);
            }
            if (data.redirect !== undefined) { // Redirect
                if (parent !== undefined) {
                    parent.window.location.href = data.redirect;
                } else {
                    window.location.href = data.redirect;
                }
            }
        },
        error: function (data) {
            $('body').append(data.responseText);
            alert('An unknown error occured.');
        }
    });
});


$(document).on('click','[data-delete-section]',function(e) {
    section = $(this).parent();
    delete_uri = $(this).attr('data-delete-section');
    section_id = delete_uri.split('/')[1];
    if (window.confirm("Are you sure you want to delete this section and it's children? This cannot be undone.")) {
        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="_token"]').attr('content')
            },
            type: "post",
            url: base_url + delete_uri,
            data: {
                _method: 'DELETE',
                // _token: $('meta[name="_token"]').attr('content'),
            },
            success: function(data) {
                if (data.success !== undefined) { // Success
                    if (parent.parent !== undefined) {
                        parent.parent.$('#alert').addClass('success');
                    }
                    if (parent !== undefined) {
                        parent.$('#alert').addClass('success');
                    } else {
                        $('#alert').addClass('success');
                    }
                    showAlert(data.success);
                }
                if (data.error !== undefined) { // Error
                    if (parent.parent !== undefined) {
                        parent.parent.$('#alert').addClass('error');
                    }
                    if (parent !== undefined) {
                        parent.$('#alert').addClass('error');
                    } else {
                        $('#alert').addClass('error');
                    }
                    showAlert(data.error);
                } else {
                    $('#'+section_id).remove();
                    // alert('Successfully deleted');
                }
            },
            error: function (data) {
                $(document).find('footer').before(data.responseText);
            }
        });
    }
});

// $(document).on('change','[data-page-class]',function() {
//     // Loop through each data-page-class in the parent form and add to styles input
// });
// $(document).on('keyup','[data-page-style]',function() {
//     // Loop through each data-page-style in the parent form and add values to the styles input
// });

// window.onload = function () {
//     if ($('#css').length) {
//         // var readOnlyCodeMirror = CodeMirror.fromTextArea(document.getElementById('codesnippet_readonly'), {
//         //     mode: "javascript",
//         //     theme: "default",
//         //     lineNumbers: true,
//         //     readOnly: true
//         // });  
//         var editableCodeMirror = CodeMirror.fromTextArea(document.getElementById('css'), {
//             mode: "css",
//             theme: "default",
//             lineNumbers: true
//         });
//     }
// }


// Hide page element toolbar
$(document).on('click','.close-edit-element-menu',function(e) {
    $(this).fadeOut();
    $('.edit-element-save-button').attr('disabled',false);
    $('.edit-element-menu, .edit-element-save-button').addClass('animated fadeOutLeft');
    $('.edit-element-menu, .edit-element-save-button, .add-element-to-block-button').fadeOut();
    setTimeout(function() {
        $('.edit-element-menu, .edit-element-save-button, .add-element-to-block-button').removeClass('animated fadeOutLeft'); // Remove after delay
    }, 1200);
});

$(document).on('click','.close-add-element-menu',function(e) {
    $(this).fadeOut();
    $('.add-element-save-button').attr('disabled',false);
    $('.add-element-menu, .add-element-save-button').addClass('animated fadeOutLeft');
    $('.add-element-menu, .add-element-save-button, .add-element-to-block-button').fadeOut();
    setTimeout(function() {
        $('.add-element-menu, .add-element-save-button, .add-element-to-block-button').removeClass('animated fadeOutLeft'); // Remove after delay
    }, 1200);
});

// Updates the element on the page and posts to backend
$(document).on('click','.edit-element-save-button',function(e) {
    // Define form elements
    var html = $('#edit_element_menu_form').find('textarea[name=html]');
    var height = $('#edit_element_menu_form').find('input[name=height]');
    var width = $('#edit_element_menu_form').find('input[name=width]');
    var margin = $('#edit_element_menu_form').find('input[name=margin]');
    var padding = $('#edit_element_menu_form').find('input[name=padding]');
    var font_size = $('#edit_element_menu_form').find('input[name=font_size]');
    var color = $('#edit_element_menu_form').find('input[name=color]');
    var text_align = $('#edit_element_menu_form').find('select[name=text_align]');
    var image = $('#edit_element_menu_form').find('input[name=image]');
    var background_color = $('#edit_element_menu_form').find('input[name=background_color]');
    var background_image = $('#edit_element_menu_form').find('input[name=background_image]');
    var background_overlay = $('#edit_element_menu_form').find('input[name=background_overlay]');
    var styles = $('#edit_element_menu_form').find('textarea[name=styles]');
    var classes = $('#edit_element_menu_form').find('input[name=classes]');
    var modal = $('#edit_element_menu_form').find('input[name=modal]');
    var details = $('#edit_element_menu_form').find('input[name=details]');
    var type = $('#edit_element_menu_form').find('input[name=type]');

    // Populate elements
    element.html(html.val());
    // Reset style
    element.attr('style','');
    // if (type.val().substring(0, 10) == 'page-block') {
    //     element.attr('style','display:table;position:relative');
    // }
    if(modal.val() !== '') {
        element.attr('data-modal',modal.val());
    } else {
        element.removeAttr('data-modal');
    }
    if(details.val() !== '') {
        element.attr('data-details',details.val());
    } else {
        element.removeAttr('data-details');
    }
    if(classes.val() !== '') {
        element.addClass(classes.val());
    }
    element.attr('src',base_url + image.val());
    if(height.val() !== '') {
        element.css('height',height.val());
    }
    element.css('width',width.val());
    element.css('margin',margin.val());
    element.css('padding',padding.val());
    element.css('font-size',font_size.val());
    element.css('color',color.val());
    element.css('text-align',text_align.val());
    element.css('background-color',background_color.val());
    element.find('.page-block-overlay').css('background-color',background_overlay.val());
    if(background_image.val() !== '') {
        element.css('background-image','url('+base_url+'/'+encodeURI(background_image.val())+')');
    }

    // Add inline styles at the end
    element.attr('style',element.attr('style') + ';' + styles.val());
    // Resize to set height equal to sibling
    $(window).resize();
    $.ajax({
        url : base_url +'page/element/'+element.attr('id')+'/update',
        type: "POST",
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        },
        data: {
            html: html.val(),
            height: height.val(),
            type: type.val(),
            width: width.val(),
            margin: margin.val(),
            padding: padding.val(),
            font_size: font_size.val(),
            color: color.val(),
            text_align: text_align.val(),
            image: image.val(),
            background_color: background_color.val(),
            background_image: background_image.val(),
            background_overlay: background_overlay.val(),
            style: styles.val(),
            class: classes.val(),
            modal: modal.val(),
            details: details.val(),
        },
        beforeSend: function() {
            $('.edit-element-save-button').html('Saving...');
        },
        success: function(data) {
            console.log(data);
            $('.edit-element-save-button').html('Save Changes');
        },
        error: function (data) {
            alert('An unknown error occured.');
        }
    });

});

// Select the element you would like to edit
$(document).on('click', '[data-edit-element]', function(e) {
    element_id = $(this).attr('data-edit-element');
    element = $('#'+element_id); // Select by matching id
    $('.edit-element-save-button').attr('disabled',true);
    // Show edit view
    $.get(base_url + '/element/'+element_id+'/edit', function(response){ 
        $('.edit-element-menu-wrapper').html(response);
        $('.close-edit-element-menu').fadeIn();
        $('.edit-element-menu, .edit-element-save-button').addClass('animated fadeInLeft');
        $('.edit-element-menu, .edit-element-save-button').show();
        setTimeout(function() {
            $('.edit-element-menu, .edit-element-save-button').removeClass('animated fadeInLeft'); // Remove after delay
        }, 1200);
    });
});

// Show view to choose element type
$(document).on('click', '[data-add-element]', function(e) {
    block_id = $(this).attr('data-add-element');
    $.get(base_url + 'block/'+block_id+'/element/add', function(response){ 
        $('.edit-element-menu-wrapper').html(response);
        // alert(response);
        $('.close-add-element-menu').fadeIn();
        $('.add-element-menu, .add-element-save-button').addClass('animated fadeInLeft');
        $('.add-element-menu, .add-element-save-button').show();
        setTimeout(function() {
            $('.add-element-menu, .add-element-save-button').removeClass('animated fadeInLeft'); // Remove after delay
        }, 1200);
    });
});

// Show view to edit element of type
$(document).on('click', '[data-create-element]', function(e) {
    uri = $(this).attr('data-create-element');
    $.get(base_url + uri, function(response) {
        $('.edit-element-menu-wrapper').html(response);
        $('.close-edit-element-menu').fadeIn();
        $('.edit-element-menu, .edit-element-save-button, .add-element-to-block-button').addClass('animated fadeInLeft');
        $('.edit-element-menu, .edit-element-save-button').show();
        setTimeout(function() {
            $('.edit-element-menu, .edit-element-save-button').removeClass('animated fadeInLeft'); // Remove after delay
        }, 1200);
    });
});

var mouseout_timeout;
$(document).on('mouseover','.page-element-wrapper',function(e) {
    // if (mouseout_timeout !== 'undefined') {
    //     clearTimeout(mouseout_timeout); // prevent
    // }
    $(this).find('.page-editor-element-toolbar').addClass('visible');
});
$(document).on('mouseout','.page-element-wrapper',function(e) {
    $(this).find('.page-editor-element-toolbar').addClass('visible');
    var $this = $(this);
    mouseout_timeout = setTimeout(function() {
        $($this).find('.page-editor-element-toolbar').removeClass('visible');
    }, 200);
});

// Save the element and update the page
$(document).on('click','[add-element-to-block]',function(e) {
    block_id = $(this).attr('add-element-to-block');
    $.ajax({
        url : $('#add_element_to_block_form').attr('action'),
        type: "POST",
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        },
        data: $('#add_element_to_block_form').serialize(),
        beforeSend: function() {
            $('.add-element-to-block-button').attr('disabled',true);
            $('.add-element-to-block-button').html('Saving...');
        },
        success: function(data) {
            $('.close-edit-element-menu').click();
            // Append data to the block
            // console.log(block_id);
            $('#'+block_id + ' .page-block').append(data);
            // Resize to set height equal to sibling
            $(window).resize();
            if (data.error !== undefined) { // Error
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('error');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('error');
                } else {
                    $('#alert').addClass('error');
                }
                showAlert(data.error);
            }
        },
        error: function (data) {
            alert('An unknown error occured.');
        }
    });
});


$(document).on('click','[data-delete-element]',function(e) {
    delete_uri = $(this).attr('data-delete-element');
    if (window.confirm("Are you sure you want to delete this element? This cannot be undone.")) {
        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="_token"]').attr('content')
            },
            type: "post",
            url: base_url + delete_uri,
            data: {
                _method: 'DELETE',
                // _token: $('meta[name="_token"]').attr('content'),
            },
            success: function(data) {
                if (data.success !== undefined) { // Success
                    if (parent.parent !== undefined) {
                        parent.parent.$('#alert').addClass('success');
                    }
                    if (parent !== undefined) {
                        parent.$('#alert').addClass('success');
                    } else {
                        $('#alert').addClass('success');
                    }
                    showAlert(data.success);
                    element.parent('.page-element-wrapper').remove();
                     $(document).find('.close-edit-element-menu').click();
                     // console.log($(document).find('.close-edit-element'));
                }
                if (data.error !== undefined) { // Error
                    if (parent.parent !== undefined) {
                        parent.parent.$('#alert').addClass('error');
                    }
                    if (parent !== undefined) {
                        parent.$('#alert').addClass('error');
                    } else {
                        $('#alert').addClass('error');
                    }
                    showAlert(data.error);
                } else {
                    // alert('Successfully deleted');
                }

            },
            error: function (data) {
                $(document).find('footer').before(data.responseText);
            }
        });
    }
});

if ($('#page').length) {

    // Sort sections
    // var sort_sections = new Sortable(document.getElementById('page'),  {
    //     animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
    //     handle: ".uk-icon-arrows-v", // Restricts sort start click/touch to the specified element
    //     draggable: ".page-section", // Specifies which items inside the element should be sortable
    //     onUpdate: function (event){
    //         var item = event.item; // the current dragged HTMLElement
    //         // console.log(item.id);
    //         var order = event.newIndex;
    //         // $.ajax({
    //         //     url : 'element/' + order + '/order',
    //         //     type: "POST",
    //         //     data: { order: order },
    //         //     success: function(data) {
    //         //         console.log('order updated');
    //         //     },
    //         //     error: function (data) {
    //         //         alert('An unknown error occured.');
    //         //     }
    //         // });
    //     }
    // });

    // Sort page blocks
    // $.each($('.uk-grid'), function(index, value) {
    //     console.log(index + ':' + value);
    //     Sortable.create(value,  {
    //         animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
    //         handle: ".uk-icon-arrows-h", // Restricts sort start click/touch to the specified element
    //         draggable: ".page-block-wrapper", // Specifies which items inside the element should be sortable
    //         onUpdate: function (event){
    //             console.log(event.newIndex);
    //         }
    //     });
    // });


$(document).on('mouseover','.uk-icon-arrows-v',function(e) {
    sortable_parent = $('#page')[0];
    // Sort page elements
    // $.each($('.page-block'), function(index, value) { 
        // console.log(sortable_parent);
        // new Sortable(sortable_parent,  {
        //     animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
        //     group: 'elements',
        //     handle: ".uk-icon-arrows", // Restricts sort start click/touch to the specified element
        //     draggable: ".page-element-wrapper", // Specifies which items inside the element should be sortable
        //     onUpdate: function (event) {
        //         console.log(event.newIndex);
        //     },
        // });
        Sortable.create(sortable_parent,  {
        animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
        handle: ".uk-icon-arrows-v", // Restricts sort start click/touch to the specified element
        draggable: ".page-section", // Specifies which items inside the element should be sortable
        onUpdate: function (event){
            var item = event.item; // the current dragged HTMLElement
            var order = event.newIndex;
            $.each($('.page-section'), function(index, value) { 
                // Get id's in order they appear
                // console.log(value.id);
                // Update the order for each element as it appears
                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                    },
                    type: "post",
                    url: base_url + 'element/'+value.id+'/order',
                    data: {
                        order: index,
                        // _token: $('meta[name="_token"]').attr('content'),
                    },
                    success: function(data) {
                        if (data.error !== undefined) { // Error
                            if (parent.parent !== undefined) {
                                parent.parent.$('#alert').addClass('error');
                            }
                            if (parent !== undefined) {
                                parent.$('#alert').addClass('error');
                            } else {
                                $('#alert').addClass('error');
                            }
                            showAlert(data.error);
                        }
                    },
                    error: function (data) {
                        console.log('An error occured on reorder. Please refresh and try again.');
                    }
                });
            });
        }
    });
});

$(document).on('mouseover','.uk-icon-arrows',function(e) {
    sortable_parent = $(this).parent().parent().parent('.page-block')[0];
    sortable_parent_obj = $(this).parent().parent().parent('.page-block');
    // Sort page elements
    // $.each($('.page-block'), function(index, value) { 
        console.log(sortable_parent);
        // new Sortable(sortable_parent,  {
        //     animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
        //     group: 'elements',
        //     handle: ".uk-icon-arrows", // Restricts sort start click/touch to the specified element
        //     draggable: ".page-element-wrapper", // Specifies which items inside the element should be sortable
        //     onUpdate: function (event) {
        //         console.log(event.newIndex);
        //     },
        // });
    Sortable.create(sortable_parent,  {
        animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
        handle: ".uk-icon-arrows", // Restricts sort start click/touch to the specified element
        draggable: ".page-element-wrapper", // Specifies which items inside the element should be sortable
        onUpdate: function (event){
            var item = event.item; // the current dragged HTMLElement
            var order = event.newIndex;
            $.each($(sortable_parent).find('.page-element'), function(index, value) { 
                // Get id's in order they appear
                // console.log('Val:'+value.id);
                // console.log('Order:'+index);
                // Update the order for each element as it appears
                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                    },
                    type: "post",
                    url: base_url + 'element/'+value.id+'/order',
                    data: {
                        order: index,
                        // _token: $('meta[name="_token"]').attr('content'),
                    },
                    success: function(data) {
                        if (data.error !== undefined) { // Error
                            if (parent.parent !== undefined) {
                                parent.parent.$('#alert').addClass('error');
                            }
                            if (parent !== undefined) {
                                parent.$('#alert').addClass('error');
                            } else {
                                $('#alert').addClass('error');
                            }
                            showAlert(data.error);
                        }
                    },
                    error: function (data) {
                        console.log('An error occured on reorder. Please refresh and try again.');
                    }
                });
            });
        }
    });
    // });
});

$(document).on('mouseover','.uk-icon-arrows-h',function(e) {
    // alert($(this).parent().parent().parent('.uk-grid'));
    sortable_parent = $(this).parent().parent().parent().parent('.uk-grid')[0];

        // Sortable.create(sortable_parent,  {
        //     animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
        //     handle: ".uk-icon-arrows-h", // Restricts sort start click/touch to the specified element
        //     draggable: ".page-block-wrapper", // Specifies which items inside the element should be sortable
        //     onUpdate: function (event){
        //         console.log(event.newIndex);
        //     }
        // });
    // console.log(sortable_parent);

    Sortable.create(sortable_parent,  {
        animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
        handle: ".uk-icon-arrows-h", // Restricts sort start click/touch to the specified element
        draggable: ".page-block-wrapper", // Specifies which items inside the element should be sortable
        onUpdate: function (event){
            var item = event.item; // the current dragged HTMLElement
            var order = event.newIndex;
            $.each($(sortable_parent).find('.page-block-wrapper'), function(index, value) { 
                // Get id's in order they appear
                // console.log(value.id);
                // Update the order for each element as it appears
                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                    },
                    type: "post",
                    url: base_url + 'element/'+value.id+'/order',
                    data: {
                        order: index,
                        // _token: $('meta[name="_token"]').attr('content'),
                    },
                    success: function(data) {
                        if (data.error !== undefined) { // Error
                            if (parent.parent !== undefined) {
                                parent.parent.$('#alert').addClass('error');
                            }
                            if (parent !== undefined) {
                                parent.$('#alert').addClass('error');
                            } else {
                                $('#alert').addClass('error');
                            }
                            showAlert(data.error);
                        }
                    },
                    error: function (data) {
                        console.log('An error occured on reorder. Please refresh and try again.');
                    }
                });
            });
        }
    });


    // Sort page blocks
    // $.each($('.uk-grid'), function(index, value) {
    //     console.log(index + ':' + value);
    //     Sortable.create(value,  {
    //         animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
    //         handle: ".uk-icon-arrows-h", // Restricts sort start click/touch to the specified element
    //         draggable: ".page-block-wrapper", // Specifies which items inside the element should be sortable
    //         onUpdate: function (event){
    //             console.log(event.newIndex);
    //         }
    //     });
    // });
});

}

//     $.each($('.page-section'), function(index, value) { 
//         // console.log(value.id);
//         ele = document.getElementById(value.id);
// console.log(ele.id);
//         new Sortable(document.getElementById(ele.id),  {
//             animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
//             handle: ".uk-icon-arrows-h", // Restricts sort start click/touch to the specified element
//             draggable: ".page-block-wrapper", // Specifies which items inside the element should be sortable
//             onUpdate: function (event){
//                 var item = event.item; // the current dragged HTMLElement
//             }
//         });
//     });
