$(document).on('click','[data-cancel-payment]',function(e) {
    delete_uri = $(this).attr('data-cancel-payment');
    if (window.confirm("Confirm scheduled payments cancelation? This will remove future payments for this amount and customer.")) {
        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="_token"]').attr('content')
            },
            type: "post",
            url: delete_uri,
            data: {
                _method: 'DELETE',
                _token: $('meta[name="_token"]').attr('content'),
            },
            success: function(data) {
                if (data.success !== undefined) { // Success
                    if (parent.parent !== undefined) {
                        parent.parent.$('#alert').addClass('success');
                    }
                    if (parent !== undefined) {
                        parent.$('#alert').addClass('success');
                    } else {
                        $('#alert').addClass('success');
                    }
                    showAlert(data.success);
                }
                if (data.refresh !== undefined) { // Reload page
                    if (data.error !== undefined || data.success !== undefined) {
                        setTimeout(function() {
                            if (parent !== undefined) {
                                parent.window.location.reload();
                            } else {
                                window.location.reload();
                            }
                        }, 500);
                        return false;
                    }
                    if (parent !== undefined) {
                        parent.window.location.reload();
                    } else {
                        window.location.reload();
                    }
                }
                if (data.error !== undefined) { // Error
                    if (parent.parent !== undefined) {
                        parent.parent.$('#alert').addClass('error');
                        parent.parent.$('#alert').removeClass('success');
                    }
                    if (parent !== undefined) {
                        parent.$('#alert').addClass('error');
                        parent.$('#alert').removeClass('success');
                    } else {
                        $('#alert').addClass('error');
                        $('#alert').removeClass('success');
                    }
                    showAlert(data.error);
                } else {
                    alert('Successfully deleted');
                }
                if (data.closeModal !== undefined) {
                    if (parent !== undefined) {
                        parent.$('.uk-modal-close').click();
                    } else {
                       $('.uk-modal-close').click();
                    }
                }
                if (data.refreshDynamicView !== undefined) { // Reload page
                    if (parent !== undefined) {
                        parent.$('#dynamic_view').attr( 'src', function ( i, val ) { return val; });
                        setTimeout(function() {
                            parent.$('#details_view').fadeOut();
                        }, 1000);

                    } else {
                        $('#dynamic_view').attr( 'src', function ( i, val ) { return val; });
                        setTimeout(function() {
                            $('#details_view').fadeOut();
                        }, 1000);

                    }
                }
            },
            error: function (data) {
                $(document).find('footer').before(data.responseText);
            }
        });
    }
});

$(document).on('click','[data-remove-invoice-item]',function(e) {
    $(this).parent().parent('tr').remove();
    // Recalc total
    var invoice_total_sum = 0;
    $.each($('input[name="total[]"]'), function(index, value) {
       invoice_total_sum = parseFloat(invoice_total_sum) + parseFloat($(this).val());
       // console.log(invoice_total_sum);
       $('input[name=total_due]').val(invoice_total_sum.toFixed(2));
    });
});

// Adds an item to the invoice form
$(document).on('click','[data-add-invoice-item]',function(e) {
   var addButton = $('[data-add-invoice-item]');
   var table = $(this).parent().find('table');
    $.get(base_url + 'invoice/item/generate', function(response){ 
        $(table).append(response);
    });
    $(table).after(addButton);
});

// Calculate total on keyup on tax, rate or quanity
$(document).on('keyup','input[name^=rate], input[name^=quantity], input[name^=tax]',function(e) {
    var $this = $(this);
    if ($this.attr('name') == 'rate[]') {
        var rate = $this.val();
        var quantity = $this.parent().parent().next('td').find('input[name^=quantity]').val();
    } else {
        var quantity = $this.val();
        var rate = $this.parent().parent().prev('td').find('input[name^=rate]').val();
    }
    total = parseFloat(quantity)*parseFloat(rate);
    if (total > 0) {
        // If total is calculatable
        total = parseFloat(quantity)*parseFloat(rate);
        // if (total !== NaN) {
        if ($this.attr('name') == 'rate[]') {
            var $total_input = $this.parent().parent().next('td').next('td').find('input[name^=total]');
        }
        if ($this.attr('name') == 'quantity[]') {
            var $total_input = $this.parent().parent().next('td').find('input[name^=total]');
        }
        if ($total_input !== undefined) {
            if(total.toFixed(2) !== 'NaN') {
                $total_input.val(total.toFixed(2));
            } else {
                $total_input.val('');
            }
        }
    } else {
        if ($this.attr('name') == 'rate[]') {
            var $total_input = $this.parent().parent().next('td').next('td').find('input[name^=total]');
        }
        if ($this.attr('name') == 'quantity[]') {
            var $total_input = $this.parent().parent().next('td').find('input[name^=total]');
        }
        if ($total_input !== undefined) {
            if(total.toFixed(2) !== 'NaN') {
                $total_input.val(total.toFixed(2));
            } else {
                $total_input.val('');
            }
        }
    }
    // Add tax value to total if formatted correctly
    var invoice_total_sum = parseFloat($('input[name=tax]').val() || 0) ;
    $.each($('input[name="total[]"]'), function(index, value) {
       // console.log(invoice_total_sum);
       invoice_total_sum = parseFloat(invoice_total_sum) + parseFloat($(this).val());
        if(invoice_total_sum.toFixed(2) !== 'NaN') {
           $('input[name=total_due]').val(invoice_total_sum.toFixed(2));
        }
    });
});

$(document).on('keyup','[data-find-customer]',function(e) {
    var search = $(this).val();
    var list =  $('[data-customer-list]');
    // list.html(query);
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        },
        url : base_url + 'stripe/customers/find',
        type: "POST",
        data: {search: search, _token: $('meta[name="_token"]').attr('content') },
        beforeSend: function() {
            list.html('<div class="message-box">Loading...</div>');
        },
        success: function(data) {
            list.html(data);
        },
        error: function (data) {
            alert('An unknown error occured.');
            list.html(data.responseText);
        }
    });
});

$(document).on('click','[data-add-customer-to-form]',function(e) {
    var search_form_item = $(this).parent('[data-customer-list]').prev('.form-item');
    search_form_item.hide();
    var stripe_customer_id = $(this).attr('data-add-customer-to-form');
    $(this).parent('').find('.list-item').hide(); // Hide other results
    $(this).show();
    $(this).find('.list-item-icon-right').removeClass('icon-plus');
    $(this).find('.list-item-icon-right').addClass('icon-check'); // Add checkmark
    $(this).parent('[data-customer-list]').append('<input type="hidden" name="customer" value="'+stripe_customer_id+'"/>');
    $(this).removeAttr('data-add-customer-to-form'); // Remove add customer attribute
});
