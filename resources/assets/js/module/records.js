$(document).on('change','[data-choose-record-set]',function() {
    // top.location.hash = $(this).val();
    loadView($(this).val());
    // window.open($(this).val(),'_blank');
});

$(document).on('click','.pager li a',function(e) {
    e.preventDefault();
    var href = $(this).attr('href').split('?', 2); // split into parts at ?
    href= href[1]; // get part after ?
    var parent_url = window.top.location.href;
    parent_url = parent_url.split('?', 2); // split into parts at ?
    parent_url = parent_url[0]; // remove part after ?
    window.top.location.href = parent_url +'?'+ href;
    // window.open($(this).val(),'_blank');
});
