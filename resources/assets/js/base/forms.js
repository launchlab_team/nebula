// Javascript related to forms goes here
// Stimulate radio click on form-item radio
$(document).on('click','.radio-label',function() {
    // Set other inputs in the .radios set to checked = false
    $(this).parent().parent('.radios').find('form-item input').prop('checked',false);
    // Remove .checked from the other labels in the group
    $(this).parent().parent('.radios').find('.checked').removeClass('checked');
    // Toggle icon change for other icons in the .radios group
    $(this).parent().parent('.radios').find('span').not('.form-item-sublabel').addClass("icon-circle");
    $(this).parent().parent('.radios').find('span').removeClass("icon-radio-checked");
    // Select and add selection classes
    $(this).find('input').prop('checked',true);
    $(this).addClass('checked');
    // Toggle icon change
    $(this).find('span').addClass("icon-radio-checked");
    $(this).find('span').removeClass("icon-circle");
});

// Stimulate checkbox click on form-item checkbox
$(document).on('click','.checkbox-label',function(e) {
    e.preventDefault();
    $label = $(this);
    if ($label.hasClass('checked')) { // uncheck
        $label.removeClass("checked");
        $label.find('span').addClass("icon-square");
        $label.find('span').removeClass("icon-square-check");
        $label.find('input').prop('checked',false);
    } else { // check
        $label.addClass("checked");
        $label.find('span').addClass("icon-square-check");
        $label.find('span').removeClass("icon-square");
        $label.find('input').prop('checked',true);
    }
});

// Catches href for a link inside checkbox label e.g. terms of service
$(document).on('click','.checkbox-label a',function(e) {
    window.open($(this).attr('href'));
});

// Input masking vendor/mask.min.js
$(document).ready(function(){
    $('[data-date]').mask('00/00/0000');
    // $('[data-time]').mask('00:00:00');
    $('[data-time]').timepicker();
    $('[data-date_time]').mask('00/00/0000 00:00:00');
    $('[data-cep]').mask('00000-000');
    $('[data-phone').mask('(000) 000-0000');
    $('[data-money]').mask('000.000.000.000.000,00', {reverse: true});
});


/* 
------------------------------------------------------
 Upload Functions
------------------------------------------------------
*/

// Set global var last_input to current selection as modal opens
$(document).on('click','[data-image-input],[data-file-input]',function() {
    window.top.last_input = $(this);
});

// Submit file in iframe uploader when selected
$(document).on('change','#upload_input',function(e) {
    $(this).parent('#upload_form').append('<style>#upload_form:before{content:"Processing..."}</style>');
    $(this).parent('#upload_form').submit();
});

// Chooses an image and inserts value into input
$(document).on('click','[data-choose-image]',function() {
    closeModal();
    var image_source = $(this).attr('data-choose-image');
    window.top.last_input.val('Image attached');
    window.top.last_input.addClass('animated pulse');
    setTimeout(function() {
        window.top.last_input.removeClass('animated pulse');
    }, 1000);
    window.top.last_input.next('input').val(image_source);
    window.top.last_input.prev('.icon-x').show();
});

$(document).on('click','[data-remove-image]',function(e) {
    $(this).parent('').find('input').val('');
    $(this).hide();
});

$(document).on('click','[data-choose-file]',function() {
    closeModal();
    var file_source = $(this).attr('data-choose-file');
    window.top.last_input.val('File attached');
    window.top.last_input.next('input').val(file_source);
});

// Submits forms via ajax with the exception of the upload form
$(document).on('submit', '[data-ajax-form]', function(e) {
    lastForm = $(this);
    // Prevent default submission
    e.preventDefault();
    // Post the form via ajax
    $.ajax({
        url : $(this).attr('action'),
        type: "POST",
        data: $(this).serialize(),
        headers: {
            'X-CSRF-Token': $(document).find('meta[name="_token"]').attr('content')
        },
        beforeSend: function() {
            $('#alert').hide();
            // lastForm.find('button[type=button]').hide();
        },
        complete: function(data) { // complete executes after success or error
            // Remove spin from any buttons in the form
            $(lastForm).find('.uk-icon-spin').removeClass('uk-icon-spin');
            // Custom modifications depending on form
            // if (lastForm.attr('id') === 'add-element-form') {
            //     alert('add element form');
            // }
        },
        success: function(data) {
            if (lastButton !== undefined) {
                lastButton.attr('disabled',false);
            }
            if (data.success !== undefined) { // Success
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('success');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('success');
                } else {
                    $('#alert').addClass('success');
                }
                showAlert(data.success);
            }
            if (data.error !== undefined) { // Error
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('error');
                    parent.parent.$('#alert').removeClass('success');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('error');
                    parent.$('#alert').removeClass('success');
                } else {
                    $('#alert').addClass('error');
                    $('#alert').removeClass('success');
                }
                showAlert(data.error);
            }
            if (data.redirect !== undefined) { // Redirect
                if (parent !== undefined) {
                    parent.window.location.href = data.redirect;
                } else {
                    window.location.href = data.redirect;
                }
            }
            if (data.closeModal !== undefined) {
                if (parent !== undefined) {
                    parent.$('.uk-modal-close').click();
                } else {
                   $('.uk-modal-close').click();
                }
            }
            if (data.closeDetails !== undefined) {
                hideDetails();
            }
            if (data.refresh !== undefined) { // Reload page
                if (data.error !== undefined || data.success !== undefined) {
                    setTimeout(function() {
                        if (parent !== undefined) {
                            parent.window.location.reload();
                        } else {
                            window.location.reload();
                        }
                    }, 800);
                    return false;
                }
                if (parent !== undefined) {
                    parent.window.location.reload();
                } else {
                    window.location.reload();
                }
            }
            if (data.refreshDynamicView !== undefined) {
                if (parent !== undefined) {
                    parent.$('#dynamic_view').attr( 'src', function ( i, val ) { return val; });
                    setTimeout(function() {
                        parent.$('#details_view').fadeOut();
                    }, 1000);
                } else {
                    $('#dynamic_view').attr( 'src', function ( i, val ) { return val; });
                    setTimeout(function() {
                        $('#details_view').fadeOut();
                    }, 1000);

                }
            }
            if (data.refreshDetailsView !== undefined) {
                if (parent !== undefined) {
                    parent.$('#details_view').attr( 'src', function ( i, val ) { return val; });
                } else {
                    $('#details_view').attr( 'src', function ( i, val ) { return val; });
                }
            }
            if (data.loadDetailsView !== undefined) {
                loadDetails(data.loadDetailsView);
            }
            if (data.loadView !== undefined) {
                loadView(data.loadView);
            }
        },
        error: function (data) {
            if (lastButton !== undefined) {
                lastButton.attr('disabled',false);
            }
            $('body').append(data.responseText);
            alert('An unknown error occured.');
            // if (parent !== undefined) {
            //     parent.window.location.reload();
            // } else {
            //     window.location.reload();
            // }
        }
    });
    return false;
});

$(document).on('click','[data-delete]',function(e) {
    delete_uri = $(this).attr('data-delete');
    var confirm_text = $(this).attr('data-confirm');
    if (confirm_text == undefined) confirm_text = 'Confirm delete? This cannot be undone.';
    if (window.confirm(confirm_text)) {
        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="_token"]').attr('content')
            },
            type: "post",
            url: delete_uri,
            data: {
                _method: 'DELETE',
                _token: $('meta[name="_token"]').attr('content'),
            },
            success: function(data) {
                if (data.success !== undefined) { // Success
                    if (parent.parent !== undefined) {
                        parent.parent.$('#alert').addClass('success');
                    }
                    if (parent !== undefined) {
                        parent.$('#alert').addClass('success');
                    } else {
                        $('#alert').addClass('success');
                    }
                    showAlert(data.success);
                }
                if (data.refresh !== undefined) { // Reload page
                    if (data.error !== undefined || data.success !== undefined) {
                        setTimeout(function() {
                            if (parent !== undefined) {
                                parent.window.location.reload();
                            } else {
                                window.location.reload();
                            }
                        }, 500);
                        return false;
                    }
                    if (parent !== undefined) {
                        parent.window.location.reload();
                    } else {
                        window.location.reload();
                    }
                }
                if (data.error !== undefined) { // Error
                    if (parent.parent !== undefined) {
                        parent.parent.$('#alert').addClass('error');
                        parent.parent.$('#alert').removeClass('success');
                    }
                    if (parent !== undefined) {
                        parent.$('#alert').addClass('error');
                        parent.$('#alert').removeClass('success');
                    } else {
                        $('#alert').addClass('error');
                        $('#alert').removeClass('success');
                    }
                    showAlert(data.error);
                } else {
                    alert('Successfully deleted');
                }
                if (data.closeModal !== undefined) {
                    if (parent !== undefined) {
                        parent.$('.uk-modal-close').click();
                    } else {
                       $('.uk-modal-close').click();
                    }
                }
                if (data.closeDetails !== undefined) {
                    hideDetails();
                }
                if (data.loadView !== undefined) {
                    loadView(data.loadView);
                }
                if (data.refreshDynamicView !== undefined) { // Reload page
                    if (parent !== undefined) {
                        parent.$('#dynamic_view').attr( 'src', function ( i, val ) { return val; });
                        setTimeout(function() {
                            parent.$('#details_view').fadeOut();
                        }, 1000);

                    } else {
                        $('#dynamic_view').attr( 'src', function ( i, val ) { return val; });
                        setTimeout(function() {
                            $('#details_view').fadeOut();
                        }, 1000);

                    }
                }
            },
            error: function (data) {
                $(document).find('footer').before(data.responseText);
            }
        });
    }
});

$(document).on('click','[data-ajax-button]',function(e) {
    e.preventDefault(); // prevent form submit
    uri = $(this).attr('data-ajax-button');
    button = $(this);
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        },
        type: "POST",
        url: uri,
        data: {
            _token: $('meta[name="_token"]').attr('content'),
        },
        success: function(data) {
            if (data.success !== undefined) { // Success
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('success');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('success');
                } else {
                    $('#alert').addClass('success');
                }
                $(button).html(data.success);
            }
            if (data.refresh !== undefined) { // Reload page
                if (data.error !== undefined || data.success !== undefined) {
                    setTimeout(function() {
                        if (parent !== undefined) {
                            parent.window.location.reload();
                        } else {
                            window.location.reload();
                        }
                    }, 500);
                    return false;
                }
                if (parent !== undefined) {
                    parent.window.location.reload();
                } else {
                    window.location.reload();
                }
            }
            if (data.error !== undefined) { // Error
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('error');
                    parent.parent.$('#alert').removeClass('success');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('error');
                    parent.$('#alert').removeClass('success');
                } else {
                    $('#alert').addClass('error');
                    $('#alert').removeClass('success');
                }
                $(button).html(data.error);
            } else {
                // alert('Successfully deleted');
            }
            if (data.refreshDynamicView !== undefined) { // Reload page
                if (parent !== undefined) {
                    parent.$('#dynamic_view').attr( 'src', function ( i, val ) { return val; });
                    setTimeout(function() {
                        parent.$('#details_view').fadeOut();
                    }, 1000);

                } else {
                    $('#dynamic_view').attr( 'src', function ( i, val ) { return val; });
                    setTimeout(function() {
                        $('#details_view').fadeOut();
                    }, 1000);

                }
            }
        },
        error: function (data) {
            $(document).find('footer').before(data.responseText);
        }
    });
});

// $(document).on('submit', '#edit_field_form', function(e) {
//     e.preventDefault();
//     field_id = $(this).find('input[name=field_id]');
//     // field_id = field_uri.split('/')[1];
//     // form_fields = parent.$('#details_view').contents().find('#form_fields');
//     // console.log(form_fields);
//     var type = $(this).find('[data-field-type]').val();
//     var label = $(this).find('[data-field-label]').val();
//     var placeholder = $(this).find('[data-field-placeholder]').val();
//     var hint = $(this).find('[data-field-hint]').val();
//     var options = $(this).find('[data-field-options]').val();
//     if(type == undefined) { type = null; } // Reset from undefined
//     if(label == undefined) { label = null; } // Reset from undefined
//     if(placeholder == undefined) { placeholder = null; } // Reset from undefined
//     if(hint == undefined) { hint = null; } // Reset from undefined
//     if(options == undefined) { options = null; } // Reset from undefined
//     if ((type == 'checkbox' ||  type == 'radio' || type == 'select') && options == '') {
//         alert('Options are a required field.');
//         return false;
//     }
//     $.get('generate?type='+type+'&options='+options+'&placeholder='+placeholder+'&hint='+hint+'&label='+label, function(response){ 
//         // Append newly generated field to the end of fields
//         // $(window.parent.document).find('#form_fields').before(response);
//         $('#'+field_id).append(response);
//         closeModal();
//         // alert(response);
//     });
    
// });

$(document).on('submit', '#create_field_form', function(e) {
    e.preventDefault();
    form_fields = parent.$('#details_view').contents().find('#form_fields');
    // console.log(form_fields);
    var type = $(this).find('[data-field-type]').val();
    var label = $(this).find('[data-field-label]').val();
    var placeholder = $(this).find('[data-field-placeholder]').val();
    var hint = $(this).find('[data-field-hint]').val();
    var options = $(this).find('[data-field-options]').val();
    if(type == undefined) { type = null; } // Reset from undefined
    if(label == undefined) { label = null; } // Reset from undefined
    if(placeholder == undefined) { placeholder = null; } // Reset from undefined
    if(hint == undefined) { hint = null; } // Reset from undefined
    if(options == undefined) { options = null; } // Reset from undefined
    if ((type == 'checkbox' ||  type == 'radio' || type == 'select') && options == '') {
        alert('Options are a required field.');
        return false;
    }
    $.get(base_url + 'field/generate?type='+type+'&options='+options+'&placeholder='+placeholder+'&hint='+hint+'&label='+label, function(response){ 
        // Append newly generated field to the end of fields
        // $(window.parent.document).find('#form_fields').before(response);
        form_fields.append(response);
        closeModal();
        // alert(response);
    });
    
});

if ($('#form_fields').length) {
    var sortable_menu = new Sortable(document.getElementById('form_fields'),  {
        animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
        handle: ".form-item-icon-draggable", // Restricts sort start click/touch to the specified element
        draggable: ".form-item-draggable-field", // Specifies which items inside the element should be sortable
        onUpdate: function (event){
            var item = event.item; // the current dragged HTMLElement
        }
    });
}

$(document).on('click','[data-edit-field]',function(e) {
    // field_type = $(this).parent().parent('.form-item-draggable-field').find('input[name=field_type]').val();
    window.top.field = $(this).parent().parent('.form-item-draggable-field');
    var type = $(this).parent().parent('.form-item-draggable-field').find('input[name^=field_type]').val();
    var label = $(this).parent().parent('.form-item-draggable-field').find('input[name^=field_label]').val();
    var hint = $(this).parent().parent('.form-item-draggable-field').find('input[name^=field_hint]').val();
    var placeholder = $(this).parent().parent('.form-item-draggable-field').find('input[name^=field_placeholder]').val();
    var options = $(this).parent().parent('.form-item-draggable-field').find('input[name^=field_options]').val();
    // If undefined pass "" instead of undefined
    if (type == undefined) type = '';
    if (label == undefined) label = '';
    if (hint == undefined) hint = '';
    if (options == undefined) options = '';
    if (placeholder == undefined) placeholder = '';
    loadModal('field/edit?type='+type+'&options='+options+'&placeholder='+placeholder+'&hint='+hint+'&label='+label);
    // $.get(base_url + 'field/edit?type='+type+'&options='+options+'&placeholder='+placeholder+'&hint='+hint+'&label='+label, function(response){ 
    //     field.after(response);
    //     field.remove();
    //     closeModal();
    //     // alert(response);
    // });
});
$(document).on('click','[data-delete-field]',function(e) {
    closeModal();
    window.top.field.slideToggle();
    setTimeout(function() {
        window.top.field.remove();
    }, 1000);
});

// Edit the field and update the form (but not save)
$(document).on('submit', '#edit_field_form', function(e) {
    // alert( window.top.field ); // defined in [data-edit-field] function
    e.preventDefault();
    var type = $(this).find('[data-field-type]').val();
    var label = $(this).find('[data-field-label]').val();
    var placeholder = $(this).find('[data-field-placeholder]').val();
    var hint = $(this).find('[data-field-hint]').val();
    var options = $(this).find('[data-field-options]').val();
    if(type == undefined) { type = null; } // Reset from undefined
    if(label == undefined) { label = null; } // Reset from undefined
    if(placeholder == undefined) { placeholder = null; } // Reset from undefined
    if(hint == undefined) { hint = null; } // Reset from undefined
    if(options == undefined) { options = null; } // Reset from undefined
    if ((type == 'checkbox' ||  type == 'radio' || type == 'select') && options == '') {
        alert('Options are a required field.');
        return false;
    }
    $.get(base_url + 'field/generate?type='+type+'&options='+options+'&placeholder='+placeholder+'&hint='+hint+'&label='+label, function(response) { 
        // Append newly generated field to the end of fields
        // console.log(response);
        // Add updated field after the field chosen to edit
        window.top.field.after(response);
        // Remove field chosen to edit
        window.top.field.remove();
        closeModal();
    });
});


// Show and properly set edit element toolbar
// $(document).on('click','[data-edit-element]',function(e) {
//     element_type = $(this).attr('data-edit-element');
//     // First define form elements
//     html = $('#edit_element_menu_form').find('textarea[name=html]');
//     height = $('#edit_element_menu_form').find('input[name=height]');
//     width = $('#edit_element_menu_form').find('input[name=width]');
//     margin = $('#edit_element_menu_form').find('input[name=margin]');
//     padding = $('#edit_element_menu_form').find('input[name=padding]');
//     font_size = $('#edit_element_menu_form').find('input[name=font_size]');
//     color = $('#edit_element_menu_form').find('input[name=color]');
//     text_align = $('#edit_element_menu_form').find('select[name=text_align]');
//     background_color = $('#edit_element_menu_form').find('input[name=background_color]');
//     background_image = $('#edit_element_menu_form').find('input[name=background_image]');
//     background_overlay = $('#edit_element_menu_form').find('input[name=overlay]');
//     styles = $('#edit_element_menu_form').find('textarea[name=styles]');
//     classes = $('#edit_element_menu_form').find('input[name=classes]');
//     modal = $('#edit_element_menu_form').find('input[name=modal]');
//     details = $('#edit_element_menu_form').find('input[name=details]');

//     // Empty all form elements
//     html.html(''); // Clear current values
//     height.val('');
//     width.val('');
//     margin.val('');
//     padding.val('');
//     font_size.val('');
//     color.val('');
//     text_align.prop('selectedIndex','0');
//     background_color.val('');
//     background_image.val('');
//     background_image.parent().find('.form-item-icon-right').click(); // clear image input
//     background_overlay.val('');
//     styles.val('');
//     classes.val('');
//     // attributes.val('');
//     // Hide all form elements to be shown only if applicable
//     html.parent('.form-item').hide();
//     height.parent('.form-item').hide();
//     width.parent('.form-item').hide();
//     margin.parent('.form-item').hide();
//     padding.parent('.form-item').hide();
//     font_size.parent('.form-item').hide();
//     color.parent('.form-item').hide();
//     text_align.parent('.form-item').hide();
//     background_color.parent('.form-item').hide();
//     background_image.parent('.form-item').hide();
//     background_overlay.parent('.form-item').hide();
//     styles.parent('.form-item').hide();
//     classes.parent('.form-item').hide();
//     // attributes.parent('.form-item').hide();
//     // Hide content tab since it only applies to page elements
//     html.parent('.form-item').parent().parent('.accordion-small').hide();
//     if (element_type == 'page-block') {
//         // Page blocks
//         element = $(this).closest('.page-block');
//         // Show applicable form elements
//         height.parent('.form-item').show();
//         padding.parent('.form-item').show();
//         font_size.parent('.form-item').show();
//         color.parent('.form-item').show();
//         text_align.parent('.form-item').show();
//         background_color.parent('.form-item').show();
//         background_image.parent('.form-item').show();
//         background_overlay.parent('.form-item').show();
//         styles.parent('.form-item').show();
//         classes.parent('.form-item').show();

//         // Populate applicable form elements
//         height.val($(element).prop("style")['height']);
//         padding.val($(element).prop("style")['padding']);
//         font_size.val($(element).prop("style")['font-size']);
//         color.val($(element).prop("style")['color']);
//         text_align.find('option[value="'+$(element).prop("style")['text-align']+'"]').prop('selected',true);
//         background_color.val($(element).prop("style")['background-color']);
//         background_overlay.val(element.prev('.page-block-overlay').attr('style'));
//         if ($(element).prop("style")['background-image'] !== '') {
//             background_image.parent().find('.icon-x').show();
//             background_image.parent().find('input[type=text]').val('Image Attached');
//             background_image.val($(element).prop("style")['background-image'].replace('url(','').replace(')','').replace(/\"/gi, ""));
//         }
//         styles.val($(element).attr('style'));
//         classes.val($(element).attr('class'));
//         // attributes.parent('.form-item').show();
//         // attributes.val($(element).attr());
//         // alert(element.attributes());
//     } else if (element_type == 'page-section') {
//         element = $(this).closest('.page-section');
//         // Show applicable form elements
//         background_color.parent('.form-item').show();
//         background_image.parent('.form-item').show();
//         height.parent('.form-item').show();
//         styles.parent('.form-item').show();
//         classes.parent('.form-item').show();
//         // attributes.parent('.form-item').show();
//         // Populate applicable form elements
//         background_color.val($(element).prop("style")['background-color']);
//         if ($(element).prop("style")['background-image'] !== '') {
//             background_image.parent().find('.icon-x').show();
//             background_image.parent().find('input[type=text]').val('Image Attached');
//             background_image.val($(element).prop("style")['background-image'].replace('url(','').replace(')','').replace(/\"/gi, ""));
//         }
//         height.val($(element).prop("style")['height']);
//         styles.val($(element).attr('style'));
//         classes.val($(element).prop("style")['classes']);
//         // attributes.val($(element).attr());
//         // alert(element.attributes());
//     } else {
//         element = $(this).closest('.page-element');
//         // Show content tab
//         html.parent('.form-item').parent().parent('.accordion-small').show();
//         // Show applicable form elements
//         html.parent('.form-item').show();
//         height.parent('.form-item').show();
//         width.parent('.form-item').show();
//         margin.parent('.form-item').show();
//         padding.parent('.form-item').show();
//         font_size.parent('.form-item').show();
//         text_align.parent('.form-item').show();
//         color.parent('.form-item').show();
//         styles.parent('.form-item').show();
//         background_color.parent('.form-item').show();
//         background_image.parent('.form-item').show();
//         classes.parent('.form-item').show();
//         // attributes.parent('.form-item').show();
        
//         // Populate applicable form elements
//         html.html(element.find(':last').html());
//         height.val($(element).prop("style")['height']);
//         width.val($(element).prop("style")['width']);
//         margin.val($(element).prop("style")['margin']);
//         padding.val($(element).prop("style")['padding']);
//         font_size.val($(element).prop("style")['font-size']);
//         color.val($(element).prop("style")['color']);
//         text_align.val($(element).prop("style")['text-align']);
//         if ($(element).prop("style")['background-image'] !== '') {
//             background_image.parent().find('.icon-x').show();
//             background_image.parent().find('input[type=text]').val('Image Attached');
//             background_image.val($(element).prop("style")['background-image'].replace('url(','').replace(')','').replace(/\"/gi, ""));
//         }
//         styles.val($(element).attr('style'));
//         classes.val($(element).attr('class'));
//         // attributes.val($(element).attr());
//         // alert(element.attributes());
//     }
//     $('.close-edit-element-menu').fadeIn();
//     $('.edit-element-menu, .edit-element-save-button').addClass('animated fadeInLeft');
//     $('.edit-element-menu, .edit-element-save-button').show();
//     setTimeout(function() {
//         $('.edit-element-menu, .edit-element-save-button').removeClass('animated fadeInLeft'); // Remove after delay
//     }, 1200);
// });

$(document).on('click','.checkboxes .checkbox-label',function(e) {
    // alert($(this).find('input').val());
    var checkboxes_input_value = '';
    // Loop through checkboxes to find the selected values
    $(this).parent('.checkboxes').find('.checkbox-label.checked').each(function(){
        if (checkboxes_input_value !== '') {
            checkboxes_input_value = checkboxes_input_value + ', ' + $(this).find('input').val().trim();
        } else {
            checkboxes_input_value = $(this).find('input').val().trim();
        }
    });
    // Fill master checkbox input with values
    $(this).parent('.checkboxes').find('input[type=hidden]').val(checkboxes_input_value);
});

// Disable buttons to prevent multiple clicks
var lastButton;
$(document).on('submit','form',function(e) {
    if (lastButton !== undefined) {
        lastButton.attr('disabled',true);
    }
});
$(document).on('click','button',function(e) {
    lastButton = $(this);
});

// $( "[data-date]" ).datepicker();
$( "[data-date]" ).datepicker({
    dateFormat: "mm/dd/y",
});


$(document).on('click','[data-contact-nebula] button',function(e) {
    $(this).html('Submiting...');
    e.preventDefault();
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        },
        type: "post",
        url: base_url + 'contact',
        data: {
            _method: 'POST',
            _token: $('meta[name="_token"]').attr('content'),
        },
        data: $('[data-contact-nebula]').serialize(),
        success: function(data) {
            if (data.success !== undefined) { // Success
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('success');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('success');
                } else {
                    $('#alert').addClass('success');
                }
                showAlert(data.success);
            }
            if (data.error !== undefined) { // Error
                if (parent.parent !== undefined) {
                    parent.parent.$('#alert').addClass('error');
                    parent.parent.$('#alert').removeClass('success');
                }
                if (parent !== undefined) {
                    parent.$('#alert').addClass('error');
                    parent.$('#alert').removeClass('success');
                } else {
                    $('#alert').addClass('error');
                    $('#alert').removeClass('success');
                }
                showAlert(data.error);
            } else {
                // alert('Thanks! We\'ll send you an email shortly.');
                $('[data-contact-nebula] button').html('Thanks!');
            }
        },
        error: function (data) {
            $(document).find('footer').before(data.responseText);
        }
    });
});

