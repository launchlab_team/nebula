var base_url = window.location.protocol + '//' + window.location.host + '/';

function isScrolledIntoView(elem) {
    var $elem = $(elem);
    if ($elem.length) {    
      var $window = $(window);
      var docViewTop = $window.scrollTop();
      var docViewBottom = docViewTop + $window.height();
      var elemTop = $elem.offset().top;
      var elemBottom = elemTop + $elem.height();
      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }
}

/* 
------------------
 Layout Functions
------------------ 
*/

// Loads content into #dynamic_view section
function loadView(view) {
    // Scroll to view
    var view = (typeof view === 'undefined') ? '404' : view;
    hideDetails();
    showLoader();
    if (window == undefined) {
        // Remove min height from #sidebar and #main parent
        // $('#sidebar').css('min-height','100vh');
        // $('#main').css('min-height','100vh');        
        // $('#sidebar').css('height','100vh');
        // $('#main').css('height','100vh');
        // empty the container so the page is not too long
        // $('.container').html('');
        // Reset 100vh
        // $('.container').css('height','100vh');

        // // scroll to top of iframe
        // $('html, body').stop().animate({
        //   'scrollTop': $('#dynamic_view').offset().top-40
        // });
        $('#dynamic_view').attr('src', base_url + view);
        jQuery('html,body').animate({scrollTop:0},0);
        document.getElementById('dynamic_view').onload= function() {
            hideLoader();
            $('#dynamic_view').show();
            $('#main').css('min-height','100vh');
        };
    } else {
        // window.top.$('#sidebar').css('min-height','100vh');
        // window.top.$('#main').css('min-height','100vh');
        // $('#sidebar').css('height','100vh');
        // $('#main').css('height','100vh');
        // window.top.$('#sidebar').css('min-height','0px');
        // window.top.$('#main').css('min-height','0px');
        // window.top.$('.container').html('');
        // window.top.$('.container').css('height','100vh');
        // window.top.$('html, body').stop().animate({
        //   'scrollTop': window.top.$('#dynamic_view').offset().top-40
        // });
        jQuery('html,body').animate({scrollTop:0},0);
        // window.top.$('.container').css('min-height','100vh');
        window.top.$('#dynamic_view').attr('src', base_url + view);
        window.top.document.getElementById('dynamic_view').onload= function(e) {
            hideLoader();
        };
        window.top.$('#dynamic_view').show();
        window.top.$('#main').css('min-height','100vh');
        // window.top.$('#main').css('min-height',$(window).height() - $('#footer').height() + 10);
        // window.top.$('#main').css('min-height','100vh');
    }
}

// $(document).onload('#dynamic_view', function() {
//     alert('loaded');
// });

function loadModal(view) {
    if (window == undefined) {
        $('#modal_iframe').attr('src', 'about:blank');
    } else {
        window.top.$('#modal_iframe').attr('src', 'about:blank');
    }
    setTimeout(function() {
        if (window == undefined) {
            $('#modal_iframe').attr('src', base_url + view);
            var modal = UIkit.modal("#modal", {center: true});
        } else {
            window.top.$('#modal_iframe').attr('src', base_url + view);
            var modal = window.top.UIkit.modal("#modal", {center: true});
        }
        if ( modal.isActive() ) {
            modal.hide();
        } else {
            modal.show();
        }
    }, 100);
}

function loadDetails(view) {
    var view = (typeof view === 'undefined') ? '404' : view;


    // Reset the iframe first
    // if (window == undefined) {
    //     $('#details_view').attr('src', 'about:blank');
    // } else {
    //     window.top.$('#details_view').attr('src', 'about:blank');
    // }

    // Empty
    if (window == undefined) {
        $("#details_view").contents().find("body").html('');
    } else {
        window.top.$("#details_view").contents().find("body").html('');
    }

    // setTimeout(function() {
        if (window == undefined) {
            $('#details_view').css('top','0px');
            $('#details_view').show();
            $('#details_view').attr('src', base_url + view);
            jQuery('html,body').animate({scrollTop:0},0);
            // $('.container').hide();
            // Reset 100vh
            // $('.container').css('height','100vh');
        } else {
            // window.top.$('.container').hide();
            // window.top.$('.container').css('height','100vh');
            window.top.jQuery('html,body').animate({scrollTop:0},0);
            window.top.$('#details_view').css('top','0px');
            window.top.$('#details_view').show();
            window.top.$('#details_view').attr('src', base_url + view);
        }
        // document.getElementById('details_view').onload= function() {
        //     $('#details_view').css('opacity','1');
        //     $('#details_view').css('top','0px');
        // };
    // }, 100);  

    // Reset height for mobile bug fix
    window.top.$('#details_view').height(window.top.$('#dynamic_view').height()+'px');
}

function showAddMenu() {
}

function showLoader() {
    // if (window == undefined) {
        $('#loader').fadeIn(200);
        $('#loader').css('display','block');
    // } else {
    //     // window.top.$('#loader').addClass('vanishIn');
    //     window.top.$('#loader').addClass('vanishIn');
    //     window.top.$('#loader').css('display','block');
    // }
}

function hideLoader() {
    // if (window == undefined) {
        // $('#loader').addClass('animated blurOut');
        $('#loader').fadeOut();
    // } else {
    //     window.top.$('#loader').addClass('vanishOut');
    //     window.top.$('#loader').fadeOut();
    // }
}

// function redirectModal(view) {
//     if (window == undefined) {
//         $('#modal_iframe').attr('src', base_url + view);
//     } else {
//         window.top.$('#modal_iframe').attr('src', base_url + view);
//     }
// }

function rotate(element) {
    $(element).addClass('uk-icon-spin');
}

function hideDetails() {
    if (window !== undefined) {
        // window.top.$('#details_view').removeClass('slideInUp');
        // window.top.$('#details_view').addClass('slideOutDown');
        window.top.$('#details_view').fadeOut();
        // setTimeout(function() {
            // window.top.$('#details_view').removeClass('slideOutDown');
            // window.top.$('#details_view').hide();
        // }, 1000);
        // $('#details_view').css('top','100%');
        // $('#details_view').fadeOut(500);
    } else {
        // $('#details_view').removeClass('slideInUp');
        // $('#details_view').addClass('slideOutDown');
        $('#details_view').fadeOut();
        // setTimeout(function() {
        //     $('#details_view').removeClass('slideOutDown');
        // }, 1000);
        // window.top.$('#details_view').css('top','100%');
        // window.top.$('#details_view').fadeOut(500);
    }
}

function closeModal() {
    if (window == undefined) {
        var modal = UIkit.modal("#modal", {center: true});
    } else {
        var modal = window.top.UIkit.modal("#modal", {center: true});
    }
    if ( modal.isActive() ) {
        modal.hide();
    }
}

/* 
-----------------
 Alert Functions
-----------------
*/

function showAlert(message) {
    if (window == undefined) {
        $('#alert').show();
        animate($('#alert'),'fadeInUp');
        $('#alert .alert-text').html(message);
        setTimeout(function() {
            $('#alert .alert-close').click();
        }, 3000);
    } else {
        window.top.$('#alert').show();
        animate(window.top.$('#alert'),'fadeInUp');
        window.top.$('#alert .alert-text').html(message);
        setTimeout(function() {   
            window.top.$('#alert .alert-close').click();
        }, 3000);
    }
}

/* 
---------------------
 Animation Functions
---------------------
*/

function animate(selector,animation) {
    // add animation classes
    $(selector).addClass('animated ' + animation);
    if ($(selector).is(':hidden')) $(selector).show();
    // remove after 1 second
    setTimeout(function () {
        $(selector).removeClass('animated ' + animation);
    }, 1000);
}

function animateOut(selector,animation) {
    animate(selector,animation);
    setTimeout(function () {
        $(selector).removeClass('animated ' + animation);
        $(selector).hide();
    }, 1000);
}

/* 
------------------------------------------------------
 Click Functions
------------------------------------------------------
*/

$(document).on('click', '[data-click]', function() {
    var selector = $(this).attr("data-click");
    $(selector).click();
});

$(document).on('click', '[data-focus]', function() {
    var selector = $(this).attr("data-focus");
    $(selector).focus();
});

$(document).on('click', '[data-submit]', function() {
    var selector = $(this).attr("data-submit");
    $(selector).submit();
});

$(document).on('click', '[data-fadeIn]', function() {
    var selector = $(this).attr("data-fadeIn");
    $(selector).fadeIn();
});

$(document).on('click', '[data-fadeOut]', function() {
    var selector = $(this).attr("data-fadeOut");
    $(selector).fadeOut();
});

$(document).on('click', '[data-hide]', function() {
    var selector = $(this).attr("data-hide");
    $(selector).hide();
});

$(document).on('click', '[data-show]', function() {
    var selector = $(this).attr("data-show");
    $(selector).show();
});

$(document).on('click', '[data-slideToggle]', function() {
    var selector = $(this).attr("data-slideToggle");
    $(selector).slideToggle();
});

$(document).on('click', '[data-fadeToggle]', function() {
    var selector = $(this).attr("data-fadeToggle");
    $(selector).fadeToggle();
});

