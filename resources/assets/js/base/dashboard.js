// data-add-manager
// data-add-member
$(document).on('click', '[data-add-manager]', function() {
    $(this).parent('form').submit();
});

// Shows all elements with settings-visible class
$(document).on('click', '[data-toggle-settings]', function() {
    if ($('.settings-visible').is(':visible')) {
        // $(this).addClass('rotate-reset');
        $('.settings-visible').addClass('animated vanishOut');
        $(this).find('.list-item-text').html('Customize');
        setTimeout(function () {
            $('.settings-visible').hide();
            $('.settings-visible').removeClass('animated vanishOut');
        }, 1000);
    } else {
        // $(this).removeClass('rotate-reset');
        // $(this).addClass('rotate');
        $('.settings-visible').show();
        $(this).find('.list-item-text').html('Exit Customize');
        $('.settings-visible').addClass('animated vanishIn');
        setTimeout(function () {
            $('.settings-visible').removeClass('animated vanishIn');
        }, 1000);
    }
});

if ($('#sortable_sidebar').length) {
    var sortable_menu = new Sortable(document.getElementById('sortable_sidebar'),  {
        animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
        handle: ".icon-draggable", // Restricts sort start click/touch to the specified element
        draggable: ".tab", // Specifies which items inside the element should be sortable
        onUpdate: function (event){
            var item = event.item; // the current dragged HTMLElement
            // submit the form to update modules
            $('#sortable_sidebar').submit();
        }
    });
}

// if ($('#sortable_menu').length) {
//     var sortable_menu = new Sortable(document.getElementById('sortable_menu'),  {
//         animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
//         handle: ".icon-draggable", // Restricts sort start click/touch to the specified element
//         draggable: "button", // Specifies which items inside the element should be sortable
//         // onUpdate: function (evt/**Event*/){
//         //     var item = evt.item; // the current dragged HTMLElement
//         // }
//     });
// }

// $(document).on('click', '[data-add-manager]', function() {
//     var manager_id = $(this).attr('data-add-manager');
//     $.ajax({
//         url : base_url + 'manager/add',
//         type: "POST",
//         data: { manager_id: manager_id },
//         beforeSend: function() {
//         },
//         success: function(data) {
//             if (data.success !== undefined) { // Success
//                 if (parent.parent !== undefined) {
//                     parent.parent.$('#alert').addClass('success');
//                 }
//                 if (parent !== undefined) {
//                     parent.$('#alert').addClass('success');
//                 } else {
//                     $('#alert').addClass('success');
//                 }
//                 showAlert(data.success);
//             }
//             if (data.error !== undefined) { // Error
//                 if (parent.parent !== undefined) {
//                     parent.parent.$('#alert').addClass('error');
//                 }
//                 if (parent !== undefined) {
//                     parent.$('#alert').addClass('error');
//                 } else {
//                     $('#alert').addClass('error');
//                 }
//                 showAlert(data.error);
//             }
//             if (data.redirect !== undefined) { // Redirect
//                 if (parent !== undefined) {
//                     parent.window.location.href = data.redirect;
//                 } else {
//                     window.location.href = data.redirect;
//                 }
//             }
//         },
//         error: function (data) {
//             $('body').appendText(data.responseText);
//             alert('An unknown error occured.');
//         }
//     });
// });
