// Javascript related to layout goes here
$(document).on('click', '[data-view]', function() {
    $('.selected').removeClass('selected');
    $(this).addClass('selected');
    var view = $(this).attr('data-view');
    loadView(view);
});

$(document).on('click', '[data-details]', function() {
    var details = $(this).attr('data-details');
    loadDetails(details);
});

// When a hash is set on load, it will be loaded into #dynamic_view (dashboard only)
if (location.pathname.substring(location.pathname.lastIndexOf("/") + 1) == 'dashboard') {
    // alert(location.pathname.substring(location.pathname.lastIndexOf("/") + 1));
    if (window.location.hash.length) {
        loadView(window.location.hash.substring(1));
        $('a[href="#'+window.location.hash.substring(1).split("/")[0]+'"]').parent('.tab').addClass('selected');
        if (('a[href="#'+window.location.hash.substring(1)+'"]').length) {
            $('a[href="#'+window.location.hash.substring(1)+'"]').parent('.tab').addClass('selected');
        }
    }

    // When an href beginning with # is set, it will be loaded into the #dynamic_view
    $(window).on('hashchange', function() {
        // Remove other tab selections
        $('.tab.selected').removeClass('selected');
        // $('#offcanvas_sortable_sidebar .tabs, #sortable_sidebar .tabs').find('.tab.selected').removeClass('selected');
        // Select tab e.g. #reservations/schedule/36 selects #reservations
        $('a[href="#'+window.location.hash.substring(1).split("/")[0]+'"]').parent('.tab').addClass('selected');
        // If there's a perfect match for the entire has string, select that (e.g. as in href="#records/list")
        if (('a[href="#'+window.location.hash.substring(1)+'"]').length) {
            $('a[href="#'+window.location.hash.substring(1)+'"]').parent('.tab').addClass('selected');
        }
        if(location.pathname !== '/login') {
            if (window.location.hash.length) {
                var uri = window.location.hash.substring(1);
                loadView(uri);
            } else { // show the default list
                hideDetails();
            }
        }
    });
}

$(document).on('click', '.tabs .tab', function() {
    window.location.hash = ''; // reset hash
    $(this).parent('.tabs').find('.tab.selected').removeClass('selected');
    $(this).parent().parent('.tabs').find('.tab.selected').removeClass('selected'); // for tabs inside draggable parent
    $(this).addClass('selected');
});


// Added for better mobile experience. Modal was being hidden when text was entered on mobile
// $(document).on('keydown', 'input, textarea', function () {
//     setTimeout(function () {
//         window.scrollTo(document.body.scrollLeft, document.body.scrollTop);
//         window.resize();
//     }, 0);
// });

// $(document).on('click', '.horizontal-tabs .tab', function() {
//     window.location.hash = ''; // reset hash
//     $(this).parent('.horizontal-tabs ').find('.tab.selected').removeClass('selected');
//     $(this).parent().parent('.horizontal-tabs ').find('.tab.selected').removeClass('selected'); // for tabs inside draggable parent
//     $(this).addClass('selected');
// });

    // $("select").each(function(){
    //     $(this).size = $(this).length;
    // };​
// jQuery(document).ready(function() {
// });​

// $(document).on('click', 'a', function() {
//     if (window.location.hash.length) {
//         var uri = window.location.hash.substring(1);
//         loadView(uri);
//         $('.tab').removeClass('selected'); 
//         var hash = window.location.hash;
//         console.log(hash);
//         $('a[href="'+hash+'"]').parent('.tab').addClass('selected');
//     } else { // show the default list
//         hideDetails();
//     }
// });

$(document).on('click', '[data-modal]', function() {
    UIkit.offcanvas.hide([force = false]);
});

$(document).on('click', '.sidebar-toolbar-toggle', function() {
    $('.sidebar-toolbar').slideToggle(500);
    $('.sidebar-toolbar-toggle-arrow ').toggleClass('icon-angle-right icon-angle-down');
});

