$(document).on('keyup', '.searchable-list-input', function() {    
    var input = $(this).find('input').val();
    var action = $(this).parent('form').attr('action');
    var results = $(this).parent('form').parent('.searchable-list').find('.searchable-list-results');
    if (action !== undefined) {
        $.ajax({
            url : action,
            type: "POST",
            headers: {
                'X-CSRF-Token': $('meta[name="_token"]').attr('content')
            },
            data: {
                query: input
            },
            beforeSend: function() {
                // Clear results object
                $(results).hide();
            },
            success: function(data) {
                // alert(data);
                if (data.length > 1) {
                    results.show();
                } else {
                    results.hide();
                }
                results.html(data);
            },
            error: function (data) {
                alert('An unknown error occured or your session timed out. Please refresh to try again.');
                // results.html(data.responseText);
            }
        });
    }
});

$(document).on('submit', '.searchable-list-form', function(e) {
    e.preventDefault();
});