// Show the search window on mobile
$(document).on('click','[data-show-search]',function() {
    $('.header-searchbar').addClass('magictime vanishIn');
    $('.header-searchbar').css('display','table-cell');
    setTimeout(function () {
        $('.header-searchbar').removeClass('magictime vanishIn');
    }, 1000);
    $('.searchbar-input').focus();
});

// Close the search window on mobile
$(document).on('click','[data-close-search]',function() {
    $('.header-searchbar').addClass('magictime vanishOut');
    $('.header-searchbar').fadeOut(1000);
    setTimeout(function () {
        $('.header-searchbar').removeClass('magictime vanishOut');
    }, 1000);
});

$(document).on('click','.searchbar-results .list-item',function() {
    setTimeout(function () {
        $(".searchbar-results").fadeOut();
        if ($('[data-show-search]').is(':visible')) {
            $("[data-close-search]").click();
        }
    }, 300);

});

$(window).on('resize', function(){
    if ($(window).width() > 769) {
        if ($('.searchbar-results').is(':hidden')) {
            $(".searchbar-results").fadeIn();
        }
        if ($('.header-searchbar').is(':hidden')) {
            $(".header-searchbar").fadeIn();
        }
        $('#header_search_input').parent().find('.icon-x').addClass('uk-hidden');
    }
});

$(document).on('keyup', '#header_search_input', function() {
    if ($(window).width() > 769) {
        if ($(this).val().length > 1) {
            $(this).parent().find('.icon-x').removeClass('uk-hidden');
        } else {
            $(this).parent().find('.icon-x').addClass('uk-hidden');
        }
    }
});

$(document).on('click', '#searchbar .icon-x', function(e) {
     $('#header_search_input').val('');
     $('#header_search_input').keyup();
});
// Show the searchbar on resize from mobile to desktop
// $(window).on('resize', function(){
//     var win = $(this); //this = window
//     if (win.width() >= 960) { 
//         $('.header-searchbar').css('display','table-cell');
//     }
// });
