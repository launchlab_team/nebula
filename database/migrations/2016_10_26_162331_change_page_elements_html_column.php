<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePageElementsHtmlColumn extends Migration
{
    public function up()
    {
        Schema::table('page_elements', function (Blueprint $table) {
            $table->string('html',10000)->change();
        });
    }

    public function down()
    {
        Schema::table('page_elements', function (Blueprint $table) {
            $table->string('html',10000)->change();
        });
    }
}
