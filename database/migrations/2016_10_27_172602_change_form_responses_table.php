<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFormResponsesTable extends Migration
{
    public function up()
    {
        Schema::table('form_responses', function (Blueprint $table) {
            $table->string('questions',5000); // json
            $table->string('response',5000)->change(); // update length
            $table->renameColumn('response','answers')->change(); // json
        });
    }

    public function down()
    {
        Schema::table('form_responses', function (Blueprint $table) {
            $table->dropColumn('questions'); // json
            $table->renameColumn('answers','response')->change(); // json
            $table->string('response')->change(); // update length
        });
    }
}
