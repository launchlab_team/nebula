<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reservation_schedule_id');
            $table->integer('reservable_id')->nullable(); // Normally required but made nullable in case of future reservations that are not connected to a reservable item
            $table->integer('user_id')->nullable();
            $table->time('start_time');
            $table->time('end_time');
            $table->date('date');
            $table->string('name');
            $table->string('email');
            $table->string('purpose',5000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservations');
    }
}
