<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_elements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id');
            $table->integer('parent_id')->nullable();
            $table->string('type');
            $table->string('class')->nullable();
            $table->string('url')->nullable();
            $table->string('image')->nullable();
            $table->string('overlay')->nullable();
            $table->string('parallax')->boolean()->default(0);
            $table->string('style')->nullable();
            $table->string('background_image')->nullable();
            $table->string('html')->nullable();
            $table->string('attributes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_elements');
    }
}
