<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function(Blueprint $table) {
            $table->increments('id');
            $table->string('number')->unique();
            $table->date('date');
            $table->date('due_date');
            $table->decimal('amount_due',10,2);
            $table->string('shared_with',3000)->nullable(); // comma seperated emails
            $table->string('logo')->nullable;
            $table->string('from',2000);
            $table->string('bill_to',2000);
            $table->string('notes',2000)->nullable();
            $table->string('items',2000); // json
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoices');
    }
}
