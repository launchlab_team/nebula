<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pipeline',2000); // comma seperated pipeline stages
            $table->string('columns',2000); // comma seperated column titles
            $table->integer('label_key');
            $table->integer('sublabel_key')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('crm');
    }
}
