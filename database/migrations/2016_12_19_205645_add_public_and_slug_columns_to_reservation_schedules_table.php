<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPublicAndSlugColumnsToReservationSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservation_schedules', function (Blueprint $table) {
            $table->boolean('public')->after('id'); // Whether or not the schedule can be viewed publicly
            $table->string('slug')->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservation_schedules', function (Blueprint $table) {
            $table->dropColumn('public');
            $table->dropColumn('slug');
        });
    }
}
