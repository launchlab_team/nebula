<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrmRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pipeline_key')->nullable(); // key in crm.pipeline
            $table->string('data',4000); // json_encoded string
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('crm_records');
    }
}
