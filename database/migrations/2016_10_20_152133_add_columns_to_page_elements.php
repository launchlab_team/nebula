<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToPageElements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('page_elements', function (Blueprint $table) {
            $table->string('order')->integer();
            $table->string('height')->nullable();
            $table->string('width')->nullable();
            $table->string('margin')->nullable();
            $table->string('padding')->nullable();
            $table->string('font_size')->nullable();
            $table->string('text_align')->nullable();
            $table->string('color')->nullable();
            $table->string('background_color')->nullable();
            $table->string('background_overlay')->nullable();
            $table->string('modal')->nullable();
            $table->string('details')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feed_items', function($table) {
            $table->dropColumn('order');
            $table->dropColumn('height');
            $table->dropColumn('width');
            $table->dropColumn('margin');
            $table->dropColumn('padding');
            $table->dropColumn('font_size');
            $table->dropColumn('text_align');
            $table->dropColumn('color');
            $table->dropColumn('background_color');
            $table->dropColumn('background_overlay');
            $table->dropColumn('modal');
            $table->dropColumn('details');
        });
    }
}
