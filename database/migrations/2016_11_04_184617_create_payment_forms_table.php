<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_forms', function(Blueprint $table) {
            $table->increments('id');
            $table->boolean('recurring')->default(0);
            $table->boolean('active')->default(0);
            $table->integer('interval')->nullable(); // number of days between charges if recurring
            $table->string('title');
            $table->integer('amount'); // amount in cents
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_forms');
    }
}
