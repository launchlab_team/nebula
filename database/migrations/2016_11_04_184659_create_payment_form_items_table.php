<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentFormItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_form_items', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_form_id')->unsigned();
            $table->integer('order')->default('0');
            $table->string('label');
            $table->string('hint')->nullable();
            $table->string('placeholder');
            $table->string('type');
            $table->string('options');
            $table->boolean('required')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_form_items');
    }
}
