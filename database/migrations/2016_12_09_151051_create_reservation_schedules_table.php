<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id'); // created by
            $table->string('name'); // schedule name e.g. employees
            $table->time('available_start_time')->nullable();
            $table->time('available_end_time')->nullable();
            $table->boolean('mondays')->default(0);
            $table->boolean('tuesdays')->default(0);
            $table->boolean('wednesdays')->default(0);
            $table->boolean('thursdays')->default(0);
            $table->boolean('fridays')->default(0);
            $table->boolean('saturdays')->default(0);
            $table->boolean('sundays')->default(0);
            $table->boolean('require_login')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservation_schedules');
    }
}
