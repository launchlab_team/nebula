<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSettingsTableStripeIdColumn extends Migration
{
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->renameColumn('subscription_id','stripe_id')->change();
        });
    }

    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->renameColumn('stripe_id','subscription_id')->change();
        });
    }
}
