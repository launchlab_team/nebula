<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_sets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('module_id'); // group under tab/module
            $table->string('name');
            $table->string('columns',2000);
            $table->integer('label_key');
            $table->integer('sublabel_key')->nullable();
            $table->string('icon')->nullable(); // class of icon
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('record_sets');
    }
}
