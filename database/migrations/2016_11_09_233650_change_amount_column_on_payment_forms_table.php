<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAmountColumnOnPaymentFormsTable extends Migration
{
    public function up()
    {
        Schema::table('payment_forms', function (Blueprint $table) {
            $table->decimal('amount',10,2)->change();
        });
    }

    public function down()
    {
        Schema::table('payment_forms', function (Blueprint $table) {
            $table->integer('amount')->change();
        });
    }
}
