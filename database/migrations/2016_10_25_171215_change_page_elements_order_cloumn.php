<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePageElementsOrderCloumn extends Migration
{
    public function up()
    {
        Schema::table('page_elements', function (Blueprint $table) {
            $table->integer('order')->change();
        });
    }

    public function down()
    {
        Schema::table('page_elements', function (Blueprint $table) {
            $table->string('order')->change();
        });
    }
}
