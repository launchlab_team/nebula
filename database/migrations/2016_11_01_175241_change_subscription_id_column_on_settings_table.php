<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSubscriptionIdColumnOnSettingsTable extends Migration
{
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->string('subscription_id')->change();
        });
    }

    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->integer('subscription_id')->change();
        });
    }
    // As it was found Nov 14:
    // public function up()
    // {
    //     Schema::table('settings', function (Blueprint $table) {
    //         $table->renameColumn('subscription_id','stripe_id')->change();
    //     });
    // }

    // public function down()
    // {
    //     Schema::table('settings', function (Blueprint $table) {
    //         $table->renameColumn('stripe_id','subscription_id')->change();
    //     });
    // }
}
