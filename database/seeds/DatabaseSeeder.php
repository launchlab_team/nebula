<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ApplicationSeeder::class);
    }
}

class ApplicationSeeder extends Seeder
{
    public function run()
    {

        // Add an administrator record first
        DB::table('users')->insert([
            'name' => 'Geoff Milburn',
            'is_admin' => true,
            'is_manager' => true,
            'email' => 'geoff@launchlab.us',
            'password' => bcrypt('password'),
        ]);

        // Generate 20 fake users
        // $faker = Faker::create();
        // foreach (range(1, 20) as $index)
        // {
        //     DB::table('users')->insert([
        //         'name' => $faker->firstName . ' ' . $faker->lastName,
        //         'email' => $faker->email,
        //         'password' => bcrypt('password'),
        //     ]);
        // }

        // Create page record
        // DB::table('pages')->insert([
        //     // 'user_id' => 1, // creator
        //     'title' => 'Organization Page',
        //     'slug' => '',
        // ]);

        // Create page elements for example homepage
        // Insert page section 
        // DB::table('page_elements')->insert([ #1
        //     'page_id' => '1',
        //     'parent_id' => NULL,
        //     'type' => 'page-section',
        //     // 'html' => '<h1>Helllo</h1>',
        //     // 'class' => '',
        //     // 'attributes' => '',
        //     // 'options' => '',
        // ]);
        // Insert page block
        // DB::table('page_elements')->insert([ #2
        //     'page_id' => '1',
        //     'parent_id' => '1',
        //     'type' => 'page-block-full',
        //     'html' => '<h1>Helllo</h1>',
        // ]);
        // #
        // DB::table('page_elements')->insert([ #3
        //     'page_id' => '1',
        //     'parent_id' => NULL,
        //     'type' => 'page-section',
        // ]);
        // // Insert another page block
        // DB::table('page_elements')->insert([ #4
        //     'page_id' => '1',
        //     'parent_id' => '3', // in section #3
        //     'type' => 'page-block-half',
        // ]);
        // // Insert another page block
        // DB::table('page_elements')->insert([ #5
        //     'page_id' => '1',
        //     'parent_id' => '3', // in section #3
        //     'type' => 'page-block-half',
        // ]);
        // // Insert an element
        // DB::table('page_elements')->insert([ #6
        //     'page_id' => '1',
        //     'parent_id' => '2', // inside first page block
        //     'type' => 'paragraph',
        //     'html' => 'Here is the paragraph text'
        // ]);
        // // Insert an element
        // DB::table('page_elements')->insert([ #7
        //     'page_id' => '1',
        //     'parent_id' => '4', // inside page block #4
        //     'type' => 'paragraph',
        //     'html' => 'Here is the paragraph text'
        // ]);
        // // Insert an element
        // DB::table('page_elements')->insert([ #6
        //     'page_id' => '1',
        //     'parent_id' => '5', // inside page block #5
        //     'type' => 'paragraph',
        //     'html' => 'Here is the paragraph text'
        // ]);
    }
}